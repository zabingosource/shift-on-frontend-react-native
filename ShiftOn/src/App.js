/**
 * 10.09.2020 started working on ShiftOn by Arnab
 * 
 */
import React, { Component } from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Splash from './module/splash/Splash';
import Login from './module/login/Login';
import ForgotPassword from './module/forgotpass/ForgotPassword';
import RegistrationFirst from './module/registration/RegistrationFirst';
import RegistrationSecond from './module/registration/RegistrationSecond';
import RegistrationThird from './module/registration/ResistrationThird';
import UnitRegistration from './module/registration/unit/UnitRegistration';
import UnitResistrationName from './module/registration/unit/UnitResistrationName';
import Home from './module/home/WorkerHome';
import WorkerMyJobs from './module/jobs/WorkerMyJobs';
import WorkerCreatedMyJobs from './module/jobs/WorkerCreatedMyJobs';
import WorkerProfile from './module/profile/WorkerProfile';
import WorkerHome from './module/home/WorkerHome';
import WorkerProfileAgency from './module/agency/WorkerProfileAgency';
import WorkerAgency from './module/agency/WorkerAgency';
import WorkerAddNewAgency from './module/agency/WorkerAddNewAgency';
import WorkerMyTimeSheet from './module/timesheet/WorkerMyTimeSheet';
import WorkerCurrentJobSheet from './module/jobSheet/WorkerCurrentJobSheet';
import WorkerAddNewCalender from './module/calendar/WorkerAddNewCalender';
import Settings from './module/settings/Settings';
import WorkerCalender from './module/calendar/WorkerCalender';
import ControlerHome from './module/home/ControlerHome';
import ControlerShift from './module/shift/ControlerShift';
import ControlerCreateNewShift from './module/shift/ControlerCreateNewShift';
import UnitJobList from './module/jobs/UnitJobList';
import ViewJobDetail from './module/jobs/ViewJobDetail';
import UnitHome from './module/home/UnitHome';
import UnitAddNewShift from './module/shift/UnitAddNewShift';
import UnitUpdateNewShift from './module/shift/UnitUpdateNewShift';
import ViewShiftDetail from './module/shift/ViewShiftDetail';
import UnitAvailabilityList from './module/jobs/UnitAvailabilityList';
import UnitAgencyAvailabilityList from './module/jobs/UnitAgencyAvailabilityList';
import UnitAgencyAgentList from './module/agency/UnitAgencyAgentList';
import UnitMyTimeSheet from './module/timesheet/UnitMyTimeSheet';
import ControlerAvailabilityList from './module/shift/ControlerAvailabilityList';
import ControlerPostToWorkerAvailabilityList from './module/shift/ControlerPostToWorkerAvailabilityList';
import ControlerRepostToUnitAvailabilityList from './module/shift/ControlerRepostToUnitAvailabilityList';
import ControlerConfirmToWorkerAvailabilityList from './module/shift/ControlerConfirmToWorkerAvailabilityList';
import WorkerCurrentJobs from './module/jobSheet/WorkerCurrentJobs';
import UnitCalender from './module/calendar/UnitCalender';
import UnitProfile from './module/profile/UnitProfile';
import ControlerProfile from './module/profile/ControlerProfile';
import ControlerCalender from './module/calendar/ControlerCalender';
import WorkerJobDetails from './module/jobs/WorkerJobDetails';
import ControllerSettings from './module/settings/ControllerSettings';
import UnitAgencys from './module/agency/UnitAgencys';
import ViewAgencyDetail from './module/agency/ViewAgencyDetail';
import UnitRole from './module/role/UnitRole';
import UnitAddRole from './module/role/UnitAddRole';
import UpdateUnitRole from './module/role/UpdateUnitRole';
import ViewRoleDetail from './module/role/ViewRoleDetail';
import UnitAddNewJob from './module/jobs/UnitAddNewJob';
import UnitShiftList from './module/shift/UnitShiftList';
import UnitWard from './module/ward/UnitWard';
import UnitAddWard from './module/ward/UnitAddWard';
import UpdateUnitWard from './module/ward/UpdateUnitWard';
import ViewWardDetail from './module/ward/ViewWardDetail';
import UnitPayRate from './module/payrate/UnitPayRate';
import UnitAddPayRate from './module/payrate/UnitAddPayRate';
import UpdateUnitPayRate from './module/payrate/UpdateUnitPayRate';
import ViewPayRateDetail from './module/payrate/ViewPayRateDetail';
import WardShiftListing from './module/ward/Add Shifts/WardShiftListing';
import WardAddShift from './module/ward/Add Shifts/WardAddShift';
import Locations from './module/ward/Location List/Locations';
import ShiftPayRateListing from './module/ward/App Payrates/ShiftPayRateListing';
import ShiftAddPayRate from './module/ward/App Payrates/ShiftAddPayRate';
import RegistrationSelection from './module/registration/RegistrationSelection';
import WorkerStepOne from './module/registration/worker/WorkerStepOne';
import WorkerStepThree from './module/registration/worker/WorkerStepThree';
import WorkerStepTwo from './module/registration/worker/WorkerStepTwo';
import WorkerStepFour from './module/registration/worker/WorkerStepFour';
import WorkerStepFive from './module/registration/worker/WorkerStepFive';
import WorkerStepSix from './module/registration/worker/WorkerStepSix';
import WorkerStepSeven from './module/registration/worker/WorkerStepSeven';
import WorkerStepEight from './module/registration/worker/WorkerStepEight';
import WorkerStepComplete from './module/registration/worker/WorkerStepComplete';
import EditWorkerProfileOption from './module/profile/worker/EditWorkerProfileOption';
import WorkerPersonalInfo from './module/profile/worker/WorkerPersonalInfo';
import WorkerAcademicProfessionalInfo from './module/profile/worker/WorkerAcademicProfessionalInfo';
import WorkerCurrentEmploymentInfo from './module/profile/worker/WorkerCurrentEmploymentInfo';
import WorkerEmploymentHistory from './module/profile/worker/WorkerEmploymentHistory';
import WorkerMNCPin from './module/profile/worker/WorkerMNCPin';
import WorkerProfessionalReference from './module/profile/worker/WorkerProfessionalReference';
import WorkerEmergencyContact from './module/profile/worker/WorkerEmergencyContact';
import WorkerCriminalRecord from './module/profile/worker/WorkerCriminalRecord';
import WorkerEmploymentAct from './module/profile/worker/WorkerEmploymentAct';
import WorkerWebView from './module/WebViewPage/WorkerWebView';
import Currency from './module/currency/Currency';
import CurrencyList from './module/currency/CurrencyList';
import UpdateCurrency from './module/currency/UpdateCurrency';


const rootStackNavigator = createStackNavigator(
  {
    Splash: {
      screen: Splash,
      navigationOptions: {
        headerShown: false,
      }
    },
    Login: {
      screen: Login,
      navigationOptions: {
        headerShown: false,
      }
    },
    ForgotPassword: {
      screen: ForgotPassword,
      navigationOptions: {
        headerShown: false,
      }
    },
    RegistrationSelection: {
      screen: RegistrationSelection,
      navigationOptions: {
        headerShown: false,
      }
    },
    RegistrationFirst: {
      screen: RegistrationFirst,
      navigationOptions: {
        headerShown: false,
      }
    },
    RegistrationSecond: {
      screen: RegistrationSecond,
      navigationOptions: {
        headerShown: false,
      }
    },
    RegistrationThird: {
      screen: RegistrationThird,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitRegistration: {
      screen: UnitRegistration,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitResistrationName: {
      screen: UnitResistrationName,
      navigationOptions: {
        headerShown: false,
      }
    },

    /*  Worker section */
    WorkerStepOne: {
      screen: WorkerStepOne,
      navigationOptions: {
        headerShown: false,
      }
    },
    EditWorkerProfileOption: {
      screen: EditWorkerProfileOption,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerPersonalInfo: {
      screen: WorkerPersonalInfo,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerAcademicProfessionalInfo: {
      screen: WorkerAcademicProfessionalInfo,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerCurrentEmploymentInfo: {
      screen: WorkerCurrentEmploymentInfo,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerEmploymentHistory: {
      screen: WorkerEmploymentHistory,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerMNCPin: {
      screen: WorkerMNCPin,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerProfessionalReference: {
      screen: WorkerProfessionalReference,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerEmergencyContact: {
      screen: WorkerEmergencyContact,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerCriminalRecord: {
      screen: WorkerCriminalRecord,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerEmploymentAct: {
      screen: WorkerEmploymentAct,
      navigationOptions: {
        headerShown: false,
      }
    },


    WorkerHome: {
      screen: WorkerHome,
      navigationOptions: {
        headerShown: false,
      }
    },

    WorkerMyJobs: {
      screen: WorkerMyJobs,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerCreatedMyJobs: {
      screen: WorkerCreatedMyJobs,
      navigationOptions: {
        headerShown: false,
      }
    },

    WorkerJobDetails: {
      screen: WorkerJobDetails,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerProfile: {
      screen: WorkerProfile,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerProfileAgency: {
      screen: WorkerProfileAgency,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerAgency: {
      screen: WorkerAgency,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerAddNewAgency: {
      screen: WorkerAddNewAgency,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerMyTimeSheet: {
      screen: WorkerMyTimeSheet,
      navigationOptions: {
        headerShown: false,
      }
    }
    , WorkerCurrentJobSheet: {
      screen: WorkerCurrentJobSheet,
      navigationOptions: {
        headerShown: false,
      }
    }, WorkerCurrentJobs: {
      screen: WorkerCurrentJobs,
      navigationOptions: {
        headerShown: false,
      }
    }
    , WorkerAddNewCalender: {
      screen: WorkerAddNewCalender,
      navigationOptions: {
        headerShown: false,
      }
    }
    , Settings: {
      screen: Settings,
      navigationOptions: {
        headerShown: false,
      }
    }
    , WorkerCalender: {
      screen: WorkerCalender,
      navigationOptions: {
        headerShown: false,
      }
    },


    /*  controler section */

    ControlerHome: {
      screen: ControlerHome,
      navigationOptions: {
        headerShown: false,
      }
    },
    ControlerProfile: {
      screen: ControlerProfile,
      navigationOptions: {
        headerShown: false,
      }
    }, ControllerSettings: {
      screen: ControllerSettings,
      navigationOptions: {
        headerShown: false,
      }
    },
    ControlerShift: {
      screen: ControlerShift,
      navigationOptions: {
        headerShown: false,
      }
    },
    ControlerAvailabilityList: {
      screen: ControlerAvailabilityList,
      navigationOptions: {
        headerShown: false,
      }
    },
    ControlerPostToWorkerAvailabilityList: {
      screen: ControlerPostToWorkerAvailabilityList,
      navigationOptions: {
        headerShown: false,
      }
    },
    ControlerRepostToUnitAvailabilityList: {
      screen: ControlerRepostToUnitAvailabilityList,
      navigationOptions: {
        headerShown: false,
      }
    },
    ControlerConfirmToWorkerAvailabilityList: {
      screen: ControlerConfirmToWorkerAvailabilityList,
      navigationOptions: {
        headerShown: false,
      }
    },
    ControlerCreateNewShift: {
      screen: ControlerCreateNewShift,
      navigationOptions: {
        headerShown: false,
      }
    },
    ControlerCalender: {
      screen: ControlerCalender,
      navigationOptions: {
        headerShown: false,
      }
    },



    /* Unit Section */

    UnitHome: {
      screen: UnitHome,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitProfile: {
      screen: UnitProfile,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitJobList: {
      screen: UnitJobList,
      navigationOptions: {
        headerShown: false,
      }
    },
    ViewJobDetail: {
      screen: ViewJobDetail,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitAddNewJob: {
      screen: UnitAddNewJob,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitShiftList: {
      screen: UnitShiftList,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitAddNewShift: {
      screen: UnitAddNewShift,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitUpdateNewShift: {
      screen: UnitUpdateNewShift,
      navigationOptions: {
        headerShown: false,
      }
    },
    ViewShiftDetail: {
      screen: ViewShiftDetail,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitWard: {
      screen: UnitWard,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitAddWard: {
      screen: UnitAddWard,
      navigationOptions: {
        headerShown: false,
      }
    },
    UpdateUnitWard: {
      screen: UpdateUnitWard,
      navigationOptions: {
        headerShown: false,
      }
    },
    ViewWardDetail: {
      screen: ViewWardDetail,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitPayRate: {
      screen: UnitPayRate,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitAddPayRate: {
      screen: UnitAddPayRate,
      navigationOptions: {
        headerShown: false,
      }
    },
    UpdateUnitPayRate: {
      screen: UpdateUnitPayRate,
      navigationOptions: {
        headerShown: false,
      }
    },
    ViewPayRateDetail: {
      screen: ViewPayRateDetail,
      navigationOptions: {
        headerShown: false,
      }
    },
    WardShiftListing: {
      screen: WardShiftListing,
      navigationOptions: {
        headerShown: false,
      }
    },
    WardAddShift: {
      screen: WardAddShift,
      navigationOptions: {
        headerShown: false,
      }
    },
    Locations: {
      screen: Locations,
      navigationOptions: {
        headerShown: false,
      }
    },
    ShiftPayRateListing: {
      screen: ShiftPayRateListing,
      navigationOptions: {
        headerShown: false,
      }
    },
    ShiftAddPayRate: {
      screen: ShiftAddPayRate,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitAvailabilityList: {
      screen: UnitAvailabilityList,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitAgencyAvailabilityList: {
      screen: UnitAgencyAvailabilityList,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitCalender: {
      screen: UnitCalender,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitAgencyAgentList: {
      screen: UnitAgencyAgentList,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitAgencys: {
      screen: UnitAgencys,
      navigationOptions: {
        headerShown: false,
      }
    },
    ViewAgencyDetail: {
      screen: ViewAgencyDetail,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitMyTimeSheet: {
      screen: UnitMyTimeSheet,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitRole: {
      screen: UnitRole,
      navigationOptions: {
        headerShown: false,
      }
    },
    UnitAddRole: {
      screen: UnitAddRole,
      navigationOptions: {
        headerShown: false,
      }
    },
    UpdateUnitRole: {
      screen: UpdateUnitRole,
      navigationOptions: {
        headerShown: false,
      }
    },
    ViewRoleDetail: {
      screen: ViewRoleDetail,
      navigationOptions: {
        headerShown: false,
      }
    },
    WorkerWebView: {
      screen: WorkerWebView,
      navigationOptions: {
        headerShown: false,
      }
    },
    Currency: {
      screen: Currency,
      navigationOptions: {
        headerShown: false,
      }
    },
    CurrencyList: {
      screen: CurrencyList,
      navigationOptions: {
        headerShown: false,
      }
    },
    UpdateCurrency: {
      screen: UpdateCurrency,
      navigationOptions: {
        headerShown: false,
      }
    }



  }, {
  initialRouteName: 'Splash',
}
);




export default class App extends Component {

  componentDidMount() {

  }

  render() {

    const AppNavigator = createAppContainer(rootStackNavigator);
    return (
      <AppNavigator />
    )


  }

}





const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  roundTopHeaderImage: {
    width: 90,
    height: 90,
    borderRadius: 45,
    borderWidth: 3,
    borderColor: '#ffffff',
    marginTop: 30,
    alignSelf: 'center',
    marginBottom: 20

  },
  roundTopHeaderText: {
    color: '#FFFFFF',
    padding: 5,
    marginLeft: 10,
    fontSize: 18,
    marginTop: 10


  },
  emailHeaderText: {
    color: '#FFFFFF',

    marginLeft: 15,
    marginBottom: 10,
    fontSize: 13

  },
  singleProduct: {
    flexDirection: 'row',
    backgroundColor: 'rgb(246, 246, 246)',
    borderColor: '#E2E2E2',
    borderBottomWidth: 1,
    padding: 5,
    alignItems: 'center',
    justifyContent: 'space-between'

  },
  menuText: {
    fontSize: 14,
    padding: 10,
    color: '#000',
    textAlignVertical: 'center',
    fontWeight: 'bold',

  },
  menuIcon: {
    width: 9,
    height: 9,
    marginRight: 5
  },
});


