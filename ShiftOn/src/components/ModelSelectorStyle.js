'use strict';

import { Platform, StyleSheet } from 'react-native';

const PADDING = 10;
const SEARCH_PADDING = 8;
const BORDER_RADIUS = 20;
const FONT_SIZE = 16;
const HIGHLIGHT_COLOR = 'rgba(50,212,224,255)';
const SEARCH_FONT_COLOR = '#585858';
const TEXT_ALIGN = 'left';

export default StyleSheet.create({

    overlayStyle: {
        flex: 1,
        padding: '8%',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.7)',
    },

    optionContainer: {
        borderRadius: BORDER_RADIUS,
        flexShrink: 1,
        marginBottom: 8,
        padding: PADDING,
        //backgroundColor: 'rgba(255,255,255,0.8)',
        backgroundColor: '#f7f7f7'
    },

    cancelContainer: {
        alignSelf: 'stretch',
    },

    selectStyle: {
        borderColor: '#ccc',
        borderWidth: 1,
        padding: PADDING,
        borderRadius: BORDER_RADIUS,
    },

    selectTextStyle: {
        textAlign: TEXT_ALIGN,
        color: '#333',
        fontSize: FONT_SIZE,
    },

    cancelStyle: {
        borderRadius: 5,
        //backgroundColor: 'rgba(255,255,255,0.8)',
        backgroundColor: '#dedede',
        padding: PADDING,
        margin: 8,
    },

    cancelTextStyle: {
        textAlign: 'center',
        color: '#333',
        fontSize: FONT_SIZE,
        fontFamily: "montserrat_bold"
    },

    optionStyle: {
        padding: PADDING,
        borderBottomWidth: 0,
        borderBottomColor: '#ccc',
    },

    optionTextStyle: {
        textAlign: TEXT_ALIGN,
        fontSize: FONT_SIZE,
        color: HIGHLIGHT_COLOR,
        fontFamily: "montserrat_semibold"
    },

    sectionStyle: {
        padding: PADDING * 2,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
    },

    sectionTextStyle: {
        textAlign: TEXT_ALIGN,
        fontSize: FONT_SIZE,
    },

    initValueTextStyle: {
        textAlign: TEXT_ALIGN,
        fontSize: FONT_SIZE,
        color: '#d3d3d3',
    },
    searchStyle: {
        borderWidth: 0,
        borderRadius: 0,
        borderColor: 'rgba(0,0,0,0.2)',
        paddingHorizontal: SEARCH_PADDING,
        paddingVertical: Platform.OS === 'ios' ? SEARCH_PADDING : 0,
        marginHorizontal: SEARCH_PADDING,
    },
    searchTextStyle: {
        textAlign: TEXT_ALIGN,
        fontSize: FONT_SIZE,
        color: SEARCH_FONT_COLOR,
        fontFamily: "montserrat_semibold"
    },
});
