import React, { useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, Dimensions } from "react-native";

const WIDTH = Dimensions.get("window").width;
const HEIGHT = Dimensions.get('window').height;


const ModalPicker = ({
    changeModelVisiblity = () => { },
    setOption = () => { },
    setDATA = [],
}) => {

    const onPressItem = (item) => {
        changeModelVisiblity(false);
        setOption(item);
    }

    return (
        <TouchableOpacity
            onPress={() => changeModelVisiblity(false)}

            style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
        >

            <View style={{ backgroundColor: '#bfbfbf', padding: 10, borderRadius: 10, width: WIDTH - 20, height: HEIGHT / 2 }}>
                <ScrollView>

                    {
                        setDATA.map((item, index) => {
                            return (
                                <TouchableOpacity
                                    key={index}
                                    onPress={() => onPressItem(item)}
                                    style={{ alignItems: 'flex-start', backgroundColor: 'white', marginBottom: 8, borderRadius: 10 }}
                                >
                                    <Text style={{ margin: 10, fontSize: 16, fontFamily: "montserrat_regular", color: 'black' }}>
                                        {item}
                                    </Text>

                                </TouchableOpacity>
                            )
                        })
                    }

                </ScrollView>
            </View>

        </TouchableOpacity>
    )

}

export default ModalPicker;