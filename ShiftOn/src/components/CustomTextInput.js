import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image
} from 'react-native';

const CustomTextInput = ({ onSubmitEditing, blurOnSubmit, style, floatingStyle, autoCapitalize, multiline, onTouchEnd, caretHidden, showSoftInputOnFocus, isVisibleDropDown, isVisibleRightArrow, isVisibleCalendar, fontSize, keyboardType, onChangeText, value, maxLength, placeholder, placeholderTextColor, floatingText, secureTextEntry, editable, onFocus, returnKeyType, inputRef }) => {
  const { textInput_style, floatingLabelText, inputHolder } = styles;
  const [isFocused, setIsFocused] = useState(false);
  const isFocus = () => {
    setIsFocused(true);
  }
  const deFocus = () => {
    if (!value) {
      setIsFocused(false);
    } else {
      setIsFocused(true);
    }
  }
  const setvalue = (data) => {
    if (data && data != "") {
      setIsFocused(true);
      return data;
    } else {
      return "";
    }
  }

  return (
    <View style={inputHolder}>

      <View>
        <TextInput
          multiline={multiline}
          editable={editable}
          secureTextEntry={secureTextEntry}
          style={[textInput_style, style]}
          //style={textInput_style}
          placeholder={placeholder}
          value={value}
          maxLength={maxLength}
          keyboardType={keyboardType}
          placeholderTextColor={placeholderTextColor}
          onChangeText={onChangeText}
          underlineColorAndroid="transparent"
          autoCorrect={false}
          onFocus={isFocus}
          onSubmitEditing={onSubmitEditing}
          onEndEditing={deFocus}
          accessible={false}
          returnKeyType={returnKeyType}
          autoFocus={false}
          autoCapitalize={autoCapitalize}
          showSoftInputOnFocus={showSoftInputOnFocus}
          onTouchEnd={onTouchEnd}
          caretHidden={caretHidden}
          blurOnSubmit={blurOnSubmit}
          ref={input => inputRef && inputRef(input)}

        //clearTextOnFocus={true}

        />
        {
          (isFocused || value) ?
            (floatingText ?
              <Text style={[floatingLabelText, floatingStyle]} >{floatingText.toUpperCase()}</Text>
              : <Text></Text>
            )
            : (<View></View>)
        }
        {
          (isVisibleDropDown) ? (<Image style={{ height: 10, width: 15, position: 'absolute', end: 20, top: 22 }} source={require('../assets/dropdown_icn.png')} resizeMode="stretch" />) : (<View></View>)
        }
        {
          (isVisibleRightArrow) ? (<Image style={{ height: 15, width: 10, position: 'absolute', end: 20, top: 19 }} source={require('../assets/right_icn.png')} resizeMode="stretch" />) : (<View></View>)
        }
        {
          (isVisibleCalendar) ? (<Image style={{ height: 20, width: 20, position: 'absolute', end: 10, top: 13 }} source={require('../assets/calendar_icn_c.png')} resizeMode="stretch" />) : (<View></View>)
        }

      </View>
    </View>
  );
};



const styles = StyleSheet.create({
  inputHolder: {
    flexDirection: 'column',
    marginTop: 10,

    width: '100%'
  },

  floatingLabelText: {
    color: "#393FA0",
    marginLeft: 25,
    position: 'absolute',
    marginTop: -10,
    backgroundColor: '#FFFFFF',
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 10,
    fontFamily: "montserrat_regular"
  },
  textInput_style: {

    fontSize: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 1,
    paddingHorizontal: 25,
    paddingTop: 10,
    paddingBottom: 12,
    color: '#464646',
    fontFamily: "montserrat_regular"
  },
});

export default CustomTextInput;
