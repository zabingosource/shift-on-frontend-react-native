/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions,
  TextInput, ToastAndroid, CheckBox
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import RadioGroup from 'react-native-radio-buttons-group';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Api from '../../apis/Api';
import { setToastMsg } from '../../utils/ToastMessage';
import Loader from '../../utils/Loader';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../components/CustomModelSelector';
import { connect } from 'react-redux';

const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userId: props.userLoginDetails.id,
    userRoleType: props.userLoginDetails.roleType,
  }
}


const { width } = Dimensions.get('window');


class ControlerCreateNewShift extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      selectedShift: '',
      selectedShiftid: '',
      selectedRole: '',
      selectedRoleId: '',
      roleData: [],
      shiftData: [],
      unitData: [],
      unitDataWithLocation: [],
      wardData: [],
      selectedUnit: '',
      selectedUnitId: '',
      gender: "",
      noofEmployee: '',
      description: '',
      startTime: '',
      endTime: '',
      start_date: '',
      finish_date: '',
      profile: "",
      zip: '',
      full_location: '',
      unit_location: '',
      locationId: '',
      currencyValue: '',
      ward: '',
      wardId: '',
      landmark: '',
      isLoading: false,
      isStartTimePickerVisible: false,
      isEndTimePickerVisible: false,
      isStartDatePickerVisible: false,
      isEndDatePickerVisible: false,
      // for compare date and time values
      compareTimeValues: '',
      compareDateValues: '',

      weekdays: [],

      isButtonEnable: false,
      setErrors: {
        field: '',
        message: ''
      }
    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    this._controllerAgencyUnitList();
  }

  _controllerAgencyUnitList = async () => {
    this.setState({ isLoading: true });
    var id = await AsyncStorage.getItem('agency_id')

    var formData = new FormData();
    formData.append("agency_id", id);

    Api._controllerAgencyUnitList(formData)
      .then((response) => {
        console.log(response.data)

        var tempData = [];
        for (var i = 0; i < response.data.length; i++) {
          if (response.data[i].status.toString() == "1" && response.data[i].agency_status.toString() == "1" && response.data[i].unit != null) {
            tempData.push(response.data[i].unit)
          }

        }

        let newArray = tempData.map((item) => {
          return (
            {
              "key": item.id,
              "label": item.name
            }
          )
        })

        this.setState({ unitData: newArray, isLoading: false });

        console.log(newArray)
      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  // get role listings...
  _unitRoleListing = (id) => {
    this.setState({ isLoading: true });

    var formData = new FormData();
    formData.append("unit_id", id);

    Api._unitRoleListing(formData)
      .then((response) => {
        console.log(response.data)

        var tempData = [];
        for (var i = 0; i < response.data.length; i++) {
          if (response.data[i].is_active.toString() == "1") {
            tempData.push(response.data[i])
          }
        }

        let newArray = tempData.map((item) => {
          return (
            {
              "key": item.id,
              "label": item.name
            }
          )
        })
        this.setState({ roleData: newArray });
        this._unitShiftListing(id);
        // setToastMsg(response.message.toString());

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  _unitShiftListing = (id) => {

    let rawData = {
      "shift_user_type": "unit",
      "user_id": id,
    }
    Api._unitShiftListing(rawData)
      .then((response) => {
        console.log(response.data)

        var tempData = [];
        for (var i = 0; i < response.data.length; i++) {
          if (response.data[i].status.toString() == "1") {
            tempData.push(response.data[i])
          }
        }

        let newArray = tempData.map((item) => {
          return (
            {
              "key": item.id,
              "label": item.shift_title,
              "start_time": item.start_time,
              "end_time": item.end_time,
              "start_date": item.start_date,
              "end_date": item.end_date,
            }
          )
        })
        this.setState({ shiftData: newArray });
        //setToastMsg(response.message.toString());
        this.unitLocationListing(id)
      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  // This is for store unit locations data in dropdown from unitLocationListing api...
  unitLocationListing = (id) => {

    var rawData = { "unit_id": id }
    Api._getUnitAllLocations(rawData)
      .then((response) => {
        console.log(response.data)

        var tempData = [];
        for (var i = 0; i < response.data.length; i++) {
          if (response.data[i].status.toString() == "1" && response.data[i].currency) {
            tempData.push(response.data[i])
          }
        }

        let newArray = tempData.map((item) => {
          return (
            {
              "key": item.id,
              "label": item.address + ", " + item.landmark + ", " + item.post_code,
              "address": item.address,
              "landmark": item.landmark,
              "zip": item.post_code,
              "currencyId": item.currency,
            }
          )
        })
        this.setState({ unitDataWithLocation: newArray });
        this.unitWardListing(id);

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  unitWardListing = (id) => {

    var rawData = { "unit_id": id }
    Api._unitWardListing(rawData)
      .then((response) => {
        console.log(response.data)

        var tempData = [];
        for (var i = 0; i < response.data.length; i++) {
          if (response.data[i].is_active.toString() == "1") {
            tempData.push(response.data[i])
          }
        }

        let newArray = tempData.map((item) => {
          return (
            {
              "key": item.id,
              "label": item.name,
            }
          )
        })
        this.setState({ wardData: newArray, isLoading: false });

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }


  checkValidation = () => {
    let shiftTitleError = { field: '', message: '' }
    let shiftTypeError = { field: '', message: '' }
    let unitTypeError = { field: '', message: '' }
    let descriptionError = { field: '', message: '' }
    let noOfEmpError = { field: '', message: '' }
    let roleError = { field: '', message: '' }
    let locationError = { field: '', message: '' }
    let wardError = { field: '', message: '' }

    if (this.state.title == '') {
      shiftTitleError.field = "job_title";
      shiftTitleError.message = "Job Title is required!";
      this.setState({ setErrors: shiftTitleError })
    } else if (this.state.description == '') {
      descriptionError.field = "description";
      descriptionError.message = "Shift Description is required!";
      this.setState({ setErrors: descriptionError })
    } else if (this.state.selectedUnit == '') {
      unitTypeError.field = "unit_type";
      unitTypeError.message = "Unit is required!";
      this.setState({ setErrors: unitTypeError })
    } else if (this.state.selectedRole == '') {
      roleError.field = "role";
      roleError.message = "Role is required!";
      this.setState({ setErrors: roleError })
    } else if (this.state.selectedShift == '') {
      shiftTypeError.field = "shift_type";
      shiftTypeError.message = "Shift is required!";
      this.setState({ setErrors: shiftTypeError })
    } else if (this.state.full_location == '') {
      locationError.field = "unit_location";
      locationError.message = "Unit/Location is required!";
      this.setState({ setErrors: locationError })
    } else if (this.state.ward == '') {
      wardError.field = "ward";
      wardError.message = "Ward is required!";
      this.setState({ setErrors: wardError })
    } else if (this.state.noofEmployee == '') {
      noOfEmpError.field = "no_of_employee";
      noOfEmpError.message = "Please add required no. of employees!";
      this.setState({ setErrors: noOfEmpError })
    } else {
      this.setState({ setErrors: { field: '', message: '' } })
      this.checkPayrateLinkOrNot();
    }

  }

  checkPayrateLinkOrNot = () => {
    this.setState({ isLoading: true })

    var rawData = {
      "ward_id": this.state.wardId,
      "shift_id": this.state.selectedShiftid,
      "unit_id": this.state.selectedUnitId,
      "role": this.state.selectedRole,
    }

    Api._unitCheckPayrateLinkStatus(rawData)
      .then((response) => {

        console.log(response.status)
        if (response.status == 1) {
          this._controllerAddNewShift()
        } else {
          this.setState({ isLoading: false })
          alert('No one Payrate is link with this Role, Shift and Ward.')
        }

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });

  }

  _controllerAddNewShift = () => {
    this.setState({ isLoading: true });

    var formData = new FormData();
    formData.append("user_id", this.props.userId);
    formData.append("job_type", "controller");
    formData.append("job_title", this.state.title);
    formData.append("job_description", this.state.description);
    formData.append("job_role", this.state.selectedRoleId);
    formData.append("shift_id", this.state.selectedShiftid);
    formData.append("unit_id", this.state.selectedUnitId);
    formData.append("ward", this.state.wardId);
    formData.append("required_people", this.state.noofEmployee);
    formData.append("zip", this.state.zip);
    formData.append("landmark", this.state.landmark);
    formData.append("location", this.state.locationId);


    Api._controllerAddNewShift(formData)
      .then((response) => {

        console.log(response.data)
        this.setState({ isLoading: false });
        setToastMsg(response.message.toString());
        this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>

              <Loader isLoading={this.state.isLoading} />

              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>CREATE NEW SHIFT</Text>

              </View>
              <ScrollView>
                <View style={{ marginTop: 10, marginLeft: 30, marginRight: 30 }}>

                  <TouchableOpacity>
                    <CustomTextInput
                      placeholder='JOB TITLE'
                      floatingText="JOB TITLE"
                      editable={true}
                      onFocus={true}
                      value={this.state.title}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          title: text,
                        });
                      }}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "job_title" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='JOB DESCRIPTION'
                      keyboardType='default'
                      floatingText="JOB DESCRIPTION"
                      style={{ textAlignVertical: "top", height: 120, }}
                      multiline={true}
                      value={this.state.description}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          description: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "description" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <ModalSelector
                    data={this.state.unitData}
                    keyExtractor={(item, index) => item.key.toString()}
                    initValue="SELECT UNIT"
                    accessible={true}
                    animationType="fade"
                    search={false}
                    scrollViewAccessibilityLabel={'Scrollable options'}
                    cancelButtonAccessibilityLabel={'Cancel Button'}
                    onChange={(option) => {
                      this.setState({
                        selectedUnit: option.label,
                        selectedUnitId: option.key,
                        roleData: [],
                        shiftData: [],
                        unitDataWithLocation: [],
                        wardData: [],
                        selectedRole: '',
                        selectedRoleId: '',
                        selectedShift: '',
                        selectedShiftid: '',
                        full_location: '',
                        unit_location: '',
                        landmark: '',
                        zip: '',
                        ward: '',
                        wardId: '',

                      })
                      this._unitRoleListing(option.key);
                    }}>
                    <CustomTextInput
                      placeholder='SELECT UNIT'
                      floatingText="SELECT UNIT"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleDropDown={true}
                      returnKeyType={'done'}
                      value={this.state.selectedUnit}
                    />
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "unit_type" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  {
                    this.state.selectedUnit ?
                      (<ModalSelector
                        data={this.state.roleData}
                        keyExtractor={(item, index) => item.key.toString()}
                        initValue="SELECT ROLE"
                        accessible={true}
                        animationType="fade"
                        search={false}
                        scrollViewAccessibilityLabel={'Scrollable options'}
                        cancelButtonAccessibilityLabel={'Cancel Button'}
                        onChange={(option) => {
                          this.setState({ selectedRole: option.label, selectedRoleId: option.key })
                        }}>
                        <CustomTextInput
                          placeholder='SELECT ROLE'
                          floatingText="SELECT ROLE"
                          editable={false}
                          onFocus={true}
                          showSoftInputOnFocus={false}
                          caretHidden={true}
                          isVisibleDropDown={true}
                          value={this.state.selectedRole}
                          returnKeyType={'done'}
                        />
                      </ModalSelector>)
                      :
                      (<CustomTextInput
                        placeholder='SELECT ROLE'
                        floatingText="SELECT ROLE"
                        editable={true}
                        onFocus={false}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        isVisibleDropDown={true}
                        value={this.state.selectedRole}
                        returnKeyType={'done'}
                        onTouchEnd={() => { setToastMsg("Please select unit first.") }}
                      />)
                  }
                  {
                    this.state.setErrors.field == "role" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  {
                    this.state.selectedUnit ?
                      (<ModalSelector
                        data={this.state.shiftData}
                        keyExtractor={(item, index) => item.key.toString()}
                        initValue="SELECT SHIFT"
                        accessible={true}
                        animationType="fade"
                        search={false}
                        scrollViewAccessibilityLabel={'Scrollable options'}
                        cancelButtonAccessibilityLabel={'Cancel Button'}
                        onChange={(option) => {
                          this.setState({
                            selectedShift: option.label,
                            selectedShiftid: option.key,
                            startTime: option.start_time,
                            endTime: option.end_time,
                            start_date: option.start_date,
                            finish_date: option.end_date,
                          })
                        }}>
                        <CustomTextInput
                          placeholder='SELECT SHIFT'
                          floatingText="SELECT SHIFT"
                          editable={true}
                          onFocus={true}
                          showSoftInputOnFocus={false}
                          caretHidden={true}
                          isVisibleDropDown={true}
                          returnKeyType={'done'}
                          value={this.state.selectedShift}
                        />
                      </ModalSelector>)
                      :
                      (<CustomTextInput
                        placeholder='SELECT SHIFT'
                        floatingText="SELECT SHIFT"
                        editable={true}
                        onFocus={false}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        isVisibleDropDown={true}
                        value={this.state.selectedShift}
                        returnKeyType={'done'}
                        onTouchEnd={() => { setToastMsg("Please select unit first.") }}
                      />)
                  }
                  {
                    this.state.setErrors.field == "shift_type" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }


                  {/* <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginTop: 10, flex: 1, marginRight: 3 }}>
                      <CustomTextInput
                        placeholder='START TIME'
                        floatingText="START TIME"
                        editable={true}
                        onFocus={true}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        value={this.state.startTime}
                        returnKeyType={'done'}
                        onTouchEnd={() =>
                          this.setState({ isStartTimePickerVisible: false })
                        }
                      />
                      <DateTimePickerModal
                        isVisible={this.state.isStartTimePickerVisible}
                        mode="time"
                        locale="en_GB"
                        date={new Date()}
                        onConfirm={(date) => this.handleStartTimeConfirm(date)}
                        onCancel={() => this.setState({ isStartTimePickerVisible: false })}
                      />
                    </View>
                    {
                      this.state.setErrors.field == "start_time" && (
                        <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                      )
                    }

                    <View style={{ marginTop: 10, flex: 1, marginLeft: 3 }}>
                      <TouchableOpacity>
                        <CustomTextInput
                          placeholder='END TIME'
                          floatingText="END TIME"
                          editable={true}
                          onFocus={true}
                          showSoftInputOnFocus={false}
                          caretHidden={true}
                          value={this.state.endTime}
                          returnKeyType={'done'}
                          onTouchEnd={() =>
                            this.setState({ isEndTimePickerVisible: false })
                          }
                        />
                        <DateTimePickerModal
                          isVisible={this.state.isEndTimePickerVisible}
                          mode="time"
                          locale="en_GB"
                          date={new Date()}
                          onConfirm={(date) => this.handleEndTimeConfirm(date)}
                          onCancel={() => this.setState({ isEndTimePickerVisible: false })}
                        />
                      </TouchableOpacity>
                    </View>
                    {
                      this.state.setErrors.field == "end_time" && (
                        <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                      )
                    }
                  </View>

                  <TouchableOpacity>
                    <CustomTextInput
                      placeholder='START DATE'
                      floatingText="START DATE"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleCalendar={true}
                      value={this.state.start_date}
                      returnKeyType={'done'}
                      onTouchEnd={() => {
                        this.setState({ isStartDatePickerVisible: false })
                      }}
                    />
                    <DateTimePickerModal
                      isVisible={this.state.isStartDatePickerVisible}
                      mode="date"
                      date={new Date()}
                      onConfirm={(date) => this.handleStartDateConfirm(date)}
                      onCancel={() => this.setState({ isStartDatePickerVisible: false })}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "start_date" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <TouchableOpacity>
                    <CustomTextInput
                      placeholder='FINISH DATE'
                      floatingText="FINISH DATE"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleCalendar={true}
                      value={this.state.finish_date}
                      returnKeyType={'done'}
                      onTouchEnd={() => {
                        this.setState({ isEndDatePickerVisible: false })
                      }}
                    />
                    <DateTimePickerModal
                      isVisible={this.state.isEndDatePickerVisible}
                      mode="date"
                      date={new Date()}
                      onConfirm={(date) => this.handleEndDateConfirm(date)}
                      onCancel={() => this.setState({ isEndDatePickerVisible: false })}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "finish_date" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  } */}


                  {
                    this.state.selectedUnit ?
                      <View style={{ marginTop: 5 }}>
                        <ModalSelector
                          data={this.state.unitDataWithLocation}
                          keyExtractor={(item, index) => item.key.toString()}
                          initValue="UNIT/LOCATION"
                          accessible={true}
                          animationType="fade"
                          search={false}
                          scrollViewAccessibilityLabel={'Scrollable options'}
                          cancelButtonAccessibilityLabel={'Cancel Button'}
                          onChange={(option) => {
                            this.setState({
                              full_location: option.label,
                              unit_location: option.address,
                              landmark: option.landmark,
                              zip: option.zip,
                              locationId: option.key,
                              currencyValue: option.currencyId,
                            })
                            {
                              option.currencyId ?
                                (
                                  console.log("currency added"),
                                  this.setState({ isButtonEnable: true })
                                )
                                :
                                (
                                  console.log("currency not added"),
                                  alert('Currency has not set yet in this location'),
                                  this.setState({ isButtonEnable: false })
                                )
                            }
                          }}>
                          <CustomTextInput
                            placeholder='UNIT/LOCATION'
                            keyboardType='default'
                            floatingText="UNIT/LOCATION"
                            editable={true}
                            onFocus={false}
                            showSoftInputOnFocus={false}
                            caretHidden={true}
                            isVisibleDropDown={true}
                            value={this.state.full_location}
                            returnKeyType={'done'}
                          />
                        </ModalSelector>
                      </View>
                      :
                      <View style={{ marginTop: 5 }}>
                        <CustomTextInput
                          placeholder='UNIT/LOCATION'
                          keyboardType='default'
                          floatingText="UNIT/LOCATION"
                          editable={true}
                          onFocus={false}
                          showSoftInputOnFocus={false}
                          caretHidden={true}
                          isVisibleDropDown={true}
                          value={this.state.unit_location}
                          returnKeyType={'done'}
                          onTouchEnd={() => { setToastMsg("Please select unit first.") }}
                        />
                      </View>
                  }
                  {
                    this.state.setErrors.field == "unit_location" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }


                  {
                    this.state.selectedUnit ?
                      <View style={{ marginTop: 5, marginBottom: 10 }}>
                        <ModalSelector
                          data={this.state.wardData}
                          keyExtractor={(item, index) => item.key.toString()}
                          initValue="WARD"
                          accessible={true}
                          animationType="fade"
                          search={false}
                          scrollViewAccessibilityLabel={'Scrollable options'}
                          cancelButtonAccessibilityLabel={'Cancel Button'}
                          onChange={(option) => {
                            this.setState({ ward: option.label, wardId: option.key })
                          }}>
                          <CustomTextInput
                            placeholder='WARD'
                            keyboardType='default'
                            floatingText="WARD"
                            editable={true}
                            onFocus={false}
                            showSoftInputOnFocus={false}
                            caretHidden={true}
                            isVisibleDropDown={true}
                            value={this.state.ward}
                            returnKeyType={'done'}
                          />
                        </ModalSelector>
                      </View>
                      :
                      <View style={{ marginTop: 5, marginBottom: 10 }}>
                        <CustomTextInput
                          placeholder='WARD'
                          keyboardType='default'
                          floatingText="WARD"
                          editable={true}
                          onFocus={false}
                          showSoftInputOnFocus={false}
                          caretHidden={true}
                          isVisibleDropDown={true}
                          value={this.state.ward}
                          returnKeyType={'done'}
                          onTouchEnd={() => { setToastMsg("Please select unit first.") }}
                        />
                      </View>
                  }
                  {
                    this.state.setErrors.field == "ward" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='REQUIRED NO OF EMPLOYEE'
                      keyboardType='numeric'
                      floatingText="REQUIRED NO OF EMPLOYEE"
                      value={this.state.noofEmployee}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          noofEmployee: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "no_of_employee" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                </View>
              </ScrollView>
            </View>


            <View >

              <View style={styles.shadow}></View>
              {
                this.state.isButtonEnable ?
                  <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                    onPress={() => this.checkValidation()}
                  >
                    <Text style={styles.loginText}>SUBMIT</Text>
                  </TouchableOpacity>
                  :
                  <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#c2c2c2", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                    onPress={() => { }}
                  >
                    <Text style={styles.loginText}>SUBMIT</Text>
                  </TouchableOpacity>
              }


            </View>


          </View>
        </SafeAreaView>
      </Fragment>
    );
  }


  // this is for start time picker...
  handleStartTimeConfirm = (time) => {

    let dateTimeString = moment(time).local().format('HH:mm');

    this.setState({
      startTime: dateTimeString,
      endTime: '',
      compareTimeValues: time,
      isStartTimePickerVisible: false,
    })

    console.log(dateTimeString)
  };

  // this is for end time picker...
  handleEndTimeConfirm = (time) => {
    let dateTimeString = moment(time).local().format('HH:mm');

    var start = new Date(this.state.compareTimeValues);
    var end = new Date(time);

    if (this.state.compareTimeValues != '') {
      if (start.getTime() > end.getTime()) {
        this.setState({
          isEndTimePickerVisible: false,
        })
        setToastMsg("Selected Time must be greater than Start time.");
        console.log("end time is smaller")
      } else {
        this.setState({
          endTime: dateTimeString,
          isEndTimePickerVisible: false,
        })
        console.log("end time is greater")
      }
    } else {
      this.setState({
        isEndTimePickerVisible: false,
      })
      setToastMsg("Please select Start time first.");
      console.log("start time not selected yet.")
    }
    console.log(dateTimeString)
  };



  // this is for start date picker...
  handleStartDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    this.setState({
      start_date: dateTimeString,
      compareDateValues: date,
      finish_date: "",
      isStartDatePickerVisible: false,
    })

    console.log(dateTimeString)
  };


  // this is for end date picker...
  handleEndDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    var start = new Date(this.state.compareDateValues);
    var end = new Date(date);

    if (this.state.compareDateValues != '') {
      if (start.getTime() > end.getTime()) {
        this.setState({
          isEndDatePickerVisible: false,
          finish_date: "",
        })
        setToastMsg("Date must be greater than start date.");
        console.log("end date is smaller")
      } else {
        this.setState({
          finish_date: dateTimeString,
          isEndDatePickerVisible: false,
        })
        console.log("end date is greater")
      }
    } else {
      this.setState({
        isEndDatePickerVisible: false,
      })
      setToastMsg("Please select start date first.");
      console.log("start date not selected yet.")
    }
    console.log(dateTimeString)
  };

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center',

  },

  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5


  },
  floatingLabelText: {
    color: "#393FA0",
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    paddingRight: 5,

    fontSize: 14,
    fontFamily: "montserrat_regular"
  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  profileImage: {
    height: 150,
    width: 150,
    position: 'absolute',
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: 60

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#EEEEEE",
    height: 0.2,
    elevation: 3,
    shadowColor: "#EEEEEE",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }

});

export default connect(mapStateToPrpos)(ControlerCreateNewShift);