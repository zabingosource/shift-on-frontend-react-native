/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity,
    ScrollView,
    Alert,
    SafeAreaView,
    Dimensions,
    TextInput,
    CheckBox, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import ModalSelector from '../../components/CustomModelSelector';
import WebView from 'react-native-webview';
import AppConstants from '../../apis/AppConstants';


const { width } = Dimensions.get('window');

export default class ViewShiftDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.navigation.getParam('details', 'NO-item').id,
            shiftTitle: this.props.navigation.getParam('details', 'NO-item').shift_title,
            shiftType: this.props.navigation.getParam('details', 'NO-item').shift_type,
            gender: this.props.navigation.getParam('details', 'NO-item').gender,
            shiftDesc: this.props.navigation.getParam('details', 'NO-item').shift_description,
            startDate: this.props.navigation.getParam('details', 'NO-item').start_date,
            endDate: this.props.navigation.getParam('details', 'NO-item').end_date,
            startTime: this.props.navigation.getParam('details', 'NO-item').start_time,
            endTime: this.props.navigation.getParam('details', 'NO-item').end_time,
            backDataDays: this.props.navigation.getParam('details', 'NO-item').days,
            break_start: this.props.navigation.getParam('details', 'NO-item').start_break_time,
            break_end: this.props.navigation.getParam('details', 'NO-item').end_break_time,
            weekdays: [],

        };
        console.log(this.props.navigation);
    }


    componentDidMount() {
        const daysdata = [
            {
                label: 'SUN',
                isSelected: false,
            },
            {
                label: 'MON',
                isSelected: false,
            },
            {
                label: 'TUE',
                isSelected: false,
            },
            {
                label: 'WED',
                isSelected: false,
            },
            {
                label: 'THU',
                isSelected: false,
            },
            {
                label: 'FRI',
                isSelected: false,
            },
            {
                label: 'SAT',
                isSelected: false,
            },
        ]
        var days = [];
        const myArray = this.state.backDataDays.split(",");

        for (var i = 0; i < daysdata.length; i++) {
            for (var j = 0; j < myArray.length; j++) {
                if (myArray[j].toString() == daysdata[i].label.toString()) {
                    daysdata[i].isSelected = true
                }
            }
            days.push(daysdata[i])
        }

        this.setState({
            weekdays: days,
        });
        console.log("weekdays===========", this.state.weekdays);
    }


    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
                    <View style={styles.mainContainer}>


                        <View style={styles.mainContainer}>


                            <View style={{ backgroundColor: '#00D5E1' }}>
                                <TouchableOpacity
                                    style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                                    onPress={() => this.props.navigation.goBack()}
                                >
                                    <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                                </TouchableOpacity>
                                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>SHIFT DETAILS</Text>

                            </View>

                            {/* <ScrollView>
                                <View style={{ marginTop: 10, marginLeft: 30, marginRight: 30 }}>


                                    <View style={{ marginTop: 5 }}>
                                        <Text style={{ color: '#000000', fontSize: 16, fontFamily: 'montserrat_semibold' }}>SHIFT TITLE :</Text>
                                        <Text style={{ marginLeft: 20, color: '#626262', fontFamily: 'montserrat_regular', fontSize: 14, }}>
                                            {this.state.shiftTitle}
                                        </Text>
                                    </View>


                                    <View style={{ marginTop: 10 }}>

                                        <Text style={{ color: '#000000', fontSize: 16, fontFamily: 'montserrat_semibold' }}>SHIFT DESCRIPTION :</Text>
                                        <Text style={{ marginLeft: 20, color: '#626262', fontFamily: 'montserrat_regular', fontSize: 14, }}>
                                            {this.state.shiftDesc}
                                        </Text>
                                    </View>

                                    <View style={{ marginTop: 10 }}>

                                        <Text style={{ color: '#000000', fontSize: 16, fontFamily: 'montserrat_semibold' }}>SHIFT TYPE :</Text>
                                        <Text style={{ marginLeft: 20, color: '#626262', fontFamily: 'montserrat_regular', fontSize: 14, }}>
                                            {this.state.shiftType}
                                        </Text>
                                    </View>


                                    <View style={{ marginTop: 10 }}>

                                        <Text style={{ color: '#000000', fontSize: 16, fontFamily: 'montserrat_semibold' }}>GENDER :</Text>
                                        <Text style={{ marginLeft: 20, color: '#626262', fontFamily: 'montserrat_regular', fontSize: 14, }}>
                                            {this.state.gender}
                                        </Text>
                                    </View>


                                    <View style={{ marginTop: 10 }}>

                                        <Text style={{ color: '#000000', fontSize: 16, fontFamily: 'montserrat_semibold' }}>START DATE :</Text>
                                        <Text style={{ marginLeft: 20, color: '#626262', fontFamily: 'montserrat_regular', fontSize: 14, }}>
                                            {this.state.startDate}
                                        </Text>
                                    </View>


                                    <View style={{ marginTop: 10 }}>

                                        <Text style={{ color: '#000000', fontSize: 16, fontFamily: 'montserrat_semibold' }}>FINISH DATE :</Text>
                                        <Text style={{ marginLeft: 20, color: '#626262', fontFamily: 'montserrat_regular', fontSize: 14, }}>
                                            {this.state.endDate}
                                        </Text>
                                    </View>

                                    <View style={{ marginTop: 10 }}>

                                        <Text style={{ color: '#000000', fontSize: 16, fontFamily: 'montserrat_semibold' }}>START TIME :</Text>
                                        <Text style={{ marginLeft: 20, color: '#626262', fontFamily: 'montserrat_regular', fontSize: 14, }}>
                                            {this.state.startTime}
                                        </Text>
                                    </View>

                                    <View style={{ marginTop: 10 }}>

                                        <Text style={{ color: '#000000', fontSize: 16, fontFamily: 'montserrat_semibold' }}>END TIME :</Text>
                                        <Text style={{ marginLeft: 20, color: '#626262', fontFamily: 'montserrat_regular', fontSize: 14, }}>
                                            {this.state.endTime}
                                        </Text>
                                    </View>

                                    <View style={{ marginTop: 10 }}>

                                        <Text style={{ color: '#000000', fontSize: 16, fontFamily: 'montserrat_semibold' }}>BREAK START TIME :</Text>
                                        <Text style={{ marginLeft: 20, color: '#626262', fontFamily: 'montserrat_regular', fontSize: 14, }}>
                                            {this.state.break_start}
                                        </Text>
                                    </View>

                                    <View style={{ marginTop: 10 }}>

                                        <Text style={{ color: '#000000', fontSize: 16, fontFamily: 'montserrat_semibold' }}>BREAK END TIME :</Text>
                                        <Text style={{ marginLeft: 20, color: '#626262', fontFamily: 'montserrat_regular', fontSize: 14, }}>
                                            {this.state.break_end}
                                        </Text>
                                    </View>

                                    <View style={{ marginTop: 10 }}>

                                        <Text style={{ color: '#000000', fontSize: 16, fontFamily: 'montserrat_semibold' }}>SELECT DAYS :</Text>
                                        <FlatList
                                            style={{ marginLeft: 20 }}
                                            data={this.state.weekdays}
                                            numColumns={7}
                                            keyExtractor={(item, index) => index.toString()}
                                            renderItem={({ item, index }) =>

                                                <View style={{ flexDirection: 'row' }}>

                                                    {
                                                        item.isSelected ?
                                                            <Text style={{ alignSelf: 'center', fontSize: 14, padding: 5, color: '#626262', }}>
                                                                {item.label}
                                                            </Text>
                                                            :
                                                            <Text></Text>
                                                    }

                                                </View>
                                            }
                                        />
                                    </View>

                                </View>
                            </ScrollView> */}

                            <WebView source={{
                                uri: AppConstants.BASE_URL + AppConstants.SHIFT_DETAILS_WEBVIEW + this.state.id
                            }} />
                        </View>


                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },
    headerBg: {
        width: width,
        height: 150,
        alignSelf: 'center',

    },

    rowContainer: {
        flexDirection: 'row',
        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        borderColor: '#EAEAEA',
        borderWidth: 2,
        marginBottom: 5


    },
    floatingLabelText: {
        color: "#393FA0",
        marginTop: 10,
        backgroundColor: '#FFFFFF',
        paddingRight: 5,

        fontSize: 14,
        fontFamily: "montserrat_regular"
    },
    heading: {
        alignSelf: 'center',
        fontSize: 22,
        fontFamily: 'montserrat_semibold',
        color: '#353C9E',
        marginTop: 10

    },
    loginText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontFamily: "montserrat_semibold",
        alignSelf: 'center'

    },
    textInput_style: {
        flex: 1,
        fontSize: 20,
        padding: 15,
        backgroundColor: '#FFFFFF',
        borderColor: '#00D5E1',
        borderRadius: 20,
        borderWidth: 2,
        color: 'rgb(63, 82, 110)',
    },
    profileImage: {
        height: 150,
        width: 150,
        position: 'absolute',
        borderRadius: 100,
        alignSelf: 'center',
        marginTop: 60

    },
    shadow: {
        flexDirection: 'column-reverse',
        backgroundColor: "#F3F3F3",
        height: 0.2,
        elevation: 3,
        shadowColor: "#F3F3F3",
        shadowOpacity: 1,
        shadowRadius: 10,
        shadowOffset: {
            height: 10,
            width: 10
        }

    },

});

