/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput,
  ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import RadioGroup from 'react-native-radio-buttons-group';
import CheckBox from '@react-native-community/checkbox';
import Loader from '../../utils/Loader';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import { setToastMsg } from '../../utils/ToastMessage';
import moment from 'moment';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../components/CustomModelSelector';



const { width } = Dimensions.get('window');


const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});
const resetToHome = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'WorkerHome' })],
});

export default class UnitUpdateNewShift extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shiftTitle: this.props.navigation.getParam('details', 'NO-item').shift_title,
      shiftType: this.props.navigation.getParam('details', 'NO-item').shift_type,
      shiftTypeData: [],
      gender: this.props.navigation.getParam('details', 'NO-item').gender,
      genderData: [],
      shiftDesc: this.props.navigation.getParam('details', 'NO-item').shift_description,
      startDate: this.props.navigation.getParam('details', 'NO-item').start_date,
      endDate: this.props.navigation.getParam('details', 'NO-item').end_date,
      startTime: this.props.navigation.getParam('details', 'NO-item').start_time,
      endTime: this.props.navigation.getParam('details', 'NO-item').end_time,
      backDataDays: this.props.navigation.getParam('details', 'NO-item').days,
      break_start: this.props.navigation.getParam('details', 'NO-item').start_break_time,
      break_end: this.props.navigation.getParam('details', 'NO-item').end_break_time,
      status: this.props.navigation.getParam('details', 'NO-item').status,
      weekdays: [],
      isLoading: false,
      isStartDatePickerVisible: false,
      isEndDatePickerVisible: false,
      isStartTimePickerVisible: false,
      isEndTimePickerVisible: false,
      isBreakStartTimePickerVisible: false,
      isBreakEndTimePickerVisible: false,
      compareDateValues: this.props.navigation.getParam('details', 'NO-item').start_date,
      compareTimeValues: this.props.navigation.getParam('details', 'NO-item').start_time,
      compareBreakTimeValues: '',
      setErrors: {
        field: '',
        message: ''
      }
    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    const daysdata = [
      {
        label: 'SUN',
        isSelected: false,
      },
      {
        label: 'MON',
        isSelected: false,
      },
      {
        label: 'TUE',
        isSelected: false,
      },
      {
        label: 'WED',
        isSelected: false,
      },
      {
        label: 'THU',
        isSelected: false,
      },
      {
        label: 'FRI',
        isSelected: false,
      },
      {
        label: 'SAT',
        isSelected: false,
      },
    ]
    var days = [];
    const myArray = this.state.backDataDays.split(",");

    for (var i = 0; i < daysdata.length; i++) {
      for (var j = 0; j < myArray.length; j++) {
        if (myArray[j].toString() == daysdata[i].label.toString()) {
          daysdata[i].isSelected = true
        }
      }
      days.push(daysdata[i])
    }

    this.setState({
      weekdays: days,
    });
    console.log("weekdays===========", this.state.weekdays);

    this._unitGenderListing();
  }

  _unitGenderListing = () => {
    this.setState({ isLoading: true });

    Api._getGenderListApi()
      .then((response) => {
        console.log(response.data)

        let newArray = response.data.map((item) => {
          return (
            {
              "key": item.id,
              "label": item.gender
            }
          )
        })
        this.setState({ genderData: newArray })
        // setToastMsg(response.message.toString());
        this._unitShiftTypeListing();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  _unitShiftTypeListing = () => {

    Api._getShiftTypeListApi()
      .then((response) => {
        console.log(response.data)

        let newArray = response.data.map((item) => {
          return (
            {
              "key": item.id,
              "label": item.name
            }
          )
        })
        this.setState({ shiftTypeData: newArray, isLoading: false })

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  // this is for start date picker...
  handleStartDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    this.setState({
      startDate: dateTimeString,
      endDate: "",
      compareDateValues: dateTimeString,
      isStartDatePickerVisible: false,
    })

    console.log(dateTimeString)
  };

  // this is for end date picker...
  handleEndDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    var start = new Date(this.state.compareDateValues);
    var end = new Date(date);

    if (this.state.compareDateValues != '') {
      if (start.getTime() > end.getTime()) {
        this.setState({
          isEndDatePickerVisible: false,
          endDate: "",
        })
        setToastMsg("Date must be greater than start date.");
        console.log("end date is smaller")
      } else {
        this.setState({
          endDate: dateTimeString,
          isEndDatePickerVisible: false,
        })
        console.log("end date is greater")
      }
    } else {
      this.setState({ isEndDatePickerVisible: false })
      setToastMsg("Please select start date first.");
      console.log("start date not selected yet.")
    }
    console.log(dateTimeString)
  };

  // this is for start time picker...
  handleStartTimeConfirm = (time) => {

    let dateTimeString = moment(time).local().format('HH:mm');

    this.setState({
      startTime: dateTimeString,
      endTime: '',
      compareTimeValues: dateTimeString,
      isStartTimePickerVisible: false,
    })

    console.log(dateTimeString)
  };

  // this is for end time picker...
  handleEndTimeConfirm = (time) => {
    let dateTimeString = moment(time).local().format('HH:mm');

    if (this.state.compareTimeValues != '') {
      // if (this.state.compareTimeValues > dateTimeString) {
      //   this.setState({
      //     isEndTimePickerVisible: false,
      //     endTime: '',
      //   })
      //   setToastMsg("End Time must be greater than Start time.");
      //   console.log("end time is smaller")
      // } else {
      //   this.setState({
      //     endTime: dateTimeString,
      //     isEndTimePickerVisible: false,
      //   })
      //   console.log("end time is greater")
      // }

      this.setState({
        endTime: dateTimeString,
        isEndTimePickerVisible: false,
      })
      console.log("end time is greater")

    } else {
      this.setState({
        isEndTimePickerVisible: false,
      })
      setToastMsg("Please select Start time first.");
      console.log("start time not selected yet.")
    }
    console.log(dateTimeString)
  };

  checkValidation = () => {
    let titleError = { field: '', message: '' }
    let shiftError = { field: '', message: '' }
    let genderError = { field: '', message: '' }
    let descriptionError = { field: '', message: '' }
    let startDateError = { field: '', message: '' }
    let endDateError = { field: '', message: '' }
    let startTimeError = { field: '', message: '' }
    let endTimeError = { field: '', message: '' }
    let startBreakError = { field: '', message: '' }
    let endBreakError = { field: '', message: '' }
    let daysError = { field: '', message: '' }


    if (this.state.shiftTitle == '') {
      titleError.field = "shift_title";
      titleError.message = "Shift title is required!";
      this.setState({ setErrors: titleError })
    } else if (this.state.shiftDesc == '') {
      descriptionError.field = "description";
      descriptionError.message = "Description is required!";
      this.setState({ setErrors: descriptionError })
    } else if (this.state.shiftType == '') {
      shiftError.field = "shift_type";
      shiftError.message = "Shift type is required!";
      this.setState({ setErrors: shiftError })
    } else if (this.state.gender == '') {
      genderError.field = "gender";
      genderError.message = "Gender is required!";
      this.setState({ setErrors: genderError })
    } else if (this.state.startDate == '') {
      startDateError.field = "start_date";
      startDateError.message = "Start date is required!";
      this.setState({ setErrors: startDateError })
    } else if (this.state.endDate == '') {
      endDateError.field = "end_date";
      endDateError.message = "Finish Date is required!";
      this.setState({ setErrors: endDateError })
    } else if (this.state.startTime == '') {
      startTimeError.field = "start_time";
      startTimeError.message = "Start time is required!";
      this.setState({ setErrors: startTimeError })
    } else if (this.state.endTime == '') {
      endTimeError.field = "end_time";
      endTimeError.message = "End time is required!";
      this.setState({ setErrors: endTimeError })
    } else if (this.state.break_start == '') {
      startBreakError.field = "break_start";
      startBreakError.message = "Break start time is required!";
      this.setState({ setErrors: startBreakError })
    } else if (this.state.break_end == '') {
      endBreakError.field = "break_end";
      endBreakError.message = "Break end time is required!";
      this.setState({ setErrors: endBreakError })
    } else {
      var filterData = [];
      for (var i = 0; i < this.state.weekdays.length; i++) {
        if (this.state.weekdays[i].isSelected) {
          filterData.push(this.state.weekdays[i].label)
        }
      }
      if (filterData.length <= 0) {
        daysError.field = "days";
        daysError.message = "Days is required!";
        this.setState({ setErrors: daysError })
      } else {
        this.setState({ setErrors: { field: '', message: '' } })
        this._unitUpdateShift();
      }
    }
  }


  _unitUpdateShift = () => {
    this.setState({ isLoading: true })

    var filterData = [];
    for (var i = 0; i < this.state.weekdays.length; i++) {
      if (this.state.weekdays[i].isSelected) {
        filterData.push(this.state.weekdays[i].label.toString())
      }
    }

    console.log(Object.values(filterData).toString());

    var formData = new FormData();
    formData.append("id", this.props.navigation.getParam('details').id);
    formData.append("user_id", this.props.navigation.getParam('details').user_id);
    formData.append("shift_title", this.state.shiftTitle);
    formData.append("shift_type", this.state.shiftType);
    formData.append("shift_description", this.state.shiftDesc);
    formData.append("gender", this.state.gender);
    formData.append("start_date", this.state.startDate);
    formData.append("end_date", this.state.endDate);
    formData.append("start_time", this.state.startTime);
    formData.append("end_time", this.state.endTime);
    formData.append("start_break_time", this.state.break_start);
    formData.append("end_break_time", this.state.break_end);
    formData.append("status", this.state.status);
    formData.append("days", Object.values(filterData).toString());
    formData.append("shift_user_type", "unit");

    Api._unitUpdateShift(formData)
      .then((response) => {
        console.log(response)

        this.setState({ isLoading: false })
        setToastMsg(response.message.toString());
        this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>UPDATE SHIFT</Text>

              </View>

              <Loader isLoading={this.state.isLoading} />

              <ScrollView>
                <View style={{ marginTop: 10, marginLeft: 30, marginRight: 30 }}>

                  <TouchableOpacity
                  // onPress={() => this._setDate("start")}
                  >
                    <CustomTextInput
                      placeholder='SHIFT TITLE'
                      floatingText="SHIFT TITLE"
                      editable={true}
                      onFocus={true}
                      caretHidden={false}
                      value={this.state.shiftTitle}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          shiftTitle: text,
                        });
                      }}

                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "shift_title" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='SHIFT DESCRIPTION'
                      keyboardType='default'
                      floatingText="SHIFT DESCRIPTION"
                      returnKeyType={'done'}
                      editable={true}
                      onFocus={true}
                      style={{ textAlignVertical: "top", height: 120, }}
                      multiline={true}
                      caretHidden={false}
                      value={this.state.shiftDesc}
                      onChangeText={(text) => {
                        this.setState({
                          shiftDesc: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "description" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <ModalSelector
                    data={this.state.shiftTypeData}
                    initValue="SHIFT TYPE"
                    accessible={true}
                    search={false}
                    animationType={"fade"}
                    scrollViewAccessibilityLabel={'Scrollable options'}
                    cancelButtonAccessibilityLabel={'Cancel Button'}
                    onChange={(option) => { this.setState({ shiftType: option.label }) }}>

                    <CustomTextInput
                      placeholder='SHIFT TYPE'
                      floatingText="SHIFT TYPE"
                      editable={false}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleDropDown={true}
                      returnKeyType={'done'}
                      value={this.state.shiftType}
                    />
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "shift_type" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <ModalSelector
                    data={this.state.genderData}
                    initValue="GENDER"
                    accessible={true}
                    search={false}
                    animationType={"fade"}
                    scrollViewAccessibilityLabel={'Scrollable options'}
                    cancelButtonAccessibilityLabel={'Cancel Button'}
                    onChange={(option) => { this.setState({ gender: option.label }) }}>

                    <CustomTextInput
                      placeholder='GENDER'
                      floatingText="GENDER"
                      editable={false}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleDropDown={true}
                      returnKeyType={'done'}
                      value={this.state.gender}
                    />
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "gender" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <TouchableOpacity style={{ marginTop: 5 }} >
                    <CustomTextInput
                      placeholder='START DATE'
                      floatingText="START DATE"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleCalendar={true}
                      value={this.state.startDate}
                      returnKeyType={'done'}
                      onTouchEnd={() => { this.setState({ isStartDatePickerVisible: true }) }}
                    />
                    <DateTimePickerModal
                      isVisible={this.state.isStartDatePickerVisible}
                      mode="date"
                      date={new Date()}
                      onConfirm={(date) => this.handleStartDateConfirm(date)}
                      onCancel={() => this.setState({ isStartDatePickerVisible: false })}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "start_date" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <TouchableOpacity style={{ marginTop: 5 }} >
                    <CustomTextInput
                      placeholder='FINISH DATE'
                      floatingText="FINISH DATE"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleCalendar={true}
                      value={this.state.endDate}
                      returnKeyType={'done'}
                      onTouchEnd={() => this.setState({ isEndDatePickerVisible: true })}
                    />
                    <DateTimePickerModal
                      isVisible={this.state.isEndDatePickerVisible}
                      mode="date"
                      date={new Date()}
                      onConfirm={(date) => this.handleEndDateConfirm(date)}
                      onCancel={() => this.setState({ isEndDatePickerVisible: false, })}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "end_date" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ flexDirection: 'row' }}>

                    <View style={{ marginTop: 5, flex: 1, marginRight: 3 }}>
                      <CustomTextInput
                        placeholder='START TIME'
                        floatingText="START TIME"
                        editable={true}
                        onFocus={true}
                        showSoftInputOnFocus={false}
                        isVisibleCalendar={true}
                        caretHidden={true}
                        //style={{ fontSize: 18, paddingLeft: 10 }}
                        value={this.state.startTime}
                        returnKeyType={'done'}
                        onTouchEnd={() => this.setState({ isStartTimePickerVisible: true })}
                      />

                      <DateTimePickerModal
                        isVisible={this.state.isStartTimePickerVisible}
                        mode="time"
                        locale="en_GB"
                        date={new Date()}
                        onConfirm={(date) => this.handleStartTimeConfirm(date)}
                        onCancel={() => this.setState({ isStartTimePickerVisible: false })}
                      />
                    </View>

                    <View style={{ marginTop: 5, flex: 1, marginLeft: 3 }}>
                      <CustomTextInput
                        placeholder='END TIME'
                        floatingText="END TIME"
                        editable={true}
                        onFocus={true}
                        showSoftInputOnFocus={false}
                        isVisibleCalendar={true}
                        caretHidden={true}
                        //style={{ fontSize: 18, paddingLeft: 10 }}
                        value={this.state.endTime}
                        returnKeyType={'done'}
                        onTouchEnd={() => this.setState({ isEndTimePickerVisible: true })}
                      />

                      <DateTimePickerModal
                        isVisible={this.state.isEndTimePickerVisible}
                        mode="time"
                        locale="en_GB"
                        date={new Date()}
                        onConfirm={(date) => this.handleEndTimeConfirm(date)}
                        onCancel={() => this.setState({ isEndTimePickerVisible: false })}
                      />
                    </View>

                  </View>
                  {
                    this.state.setErrors.field == "start_time" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }
                  {
                    this.state.setErrors.field == "end_time" && (
                      <Text style={[styles.errors, { textAlign: 'right', marginEnd: 10 }]}>{this.state.setErrors.message}</Text>
                    )
                  }


                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginTop: 5, flex: 1, marginRight: 3 }}>
                      <CustomTextInput
                        placeholder='BREAK START TIME'
                        floatingText="BREAK START TIME"
                        keyboardType='numeric'
                        editable={true}
                        onFocus={true}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        isVisibleCalendar={true}
                        value={this.state.break_start}
                        returnKeyType={'done'}
                        onTouchEnd={() => {
                          this.setState({ isBreakStartTimePickerVisible: true })
                        }}
                      />
                      <DateTimePickerModal
                        isVisible={this.state.isBreakStartTimePickerVisible}
                        mode="time"
                        locale="en_GB"
                        date={new Date()}
                        onConfirm={(date) => this.handleBreakStartTimeConfirm(date)}
                        onCancel={() => this.setState({ isBreakStartTimePickerVisible: false })}
                      />
                    </View>

                    <View style={{ marginTop: 5, flex: 1, marginLeft: 3 }}>
                      <CustomTextInput
                        placeholder='BREAK END TIME'
                        floatingText="BREAK END TIME"
                        keyboardType='numeric'
                        returnKeyType={'done'}
                        editable={true}
                        onFocus={true}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        isVisibleCalendar={true}
                        value={this.state.break_end}
                        onTouchEnd={() => {
                          this.setState({ isBreakEndTimePickerVisible: true })
                        }}
                      />
                      <DateTimePickerModal
                        isVisible={this.state.isBreakEndTimePickerVisible}
                        mode="time"
                        locale="en_GB"
                        date={new Date()}
                        onConfirm={(date) => this.handleBreakEndTimeConfirm(date)}
                        onCancel={() => this.setState({ isBreakEndTimePickerVisible: false })}
                      />
                    </View>
                  </View>
                  {
                    this.state.setErrors.field == "break_start" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }
                  {
                    this.state.setErrors.field == "break_end" && (
                      <Text style={[styles.errors, { textAlign: 'right', marginEnd: 10 }]}>{this.state.setErrors.message}</Text>
                    )
                  }



                  <View style={{ marginTop: 10, marginBottom: 10 }}>

                    <FlatList
                      style={{ width: '100%', borderRadius: 15, borderColor: '#00D5E1', borderWidth: 2, marginTop: 12, padding: 5 }}
                      data={this.state.weekdays}
                      numColumns={4}
                      keyExtractor={(item, index) => index.toString()}
                      //ItemSeparatorComponent={this.ItemSeparator}
                      renderItem={({ item, index }) =>

                        <TouchableOpacity
                          activeOpacity={0.9}
                          style={{ margin: 5 }}
                        //onPress={() => this.props.navigation.navigate('WorkerJobDetails', { position: index })}
                        >
                          <View style={{ flexDirection: 'row', backgroundColor: '#FFFFFF', borderRadius: 5 }}>
                            <CheckBox
                              value={item.isSelected}
                              onValueChange={() => this._setDay(item)}
                              style={{ alignSelf: "center", }}
                            />
                            <Text style={{ alignSelf: 'center', fontSize: 14, paddingTop: 10, paddingBottom: 10, paddingRight: 5, fontFamily: 'montserrat_regular', color: "#000000" }}>{item.label}</Text>

                          </View>
                        </TouchableOpacity>
                      }
                    />
                    <Text style={{ position: 'absolute', padding: 5, marginLeft: 20, color: '#393FA0', backgroundColor: '#FFFFFF', fontFamily: 'montserrat_regular', fontSize: 10 }}>SELECT DAYS</Text>
                  </View>
                  {
                    this.state.setErrors.field == "days" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                </View>
              </ScrollView>
            </View>
            <View >

              <View style={styles.shadow}></View>
              {/*  <Image style={{ width:'100%',height:40,}} source={require('./../../assets/border.png')} /> */}
              <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                onPress={() => this.checkValidation()}
              >
                <Text style={styles.loginText}>UPDATE</Text>
              </TouchableOpacity>

            </View>


          </View>
        </SafeAreaView>
      </Fragment>
    );
  }
  _setDay = (day) => {
    console.log("day =========", day);
    var days = [];
    for (var i = 0; i < this.state.weekdays.length; i++) {
      if (day.label == this.state.weekdays[i].label) {
        this.state.weekdays[i].isSelected = !this.state.weekdays[i].isSelected
      }
      days.push(this.state.weekdays[i])
    }
    this.setState({
      weekdays: days,
    });
  }

  // this is for break start time picker...
  handleBreakStartTimeConfirm = (time) => {

    let dateTimeString = moment(time).local().format('HH:mm');

    this.setState({
      break_start: dateTimeString,
      break_end: '',
      compareBreakTimeValues: time,
      isBreakStartTimePickerVisible: false,
    })

    console.log(dateTimeString)
  };

  // this is for break end time picker...
  handleBreakEndTimeConfirm = (time) => {
    let dateTimeString = moment(time).local().format('HH:mm');

    var start = new Date(this.state.compareBreakTimeValues);
    var end = new Date(time);

    if (this.state.compareBreakTimeValues != '') {
      // if (start.getTime() > end.getTime()) {
      //   this.setState({
      //     isBreakEndTimePickerVisible: false,
      //     break_end: '',
      //   })
      //   setToastMsg("End Time must be greater than Start time.");
      //   console.log("end time is smaller")
      // } else {
      //   this.setState({
      //     break_end: dateTimeString,
      //     isBreakEndTimePickerVisible: false,
      //   })
      //   console.log("end time is greater")
      // }

      this.setState({
        break_end: dateTimeString,
        isBreakEndTimePickerVisible: false,
      })
      console.log("end break time is greater")

    } else {
      this.setState({
        isBreakEndTimePickerVisible: false,
      })
      setToastMsg("Please select Start break time first.");
      console.log("start break time not selected yet.")
    }
    console.log(dateTimeString)
  };

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center',

  },

  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5


  },
  floatingLabelText: {
    color: "#393FA0",
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    paddingRight: 5,

    fontSize: 14,
    fontFamily: "montserrat_regular"
  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  profileImage: {
    height: 150,
    width: 150,
    position: 'absolute',
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: 60

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }

});

