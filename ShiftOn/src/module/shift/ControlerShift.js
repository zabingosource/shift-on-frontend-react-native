/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView,
  Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../components/CustomModelSelector';
import { connect } from 'react-redux';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
  }
}

const { width } = Dimensions.get('window');

class ControlerShift extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchQuery: '',
      selectedTab: '1',
      apiJobData: [],
      myJobData: [],
      unitJobData: [],
      filterData: [],
      isLoading: true,
    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    this.navFocusListener = this.props.navigation.addListener('didFocus', () => {
      this._controllerAllJobListing();
    });
  }

  componentWillUnmount() {
    this.navFocusListener.remove();
  }

  _controllerAllJobListing = async () => {
    this.setState({ isLoading: true });
    var id = await AsyncStorage.getItem('agency_id')
    console.log(id)

    var formData = new FormData();
    formData.append("agency_id", id);

    Api._controllerAllJobListing(formData)
      .then((response) => {

        console.log(response.data)

        this.unitApiData(response.data);
        this.myApiData(response.data);
        this.setState({ isLoading: false, selectedTab: '1' });
        //setToastMsg(response.message.toString());

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  unitApiData = (responseData) => {
    var tempData = [];
    for (var i = 0; i < responseData.length; i++) {
      if (responseData[i].type == "unit") {
        if (responseData[i].job_details != null) {
          tempData.push(responseData[i])
        }
      }
    }
    this.setState({
      unitJobData: tempData,
      apiJobData: tempData,
      filterData: tempData,
    })
  }

  myApiData = (responseData) => {
    var tempData = [];
    for (var i = 0; i < responseData.length; i++) {
      if (responseData[i].type == "controller") {
        if (responseData[i].job_details != null) {
          tempData.push(responseData[i])
        }
      }
    }
    this.setState({ myJobData: tempData })
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

          <View style={styles.mainContainer}>
            <View style={{ backgroundColor: '#00D5E1' }}>
              <TouchableOpacity
                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
              </TouchableOpacity>
              <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>JOB LISTS</Text>
              {/* <TouchableOpacity
                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20, right: 0 }}
                onPress={() => this.props.navigation.navigate('ControlerCreateNewShift')}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/more_icn.png')} />
              </TouchableOpacity> */}
            </View>

            <Loader isLoading={this.state.isLoading} />

            <View style={{ marginLeft: 10, marginRight: 10 }}>
              <View style={{ flexDirection: 'row', marginTop: 20, backgroundColor: '#626262', borderRadius: 40 }}>
                <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "1" ? "#00D5E1" : "#626262" }]}
                  activeOpacity={1}
                  onPress={() => this._onChangeTab("1")}
                >
                  <Text style={{ alignSelf: 'center', fontSize: 14, padding: 5, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "1" ? "#000000" : "#FFFFFF" }}>UNIT JOBS </Text>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "2" ? "#00D5E1" : "#626262" }]}
                  activeOpacity={1}
                  onPress={() => this._onChangeTab("2")}
                >
                  <Text style={{ alignSelf: 'center', fontSize: 14, padding: 5, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "2" ? "#000000" : "#FFFFFF" }}>MY JOBS </Text>
                </TouchableOpacity>


              </View>
              {
                this.state.selectedTab == "1" ?
                  <View style={{ flexDirection: 'row', marginTop: 20, backgroundColor: '#EAEAEA', borderRadius: 40 }}>
                    <Image style={{ height: 30, width: 30, padding: 5, position: 'absolute', end: 5, top: 3.5 }} source={require('./../../assets/search_icn.png')} />
                    <TextInput
                      style={{ width: '85%', paddingHorizontal: 8, paddingTop: 8, paddingBottom: 10, marginLeft: 10, marginRight: 40, color: '#969696', fontFamily: 'montserrat_regular', fontSize: 12 }}
                      placeholder='Search by agency name'
                      keyboardType='default'
                      value={this.state.searchQuery}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.searchFilter(text);
                      }}
                    />
                  </View>
                  :
                  <View></View>
              }

              {
                (this.state.apiJobData.length > 0 ?

                  <View style={{ marginTop: 20, marginBottom: 20 }}>
                    <FlatList
                      style={{ width: '100%', marginBottom: this.state.selectedTab == "1" ? 280 : 170 }}
                      data={this.state.apiJobData}
                      keyExtractor={(item, index) => index.toString()}
                      renderItem={({ item, index }) =>
                        <TouchableOpacity
                          onPress={() => this._navigatePage(index, item)}
                        >
                          <View style={styles.rowContainer}>

                            <View style={{ width: "16%", justifyContent: 'center' }}>
                              <Image style={{ height: 42, width: 40, tintColor: '#000000' }} source={require('./../../assets/shift2_icn.png')} />
                            </View>

                            <View style={{ width: "66%", }}>
                              <Text numberOfLines={1}
                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                              >{item.job_details.job_title}</Text>
                              <Text
                                numberOfLines={1}
                                style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                              >{item.job_details.job_description}</Text>
                              <View style={{ marginTop: 5, flexDirection: "row" }}>
                                <Image source={require('./../../assets/pin_icn.png')} style={{ width: 10, height: 15, alignSelf: 'center' }} resizeMode='stretch' />
                                <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>

                                  {
                                    item.job_details.location_details ?
                                      (item.job_details.location_details.address + ", " + item.job_details.location_details.landmark + ", " + item.job_details.location_details.post_code)
                                      :
                                      (item.job_details.location)
                                  }
                                </Text>
                              </View>
                              <View style={{ flexDirection: 'column' }}>
                                <View style={{ marginTop: 5, flexDirection: "row", flex: 1.5, marginRight: 2 }}>
                                  <Image source={require('./../../assets/calendar_icn_b.png')} style={{ width: 12, height: 12, alignSelf: 'center' }} resizeMode='stretch' />
                                  <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>{item.job_details.shift.start_date} to {item.job_details.shift.end_date}</Text>
                                </View>
                                <View style={{ marginTop: 5, flexDirection: "row", flex: 1, }}>
                                  <Image source={require('./../../assets/time_icn_a.png')} style={{ width: 12, height: 12, alignSelf: 'center' }} resizeMode='stretch' />
                                  <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>{item.job_details.shift.start_time} to {item.job_details.shift.end_time}</Text>
                                </View>

                              </View>

                            </View>

                            <View style={{ width: "18%", marginTop: 5, alignItems: 'center' }}>
                              <Image style={{ height: 20, width: 18, padding: 5, alignSelf: 'center' }} source={require('./../../assets/profile_icn_b.png')} resizeMode='stretch' />
                              <Text
                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 15, alignSelf: 'center' }}
                              >{item.job_details.required_people}</Text>
                              <Text
                                style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 7, alignSelf: 'center' }}
                              >No of people</Text>

                              <View style={{ marginTop: 12 }}>
                                {
                                  this.state.selectedTab == "1" ?
                                    <Text
                                      style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10, alignSelf: 'center' }}
                                    >Posted on</Text>
                                    :
                                    <Text
                                      style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10, alignSelf: 'center' }}
                                    >Created on</Text>
                                }

                                <Text
                                  style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10, alignSelf: 'center' }}
                                >{moment(item.created_at).format('yyyy-MM-DD')}</Text>
                              </View>
                            </View>
                          </View>
                        </TouchableOpacity>
                      }
                    />
                  </View>
                  :
                  <View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                    <Text
                      numberOfLines={1}
                      style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                    >No Data Available!</Text>
                  </View>)
              }

              <View>

              </View>

            </View>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _onChangeTab(tab) {
    if (tab == "1") {
      this.setState({
        selectedTab: "1",
        searchQuery: '',
        apiJobData: this.state.unitJobData,
        filterData: this.state.unitJobData,
      });
    } else {
      this.setState({
        selectedTab: "2",
        apiJobData: this.state.myJobData,
      });
    }

  }

  searchFilter = (text) => {
    if (text) {
      const newData = this.state.filterData.filter((item) => {
        const itemData = item.agency.name ? item.agency.name.toUpperCase() : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      this.setState({ apiJobData: newData, searchQuery: text })
    } else {
      this.setState({ apiJobData: this.state.filterData, searchQuery: text })
    }
  }

  _navigatePage = (index, itemData) => {

    this.props.navigation.navigate('ControlerPostToWorkerAvailabilityList', {
      details: itemData.job_details, unit_id: itemData.unit_id,
    });

    // if (itemData.status.toString() == 0) {
    //   this.props.navigation.navigate('ControlerPostToWorkerAvailabilityList', {
    //     details: itemData.job_details, unit_id: itemData.unit_id
    //   });
    // } else if (itemData.status.toString() == 1) {
    //   this.props.navigation.navigate('ControlerRepostToUnitAvailabilityList', {
    //     details: itemData.job_details, unit_id: itemData.unit_id
    //   });
    // } else if (itemData.status.toString() == 2) {
    //   this.props.navigation.navigate('ControlerConfirmToWorkerAvailabilityList', { details: itemData.job_details });
    // } else {
    //   this.props.navigation.navigate('ControlerAvailabilityList', { details: itemData.job_details });
    // }
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },

  tabHolder: {
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 30,
    flex: 1
  },
  rowContainer: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5


  },


});

export default connect(mapStateToPrpos)(ControlerShift);