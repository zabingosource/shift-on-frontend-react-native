/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions,
    TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CheckBox from '@react-native-community/checkbox';
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../components/CustomModelSelector';


const { width } = Dimensions.get('window');

export default class ControlerPostToWorkerAvailabilityList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchQuery: '',
            isLoading: false,
            selectedTab: '1',
            checkBoxCount: 0,
            isPostedAnyOne: false,
            flatListData: [],
            filterData: [],
            selectAllBox: false,
            topDetailData: this.props.navigation.getParam('details', 'NO-item'),
            topDetailUnitId: this.props.navigation.getParam('unit_id', 'NO-item'),

        };
        console.log(this.props.navigation);
    }


    componentDidMount() {
        this._controllerWorkerAvailablityList1()
    }


    _controllerWorkerAvailablityList1 = async () => {
        this.setState({ isLoading: true });
        var agency_id = await AsyncStorage.getItem('agency_id')

        var formData = new FormData();
        formData.append("job_id", this.state.topDetailData.id);
        formData.append("agency_id", agency_id);

        console.log(this.state.topDetailData.id)
        console.log(agency_id)

        Api._controllerWorkerAvailablityList1(formData)
            .then((response) => {

                console.log(response.data)
                var tempData = [];
                for (var i = 0; i < response.data.length; i++) {
                    if (response.data[i].worker != null) {
                        tempData.push(response.data[i])
                    }
                    if (response.data[i].is_posted.toString() == "1") {
                        this.setState({ isPostedAnyOne: true })
                    }
                }
                this.setState({
                    flatListData: tempData,
                    filterData: tempData,
                    isLoading: false,
                })
                //setToastMsg(response.message.toString());
            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false });
            });
    }

    _postToWorkerAction = async () => {
        this.setState({ isLoading: true });
        var agency_id = await AsyncStorage.getItem('agency_id');

        var filterData = [];
        for (var i = 0; i < this.state.flatListData.length; i++) {
            if (this.state.flatListData[i].isSelected) {
                filterData.push(this.state.flatListData[i].worker.id.toString())
            }
        }
        console.log(Object.values(filterData));

        var rowData = {
            "job_id": this.state.topDetailData.id,
            "workers_id": Object.values(filterData),
            "agency_id": agency_id,
        }

        Api._controllerWorkerAvailablityListPostToWorker(rowData)
            .then((response) => {

                console.log(response.data)
                //this.setState({ isLoading: false })
                this._controllerWorkerAvailablityList1()
                setToastMsg(response.message.toString());

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false })
            });

    }

    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

                    <View style={styles.mainContainer}>
                        <View style={{ backgroundColor: '#00D5E1' }}>
                            <TouchableOpacity
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                                onPress={() => this.props.navigation.goBack()}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                            </TouchableOpacity>
                            <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>AVAILABILITY LIST</Text>

                        </View>

                        <Loader isLoading={this.state.isLoading} />

                        <TouchableOpacity activeOpacity={0.8}>
                            <View style={styles.headerContainer}>

                                <TouchableOpacity activeOpacity={0.7}
                                    onPress={() => this.props.navigation.navigate('ViewJobDetail', { jobData: this.state.topDetailData.id })}
                                    style={{ width: "18%", justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={{ height: 42, width: 40, tintColor: '#000000' }} source={require('./../../assets/shift2_icn.png')} />
                                </TouchableOpacity>

                                <View style={{ width: "66%", padding: 10, }}>
                                    <View style={{ flexDirection: 'row' }}>

                                        <Text
                                            numberOfLines={1}
                                            style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 16, }}
                                        >{this.state.topDetailData.job_title}</Text>
                                    </View>

                                    <Text
                                        numberOfLines={1}
                                        style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 16, }}
                                    >{this.state.topDetailData.job_description} </Text>
                                    <View style={{ marginTop: 5, flexDirection: "row" }}>
                                        <Image source={require('./../../assets/pin_icn.png')} style={{ width: 15, height: 20 }} resizeMode='stretch' />
                                        <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12 }}>
                                            {this.state.topDetailData.location}{this.state.topDetailData.landmark ? ", " + this.state.topDetailData.landmark : ''}{this.state.topDetailData.zip ? ", " + this.state.topDetailData.zip : ''}
                                        </Text>
                                    </View>
                                    <View style={{ flexDirection: 'column' }}>
                                        <View style={{ marginTop: 5, flexDirection: "row", marginRight: 2 }}>
                                            <Image source={require('./../../assets/calendar_icn_b.png')} style={{ width: 15, height: 15 }} resizeMode='stretch' />
                                            <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12 }}>{this.state.topDetailData.shift.start_date} to {this.state.topDetailData.shift.end_date}</Text>
                                        </View>
                                        <View style={{ marginTop: 5, flexDirection: "row" }}>
                                            <Image source={require('./../../assets/time_icn_a.png')} style={{ width: 15, height: 15 }} resizeMode='stretch' />
                                            <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12 }}>{this.state.topDetailData.shift.start_time} to {this.state.topDetailData.shift.end_time}</Text>
                                        </View>

                                    </View>

                                </View>

                                <View style={{ width: "16%", marginTop: 5, alignItems: 'center' }}>
                                    <Text
                                        style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 7, alignSelf: 'center' }}
                                    >REQUIRED</Text>
                                    <Text
                                        style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 15, alignSelf: 'center' }}
                                    >{this.state.topDetailData.required_people}</Text>

                                    {
                                        this.state.topDetailData.status.toString() == "0" ?
                                            <Text style={{ fontSize: 10, textAlign: 'center', color: "red", fontFamily: 'montserrat_regular', marginTop: 15, }}>
                                                Already Closed</Text>
                                            :
                                            <Text></Text>
                                    }
                                </View>

                            </View>
                        </TouchableOpacity>

                        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                            {
                                this._returnButton()
                            }

                            <View style={{ marginTop: 10, marginLeft: 8, }}>
                                <TouchableOpacity
                                    style={{ backgroundColor: '#00D5E1', padding: 10, alignSelf: 'center', borderRadius: 5 }}
                                    onPress={() => {
                                        this.props.navigation.navigate('ControlerRepostToUnitAvailabilityList', {
                                            details: this.state.topDetailData, unit_id: this.state.topDetailUnitId,
                                        });
                                    }}
                                >
                                    <Text style={{ alignSelf: 'center', fontSize: 15, fontFamily: 'montserrat_regular', color: "#000000", }}>GO TO NEXT</Text>
                                </TouchableOpacity>

                            </View>
                        </View>

                        <View style={{ marginLeft: 10, marginRight: 10 }}>

                            {
                                this._returnTab()
                            }

                            <View style={{ flexDirection: 'row', marginTop: 10, backgroundColor: '#EAEAEA', borderRadius: 40 }}>
                                <Image style={{ height: 30, width: 30, padding: 5, position: 'absolute', end: 5, top: 3.5 }} source={require('./../../assets/search_icn.png')} />
                                <TextInput
                                    style={{ width: '85%', paddingHorizontal: 8, paddingTop: 8, paddingBottom: 10, marginLeft: 10, marginRight: 40, color: '#969696', fontFamily: 'montserrat_regular', fontSize: 15 }}
                                    placeholder='Search by name'
                                    keyboardType='default'
                                    value={this.state.searchQuery}
                                    returnKeyType={'done'}
                                    onChangeText={(text) => {
                                        this.searchFilter(text);
                                    }}
                                />
                            </View>

                            {
                                this.state.flatListData.length > 0 ?

                                    <FlatList
                                        style={{ width: '100%', marginBottom: 380, marginTop: 10 }}
                                        data={this.state.flatListData}
                                        keyExtractor={(item, index) => index.toString()}
                                        //ItemSeparatorComponent={this.ItemSeparator}
                                        renderItem={({ item, index }) =>
                                            <TouchableOpacity
                                                activeOpacity={0.9}
                                            >
                                                <View style={styles.rowContainer}>
                                                    <View style={{ width: '22%' }}>
                                                        {item.worker.worker_details.image ?
                                                            <Image style={{ height: 100, width: "100%", borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={{ uri: AppConstants.IMAGE_URL + item.worker.worker_details.image }} />
                                                            :
                                                            <Image style={{ height: 100, width: "100%", borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={require('./../../assets/woman_face.jpeg')} />
                                                        }

                                                    </View>

                                                    <View style={{ width: "58%", padding: 10, }}>
                                                        <Text
                                                            numberOfLines={1}
                                                            style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                                                        >{item.worker.worker_details.fore_name} {item.worker.worker_details.sur_name}</Text>

                                                        <View style={{ marginTop: 5, flexDirection: "row", alignItems: 'center' }}>
                                                            <Image source={require('./../../assets/phone_icon.png')} style={{ width: 12, height: 12, alignSelf: 'center' }} resizeMode='stretch' />
                                                            <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12 }}>
                                                                {item.worker.worker_details.mobile}
                                                            </Text>
                                                        </View>
                                                        <View style={{ marginTop: 5, flexDirection: "row", alignItems: 'center' }}>
                                                            <Image source={require('./../../assets/email_icon.png')} style={{ width: 12, height: 12, alignSelf: 'center' }} resizeMode='stretch' />
                                                            <Text numberOfLines={1} style={{ marginLeft: 3, marginRight: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12 }}>
                                                                {item.worker.worker_details.email}
                                                            </Text>
                                                        </View>

                                                    </View>

                                                    <View style={{ width: '20%', marginTop: 5, alignItems: 'center' }}>

                                                        {
                                                            item.is_posted.toString() == "0" ?
                                                                <CheckBox
                                                                    value={item.isSelected}
                                                                    onValueChange={() => this.setCheckBox(item)}
                                                                    style={{ marginTop: 5 }}
                                                                />
                                                                :
                                                                <View style={{ marginTop: 10, }}>
                                                                    {/* <CheckBox
                                                                        value={item.isSelected}
                                                                        tintColors={{ true: '#c6c6c6', false: '#c6c6c6' }}
                                                                        style={{ marginTop: 5 }}
                                                                    /> */}
                                                                    <Text style={{ fontSize: 11, color: "red", fontFamily: 'montserrat_regular', marginBottom: 10, }}>Already Posted</Text>
                                                                    <Text style={{ color: '#666666', fontFamily: 'montserrat_regular', fontSize: 10, }}>Posted on</Text>
                                                                    <Text style={{ color: '#666666', fontFamily: 'montserrat_regular', fontSize: 10, }}>
                                                                        {moment(item.posted_date).format('yyyy-MM-DD')}
                                                                    </Text>
                                                                </View>
                                                        }
                                                    </View>

                                                </View>
                                            </TouchableOpacity>
                                        }
                                    />
                                    :
                                    <View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                                        <Text
                                            numberOfLines={1}
                                            style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                                        >No Data Available!</Text>
                                    </View>
                            }
                        </View>

                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }

    selectAllCheckBox = () => {
        this.setState({ isLoading: true })

        this.setState({ selectAllBox: !this.state.selectAllBox });

        var selectedData = [];
        var filterData = [];

        setTimeout(() => {

            if (this.state.selectAllBox) {
                for (var i = 0; i < this.state.flatListData.length; i++) {
                    this.state.flatListData[i].isSelected = true
                    selectedData.push(this.state.flatListData[i])
                }
            } else {
                for (var i = 0; i < this.state.flatListData.length; i++) {
                    this.state.flatListData[i].isSelected = false
                    selectedData.push(this.state.flatListData[i])
                }
            }

            // for enable repost button
            for (var i = 0; i < selectedData.length; i++) {
                if (selectedData[i].isSelected) {
                    filterData.push(selectedData[i].worker.id)
                }
            }

            this.setState({
                flatListData: selectedData,
                checkBoxCount: filterData.length,
                isLoading: false,
            });
        }, 1500)

    }

    setCheckBox = (data) => {

        this.setState({ isLoading: true })
        console.log("data =========", data);

        var selectedData = [];
        var filterData = [];
        for (var i = 0; i < this.state.flatListData.length; i++) {
            if (data.worker.worker_details.id == this.state.flatListData[i].worker.worker_details.id) {
                this.state.flatListData[i].isSelected = !this.state.flatListData[i].isSelected
            }
            selectedData.push(this.state.flatListData[i])
        }

        // for enable repost button
        for (var i = 0; i < selectedData.length; i++) {
            if (selectedData[i].isSelected) {
                filterData.push(selectedData[i].worker.id)
            }
        }

        this.setState({
            flatListData: selectedData,
            checkBoxCount: filterData.length,
            isLoading: false,
        });

        // for enable/desable all checkbox ...
        var allSelectdValue = true;
        for (var i = 0; i < this.state.flatListData.length; i++) {
            if (!this.state.flatListData[i].isSelected) {
                allSelectdValue = false;
            }
        }
        if (allSelectdValue) {
            this.setState({ selectAllBox: true });
        } else {
            this.setState({ selectAllBox: false });
        }

    }

    _returnButton() {

        if (this.state.checkBoxCount >= 1) {
            return (
                <View style={{ marginTop: 10 }}>
                    <TouchableOpacity
                        style={{ backgroundColor: '#00D5E1', padding: 10, alignSelf: 'center', borderRadius: 5 }}
                        onPress={() => {
                            this.state.topDetailData.status.toString() == "0" ?
                                setToastMsg("This job is already closed.")
                                :
                                this._postToWorkerAction()
                        }}
                    >
                        <Text style={{ alignSelf: 'center', fontSize: 15, fontFamily: 'montserrat_regular', color: "#000000", }}>POST TO WORKERS</Text>
                    </TouchableOpacity>

                </View>
            )
        } else {
            return (
                <View style={{ marginTop: 10 }}>
                    <TouchableOpacity
                        style={{ backgroundColor: '#EAEAEA', padding: 10, alignSelf: 'center', borderRadius: 5 }}
                        onPress={() => setToastMsg("Please select atleast one worker.")}
                    >
                        <Text style={{ alignSelf: 'center', fontSize: 15, fontFamily: 'montserrat_regular', color: "#000000", }}>POST TO WORKERS</Text>
                    </TouchableOpacity>

                </View>
            )
        }

    }

    _returnTab() {
        return (
            <View style={{ flexDirection: 'row', marginTop: 10, backgroundColor: '#FFFFFF', borderRadius: 40, marginBottom: 10 }}>
                <Text style={{ fontSize: 14, padding: 7, fontFamily: 'montserrat_semibold', color: "#000000" }}>MATCHING APPLICANTS</Text>
                {
                    this.state.isPostedAnyOne ?
                        <TouchableOpacity
                            style={{ position: 'absolute', right: 2, }}
                        >
                            <View style={{ flexDirection: 'row', backgroundColor: '#c8c8c8', borderRadius: 5 }}>
                                <CheckBox
                                    value={this.state.selectAllBox}
                                    //onValueChange={() => this.selectAllCheckBox()}
                                    style={{ alignSelf: "center", }}
                                />
                                <Text style={{ alignSelf: 'center', fontSize: 14, paddingTop: 10, paddingBottom: 10, paddingRight: 5, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "1" ? "#000000" : "#FFFFFF" }}>SELECT ALL</Text>

                            </View>

                        </TouchableOpacity>
                        :
                        <TouchableOpacity
                            style={{ position: 'absolute', right: 2, }}
                            onPress={() => { this.selectAllCheckBox() }}
                        >
                            <View style={{ flexDirection: 'row', backgroundColor: '#00D5E1', borderRadius: 5 }}>
                                <CheckBox
                                    value={this.state.selectAllBox}
                                    onValueChange={() => this.selectAllCheckBox()}
                                    style={{ alignSelf: "center", }}
                                />
                                <Text style={{ alignSelf: 'center', fontSize: 14, paddingTop: 10, paddingBottom: 10, paddingRight: 5, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "1" ? "#000000" : "#FFFFFF" }}>SELECT ALL</Text>

                            </View>

                        </TouchableOpacity>
                }


            </View>
        )


    }

    searchFilter = (text) => {
        if (text) {
            const newData = this.state.filterData.filter((item) => {
                const itemData = (item.worker.worker_details.fore_name ?
                    item.worker.worker_details.fore_name.toUpperCase() : ''.toUpperCase()) + " " +
                    (item.worker.worker_details.sur_name ?
                        item.worker.worker_details.sur_name.toUpperCase() : ''.toUpperCase());
                const textData = text.toUpperCase();
                return itemData.indexOf(textData) > -1;
            });
            this.setState({ flatListData: newData, searchQuery: text })
        } else {
            this.setState({ flatListData: this.state.filterData, searchQuery: text })
        }
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },

    tabHolder: {
        padding: 5,
        backgroundColor: "#00D5E1",
        borderRadius: 30,
        flex: 1
    },
    rowContainer: {
        width: '100%',
        flexDirection: 'row',
        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        borderColor: '#EAEAEA',
        borderWidth: 2,
        marginBottom: 5
    },

    headerContainer: {
        height: 160,
        flexDirection: 'row',
        backgroundColor: "#FFFFFF",
        borderColor: '#EAEAEA',
        borderBottomWidth: 2,
        marginBottom: 5,
        padding: 10


    },
    loginHold: {
        flex: 1,
        padding: 5,
        backgroundColor: "#657CF4",
        borderRadius: 20,
        marginRight: 5
    },
    loginText: {
        color: '#FFFFFF',
        fontSize: 15,
        fontFamily: "montserrat_semibold",
        alignSelf: 'center'

    },
    registerHold: {
        flex: 1,
        padding: 5,
        backgroundColor: "#00D5E1",
        borderRadius: 20,
        marginLeft: 5
    },
    registerText: {
        color: '#FFFFFF',
        fontSize: 15,
        fontFamily: "montserrat_semibold",
        alignSelf: 'center'

    },
});

