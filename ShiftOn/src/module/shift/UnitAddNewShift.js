/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions,
  TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import RadioGroup from 'react-native-radio-buttons-group';
import CheckBox from '@react-native-community/checkbox';
import Loader from '../../utils/Loader';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import moment from 'moment';
import { setToastMsg } from '../../utils/ToastMessage';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../components/CustomModelSelector';
import { connect } from 'react-redux';


const { width } = Dimensions.get('window');


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
  }
}

class UnitAddNewShift extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shiftTitle: '',
      shiftType: '',
      shiftTypeData: [],
      gender: "",
      genderData: [],
      shiftDesc: '',
      start_date: '',
      end_date: '',
      startTime: '',
      endTime: '',
      break_start: '',
      break_end: '',
      weekdays: [],
      isLoading: false,
      isStartDatePickerVisible: false,
      isEndDatePickerVisible: false,
      isStartTimePickerVisible: false,
      isEndTimePickerVisible: false,
      isBreakStartTimePickerVisible: false,
      isBreakEndTimePickerVisible: false,
      compareDateValues: '',
      compareTimeValues: '',
      compareBreakTimeValues: '',
      setErrors: {
        field: '',
        message: ''
      }

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    const daysdata = [
      {
        label: 'SUN',
        isSelected: false,
      },
      {
        label: 'MON',
        isSelected: false,
      },
      {
        label: 'TUE',
        isSelected: false,
      },
      {
        label: 'WED',
        isSelected: false,
      },
      {
        label: 'THU',
        isSelected: false,
      },
      {
        label: 'FRI',
        isSelected: false,
      },
      {
        label: 'SAT',
        isSelected: false,
      },
    ]
    var days = [];
    for (var i = 0; i < daysdata.length; i++) {
      days.push(daysdata[i])
    }
    this.setState({
      weekdays: days,
    });
    console.log("weekdays===========", this.state.weekdays);

    this._unitGenderListing();
  }

  _unitGenderListing = () => {
    this.setState({ isLoading: true });

    Api._getGenderListApi()
      .then((response) => {
        console.log(response.data)

        let newArray = response.data.map((item) => {
          return (
            {
              "key": item.id,
              "label": item.gender
            }
          )
        })
        this.setState({ genderData: newArray })
        // setToastMsg(response.message.toString());
        this._unitShiftTypeListing();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  _unitShiftTypeListing = () => {

    Api._getShiftTypeListApi()
      .then((response) => {
        console.log(response.data)

        let newArray = response.data.map((item) => {
          return (
            {
              "key": item.id,
              "label": item.name
            }
          )
        })
        this.setState({ shiftTypeData: newArray, isLoading: false })

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  checkValidation = () => {
    let titleError = { field: '', message: '' }
    let shiftError = { field: '', message: '' }
    let genderError = { field: '', message: '' }
    let descriptionError = { field: '', message: '' }
    let startDateError = { field: '', message: '' }
    let endDateError = { field: '', message: '' }
    let startTimeError = { field: '', message: '' }
    let endTimeError = { field: '', message: '' }
    let breakStartError = { field: '', message: '' }
    let breakEndError = { field: '', message: '' }
    let daysError = { field: '', message: '' }


    if (this.state.shiftTitle == '') {
      titleError.field = "shift_title";
      titleError.message = "Shift title is required!";
      this.setState({ setErrors: titleError })
    } else if (this.state.shiftDesc == '') {
      descriptionError.field = "description";
      descriptionError.message = "Description is required!";
      this.setState({ setErrors: descriptionError })
    } else if (this.state.shiftType == '') {
      shiftError.field = "shift_type";
      shiftError.message = "Shift type is required!";
      this.setState({ setErrors: shiftError })
    } else if (this.state.gender == '') {
      genderError.field = "gender";
      genderError.message = "Gender is required!";
      this.setState({ setErrors: genderError })
    } else if (this.state.start_date == '') {
      startDateError.field = "start_date";
      startDateError.message = "Start date is required!";
      this.setState({ setErrors: startDateError })
    } else if (this.state.end_date == '') {
      endDateError.field = "end_date";
      endDateError.message = "Finish Date is required!";
      this.setState({ setErrors: endDateError })
    } else if (this.state.startTime == '') {
      startTimeError.field = "start_time";
      startTimeError.message = "Start time is required!";
      this.setState({ setErrors: startTimeError })
    } else if (this.state.endTime == '') {
      endTimeError.field = "end_time";
      endTimeError.message = "End time is required!";
      this.setState({ setErrors: endTimeError })
    } else if (this.state.break_start == '') {
      breakStartError.field = "break_start";
      breakStartError.message = "Start break time is required!";
      this.setState({ setErrors: breakStartError })
    } else if (this.state.break_end == '') {
      breakEndError.field = "break_end";
      breakEndError.message = "End break time is required!";
      this.setState({ setErrors: breakEndError })
    } else {
      var filterData = [];
      for (var i = 0; i < this.state.weekdays.length; i++) {
        if (this.state.weekdays[i].isSelected) {
          filterData.push(this.state.weekdays[i].label)
        }
      }
      if (filterData.length <= 0) {
        daysError.field = "days";
        daysError.message = "Days is required!";
        this.setState({ setErrors: daysError })
      } else {
        this.setState({ setErrors: { field: '', message: '' } })
        this._unitAddShift();
      }
    }
  }

  // Call AddJob Api after click submit button...
  _unitAddShift = () => {
    this.setState({ isLoading: true })

    var filterData = [];
    for (var i = 0; i < this.state.weekdays.length; i++) {
      if (this.state.weekdays[i].isSelected) {
        filterData.push(this.state.weekdays[i].label.toString())
      }
    }

    console.log(Object.values(filterData).toString());

    var formData = new FormData();
    formData.append("user_id", this.props.userid);
    formData.append("shift_title", this.state.shiftTitle);
    formData.append("shift_type", this.state.shiftType);
    formData.append("shift_description", this.state.shiftDesc);
    formData.append("gender", this.state.gender);
    formData.append("start_date", this.state.start_date);
    formData.append("end_date", this.state.end_date);
    formData.append("start_time", this.state.startTime);
    formData.append("end_time", this.state.endTime);
    formData.append("start_break_time", this.state.break_start);
    formData.append("end_break_time", this.state.break_end);
    formData.append("status", '1');
    formData.append("days", Object.values(filterData).toString());
    formData.append("shift_user_type", "unit");

    Api._unitAddShift(formData)
      .then((response) => {

        console.log(response)
        this.setState({ isLoading: false })
        setToastMsg(response.message.toString());
        this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        this.setState({ isLoading: false })
        setToastMsg("Somthing went wrong.");
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>ADD NEW SHIFT</Text>

              </View>

              <Loader isLoading={this.state.isLoading} />

              <ScrollView>
                <View style={{ marginTop: 10, marginLeft: 30, marginRight: 30 }}>

                  <TouchableOpacity>
                    <CustomTextInput
                      placeholder='SHIFT TITLE'
                      floatingText="SHIFT TITLE"
                      editable={true}
                      onFocus={true}
                      caretHidden={false}
                      value={this.state.shiftTitle}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({ shiftTitle: text });
                      }}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "shift_title" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='SHIFT DESCRIPTION'
                      keyboardType='default'
                      floatingText="SHIFT DESCRIPTION"
                      style={{ textAlignVertical: "top", height: 120, }}
                      multiline={true}
                      returnKeyType={'done'}
                      editable={true}
                      onFocus={true}
                      value={this.state.shiftDesc}
                      onChangeText={(text) => {
                        this.setState({ shiftDesc: text });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "description" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <ModalSelector
                    data={this.state.shiftTypeData}
                    initValue="SHIFT TYPE"
                    accessible={true}
                    animationType="fade"
                    search={false}
                    scrollViewAccessibilityLabel={'Scrollable options'}
                    cancelButtonAccessibilityLabel={'Cancel Button'}
                    onChange={(option) => { this.setState({ shiftType: option.label }) }}>
                    <CustomTextInput
                      placeholder='SHIFT TYPE'
                      floatingText="SHIFT TYPE"
                      editable={false}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleDropDown={true}
                      returnKeyType={'done'}
                      value={this.state.shiftType}
                    />
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "shift_type" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <ModalSelector
                    data={this.state.genderData}
                    initValue="GENDER"
                    accessible={true}
                    animationType="fade"
                    search={false}
                    scrollViewAccessibilityLabel={'Scrollable options'}
                    cancelButtonAccessibilityLabel={'Cancel Button'}
                    onChange={(option) => { this.setState({ gender: option.label }) }}>
                    <CustomTextInput
                      placeholder='GENDER'
                      floatingText="GENDER"
                      editable={false}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={false}
                      isVisibleDropDown={true}
                      returnKeyType={'done'}
                      value={this.state.gender}
                    />
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "gender" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <TouchableOpacity style={{ marginTop: 5 }} >
                    <CustomTextInput
                      placeholder='START DATE'
                      floatingText="START DATE"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleCalendar={true}
                      value={this.state.start_date}
                      returnKeyType={'done'}
                      onTouchEnd={() => { this.setState({ isStartDatePickerVisible: true }) }}
                    />
                    <DateTimePickerModal
                      isVisible={this.state.isStartDatePickerVisible}
                      mode="date"
                      date={new Date()}
                      onConfirm={(date) => this.handleStartDateConfirm(date)}
                      onCancel={() => this.setState({ isStartDatePickerVisible: false })}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "start_date" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <TouchableOpacity style={{ marginTop: 5 }} >
                    <CustomTextInput
                      placeholder='FINISH DATE'
                      floatingText="FINISH DATE"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleCalendar={true}
                      value={this.state.end_date}
                      returnKeyType={'done'}
                      onTouchEnd={() => this.setState({ isEndDatePickerVisible: true })}
                    />
                    <DateTimePickerModal
                      isVisible={this.state.isEndDatePickerVisible}
                      mode="date"
                      date={new Date()}
                      onConfirm={(date) => this.handleEndDateConfirm(date)}
                      onCancel={() => this.setState({ isEndDatePickerVisible: false, })}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "end_date" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginTop: 5, flex: 1, marginRight: 3 }}>
                      <CustomTextInput
                        placeholder='START TIME'
                        floatingText="START TIME"
                        editable={true}
                        onFocus={true}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        isVisibleCalendar={true}
                        value={this.state.startTime}
                        returnKeyType={'done'}
                        onTouchEnd={() => {
                          this.setState({ isStartTimePickerVisible: true })
                        }}
                      />
                      <DateTimePickerModal
                        isVisible={this.state.isStartTimePickerVisible}
                        mode="time"
                        locale="en_GB"
                        date={new Date()}
                        onConfirm={(date) => this.handleStartTimeConfirm(date)}
                        onCancel={() => this.setState({ isStartTimePickerVisible: false })}
                      />
                    </View>

                    <View style={{ marginTop: 5, flex: 1, marginLeft: 3 }}>
                      <CustomTextInput
                        placeholder='END TIME'
                        floatingText="END TIME"
                        editable={true}
                        onFocus={true}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        isVisibleCalendar={true}
                        value={this.state.endTime}
                        returnKeyType={'done'}
                        onTouchEnd={() => {
                          this.setState({ isEndTimePickerVisible: true })
                        }}
                      />
                      <DateTimePickerModal
                        isVisible={this.state.isEndTimePickerVisible}
                        mode="time"
                        locale="en_GB"
                        date={new Date()}
                        onConfirm={(date) => this.handleEndTimeConfirm(date)}
                        onCancel={() => this.setState({ isEndTimePickerVisible: false })}
                      />
                    </View>
                  </View>
                  {
                    this.state.setErrors.field == "start_time" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }
                  {
                    this.state.setErrors.field == "end_time" && (
                      <Text style={[styles.errors, { textAlign: 'right', marginEnd: 10 }]}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginTop: 5, flex: 1, marginRight: 3 }}>
                      <CustomTextInput
                        placeholder='BREAK START TIME'
                        floatingText="BREAK START TIME"
                        keyboardType='numeric'
                        editable={true}
                        onFocus={true}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        isVisibleCalendar={true}
                        value={this.state.break_start}
                        returnKeyType={'done'}
                        onTouchEnd={() => {
                          this.setState({ isBreakStartTimePickerVisible: true })
                        }}
                      />
                      <DateTimePickerModal
                        isVisible={this.state.isBreakStartTimePickerVisible}
                        mode="time"
                        locale="en_GB"
                        date={new Date()}
                        onConfirm={(date) => this.handleBreakStartTimeConfirm(date)}
                        onCancel={() => this.setState({ isBreakStartTimePickerVisible: false })}
                      />
                    </View>

                    <View style={{ marginTop: 5, flex: 1, marginLeft: 3 }}>
                      <CustomTextInput
                        placeholder='BREAK END TIME'
                        floatingText="BREAK END TIME"
                        keyboardType='numeric'
                        returnKeyType={'done'}
                        editable={true}
                        onFocus={true}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        isVisibleCalendar={true}
                        value={this.state.break_end}
                        onTouchEnd={() => {
                          this.setState({ isBreakEndTimePickerVisible: true })
                        }}
                      />
                      <DateTimePickerModal
                        isVisible={this.state.isBreakEndTimePickerVisible}
                        mode="time"
                        locale="en_GB"
                        date={new Date()}
                        onConfirm={(date) => this.handleBreakEndTimeConfirm(date)}
                        onCancel={() => this.setState({ isBreakEndTimePickerVisible: false })}
                      />
                    </View>
                  </View>
                  {
                    this.state.setErrors.field == "break_start" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }
                  {
                    this.state.setErrors.field == "break_end" && (
                      <Text style={[styles.errors, { textAlign: 'right', marginEnd: 10 }]}>{this.state.setErrors.message}</Text>
                    )
                  }


                  <View style={{ marginTop: 10, marginBottom: 10, }}>
                    <View
                      style={{ flexDirection: 'row', flexWrap: 'wrap', width: '100%', borderRadius: 15, borderColor: '#00D5E1', borderWidth: 2, marginTop: 12, padding: 5 }}>
                      {
                        this.state.weekdays.map((item, index) => {
                          return (
                            <TouchableOpacity
                              key={`${item.id}_${index}`}
                              // activeOpacity={0.3}
                              style={{ margin: 5 }}
                              onPress={() => this._setDay(item)}
                            >
                              <View style={{ flexDirection: 'row', backgroundColor: '#FFFFFF', borderRadius: 5 }}>
                                <CheckBox
                                  value={item.isSelected}
                                  onValueChange={() => this._setDay(item)}
                                  style={{ alignSelf: "center" }}

                                  tintColors={{ true: '#00D5E1', false: '#666666' }}
                                />
                                <Text style={{ alignSelf: 'center', fontSize: 14, paddingTop: 5, paddingBottom: 5, paddingRight: 15, fontFamily: 'montserrat_regular', color: "#000000" }}>{item.label}</Text>

                              </View>
                            </TouchableOpacity>
                          )
                        })
                      }
                    </View>

                    <Text style={{ position: 'absolute', padding: 5, marginLeft: 20, color: '#393FA0', backgroundColor: '#FFFFFF', fontFamily: 'montserrat_regular', fontSize: 10 }}>SELECT DAYS</Text>
                  </View>
                  {
                    this.state.setErrors.field == "days" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                </View>
              </ScrollView>
            </View>
            <View >

              <View style={styles.shadow}></View>
              {/*  <Image style={{ width:'100%',height:40,}} source={require('./../../assets/border.png')} /> */}
              <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#00D5E1", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                onPress={() => this.checkValidation()}
              >
                <Text style={styles.loginText}>SUBMIT</Text>
              </TouchableOpacity>

            </View>


          </View>
        </SafeAreaView>
      </Fragment>
    );
  }
  _setDay = (day) => {
    console.log("day =========", day);
    var days = [];
    for (var i = 0; i < this.state.weekdays.length; i++) {
      if (day.label == this.state.weekdays[i].label) {
        this.state.weekdays[i].isSelected = !this.state.weekdays[i].isSelected
      }
      days.push(this.state.weekdays[i])
    }
    this.setState({
      weekdays: days,
    });
  }

  // this is for start date picker...
  handleStartDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    this.setState({
      start_date: dateTimeString,
      end_date: "",
      compareDateValues: date,
      isStartDatePickerVisible: false,
    })

    console.log(dateTimeString)
  };

  // this is for end date picker...
  handleEndDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    var start = new Date(this.state.compareDateValues);
    var end = new Date(date);

    if (this.state.compareDateValues != '') {
      if (start.getTime() > end.getTime()) {
        this.setState({
          isEndDatePickerVisible: false,
          end_date: "",
        })
        setToastMsg("Date must be greater than start date.");
        console.log("end date is smaller")
      } else {
        this.setState({
          end_date: dateTimeString,
          isEndDatePickerVisible: false,
        })
        console.log("end date is greater")
      }
    } else {
      this.setState({ isEndDatePickerVisible: false })
      setToastMsg("Please select start date first.");
      console.log("start date not selected yet.")
    }
    console.log(dateTimeString)
  };


  // this is for start time picker...
  handleStartTimeConfirm = (time) => {

    let dateTimeString = moment(time).local().format('HH:mm');

    this.setState({
      startTime: dateTimeString,
      endTime: '',
      compareTimeValues: time,
      isStartTimePickerVisible: false,
    })

    console.log(dateTimeString)
  };

  // this is for end time picker...
  handleEndTimeConfirm = (time) => {
    let dateTimeString = moment(time).local().format('HH:mm');

    var start = new Date(this.state.compareTimeValues);
    var end = new Date(time);

    if (this.state.compareTimeValues != '') {
      // if (start.getTime() > end.getTime()) {
      //   this.setState({
      //     isEndTimePickerVisible: false,
      //     endTime: '',
      //   })
      //   setToastMsg("End Time must be greater than Start time.");
      //   console.log("end time is smaller")
      // } else {
      //   this.setState({
      //     endTime: dateTimeString,
      //     isEndTimePickerVisible: false,
      //   })
      //   console.log("end time is greater")
      // }

      this.setState({
        endTime: dateTimeString,
        isEndTimePickerVisible: false,
      })
      console.log("end time is greater")

    } else {
      this.setState({
        isEndTimePickerVisible: false,
      })
      setToastMsg("Please select Start time first.");
      console.log("start time not selected yet.")
    }
    console.log(dateTimeString)
  };

  // this is for break start time picker...
  handleBreakStartTimeConfirm = (time) => {

    let dateTimeString = moment(time).local().format('HH:mm');

    this.setState({
      break_start: dateTimeString,
      break_end: '',
      compareBreakTimeValues: time,
      isBreakStartTimePickerVisible: false,
    })

    console.log(dateTimeString)
  };

  // this is for break end time picker...
  handleBreakEndTimeConfirm = (time) => {
    let dateTimeString = moment(time).local().format('HH:mm');

    var start = new Date(this.state.compareBreakTimeValues);
    var end = new Date(time);

    if (this.state.compareBreakTimeValues != '') {
      // if (start.getTime() > end.getTime()) {
      //   this.setState({
      //     isBreakEndTimePickerVisible: false,
      //     break_end: '',
      //   })
      //   setToastMsg("End Time must be greater than Start time.");
      //   console.log("end time is smaller")
      // } else {
      //   this.setState({
      //     break_end: dateTimeString,
      //     isBreakEndTimePickerVisible: false,
      //   })
      //   console.log("end time is greater")
      // }

      this.setState({
        break_end: dateTimeString,
        isBreakEndTimePickerVisible: false,
      })
      console.log("end break time is greater")

    } else {
      this.setState({
        isBreakEndTimePickerVisible: false,
      })
      setToastMsg("Please select Start break time first.");
      console.log("start break time not selected yet.")
    }
    console.log(dateTimeString)
  };

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center',

  },

  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5


  },
  floatingLabelText: {
    color: "#393FA0",
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    paddingRight: 5,

    fontSize: 14,
    fontFamily: "montserrat_regular"
  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  profileImage: {
    height: 150,
    width: 150,
    position: 'absolute',
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: 60

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }
  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }

});

export default connect(mapStateToPrpos)(UnitAddNewShift);