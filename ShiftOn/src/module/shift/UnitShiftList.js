/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, RefreshControl,
  Alert, SafeAreaView, Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import Swipeout from 'react-native-swipeout';
import Loader from '../../utils/Loader';
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import { setToastMsg } from '../../utils/ToastMessage';
import { connect } from 'react-redux';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
  }
}

class UnitShiftList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      listData: [],
    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    this.navFocusListener = this.props.navigation.addListener('didFocus', () => {
      this.getShiftListings();
    });
  }

  componentWillUnmount() {
    this.navFocusListener.remove();
  }

  getShiftListings = () => {
    this.setState({ isLoading: true })
    let rowData = {
      "shift_user_type": "unit",
      "user_id": this.props.userid,
    }

    Api._unitShiftListing(rowData)
      .then((response) => {
        console.log(response)

        this.setState({ isLoading: false, listData: response.data })
        if (response.status.toString() !== '1') {
          setToastMsg(response.message.toString());
        }

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });
  }


  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

          <View style={styles.mainContainer}>
            <View style={{ backgroundColor: '#00D5E1' }}>
              <TouchableOpacity
                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
              </TouchableOpacity>
              <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>SHIFTS</Text>
              <TouchableOpacity
                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20, right: 0 }}
                onPress={() => this.props.navigation.navigate('UnitAddNewShift')}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/more_icn.png')} />
              </TouchableOpacity>
            </View>

            <Loader isLoading={this.state.isLoading} />

            <View style={{ margin: 20 }}>

              {
                (this.state.listData.length > 0) ?

                  (<FlatList
                    style={{ width: '100%', marginBottom: '12%' }}
                    data={this.state.listData}
                    keyExtractor={(item, index) => index.toString()}
                    //ItemSeparatorComponent={this.ItemSeparator}
                    renderItem={({ item, index }) =>
                      <View style={{ borderRadius: 2, margin: 2 }}>
                        <TouchableOpacity activeOpacity={0.9}
                          onPress={() => { this.props.navigation.navigate('ViewShiftDetail', { details: item }) }}
                        >
                          <View style={styles.rowContainer}>

                            <View style={{ width: '18%', justifyContent: 'center' }}>
                              <Image style={{ height: 40, width: 40, tintColor: '#000000' }} source={require('./../../assets/shift_icon.png')} />
                            </View>
                            <View style={{ width: '56%' }}>
                              <Text
                                numberOfLines={1}
                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 15, }}
                              >{item.shift_title}</Text>

                              <Text
                                numberOfLines={1}
                                style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                              >{item.shift_type}</Text>
                              <Text
                                numberOfLines={1}
                                style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                              >{item.gender}</Text>

                            </View>
                            <View style={{ width: '26%', position: 'absolute', right: 0, alignSelf: 'center', justifyContent: 'flex-end' }}>

                              <View style={{ flexDirection: 'row' }}>

                                {
                                  item.status == "1" ?

                                    <TouchableOpacity
                                      onPress={() => { this.props.navigation.navigate('UnitUpdateNewShift', { details: item }) }}
                                    >
                                      <Image style={{ height: 15, width: 15, padding: 11, tintColor: '#000000' }} source={require('./../../assets/pencil.png')} />
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity activeOpacity={1}>
                                      <Image style={{ height: 15, width: 15, padding: 11, tintColor: '#888888' }} source={require('./../../assets/pencil.png')} />
                                    </TouchableOpacity>
                                }
                                {
                                  item.status == "1" ?
                                    <TouchableOpacity style={{ marginLeft: 15 }}
                                      onPress={() => { this._active(item.id, "0") }}
                                    >
                                      <Image style={{ height: 18, width: 40, padding: 10 }} source={require('./../../assets/toggle_icon_on.png')} />
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity style={{ marginLeft: 10 }}
                                      onPress={() => { this._active(item.id, "1") }}
                                    >
                                      <Image style={{ height: 18, width: 40, padding: 10 }} source={require('./../../assets/toggle_icon_off.png')} />
                                    </TouchableOpacity>
                                }

                              </View>

                            </View>
                          </View>
                        </TouchableOpacity>
                      </View>
                    }
                  />)
                  :
                  (<View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                    <Text
                      numberOfLines={1}
                      style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                    >No Data Available!</Text>
                  </View>)

              }
            </View>

          </View>

        </SafeAreaView >
      </Fragment >
    );
  }


  _active(id, status) {

    Alert.alert(
      '',
      'Are you sure?',
      [
        { text: 'NO', onPress: () => console.log('Close pressed') },
        {
          text: 'YES', onPress: () => {
            this.setState({ isLoading: true })
            let rawData = {
              "id": id,
              "status": status,
            }
            Api._unitUpdateShiftStatus(rawData)
              .then((response) => {
                console.log(response)

                setToastMsg(response.message.toString());
                this.getShiftListings()

              })
              .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false })
              });
          }
        },
      ]
    )

  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },

  tabHolder: {
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 30,
    flex: 1
  },
  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderTopLeftRadius: 2,
    borderBottomLeftRadius: 2,
    borderTopRightRadius: 2,
    borderBottomRightRadius: 2,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    padding: 10,

  },


});

export default connect(mapStateToPrpos)(UnitShiftList);