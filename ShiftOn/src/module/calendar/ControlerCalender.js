/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment, useState } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView,
  Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import RadioGroup from 'react-native-radio-buttons-group';
import { Calendar } from 'react-native-calendars';
import moment from 'moment';
import { setToastMsg } from '../../utils/ToastMessage';
import Api from '../../apis/Api';
import Loader from '../../utils/Loader';
import { connect } from 'react-redux';
import { getRandomColor, getDatesInRange } from '../../utils/DateRangList';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
  }
}

var colorList = [
  "#115c25",
  "#0b6e5a",
  "#0d1f78",
  "#36077d",
  "#700e75",
  "#8c0f31",
  "#a80c0c",
  "#826a0a"
];

const { width } = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});
const resetToHome = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'WorkerHome' })],
});

var date
class ControlerCalender extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      start_date: '',
      finish_date: '',
      startTime: '',
      endTime: '',
      comment: '',
      acvity: '',
      zip: '',
      unit: '',
      ward: '',
      selectedDate: '',
      currentDate: '',
      displayedMonth: '',

      dateDataList: [],
      isLoading: false,

      tempData: {},
      calenderChangedOrNot: false,

    };
  }


  componentDidMount() {
    date = moment()
      //.utcOffset('+05:30')
      .format('yyyy-MM-DD');
    var monthData = date.split("-");
    this.setState({ currentDate: date, displayedMonth: monthData[1].toString() })
    console.log("date", date);

    this.loadingDateData()

  }

  setMonthFunc = (month) => {
    this.setState({ displayedMonth: month, dateDataList: [], calenderChangedOrNot: true })
    this.loadingDateData()
  }

  loadingDateData = () => {
    this.setState({ isLoading: true })

    let rawData = {
      "user_id": this.props.userid
    };
    Api._userCalenderListApi(rawData)
      .then((response) => {
        console.log(response.data.WorkerRefrences)

        this.dataListing(response.data.WorkerRefrences);

        this.setState({ isLoading: false, })
        // setToastMsg(response.message.toString());

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false, })
      });
  }

  dataListing = (responseData) => {
    var tempData = [];
    for (var i = 0; i < responseData.length; i++) {
      const myArray = responseData[i].start_date.split("/");
      if (myArray[1].toString() == this.state.displayedMonth) {
        tempData.push(responseData[i])
      }
    }

    var newArray = [];
    for (var i = 0; i < responseData.length; i++) {
      var newObj = {};
      const startDateSplit = responseData[i].start_date.split("/");
      const endDateSplit = responseData[i].finish_date.split("/");
      newObj.start_date = startDateSplit[2] + "-" + startDateSplit[1] + "-" + startDateSplit[0];
      newObj.finish_date = endDateSplit[2] + "-" + endDateSplit[1] + "-" + endDateSplit[0];
      newArray.push(newObj)
    }

    this.newFunction(newArray);
    this.setState({ dateDataList: tempData });
  }

  newFunction = (listings) => {
    let aa = [];
    listings.map((item, index) => {
      aa.push(getDatesInRange(new Date(item.start_date), new Date(item.finish_date)));
    });

    var tempDates = [];
    for (var i = 0; i < aa.length; i++) {
      var color = getRandomColor(colorList);

      for (var j = 0; j < aa[i].length; j++) {
        if (j == 0) {
          tempDates.push({ 'date': aa[i][j], 'isStartDate': "1", 'color': color })
        } else if (j == aa[i].length - 1) {
          tempDates.push({ 'date': aa[i][j], 'isStartDate': "3", 'color': color })
        } else {
          tempDates.push({ 'date': aa[i][j], 'isStartDate': "2", 'color': color })
        }
      }
    }

    this.anotherFunc(tempDates)

  }

  anotherFunc = (list) => {
    var obj = list.reduce((c, v) => Object.assign(c, {
      [v.date]: this.thirdFunction(v)
    }), {});
    this.setState({ tempData: obj, })

    console.log(obj)
  }

  thirdFunction = (item) => {

    if (item.isStartDate == "1") {
      return { selected: true, color: item.color, startingDay: true, endingDay: false }
    } else if (item.isStartDate == "3") {
      return { selected: true, color: item.color, startingDay: false, endingDay: true }
    } else {
      return { selected: true, color: item.color }
    }

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>MY CALENDER</Text>

              </View>
              <Loader isLoading={this.state.isLoading} />
              <ScrollView>
                <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20 }}>

                  <Calendar
                    // Specify style for calendar container element. Default = {}
                    style={{
                      borderWidth: 1,
                      borderColor: 'gray',
                      //height: 360
                    }}
                    // Specify theme properties to override specific styles for calendar parts. Default = {}
                    theme={{
                      backgroundColor: '#ffffff',
                      calendarBackground: '#ffffff',
                      textSectionTitleColor: '#b6c1cd',
                      textSectionTitleDisabledColor: '#d9e1e8',
                      selectedDayBackgroundColor: '#00adf5',
                      //textSectionTitleColor: '#FFF025',
                      selectedDayTextColor: '#ffffff',
                      todayTextColor: '#00adf5',
                      dayTextColor: '#2d4150',
                      textDisabledColor: '#d9e1e8',
                      dotColor: '#00adf5',
                      selectedDotColor: '#ffffff',
                      arrowColor: 'orange',
                      disabledArrowColor: '#d9e1e8',
                      monthTextColor: '#2d4150',
                      indicatorColor: 'blue',
                      textDayFontFamily: 'montserrat_semibold',
                      textMonthFontFamily: 'montserrat_regular',
                      textDayHeaderFontFamily: 'montserrat_regular',
                      textDayFontWeight: '300',
                      textMonthFontWeight: 'bold',
                      textDayHeaderFontWeight: '300',
                      textDayFontSize: 16,
                      textMonthFontSize: 16,
                      textDayHeaderFontSize: 12,

                    }}


                    // Initially visible month. Default = Date()
                    //current={'2012-03-01'}
                    // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                    //minDate={'2012-05-10'}
                    // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                    //maxDate={'2012-05-30'}
                    // Handler which gets executed on day press. Default = undefined
                    onDayPress={(day) => this.setState({ selectedDate: day.dateString })}
                    // Handler which gets executed on day long press. Default = undefined
                    onDayLongPress={(day) => { console.log('selected day', day.dateString) }}
                    // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                    monthFormat={' MMMM yyyy'}
                    // Handler which gets executed when visible month changes in calendar. Default = undefined
                    onMonthChange={(month) => this.setMonthFunc(month.month)}
                    // Hide month navigation arrows. Default = false
                    //hideArrows={true}
                    // Replace default arrows with custom ones (direction can be 'left' or 'right')
                    //renderArrow={(direction) => (<Arrow />)}
                    // Do not show days of other months in month page. Default = false
                    hideExtraDays={true}
                    // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                    // day from another month that is visible in calendar page. Default = false
                    //disableMonthChange={true}
                    // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                    firstDay={1}
                    // Hide day names. Default = false
                    //hideDayNames={true}
                    // Show week numbers to the left. Default = false
                    //showWeekNumbers={true}
                    // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                    onPressArrowLeft={subtractMonth => subtractMonth()}
                    // Handler which gets executed when press arrow icon right. It receive a callback can go next month
                    onPressArrowRight={addMonth => addMonth()}
                    // Disable left arrow. Default = false
                    //disableArrowLeft={true}
                    // Disable right arrow. Default = false
                    //disableArrowRight={true}
                    // Disable all touch events for disabled days. can be override with disableTouchEvent in markedDates
                    disableAllTouchEventsForDisabledDays={true}
                    // Replace default month and year title with custom one. the function receive a date as parameter.
                    //renderHeader={(date) => { console.log("header",date)} }
                    // Enable the option to swipe between months. Default = false
                    //enableSwipeMonths={true}

                    markingType={'period'}
                    markedDates={this.state.tempData}

                  />

                  {
                    this.state.dateDataList.length > 0 ?

                      (<FlatList
                        style={{ width: '100%', marginBottom: 160 }}
                        data={this.state.dateDataList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) =>

                          <View style={{ backgroundColor: '#00D5E1', marginTop: 10 }}>
                            <View style={{ marginLeft: 5, flexDirection: 'row', padding: 10, backgroundColor: '#ECECEC' }}>

                              <View style={{ flex: 1 }}>
                                <Text style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12 }}>{item.start_date} to {item.finish_date}</Text>
                                <Text style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12 }}>{item.shift} SHIFT</Text>
                              </View>

                            </View>
                            <View style={{ padding: 5 }}>
                              <Text style={{ color: '#FFFFFF', fontFamily: 'montserrat_semibold', fontSize: 12 }}>{item.comment}</Text>
                              <Text style={{ color: '#FFFFFF', fontFamily: 'montserrat_semibold', fontSize: 10 }}>{item.start_time} to {item.end_time}</Text>

                            </View>

                          </View>
                        }
                      />)
                      :
                      (<View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                        {/* <Text
                          numberOfLines={1}
                          style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                        >No Dates Available!</Text> */}
                      </View>)
                  }

                </View>
              </ScrollView>
            </View>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }



  _setDate = (type) => {
    if (type == 'start_date') {
      this.setState({ start_date: "15/09/2020" });
    } else if (type == 'finish_date') {
      this.setState({ finish_date: "20/09/2020" });
    } else if (type == 'start') {
      this.setState({ startTime: "10:00 AM" });
    } else if (type == 'end') {
      this.setState({ endTime: "04:00 PM" });
    } else {
      this.setState({ acvity: "Lorem ipsum" });
    }


  }
  _onPressRadio = () => {


  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center',

  },

  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5


  },
  floatingLabelText: {
    color: "#393FA0",
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    paddingRight: 5,

    fontSize: 14,
    fontFamily: "montserrat_regular"
  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 18,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  profileImage: {
    height: 150,
    width: 150,
    position: 'absolute',
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: 60

  },

});

export default connect(mapStateToPrpos)(ControlerCalender);