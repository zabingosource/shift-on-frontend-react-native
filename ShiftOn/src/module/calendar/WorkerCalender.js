/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment, useState } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView,
  Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import RadioGroup from 'react-native-radio-buttons-group';
import { Calendar } from 'react-native-calendars';
import moment, { now } from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { setToastMsg } from '../../utils/ToastMessage';
import Api from '../../apis/Api';
import { connect } from 'react-redux';
import Loader from '../../utils/Loader';
import { getDatesInRange, getRandomColor } from '../../utils/DateRangList';
import { notificationHandler } from '../../utils/LocalNotificationController';
import { scheduleNotificationHandler, getAllScheduledNotifications, cancelScheduledNotifications } from '../../utils/ScheduleNotificationController';
import PushNotification from 'react-native-push-notification';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
  }
}

var colorList = [
  "#115c25",
  "#0b6e5a",
  "#0d1f78",
  "#36077d",
  "#700e75",
  "#8c0f31",
  "#a80c0c",
  "#826a0a"
];


const { width } = Dimensions.get('window');


var date
class WorkerCalender extends Component {
  constructor(props) {
    super(props);
    this.state = {
      start_date: '',
      finish_date: '',
      startTime: '',
      endTime: '',
      comment: '',
      acvity: '',
      zip: '',
      unit: '',
      ward: '',
      selectedDate: '',
      currentDate: '',
      displayedMonth: '',

      dateDataList: [],
      notificationDataList: [],
      isLoading: true,

      tempData: {},
      calenderChangedOrNot: false,

      scheduledNotify: false,

    };
  }


  componentDidMount() {

    this.redirectPage();
    date = moment().format('yyyy-MM-DD');
    var dateMonthData = date.split("-");
    this.setState({ currentDate: dateMonthData[2].toString(), displayedMonth: dateMonthData[1].toString() })
    console.log("date", date);

    PushNotification.getScheduledLocalNotifications((data) => {
      this.setState({ notificationDataList: data });
      console.log(data);
    });

    this.navFocusListener = this.props.navigation.addListener('didFocus', () => {
      this.loadingDateData();
    });

    this._checkNotification = setInterval(() => {
      //console.log("call");
      this.notificationStatusChangeFunc("", "");
    }, 2500);

  }

  componentWillUnmount() {
    this.navFocusListener.remove();
    clearInterval(this._checkNotification);
  }

  redirectPage = async () => {
    try {
      await AsyncStorage.setItem('nextScreen', '');
    } catch (e) {
      console.log(e);
    }
  }

  setMonthFunc = (month) => {
    this.setState({ displayedMonth: month, dateDataList: [], calenderChangedOrNot: true })
    this.loadingDateData();
  }

  loadingDateData = () => {
    this.setState({ isLoading: true })

    let rawData = {
      "user_id": this.props.userid,
    }
    Api._userCalenderListApi(rawData)
      .then((response) => {
        console.log(response.data.WorkerRefrences)

        this.dataListing(response.data.WorkerRefrences);
        //setToastMsg(response.message.toString());

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false, })
      });
  }

  dataListing = (responseData) => {
    var tempData = [];
    for (var i = 0; i < responseData.length; i++) {
      const myArray = responseData[i].start_date.split("/");
      var newObjectData = {};

      if (myArray[1].toString() == this.state.displayedMonth) {
        var isYes = false;

        for (var j = 0; j < this.state.notificationDataList.length; j++) {
          if (responseData[i].id == this.state.notificationDataList[j].id) {
            isYes = true;
          }
        }
        newObjectData.id = responseData[i].id;
        newObjectData.comment = responseData[i].comment;
        newObjectData.end_time = responseData[i].end_time;
        newObjectData.finish_date = responseData[i].finish_date;
        newObjectData.location = responseData[i].location;
        newObjectData.shift = responseData[i].shift;
        newObjectData.start_date = responseData[i].start_date;
        newObjectData.start_time = responseData[i].start_time;
        newObjectData.ward = responseData[i].ward;
        newObjectData.zip = responseData[i].zip;
        if (myArray[0].toString() >= this.state.currentDate) {
          newObjectData.is_notification_image = '1'
        } else {
          newObjectData.is_notification_image = '0'
        }

        if (isYes) {
          newObjectData.is_notification = '1'
        } else {
          newObjectData.is_notification = '0'
        }

        tempData.push(newObjectData)

      }
    }


    var newArray = [];
    for (var i = 0; i < responseData.length; i++) {
      var newObj = {};
      const startDateSplit = responseData[i].start_date.split("/");
      const endDateSplit = responseData[i].finish_date.split("/");
      newObj.start_date = startDateSplit[2] + "-" + startDateSplit[1] + "-" + startDateSplit[0];
      newObj.finish_date = endDateSplit[2] + "-" + endDateSplit[1] + "-" + endDateSplit[0];
      newArray.push(newObj)
    }

    this.newFunction(newArray);
    this.setState({ dateDataList: tempData })
    this.setState({ isLoading: false });
  }

  newFunction = (listings) => {
    let aa = [];
    listings.map((item, index) => {
      aa.push(getDatesInRange(new Date(item.start_date), new Date(item.finish_date)));
    });

    var tempDates = [];
    for (var i = 0; i < aa.length; i++) {
      var color = getRandomColor(colorList);

      for (var j = 0; j < aa[i].length; j++) {
        if (j == 0) {
          tempDates.push({ 'date': aa[i][j], 'isStartDate': "1", 'color': color })
        } else if (j == aa[i].length - 1) {
          tempDates.push({ 'date': aa[i][j], 'isStartDate': "3", 'color': color })
        } else {
          tempDates.push({ 'date': aa[i][j], 'isStartDate': "2", 'color': color })
        }
      }
    }

    this.anotherFunc(tempDates)

  }

  anotherFunc = (list) => {
    var obj = list.reduce((c, v) => Object.assign(c, {
      [v.date]: this.thirdFunction(v)
    }), {});
    this.setState({ tempData: obj, })

    console.log(obj)
  }

  thirdFunction = (item) => {

    if (item.isStartDate == "1") {
      return { selected: true, color: item.color, startingDay: true, endingDay: false }
    } else if (item.isStartDate == "3") {
      return { selected: true, color: item.color, startingDay: false, endingDay: true }
    } else {
      return { selected: true, color: item.color }
    }

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>MY CALENDER</Text>
                {/* <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20, right: 0 }}
                  onPress={() => this.props.navigation.navigate('WorkerAddNewCalender')}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/more_icn.png')} />
                </TouchableOpacity> */}
              </View>
              <Loader isLoading={this.state.isLoading} />
              <ScrollView>
                <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20 }}>

                  <Calendar
                    // Specify style for calendar container element. Default = {}
                    style={{
                      borderWidth: 1,
                      borderColor: 'gray',
                    }}
                    theme={{
                      backgroundColor: '#ffffff',
                      calendarBackground: '#ffffff',
                      textSectionTitleColor: '#b6c1cd',
                      textSectionTitleDisabledColor: '#d9e1e8',
                      selectedDayBackgroundColor: '#00adf5',
                      selectedDayTextColor: '#ffffff',
                      todayTextColor: '#00adf5',
                      dayTextColor: '#2d4150',
                      textDisabledColor: '#d9e1e8',
                      dotColor: '#00adf5',
                      selectedDotColor: '#ffffff',
                      arrowColor: 'orange',
                      disabledArrowColor: '#d9e1e8',
                      monthTextColor: '#2d4150',
                      indicatorColor: 'blue',
                      textDayFontFamily: 'montserrat_semibold',
                      textMonthFontFamily: 'montserrat_regular',
                      textDayHeaderFontFamily: 'montserrat_regular',
                      textDayFontWeight: '300',
                      textMonthFontWeight: 'bold',
                      textDayHeaderFontWeight: '300',
                      textDayFontSize: 16,
                      textMonthFontSize: 16,
                      textDayHeaderFontSize: 12,
                    }}

                    onDayPress={(day) => this.setState({ selectedDate: day.dateString })}
                    onDayLongPress={(day) => { console.log('selected day', day.dateString) }}
                    monthFormat={' MMMM yyyy'}
                    onMonthChange={(month) => this.setMonthFunc(month.month)}
                    hideExtraDays={true}
                    firstDay={1}
                    onPressArrowLeft={subtractMonth => subtractMonth()}
                    onPressArrowRight={addMonth => addMonth()}
                    disableAllTouchEventsForDisabledDays={true}
                    markingType={'period'}
                    markedDates={this.state.tempData}

                  />

                  {
                    this.state.dateDataList.length > 0 ?

                      (<FlatList
                        style={{ width: '100%', marginBottom: 160 }}
                        data={this.state.dateDataList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) =>

                          <View style={{ backgroundColor: '#00D5E1', marginTop: 10 }}>
                            <View style={{ marginLeft: 5, flexDirection: 'row', padding: 10, backgroundColor: '#ECECEC' }}>

                              <View style={{ flex: 1 }}>

                                <Text style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12 }}>{item.start_date} to {item.finish_date}</Text>
                                <Text style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12 }}>{item.shift} SHIFT</Text>

                              </View>

                              {
                                item.is_notification == "1" ?
                                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <TouchableOpacity style={{ padding: 10 }}
                                      onPress={() => this.notificationStatusChangeFunc(item, "close")}
                                    >
                                      <Image style={{ height: 20, width: 20 }} source={require('./../../assets/notification_on_bell_icn.png')} resizeMode='stretch' />
                                    </TouchableOpacity>

                                  </View>
                                  :
                                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <TouchableOpacity style={{ padding: 10 }}
                                      onPress={() => this.notificationStatusChangeFunc(item, "set")}
                                    >
                                      <Image style={{ height: 20, width: 20 }} source={require('./../../assets/notification_bell_icn.png')} resizeMode='stretch' />
                                    </TouchableOpacity>

                                  </View>
                              }


                            </View>
                            <View style={{ padding: 5 }}>

                              <Text style={{ color: '#FFFFFF', fontFamily: 'montserrat_semibold', fontSize: 12 }}>{item.comment}</Text>
                              <Text style={{ color: '#FFFFFF', fontFamily: 'montserrat_semibold', fontSize: 10 }}>{item.start_time} to {item.end_time}</Text>

                            </View>

                          </View>
                        }
                      />)
                      :
                      (<View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                        {/* <Text
                          numberOfLines={1}
                          style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                        >No Dates Available!</Text> */}
                      </View>)
                  }

                </View>
              </ScrollView>
            </View>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }


  pushNotificationFunc = (item) => {
    const dateSplit = item.start_date.split("/");
    const timeSplit = item.start_time.split(":");

    const finalDate = dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
    const finalHour = timeSplit[0];
    const finalMinute = timeSplit[1];

    let scheduleDate = new Date(finalDate);
    scheduleDate.setHours(finalHour);
    scheduleDate.setMinutes(finalMinute);

    scheduleNotificationHandler(item.id, item.shift + " SHIFT", item.comment, scheduleDate);
  }

  //.................................................
  notificationStatusChangeFunc = (item, text) => {
    //this.setState({ isLoading: true });
    var newNotificationDataList = [];

    if (text == "close") {
      cancelScheduledNotifications(item.id);
    } else if (text == "set") {
      this.pushNotificationFunc(item);
    }

    PushNotification.getScheduledLocalNotifications((data) => {
      newNotificationDataList = data;
    });

    setTimeout(() => {

      var tempData = [];
      for (var i = 0; i < this.state.dateDataList.length; i++) {
        var isYes = false;

        for (var j = 0; j < newNotificationDataList.length; j++) {
          if (this.state.dateDataList[i].id == newNotificationDataList[j].id) {
            isYes = true;
          }
        }

        if (isYes) {
          this.state.dateDataList[i].is_notification = '1';
        } else {
          this.state.dateDataList[i].is_notification = '0';
        }

        tempData.push(this.state.dateDataList[i]);
      }
      this.setState({ dateDataList: tempData, isLoading: false });

    }, 1500)




  }


}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center',
  },
  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5
  },
  floatingLabelText: {
    color: "#393FA0",
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    paddingRight: 5,
    fontSize: 14,
    fontFamily: "montserrat_regular"
  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 18,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'
  },
  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  profileImage: {
    height: 150,
    width: 150,
    position: 'absolute',
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: 60
  },

});

export default connect(mapStateToPrpos)(WorkerCalender);