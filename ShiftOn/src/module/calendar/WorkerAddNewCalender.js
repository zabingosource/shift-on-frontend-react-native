/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView,
  Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import RadioGroup from 'react-native-radio-buttons-group';
import Api from '../../apis/Api';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../components/CustomModelSelector';
import { connect } from 'react-redux';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
  }
}

const { width } = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});
const resetToHome = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'WorkerHome' })],
});

class WorkerAddNewCalender extends Component {
  constructor(props) {
    super(props);
    this.state = {
      start_date: '',
      finish_date: '',
      startTime: '',
      endTime: '',
      comment: '',
      acvity: '',
      zip: '',
      unit_location: '',
      ward: '',
      selectShift: 'MORNING',
      selectBtn: '1',

      isLoading: false,
      isStartDatePickerVisible: false,
      isEndDatePickerVisible: false,
      isStartTimePickerVisible: false,
      isEndTimePickerVisible: false,

      // for compare start and end date
      compareDateValues: '',
      compareTimeValues: '',
      setErrors: {
        field: '',
        message: ''
      }
    };
    console.log(this.props.navigation);
  }


  setData = (type) => {
    //this.setState({ selectShift: type })
    console.log(type)
  }

  componentDidMount() {

  }


  // this is for start date picker...
  handleStartDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    this.setState({
      start_date: dateTimeString,
      finish_date: "",
      compareDateValues: date,
      isStartDatePickerVisible: false,
    })

    console.log(dateTimeString)
  };

  // this is for end date picker...
  handleEndDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    var start = new Date(this.state.compareDateValues);
    var end = new Date(date);

    if (this.state.compareDateValues != '') {
      if (start.getTime() > end.getTime()) {
        this.setState({
          isEndDatePickerVisible: false,
          finish_date: "",
        })
        setToastMsg("Date must be greater than start date.");
        console.log("end date is smaller")
      } else {
        this.setState({
          finish_date: dateTimeString,
          isEndDatePickerVisible: false,
        })
        console.log("end date is greater")
      }
    } else {
      this.setState({
        isEndDatePickerVisible: false,
      })
      setToastMsg("Please select start date first.");
      console.log("start date not selected yet.")
    }
    console.log(dateTimeString)
  };

  // this is for start time picker...
  handleStartTimeConfirm = (time) => {

    let dateTimeString = moment(time).local().format('HH:mm');

    this.setState({
      startTime: dateTimeString,
      endTime: "",
      compareTimeValues: time,
      isStartTimePickerVisible: false,
    })

    console.log(dateTimeString)
  };

  // this is for end time picker...
  handleEndTimeConfirm = (time) => {
    let dateTimeString = moment(time).local().format('HH:mm');

    var start = new Date(this.state.compareTimeValues);
    var end = new Date(time);

    if (this.state.compareTimeValues != '') {
      if (start.getTime() > end.getTime()) {
        this.setState({
          isEndTimePickerVisible: false,
        })
        setToastMsg("Selected Time must be greater than Start time.");
        console.log("end time is smaller")
      } else {
        this.setState({
          endTime: dateTimeString,
          isEndTimePickerVisible: false,
        })
        console.log("end time is greater")
      }
    } else {
      this.setState({
        isEndTimePickerVisible: false,
      })
      setToastMsg("Please select Start time first.");
      console.log("start time not selected yet.")
    }
    console.log(dateTimeString)
  };

  checkValidation = () => {
    let startTimeError = { field: '', message: '' }
    let endTimeError = { field: '', message: '' }
    let startDateError = { field: '', message: '' }
    let endDateError = { field: '', message: '' }
    let commentError = { field: '', message: '' }
    let actvityError = { field: '', message: '' }
    let zipError = { field: '', message: '' }
    let locationError = { field: '', message: '' }
    let wardError = { field: '', message: '' }

    if (this.state.startTime == '') {
      startTimeError.field = "start_time";
      startTimeError.message = "Start time is required!";
      this.setState({ setErrors: startTimeError })
    } else if (this.state.endTime == '') {
      endTimeError.field = "end_time";
      endTimeError.message = "End time is required!";
      this.setState({ setErrors: endTimeError })
    } else if (this.state.start_date == '') {
      startDateError.field = "start_date";
      startDateError.message = "Satrt date is required!";
      this.setState({ setErrors: startDateError })
    } else if (this.state.finish_date == '') {
      endDateError.field = "end_date";
      endDateError.message = "End Date is required!";
      this.setState({ setErrors: endDateError })
    } else if (this.state.comment == '') {
      commentError.field = "comment";
      commentError.message = "Comment is required!";
      this.setState({ setErrors: commentError })
    } else if (this.state.zip == '') {
      zipError.field = "zip";
      zipError.message = "Zip is required!";
      this.setState({ setErrors: zipError })
    } else if (this.state.unit_location == '') {
      locationError.field = "unit_location";
      locationError.message = "Unit/Location is required!";
      this.setState({ setErrors: locationError })
    } else if (this.state.ward == '') {
      wardError.field = "ward";
      wardError.message = "Ward is required!";
      this.setState({ setErrors: wardError })
    } else {
      this.setState({ setErrors: { field: '', message: '' } })
      this.submitBtn();
    }
  }

  submitBtn = () => {
    this.setState({ isLoading: true })

    let rawData = {
      "user_id": this.props.userid,
      "start_date": this.state.start_date,
      "finish_date": this.state.finish_date,
      "start_time": this.state.startTime,
      "end_time": this.state.endTime,
      "comment": this.state.comment,
      "type_of_activity": this.state.acvity,
      "zip": this.state.zip,
      "location": this.state.unit_location,
      "ward": this.state.ward,
      "shift": this.state.selectShift,
    }

    Api._workerAddCalenderApi(rawData)
      .then((response) => {

        console.log(response)
        this.setState({ isLoading: false })
        setToastMsg(response.message.toString());
        this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });
  }


  render() {

    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>


              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>ADD NEW MY JOB</Text>

              </View>
              <Loader isLoading={this.state.isLoading} />
              <ScrollView>
                <View style={{ marginTop: 10, marginLeft: 30, marginRight: 30 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginTop: 10, flex: 1, marginRight: 5 }}>
                      <TouchableOpacity>
                        <CustomTextInput
                          placeholder='START TIME'
                          floatingText="START TIME"
                          editable={true}
                          onFocus={true}
                          showSoftInputOnFocus={false}
                          caretHidden={true}
                          isVisibleCalendar={true}
                          value={this.state.startTime}
                          returnKeyType={'done'}
                          onTouchEnd={() => this.setState({ isStartTimePickerVisible: true, })}
                        />
                        <DateTimePickerModal
                          isVisible={this.state.isStartTimePickerVisible}
                          mode="time"
                          locale="en_GB"
                          date={new Date()}
                          onConfirm={(date) => this.handleStartTimeConfirm(date)}
                          onCancel={() => this.setState({ isStartTimePickerVisible: false, })}
                        />
                      </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: 10, flex: 1, marginLeft: 5 }}>
                      <TouchableOpacity>
                        <CustomTextInput
                          placeholder='END TIME'
                          floatingText="END TIME"
                          editable={true}
                          onFocus={true}
                          showSoftInputOnFocus={false}
                          caretHidden={true}
                          isVisibleCalendar={true}
                          value={this.state.endTime}
                          returnKeyType={'done'}
                          onTouchEnd={() => this.setState({ isEndTimePickerVisible: true, })}
                        />
                        <DateTimePickerModal
                          isVisible={this.state.isEndTimePickerVisible}
                          mode="time"
                          locale="en_GB"
                          date={new Date()}
                          onConfirm={(date) => this.handleEndTimeConfirm(date)}
                          onCancel={() => this.setState({ isEndTimePickerVisible: false, })}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                  {
                    this.state.setErrors.field == "start_time" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }
                  {
                    this.state.setErrors.field == "end_time" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <TouchableOpacity style={{ marginTop: 10 }} >
                    <CustomTextInput
                      placeholder='START DATE'
                      floatingText="START DATE"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleCalendar={true}
                      value={this.state.start_date}
                      returnKeyType={'done'}
                      onTouchEnd={() => { this.setState({ isStartDatePickerVisible: true }) }}
                    />
                    <DateTimePickerModal
                      isVisible={this.state.isStartDatePickerVisible}
                      mode="date"
                      date={new Date()}
                      onConfirm={(date) => this.handleStartDateConfirm(date)}
                      onCancel={() => this.setState({ isStartDatePickerVisible: false })}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "start_date" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <TouchableOpacity style={{ marginTop: 5 }} >
                    <CustomTextInput
                      placeholder='END DATE'
                      floatingText="END DATE"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleCalendar={true}
                      value={this.state.finish_date}
                      returnKeyType={'done'}
                      onTouchEnd={() => { this.setState({ isEndDatePickerVisible: true, }) }}
                    />
                    <DateTimePickerModal
                      isVisible={this.state.isEndDatePickerVisible}
                      mode="date"
                      date={new Date()}
                      onConfirm={(date) => this.handleEndDateConfirm(date)}
                      onCancel={() => this.setState({ isEndDatePickerVisible: false, })}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "end_date" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <TouchableOpacity style={{ marginTop: 10 }} >
                    <CustomTextInput
                      placeholder='COMMENT'
                      floatingText="COMMENT"
                      editable={true}
                      onFocus={true}
                      multiline={true}
                      style={{ textAlignVertical: "top", height: 150, }}
                      value={this.state.comment}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({ comment: text });
                      }}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "comment" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  {/* <TouchableOpacity style={{ marginTop: 10 }} >
                    <CustomTextInput
                      placeholder='TYPE OF ACTIVITY'
                      floatingText="TYPE OF ACTIVITY"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      //style={{ fontSize: 16 }}
                      isVisibleDropDown={true}
                      value={this.state.acvity}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({ acvity: text });
                      }}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "acvity" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  } */}

                  <View style={{ marginTop: 10 }}>
                    <CustomTextInput
                      placeholder='ZIP/POST CODE'
                      keyboardType='default'
                      floatingText="ZIP/POST CODE"
                      value={this.state.zip}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({ zip: text });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "zip" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 10 }}>
                    <CustomTextInput
                      placeholder='UNIT/LOCATION'
                      keyboardType='default'
                      floatingText="UNIT/LOCATION"
                      value={this.state.unit_location}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({ unit_location: text });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "unit_location" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 10 }}>
                    <CustomTextInput
                      placeholder='WARD'
                      keyboardType='default'
                      floatingText="WARD"
                      value={this.state.ward}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({ ward: text });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "ward" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 10, marginBottom: 20 }}>
                    <Text style={styles.floatingLabelText}>{"SELECT SHIFT"}</Text>
                    {/* <RadioGroup
                      radioButtons={this.state.radioBtnGroup}
                      flexDirection='row'
                      color={'#657CF4'}
                      onPress={this._onPressRadio}

                    /> */}

                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
                      <View style={styles.radioButtonContainer}>
                        <TouchableOpacity onPress={() => { this.radioBtnSelected("1") }} style={styles.radioButton}>
                          <View style={[styles.radioButtonIcon, { backgroundColor: this.state.selectBtn == "1" ? "#000000" : "#ffffff" }]} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this.radioBtnSelected("1") }}>
                          <Text style={styles.radioButtonText}>MORNING</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.radioButtonContainer}>
                        <TouchableOpacity onPress={() => { this.radioBtnSelected("2") }} style={styles.radioButton}>
                          <View style={[styles.radioButtonIcon, { backgroundColor: this.state.selectBtn == "2" ? "#000000" : "#ffffff" }]} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this.radioBtnSelected("2") }}>
                          <Text style={styles.radioButtonText}>DAY</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.radioButtonContainer}>
                        <TouchableOpacity onPress={() => { this.radioBtnSelected("3") }} style={styles.radioButton}>
                          <View style={[styles.radioButtonIcon, { backgroundColor: this.state.selectBtn == "3" ? "#000000" : "#ffffff" }]} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this.radioBtnSelected("3") }}>
                          <Text style={styles.radioButtonText}>NIGHT</Text>
                        </TouchableOpacity>
                      </View>
                    </View>

                  </View>


                </View>
              </ScrollView>
            </View>

            <View >

              <View style={styles.shadow}></View>
              {/*  <Image style={{ width:'100%',height:40,}} source={require('./../../assets/border.png')} /> */}
              <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                onPress={() => this.checkValidation()}
              >
                <Text style={styles.loginText}>SUBMIT</Text>
              </TouchableOpacity>

            </View>


          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _onPressRadio = (data) => {
    console.log(data);
    this.setState({ radioBtnGroup: data })
  }

  radioBtnSelected(select) {
    if (select == "1") {
      this.setState({ selectBtn: select, selectShift: "MORNING", })
    } else if (select == "2") {
      this.setState({ selectBtn: select, selectShift: "DAY", })
    } else if (select == "3") {
      this.setState({ selectBtn: select, selectShift: "NIGHT", })
    }
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center',

  },
  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5
  },
  floatingLabelText: {
    color: "#393FA0",
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    paddingRight: 5,
    fontSize: 14,
    fontFamily: "montserrat_regular"
  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  profileImage: {
    height: 150,
    width: 150,
    position: 'absolute',
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: 60
  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }
  },
  radioButtonContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: 20
  },
  radioButton: {
    height: 22,
    width: 22,
    backgroundColor: "#ffffff",
    borderRadius: 20,
    borderWidth: 1.5,
    borderColor: "#000000",
    alignItems: "center",
    justifyContent: "center"
  },
  radioButtonIcon: {
    height: 12,
    width: 12,
    borderRadius: 7,
  },
  radioButtonText: {
    fontSize: 16,
    marginLeft: 8,
  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }
});

export default connect(mapStateToPrpos)(WorkerAddNewCalender);