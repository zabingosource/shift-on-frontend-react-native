/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView,
  Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import { setToastMsg } from '../../utils/ToastMessage'
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import Loader from '../../utils/Loader';
import { connect } from 'react-redux';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
  }
}

const { width } = Dimensions.get('window');


class WorkerCurrentJobs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchQuery: '',
      isLoading: true,
      apiData: [],
      filterData: [],

    };
    console.log(this.props.navigation);
  }

  componentDidMount() {
    this.navFocusListener = this.props.navigation.addListener('didFocus', () => {
      this._workerCurrentJobSheet();
    });
  }

  componentWillUnmount() {
    this.navFocusListener.remove();
  }

  _workerCurrentJobSheet = () => {
    this.setState({ isLoading: true });

    var formData = new FormData();
    formData.append("worker_id", this.props.userid);

    Api._workerCurrentJobSheet(formData)
      .then((response) => {

        console.log(response.data)

        if (response.status.toString() == "1") {
          this.setState({ apiData: response.data, filterData: response.data, isLoading: false })
        } else {
          this.setState({ isLoading: false, });
          setToastMsg(response.message.toString());
        }


      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  searchFilter = (text) => {
    if (text) {
      const newData = this.state.filterData.filter((item) => {
        const itemData = item.job_details.job_title ? item.job_details.job_title.toUpperCase() : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      this.setState({ apiData: newData, searchQuery: text })
    } else {
      this.setState({ apiData: this.state.filterData, searchQuery: text })
    }
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

          <View style={styles.mainContainer}>
            <View style={{ backgroundColor: '#00D5E1' }}>
              <TouchableOpacity
                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
              </TouchableOpacity>
              <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>CURRENT JOBS</Text>

            </View>

            <Loader isLoading={this.state.isLoading} />
            <View style={{ marginLeft: 10, marginRight: 10 }}>

              <View style={{ flexDirection: 'row', marginTop: 20, backgroundColor: '#EAEAEA', borderRadius: 40 }}>

                <Image style={{ height: 30, width: 30, padding: 5, position: 'absolute', end: 5, top: 3.5 }} source={require('./../../assets/search_icn.png')} />

                <TextInput
                  style={{ width: '85%', paddingHorizontal: 8, paddingTop: 8, paddingBottom: 10, marginLeft: 10, marginRight: 40, color: '#969696', fontFamily: 'montserrat_regular', fontSize: 12 }}
                  placeholder='Search by job title'
                  keyboardType='default'
                  value={this.state.searchQuery}
                  returnKeyType={'done'}
                  onChangeText={(text) => { this.searchFilter(text) }}
                />
              </View>
              <View style={{ marginTop: 20, marginBottom: 20 }}>

                {
                  this.state.apiData.length > 0 ?

                    (<FlatList
                      style={{ width: '100%', marginBottom: 160 }}
                      data={this.state.apiData}
                      keyExtractor={(item, index) => index.toString()}
                      //ItemSeparatorComponent={this.ItemSeparator}
                      renderItem={({ item, index }) =>
                        <TouchableOpacity activeOpacity={0.9}
                          onPress={() => this.props.navigation.navigate('WorkerCurrentJobSheet', { jobDetails: item })}
                        >
                          <View style={styles.rowContainer}>

                            <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                              <Image style={{ height: 45, width: 50, tintColor: '#000000' }} source={require('./../../assets/CJShifts_icon.png')} />
                            </View>
                            <View style={{ width: "80%" }}>
                              <Text
                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                              >{item.job_details.job_title}</Text>
                              <Text
                                numberOfLines={1}
                                style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                              >{item.job_details.job_description} </Text>
                              <View style={{ marginTop: 5, flexDirection: "row" }}>
                                <Image source={require('./../../assets/pin_icn.png')} style={{ width: 10, height: 15, alignSelf: 'center' }} resizeMode='stretch' />
                                <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>
                                  {item.job_details.location}{item.job_details.landmark ? ", " + item.job_details.landmark : ''}{item.job_details.zip ? ", " + item.job_details.zip : ''}
                                </Text>
                              </View>
                              <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginTop: 5, flexDirection: "row", flex: 1.5, marginRight: 2 }}>
                                  <Image source={require('./../../assets/calendar_icn_b.png')} style={{ width: 12, height: 12, alignSelf: 'center' }} resizeMode='stretch' />
                                  <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>{item.job_details.shift.start_date} to {item.job_details.shift.end_date}</Text>
                                </View>
                                <View style={{ marginTop: 5, flexDirection: "row", flex: 1, marginLeft: 2 }}>
                                  <Image source={require('./../../assets/time_icn_a.png')} style={{ width: 12, height: 12, alignSelf: 'center' }} resizeMode='stretch' />
                                  <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}> {item.job_details.shift.start_time} to {item.job_details.shift.end_time}</Text>
                                </View>

                              </View>

                            </View>
                          </View>
                        </TouchableOpacity>
                      }
                    />)
                    :
                    (<View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                      <Text
                        numberOfLines={1}
                        style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                      >No Data Available!</Text>
                    </View>)
                }
              </View>

            </View>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }


}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },

  tabHolder: {
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 30,
    flex: 1
  },
  rowContainer: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5
  },


});

export default connect(mapStateToPrpos)(WorkerCurrentJobs);