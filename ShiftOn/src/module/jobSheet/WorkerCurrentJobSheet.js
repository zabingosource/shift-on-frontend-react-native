/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions, TextInput,
  ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import { setToastMsg } from '../../utils/ToastMessage';
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import Loader from '../../utils/Loader';
import { connect } from 'react-redux';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
  }
}

const { width } = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});
const resetToHome = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'WorkerHome' })],
});

class WorkerCurrentJobSheet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      topDetailData: this.props.navigation.getParam('jobDetails', 'NO-item'),
      date: '',
      startTime: '',
      endTime: '',
      comment: '',
      isLoading: false,
      isDatePickerVisible: false,
      isStartTimePickerVisible: false,
      isEndTimePickerVisible: false,
      compareTimeValues: '',
      setErrors: {
        field: '',
        message: ''
      }

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {

  }

  // this is for date picker...
  handleDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    this.setState({
      date: dateTimeString,
      isDatePickerVisible: false,
    })

    console.log(dateTimeString)
  };

  // this is for start time picker...
  handleStartTimeConfirm = (time) => {
    let dateTimeString = moment(time).local().format('HH:mm');

    this.setState({
      startTime: dateTimeString,
      endTime: '',
      compareTimeValues: time,
      isStartTimePickerVisible: false,
    })
    console.log(dateTimeString)
  };

  // this is for end time picker...
  handleEndTimeConfirm = (time) => {
    let dateTimeString = moment(time).local().format('HH:mm');

    var start = new Date(this.state.compareTimeValues);
    var end = new Date(time);

    if (this.state.compareTimeValues != '') {
      if (start.getTime() > end.getTime()) {
        this.setState({
          isEndTimePickerVisible: false,
          endTime: '',
        })
        setToastMsg("End Time must be greater than Start time.");
        console.log("end time is smaller")
      } else {
        this.setState({
          endTime: dateTimeString,
          isEndTimePickerVisible: false,
        })
        console.log("end time is greater")
      }
    } else {
      this.setState({
        isEndTimePickerVisible: false,
      })
      setToastMsg("Please select Start time first.");
      console.log("start time not selected yet.")
    }
    console.log(dateTimeString)
  };

  checkValidation = () => {
    let dateError = { field: '', message: '' }
    let startTimeError = { field: '', message: '' }
    let endTimeError = { field: '', message: '' }

    if (this.state.date == '') {
      dateError.field = "date";
      dateError.message = "Date is required!";
      this.setState({ setErrors: dateError })
    } else if (this.state.startTime == '') {
      startTimeError.field = "startTime";
      startTimeError.message = "Start time is required!";
      this.setState({ setErrors: startTimeError })
    } else if (this.state.endTime == '') {
      endTimeError.field = "endTime";
      endTimeError.message = "End time is required!";
      this.setState({ setErrors: endTimeError })
    } else {
      this.setState({ setErrors: { field: '', message: '' } })
      this._workerAddJobSheet();
    }
  }

  _workerAddJobSheet = () => {
    this.setState({ isLoading: true });
    var rawData = {
      "worker_id": this.props.userid,
      "unit_id": this.props.navigation.getParam('jobDetails', 'NO-item').job_details.unit_id,
      "job_id": this.props.navigation.getParam('jobDetails', 'NO-item').job_details.id,
      "approved_by": "0",
      "start_time": this.state.startTime,
      "end_time": this.state.endTime,
      "date": this.state.date,
      "comment": this.state.comment
    }

    Api._workerAddJobSheet(rawData)
      .then((response) => {

        console.log(response);
        this.setState({ isLoading: false });
        setToastMsg(response.message.toString());
        this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false, })
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>

            <View style={styles.mainContainer}>

              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>CURRENT JOB SHEET</Text>

              </View>
              <Loader isLoading={this.state.isLoading} />
              <ScrollView>

                <View style={{ marginLeft: 10, marginRight: 10 }}>
                  <View style={{ marginTop: 10 }}>
                    <TouchableOpacity activeOpacity={0.9} >
                      <View style={styles.rowContainer}>

                        <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                          <Image style={{ height: 45, width: 50, tintColor: '#000000' }} source={require('./../../assets/CJShifts_icon.png')} />
                        </View>
                        <View style={{ width: "80%", }}>
                          <Text
                            style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                          >{this.state.topDetailData.job_details.job_title}</Text>
                          <Text
                            numberOfLines={1}
                            style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                          >{this.state.topDetailData.job_details.job_description} </Text>
                          <View style={{ marginTop: 5, flexDirection: "row" }}>
                            <Image source={require('./../../assets/pin_icn.png')} style={{ width: 10, height: 15, alignSelf: 'center' }} resizeMode='stretch' />
                            <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>
                              {this.state.topDetailData.job_details.location}{this.state.topDetailData.job_details.landmark ? ", " + this.state.topDetailData.job_details.landmark : ''}{this.state.topDetailData.job_details.zip ? ", " + this.state.topDetailData.job_details.zip : ''}
                            </Text>
                          </View>
                          <View style={{ marginTop: 5, flexDirection: "row", flex: 1.5, marginRight: 2 }}>
                            <Image source={require('./../../assets/calendar_icn_b.png')} style={{ width: 12, height: 12, alignSelf: 'center' }} resizeMode='stretch' />
                            <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>
                              {this.state.topDetailData.job_details.shift.start_date} to {this.state.topDetailData.job_details.shift.end_date}
                            </Text>
                          </View>
                          <View style={{ marginTop: 5, flexDirection: "row", flex: 1, }}>
                            <Image source={require('./../../assets/time_icn_a.png')} style={{ width: 12, height: 12, alignSelf: 'center' }} resizeMode='stretch' />
                            <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>
                              {this.state.topDetailData.job_details.shift.start_time} to {this.state.topDetailData.job_details.shift.end_time}
                            </Text>
                          </View>

                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>

                  <View style={{ marginTop: 10, marginLeft: 30, marginRight: 30 }}>

                    <CustomTextInput
                      placeholder='DATE'
                      floatingText="DATE"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      style={{ fontSize: 16, }}
                      isVisibleCalendar={true}
                      value={this.state.date}
                      returnKeyType={'done'}
                      onTouchEnd={() => {
                        this.setState({ isDatePickerVisible: true })
                      }}
                    />
                    <DateTimePickerModal
                      isVisible={this.state.isDatePickerVisible}
                      mode="date"
                      date={new Date()}
                      onConfirm={(date) => this.handleDateConfirm(date)}
                      onCancel={() => this.setState({ isDatePickerVisible: false })}
                    />

                    {
                      this.state.setErrors.field == "date" && (
                        <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                      )
                    }

                    <View style={{ flexDirection: 'row' }}>

                      <View style={{ marginTop: 10, flex: 1, marginRight: 3 }}>
                        <CustomTextInput
                          placeholder='START TIME'
                          floatingText="START TIME"
                          editable={true}
                          onFocus={true}
                          showSoftInputOnFocus={false}
                          caretHidden={true}
                          style={{ fontSize: 16 }}
                          value={this.state.startTime}
                          returnKeyType={'done'}
                          onTouchEnd={() => {
                            this.setState({ isStartTimePickerVisible: true })
                          }}
                        />
                        <DateTimePickerModal
                          isVisible={this.state.isStartTimePickerVisible}
                          mode="time"
                          locale="en_GB"
                          date={new Date()}
                          onConfirm={(date) => this.handleStartTimeConfirm(date)}
                          onCancel={() => this.setState({ isStartTimePickerVisible: false })}
                        />
                      </View>

                      <View style={{ marginTop: 10, flex: 1, marginLeft: 3 }}>
                        <CustomTextInput
                          placeholder='END TIME'
                          floatingText="END TIME"
                          editable={true}
                          onFocus={true}
                          showSoftInputOnFocus={false}
                          caretHidden={true}
                          style={{ fontSize: 16 }}
                          value={this.state.endTime}
                          returnKeyType={'done'}
                          onTouchEnd={() => {
                            this.setState({ isEndTimePickerVisible: true })
                          }}
                        />
                        <DateTimePickerModal
                          isVisible={this.state.isEndTimePickerVisible}
                          mode="time"
                          locale="en_GB"
                          date={new Date()}
                          onConfirm={(date) => this.handleEndTimeConfirm(date)}
                          onCancel={() => this.setState({ isEndTimePickerVisible: false })}
                        />
                      </View>

                    </View>
                    {
                      this.state.setErrors.field == "startTime" && (
                        <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                      )
                    }
                    {
                      this.state.setErrors.field == "endTime" && (
                        <Text style={[styles.errors, { textAlign: 'right', marginEnd: 10 }]}>{this.state.setErrors.message}</Text>
                      )
                    }

                    <CustomTextInput
                      placeholder='COMMENT (Optional)'
                      floatingText="COMMENT (Optional)"
                      editable={true}
                      onFocus={true}
                      multiline={true}
                      style={{ textAlignVertical: "top", height: 150, fontSize: 16, }}
                      value={this.state.comment}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({ comment: text });
                      }}
                    />

                    <TouchableOpacity style={{ padding: 10, backgroundColor: "#657CF4", borderRadius: 20, marginTop: 50 }}
                      onPress={() => this.checkValidation()}
                    >
                      <Text style={styles.loginText}>SUBMIT</Text>
                    </TouchableOpacity>

                  </View>

                </View>

              </ScrollView>
            </View>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center',

  },

  rowContainer: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5


  },

  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  profileImage: {
    height: 150,
    width: 150,
    position: 'absolute',
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: 60

  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }
});

export default connect(mapStateToPrpos)(WorkerCurrentJobSheet);