/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions,
  TextInput, ToastAndroid
} from 'react-native';
//import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AppConstants from '../../apis/AppConstants';
import Api from '../../apis/Api';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';
import { connect } from 'react-redux';
import { loginUserDetails, workerSettings, controllerSetting } from '../../redux/actions/actions';


const mapDispatchToProps = (dispatch) => {
  return {
    userDetails: (txt1, txt2, txt3, txt4, txt5) => dispatch(loginUserDetails(txt1, txt2, txt3, txt4, txt5)),
    workerSettingSetValues: (txt1, txt2, txt3, txt4) => dispatch(workerSettings(txt1, txt2, txt3, txt4)),
    controllerSettingSetValues: (txt1, txt2) => dispatch(controllerSetting(txt1, txt2)),
  }
}

const { width } = Dimensions.get('window');
const resetToWorker = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'WorkerHome' })],
});
const resetToUnit = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'UnitHome' })],
});
const resetToController = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'ControlerHome' })],
});

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      isLoading: false,
      isCreatePassVis: false,
      setErrors: {
        field: '',
        message: ''
      }

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {

  }

  checkValidation = () => {
    let emailError = { field: '', message: '' }
    let passwordError = { field: '', message: '' }

    if (this.state.email == '') {
      emailError.field = "email";
      emailError.message = "Email is required!";
      this.setState({ setErrors: emailError })
    } else if (this.state.password == '') {
      passwordError.field = "password";
      passwordError.message = "Password is required!";
      this.setState({ setErrors: passwordError })
    } else {
      this.setState({ setErrors: { field: '', message: '' } })
      this._loginApi();
    }
  }

  _loginApi = async () => {
    this.setState({ isLoading: true });
    let device_token = await AsyncStorage.getItem('device_token');

    let endpoint = AppConstants.BASE_URL + AppConstants.LOGIN;

    var formData = new FormData();
    formData.append('email', this.state.email);
    formData.append('password', this.state.password);
    formData.append('token', device_token);
    formData.append('device_name', '');

    console.log(formData)

    fetch(endpoint, {
      method: "POST",
      headers: { 'Content-Type': 'multipart/form-data' },
      body: formData,
    })
      .then((response) => response.json())
      .then((responseData) => {

        console.log("RESPONSE:- ", responseData);

        if (responseData.status.toString() == "1") {

          if (responseData.data.role.toString() == "worker") {
            var imageValue = "";
            var sureNameValue = "";
            var foreNameValue = "";
            var myAvailabilityValue = "0";
            var searchableValue = "0";
            var seftEmployedValue = "0";
            if (responseData.data.image != null) {
              imageValue = responseData.data.image.toString()
            }
            if (responseData.data.sur_name != null) {
              sureNameValue = responseData.data.sur_name.toString()
            }
            if (responseData.data.fore_name != null) {
              foreNameValue = responseData.data.fore_name.toString()
            }

            if (responseData.data.my_availability != null) {
              myAvailabilityValue = responseData.data.my_availability.toString()
            }
            if (responseData.data.searchable != null) {
              searchableValue = responseData.data.searchable.toString()
            }
            if (responseData.data.self_employed != null) {
              seftEmployedValue = responseData.data.self_employed.toString()
            }

            // calling redux for save worker user details and setting...
            this.props.userDetails(
              responseData.data.id.toString(), foreNameValue + " " + sureNameValue,
              responseData.data.email.toString(), imageValue, responseData.data.role.toString());

            this.props.workerSettingSetValues(responseData.data.id.toString(), myAvailabilityValue,
              searchableValue, seftEmployedValue);

            // store in async storage for worker user details and setting...
            this.storeLogDetailsToAsyncStorage(responseData.data.id.toString(), foreNameValue + " " + sureNameValue,
              responseData.data.email.toString(), imageValue, responseData.data.role.toString());
            this.storeWorkerSettingToAsyncStorage(responseData.data.id.toString(), myAvailabilityValue,
              searchableValue, seftEmployedValue);

          } else if (responseData.data.role.toString() == "controller") {

            var imageValue = "";
            var firstNameValue = "";
            var lastNameValue = "";
            var passiveValue = "0";
            if (responseData.data.first_name != null) {
              firstNameValue = responseData.data.first_name.toString();
            }
            if (responseData.data.last_name != null) {
              lastNameValue = responseData.data.last_name.toString();
            }
            if (responseData.data.profile_image != null) {
              imageValue = responseData.data.profile_image.toString();
            }

            if (responseData.data.passive_mode != null) {
              passiveValue = responseData.data.passive_mode.toString();
            }

            // calling redux for save controller user details and setting...
            this.props.userDetails(
              responseData.data.id.toString(), firstNameValue + " " + lastNameValue, responseData.data.email.toString(),
              imageValue, responseData.data.role.toString());

            this.props.controllerSettingSetValues(responseData.data.id.toString(), passiveValue);

            this.storeAgencyId(responseData.data.agency_id);
            this.storeLogDetailsToAsyncStorage(responseData.data.id.toString(), firstNameValue + " " + lastNameValue, responseData.data.email.toString(),
              imageValue, responseData.data.role.toString());
            this.storeControllerSettingToAsyncStorage(responseData.data.id.toString(), passiveValue)

          } else {

            var imageValue = "";
            var nameValue = "";
            if (responseData.data.name != null) {
              nameValue = responseData.data.name.toString()
            }
            if (responseData.data.profile_image != null) {
              imageValue = responseData.data.profile_image.toString()
            }

            // calling redux for save unit user details...
            this.props.userDetails(
              responseData.data.id.toString(), nameValue, responseData.data.email.toString(),
              imageValue, responseData.data.role.toString());

            this.storeLogDetailsToAsyncStorage(responseData.data.id.toString(), nameValue, responseData.data.email.toString(),
              imageValue, responseData.data.role.toString());
          }

          this._afterLoginToNavigate(responseData.data.role.toString());

        }
        this.setState({ isLoading: false });
        setToastMsg(responseData.message.toString());

      }).catch((error) => {
        console.log("ERROR:- ", error);
        this.setState({ isLoading: false });
        setToastMsg("Someting went wrong");

      });

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <ScrollView>

            <Loader isLoading={this.state.isLoading} />

            <View style={styles.mainContainer}>

              <Image style={styles.headerBg} source={require('./../../assets/login_bgtop.png')} resizeMode="stretch" />

              <Image style={styles.logoImage} source={require('./../../assets/logo_a.png')} resizeMode="stretch" />

              <View style={{ marginLeft: 50, marginRight: 50 }}>
                <Text style={styles.welcome}>Welcome!</Text>
                <View style={{ marginTop: 30 }}>
                  <CustomTextInput
                    placeholder='EMAIL'
                    keyboardType='email-address'
                    floatingText="EMAIL"
                    autoCapitalize='none'
                    value={this.state.email}
                    returnKeyType={'done'}
                    onChangeText={(text) => {
                      this.setState({ email: text });
                    }}
                  />
                  {
                    this.state.setErrors.field == "email" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 10 }}>
                    <CustomTextInput
                      placeholder='PASSWORD'
                      keyboardType='default'
                      floatingText="PASSWORD"
                      secureTextEntry={true}
                      returnKeyType={'done'}
                      value={this.state.password}
                      onChangeText={(text) => {
                        this.setState({ password: text });
                      }}
                    />
                    {
                      this.state.setErrors.field == "password" && (
                        <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                      )
                    }

                  </View>

                  {/* <View>
                    <TouchableOpacity style={styles.forgotPassHold1}
                      onPress={() => this.props.navigation.navigate('ForgotPassword', { forWhat: "forgotPassword" })}
                    >
                      <Text style={styles.forgotPassText}>Forgot Password ?</Text>
                    </TouchableOpacity>

                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', alignSelf: 'flex-end', marginRight: 40 }}>
                      <View style={{ width: 20, height: 1, backgroundColor: '#585858' }}></View>
                      <Text style={{ color: '#585858', fontSize: 14, fontFamily: "montserrat_regular" }}> OR </Text>
                      <View style={{ width: 20, height: 1, backgroundColor: '#585858' }}></View>
                    </View>

                    <TouchableOpacity style={styles.forgotPassHold2}
                      onPress={() => this.props.navigation.navigate('ForgotPassword', { forWhat: "createNewPassword" })}
                    >
                      <Text style={styles.forgotPassText}>Create new Password.</Text>
                    </TouchableOpacity>
                  </View> */}

                  <View>
                    <TouchableOpacity style={styles.forgotPassHold1}
                      onPress={() => this.props.navigation.navigate('ForgotPassword', { forWhat: "forgotPassword" })}
                    >
                      <Text style={styles.forgotPassText}>Forgot Password ?</Text>
                    </TouchableOpacity>
                  </View>

                  <TouchableOpacity style={styles.loginHold}
                    onPress={() => this.checkValidation()}
                  >
                    <Text style={styles.loginText}>LOGIN</Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={styles.registerHold}
                    onPress={() => this.props.navigation.navigate('WorkerStepOne')}
                  >
                    <Text style={styles.registerText}>REGISTER AS WORKER</Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={styles.registerUnitHold}
                    onPress={() => this.props.navigation.navigate('UnitResistrationName')}
                  >
                    <Text style={styles.registerText}>REGISTER AS UNIT</Text>
                  </TouchableOpacity>

                </View>
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </Fragment>
    );
  }

  _afterLoginToNavigate(mode) {
    if (mode == 'worker') {
      this.props.navigation.dispatch(resetToWorker);
    } else if (mode == 'controller') {
      this.props.navigation.dispatch(resetToController);
    } else if (mode == 'unit') {
      this.props.navigation.dispatch(resetToUnit);
    }
  }

  storeAgencyId = async (id) => {
    try {
      if (id !== null) {
        await AsyncStorage.setItem('agency_id', id);
      } else {
        await AsyncStorage.setItem('agency_id', '');
      }

    } catch (e) {
      console.log(e);
    }
  }

  storeLogDetailsToAsyncStorage = async (id, name, email, image, role) => {
    try {
      await AsyncStorage.setItem('user_id', id);
      await AsyncStorage.setItem('user_name', name);
      await AsyncStorage.setItem('user_email', email);
      await AsyncStorage.setItem('user_image', image);
      await AsyncStorage.setItem('user_role', role);

    } catch (e) {
      console.log(e);
    }
  }

  storeWorkerSettingToAsyncStorage = async (id, availablity, searchable, selfEmp) => {
    try {
      await AsyncStorage.setItem('worker_id', id);
      await AsyncStorage.setItem('worker_availibility', availablity);
      await AsyncStorage.setItem('worker_searchable', searchable);
      await AsyncStorage.setItem('worker_selfEmp', selfEmp);

    } catch (e) {
      console.log(e);
    }
  }

  storeControllerSettingToAsyncStorage = async (id, passive) => {
    try {
      await AsyncStorage.setItem('controller_id', id);
      await AsyncStorage.setItem('paassive_mood', passive);

    } catch (e) {
      console.log(e);
    }
  }

}



const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  headerBg: {
    width: width,
    height: 250,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75
  },
  welcome: {
    alignSelf: 'center',
    fontSize: 35,
    fontFamily: 'montserrat_light',
    color: '#353C9E',
    marginTop: 10
  },
  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold1: {
    paddingTop: 10,
    paddingBottom: 5,
    alignSelf: 'flex-end',
    marginRight: 20
  },
  forgotPassHold2: {
    paddingTop: 5,
    paddingBottom: 10,
    alignSelf: 'flex-end',
    marginRight: 20
  },
  forgotPassText: {
    color: '#585858',
    fontSize: 14,
    textDecorationLine: 'underline',
    fontFamily: "montserrat_regular"
  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'
  },
  registerHold: {
    padding: 10,
    marginTop: 25,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginBottom: 10,
  },
  registerUnitHold: {
    padding: 10,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginBottom: 20,
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'
  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }
});

export default connect(null, mapDispatchToProps)(Login);
