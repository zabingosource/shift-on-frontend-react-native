/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert,
  SafeAreaView, Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { setToastMsg } from '../../utils/ToastMessage';
import Loader from '../../utils/Loader';
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import { connect } from 'react-redux';
import { controllerSetting } from '../../redux/actions/actions';



const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
    passiveMoodValue: props.controllerSettingRedux.passiveMood,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    controllerSettingSetValues: (txt1, txt2) => dispatch(controllerSetting(txt1, txt2)),
  }
}

const { width } = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});
const resetToHome = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'WorkerHome' })],
});

class ControllerSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleText: 'OFF',
      isVisible: false,
      visibleApiText: '0',
      isLoading: false,

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    if (this.props.passiveMoodValue == '1') {
      this.setState({ isVisible: true, visibleText: "ON", visibleApiText: "1" })
    }
  }

  _controllerSettingsApi = () => {
    this.setState({ isLoading: true })

    let rawData = {
      "user_id": this.props.userid,
      "passive_mode": this.state.visibleApiText,
    }

    Api._controllerSettingsApi(rawData)
      .then((response) => {

        console.log(response)

        if (response.status == 1) {
          this.props.controllerSettingSetValues(this.props.userid, this.state.visibleApiText);
          this.storeControllerSettingToAsyncStorage(this.props.userid, this.state.visibleApiText);
        }
        this.setState({ isLoading: false });
        setToastMsg(response.message.toString());

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>
            <ScrollView>

              <View style={styles.mainContainer}>

                <Loader isLoading={this.state.isLoading} />

                <Image style={styles.headerBg} source={require('./../../assets/header_bg.png')} resizeMode="stretch" />

                <TouchableOpacity
                  style={{ position: 'absolute', marginTop: 20, marginLeft: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                </TouchableOpacity>

                {this.props.userImage ?
                  <Image style={styles.profileImage} source={{ uri: AppConstants.IMAGE_URL + this.props.userImage }} resizeMode="stretch" />
                  :
                  <Image style={styles.profileImage} source={require('./../../assets/woman_face.jpeg')} resizeMode="stretch" />
                }

                <View style={{ marginLeft: 50, marginRight: 50, marginTop: 60 }}>
                  <Text style={styles.heading}>{(this.props.userName) != "" ? (this.props.userName) : "User"}</Text>
                  <Text style={styles.subheading}>SETTINGS</Text>
                  <View style={{ marginTop: 30 }}>


                    <View>
                      <TextInput
                        editable={false}
                        style={styles.textInput_style}
                        placeholder={"PASSIVE MODE"}
                        value={this.state.visibleText}
                      />
                      <Text style={styles.floatingLabelText}>{"PASSIVE MODE"}</Text>
                      <TouchableOpacity
                        onPress={() => this._togglevisiblity()}
                        style={{ height: 35, width: 45, position: 'absolute', end: 30, top: 8 }}>
                        <Image style={{ height: 35, width: 45, }} source={this.state.isVisible ? require('./../../assets/on_icn.png') : require('./../../assets/off_icn.png')} resizeMode="stretch" />
                      </TouchableOpacity>
                    </View>


                  </View>


                  <TouchableOpacity style={{ padding: 10, backgroundColor: "#657CF4", borderRadius: 20, marginTop: 50 }}
                    onPress={() => this._controllerSettingsApi()}
                  >
                    <Text style={styles.loginText}>DONE</Text>
                  </TouchableOpacity>

                </View>
              </View>
            </ScrollView>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }
  _togglevisiblity = () => {
    this.state.isVisible = !this.state.isVisible;
    this.setState({ isVisible: this.state.isVisible });
    if (this.state.isVisible) {
      this.setState({ visibleText: 'ON', visibleApiText: '1', });
    } else {
      this.setState({ visibleText: 'OFF', visibleApiText: '0', });

    }
  }

  storeControllerSettingToAsyncStorage = async (id, passive) => {
    try {
      await AsyncStorage.setItem('controller_id', id);
      await AsyncStorage.setItem('paassive_mood', passive);

    } catch (e) {
      console.log(e);
    }
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center',

  },

  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 20

  },
  subheading: {
    alignSelf: 'center',
    fontSize: 17,
    fontFamily: 'montserrat_semibold',
    color: '#7A7A7A',
    marginTop: 20

  },
  floatingLabelText: {
    color: "#393FA0",
    marginLeft: 25,
    position: 'absolute',
    marginTop: -10,
    backgroundColor: '#FFFFFF',
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 12,
    fontFamily: "montserrat_regular"
  },
  textInput_style: {

    fontSize: 14,
    padding: 10,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    paddingLeft: 30,
    paddingRight: 30,
    color: '#464646',
    fontFamily: "montserrat_regular"
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },

  profileImage: {
    height: 150,
    width: 150,
    position: 'absolute',
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: 60

  },

});

export default connect(mapStateToPrpos, mapDispatchToProps)(ControllerSettings);