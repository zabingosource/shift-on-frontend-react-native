/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions, TextInput
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Api from '../../apis/Api';
import { connect } from 'react-redux';
import { loginUserDetails, defaultImageUrl } from '../../redux/actions/actions';
import { removeLogDetailsToAsyncStorage } from '../../utils/Logout';


const mapDispatchToProps = (dispatch) => {
  return {
    userDetails: (txt1, txt2, txt3, txt4, txt5) => dispatch(loginUserDetails(txt1, txt2, txt3, txt4, txt5)),
    defaultImageUrl: (url) => dispatch(defaultImageUrl(url)),
  }
}

const { width } = Dimensions.get('window');

class UnitHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      unitName: '',

    };
    console.log("Navigation: ", this.props.navigation);
  }

  componentDidMount() {
    this.navFocusListener = this.props.navigation.addListener('didFocus', () => {
      this.setLogDetailsToRedux();
    });
    this._defaultImageUrl();
  }

  componentWillUnmount() {
    this.navFocusListener.remove();
  }

  setLogDetailsToRedux = async () => {
    var id = await AsyncStorage.getItem('user_id');
    var name = await AsyncStorage.getItem('user_name');
    var email = await AsyncStorage.getItem('user_email');
    var image = await AsyncStorage.getItem('user_image');
    var role = await AsyncStorage.getItem('user_role');

    this.setState({ unitName: name });
    this.props.userDetails(id, name, email, image, role);
  }

  _defaultImageUrl = async () => {

    /// redirect page from notifcation handler...
    var screen = await AsyncStorage.getItem('nextScreen');
    if (screen == 'UnitAgency') {
      this.props.navigation.navigate('UnitAgencys');
    }

    var rawData = { "type": 'agency' }
    Api._defaultImageUrl(rawData)
      .then((response) => {

        console.log(response.data);
        if (response.status.toString() == "1") {
          this.props.defaultImageUrl(response.data.img);
        }

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

          <View style={styles.mainContainer}>
            <View style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#00D5E1' }}>
              <Image />
              <Text style={{ fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>HOME</Text>
              <View style={{ flexDirection: 'row', position: 'absolute', paddingTop: 15, paddingBottom: 15, paddingRight: 20, right: 0 }}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('UnitProfile')}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/profile_icn_c.png')} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ paddingLeft: 15 }}
                  onPress={() => removeLogDetailsToAsyncStorage(this.props.navigation)}
                >
                  <Image style={{ height: 15, width: 15, padding: 12.5 }} source={require('./../../assets/logout_icon.png')} />
                </TouchableOpacity>
              </View>
            </View>

            <ScrollView>
              <View style={{ marginLeft: 50, marginRight: 50 }}>
                <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: 10, }}>
                  <Text style={{ alignSelf: 'center', fontSize: 22, fontFamily: 'montserrat_light', color: '#000000', marginTop: 10 }}>Hello, </Text>
                  <Text style={styles.heading}>{(this.state.unitName) ? (this.state.unitName) : "User"}</Text>
                </View>


                <View style={{ marginTop: 30, marginBottom: 20 }}>

                  <TouchableOpacity
                    style={{ marginTop: 10 }}
                    activeOpacity={0.9}
                    onPress={() => this.props.navigation.navigate('UnitAgencys')}
                  >
                    <View >
                      <View >
                        <Image style={{ width: '70%', marginLeft: '29%', height: 80, padding: 20, }} source={require('./../../assets/tab_bg_a.png')} resizeMode='stretch' />
                        <Image style={{ width: 40, height: 40, position: 'absolute', end: '7.5%', top: 20 }} source={require('./../../assets/agency_img.png')} resizeMode='stretch' />
                      </View>
                      <View style={{ width: '75%', height: 80, backgroundColor: '#878787', alignItems: 'center', justifyContent: 'center', borderRadius: 20, padding: 10, position: 'absolute' }}>
                        <Text style={{ color: '#FFFFFF', padding: 10, fontSize: 18 }}>Agencies</Text>
                      </View>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{ marginTop: 10 }}
                    activeOpacity={0.9}
                    onPress={() => this.props.navigation.navigate('UnitRole')}
                  >
                    <View >
                      <View >
                        <Image style={{ width: "70%", height: 80, padding: 20, }} source={require('./../../assets/tab_bg_a.png')} resizeMode='stretch' />
                        <Image style={{ width: 40, height: 40, position: 'absolute', start: '7.5%', top: 20 }} source={require('./../../assets/profile_icn_c.png')} resizeMode='stretch' />
                      </View>
                      <View style={{ width: '75%', height: 80, backgroundColor: '#878787', alignItems: 'center', justifyContent: 'center', borderRadius: 20, padding: 20, position: 'absolute', end: 5 }}>
                        <Text style={{ color: '#FFFFFF', padding: 10, fontSize: 18 }}>Roles</Text>
                      </View>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{ marginTop: 10 }}
                    activeOpacity={0.9}
                    onPress={() => this.props.navigation.navigate('UnitShiftList')}
                  >
                    <View >
                      <View >
                        <Image style={{ width: '70%', marginLeft: '29%', height: 80, padding: 20, }} source={require('./../../assets/tab_bg_a.png')} resizeMode='stretch' />
                        <Image style={{ width: 40, height: 40, position: 'absolute', end: '7.5%', top: 20 }} source={require('./../../assets/shift_icon.png')} resizeMode='stretch' />
                      </View>
                      <View style={{ width: '75%', height: 80, backgroundColor: '#878787', alignItems: 'center', justifyContent: 'center', borderRadius: 20, padding: 10, position: 'absolute' }}>
                        <Text style={{ color: '#FFFFFF', padding: 10, fontSize: 18 }}>Shifts</Text>
                      </View>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{ marginTop: 10 }}
                    activeOpacity={0.9}
                    onPress={() => this.props.navigation.navigate('UnitPayRate')}
                  >
                    <View >
                      <View >
                        <Image style={{ width: '70%', height: 80, padding: 20, }} source={require('./../../assets/tab_bg_a.png')} resizeMode='stretch' />
                        <Image style={{ width: 40, height: 40, position: 'absolute', start: '7.5%', top: 20 }} source={require('./../../assets/pay_rate_icn.png')} resizeMode='stretch' />
                      </View>
                      <View style={{ width: '75%', height: 80, backgroundColor: '#878787', alignItems: 'center', justifyContent: 'center', borderRadius: 20, padding: 10, position: 'absolute', end: 5 }}>
                        <Text style={{ color: '#FFFFFF', padding: 10, fontSize: 18 }}>Pay Rates</Text>
                      </View>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{ marginTop: 10 }}
                    activeOpacity={0.9}
                    onPress={() => this.props.navigation.navigate('UnitWard')}
                  >
                    <View >
                      <View >
                        <Image style={{ width: "70%", marginLeft: '29%', height: 80, padding: 20, }} source={require('./../../assets/tab_bg_a.png')} resizeMode='stretch' />
                        <Image style={{ width: 40, height: 40, position: 'absolute', end: '7.5%', top: 20 }} source={require('./../../assets/ward_bed_icn.png')} resizeMode='stretch' />
                      </View>
                      <View style={{ width: '75%', height: 80, backgroundColor: '#878787', alignItems: 'center', justifyContent: 'center', borderRadius: 20, padding: 20, position: 'absolute' }}>
                        <Text style={{ color: '#FFFFFF', padding: 10, fontSize: 18 }}>Wards</Text>
                      </View>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{ marginTop: 10 }}
                    activeOpacity={0.9}
                    onPress={() => this.props.navigation.navigate('UnitJobList')}
                  >
                    <View >
                      <View >
                        <Image style={{ width: "70%", height: 80, padding: 20, }} source={require('./../../assets/tab_bg_a.png')} resizeMode='stretch' />
                        <Image style={{ width: 40, height: 40, position: 'absolute', start: '7.5%', top: 20 }} source={require('./../../assets/shift2_icn.png')} resizeMode='stretch' />
                      </View>
                      <View style={{ width: '75%', height: 80, backgroundColor: '#878787', alignItems: 'center', justifyContent: 'center', borderRadius: 20, padding: 20, position: 'absolute', end: 5 }}>
                        <Text style={{ color: '#FFFFFF', padding: 10, fontSize: 18 }}>Jobs</Text>
                      </View>
                    </View>
                  </TouchableOpacity>


                  <TouchableOpacity
                    style={{ marginTop: 10 }}
                    activeOpacity={0.9}
                    onPress={() => this.props.navigation.navigate('UnitCalender')}
                  >
                    <View >
                      <View >
                        <Image style={{ width: '70%', marginLeft: '29%', height: 80, padding: 20, }} source={require('./../../assets/tab_bg_a.png')} resizeMode='stretch' />
                        <Image style={{ width: 40, height: 40, position: 'absolute', end: '7.5%', top: 20 }} source={require('./../../assets/calendar_icn_a.png')} resizeMode='stretch' />
                      </View>
                      <View style={{ width: '75%', height: 80, backgroundColor: '#878787', alignItems: 'center', justifyContent: 'center', borderRadius: 20, padding: 10, position: 'absolute', }}>
                        <Text style={{ color: '#FFFFFF', padding: 10, fontSize: 18 }}>Calender</Text>
                      </View>
                    </View>
                  </TouchableOpacity>


                  <TouchableOpacity
                    style={{ marginTop: 10 }}
                    activeOpacity={0.9}
                    onPress={() => this.props.navigation.navigate('UnitMyTimeSheet')}
                  >
                    <View >
                      <View >
                        <Image style={{ width: "70%", height: 80, padding: 20 }} source={require('./../../assets/tab_bg_a.png')} resizeMode='stretch' />
                        <Image style={{ width: 40, height: 40, position: 'absolute', start: '7.5%', top: 20 }} source={require('./../../assets/booking_icn.png')} resizeMode='stretch' />
                      </View>
                      <View style={{ width: '75%', height: 80, backgroundColor: '#878787', alignItems: 'center', justifyContent: 'center', borderRadius: 20, padding: 20, position: 'absolute', end: 5 }}>
                        <Text style={{ color: '#FFFFFF', padding: 10, fontSize: 18 }}>My Time Sheet</Text>
                      </View>
                    </View>
                  </TouchableOpacity>

                  {/* <TouchableOpacity
                    style={{ marginTop: 10 }}
                    activeOpacity={0.9}
                    onPress={() => this.props.navigation.navigate('UnitProfile')}
                  >
                    <View >
                      <View >
                        <Image style={{ width: '70%', marginLeft: '29%', height: 80, padding: 20, }} source={require('./../../assets/tab_bg_a.png')} resizeMode='stretch' />
                        <Image style={{ width: 40, height: 40, position: 'absolute', end: '7.5%', top: 20 }} source={require('./../../assets/profile_icn_c.png')} resizeMode='stretch' />
                      </View>
                      <View style={{ width: '75%', height: 80, backgroundColor: '#878787', alignItems: 'center', justifyContent: 'center', borderRadius: 20, padding: 10, position: 'absolute' }}>
                        <Text style={{ color: '#FFFFFF', padding: 10, fontSize: 18 }}>Profile</Text>
                      </View>
                    </View>
                  </TouchableOpacity> */}


                </View>
              </View>
            </ScrollView>
          </View>
        </SafeAreaView>
      </Fragment>
    );
  }


}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 250,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10,
    textAlign: 'center',

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },

});

export default connect(null, mapDispatchToProps)(UnitHome);
