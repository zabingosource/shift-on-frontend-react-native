/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';




const { width } = Dimensions.get('window');

const Jobdata = [
  {
    storename: 'Store Item 1',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  }, {
    storename: 'Store Item 1',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 2',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 3',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 4',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 5',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 6',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 7',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
];

export default class WorkerProfileAgency extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      selectedTab: '2'
    };
    console.log(this.props.navigation);
  }



  componentDidMount() {

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

          <View style={styles.mainContainer}>
            <View style={{ backgroundColor: '#00D5E1' }}>
              <TouchableOpacity
                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
              </TouchableOpacity>
              <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>AGENCY</Text>

            </View>


            <View style={{ marginLeft: 10, marginRight: 10 }}>

              <View style={{ flexDirection: 'row', marginTop: 20, backgroundColor: '#EAEAEA', borderRadius: 40 }}>

                <Image style={{ height: 30, width: 30, padding: 5, position: 'absolute', end: 5, top: 3.5 }} source={require('./../../assets/search_icn.png')} />

                <TextInput
                  style={{ width: '85%', padding: 5, marginLeft: 10, marginRight: 40, color: '#969696', fontFamily: 'montserrat_regular', fontSize: 15 }}
                  placeholder='Search by agency name'
                  keyboardType='default'
                  value={this.state.email}
                  returnKeyType={'done'}
                  onChangeText={(text) => {
                    this.setState({
                      email: text,
                    });
                  }}
                />


              </View>
              <View style={{ marginTop: 20, marginBottom: 20 }}>
                <FlatList
                  style={{ width: '100%', marginBottom: 180 }}
                  data={Jobdata}
                  keyExtractor={(item, index) => index.toString()}
                  //ItemSeparatorComponent={this.ItemSeparator}
                  renderItem={({ item, index }) =>
                    <TouchableOpacity
                      activeOpacity={0.9}
                      onPress={() => this.props.navigation.navigate('MyOrderDetails', { order: item })}
                    >
                      <View style={styles.rowContainer}>
                        <Image style={{ height: 110, width: 100, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={require('./../../assets/hospital_green.jpg')} />
                        <View style={{ width: "74%", padding: 10, }}>
                          <Text
                            style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                          >Agency Name abc</Text>
                          <Text
                            numberOfLines={1}
                            style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 12, }}
                          >The Royal London Hospital </Text>

                          <View>
                            {this._displayTag("requested")}
                          </View>

                          {/*  <View style={{ width:200, marginTop: 20, flexDirection: "row", marginLeft:2,backgroundColor:'#ECAF00',borderRadius:20,padding:8}}>
                            <Image  source={require('./../../assets/requested_icn.png')} style={{ width: 15, height: 15 ,alignSelf:'center'}} resizeMode='stretch' />
                            <Text  style={{alignSelf:'center', marginLeft: 5, color: '#FFFFFF', fontFamily: 'montserrat_regular', fontSize: 12 }}>REQUESTED</Text>
                          </View>  */}

                          {/*  <View style={{ width:200, marginTop: 20, flexDirection: "row", marginLeft:2,backgroundColor:'#657CF4',borderRadius:20,padding:8}}>
                            <Image  source={require('./../../assets/applyAgency_icn.png')} style={{ width: 15, height: 15 ,alignSelf:'center'}} resizeMode='stretch' />
                            <Text  style={{alignSelf:'center', marginLeft: 5, color: '#FFFFFF', fontFamily: 'montserrat_regular', fontSize: 12 }}>APPLY FOR THE AGENCY</Text>
                          </View> */}

                        </View>
                      </View>
                    </TouchableOpacity>
                  }
                />
              </View>

              <View>



              </View>

            </View>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }




  _displayTag(tag) {
    if (tag == 'confirm') {
      return (<View style={{ flexDirection: 'row', marginTop: 10 }}>
        <Text style={{ flex: 1.5, marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>Agency Accepted your request</Text>

        <View style={{ flexDirection: "row", marginLeft: 2, backgroundColor: '#00CC67', borderRadius: 20, padding: 10 }}>
          <Image source={require('./../../assets/confirm_icn.png')} style={{ width: 15, height: 15, alignSelf: 'center' }} resizeMode='stretch' />
          <Text style={{ alignSelf: 'center', marginLeft: 5, color: '#FFFFFF', fontFamily: 'montserrat_regular', fontSize: 10 }}>CONFIRM</Text>
        </View>

      </View>);
    } else if (tag == 'requested') {
      return (<View style={{ width: 200, marginTop: 15, flexDirection: "row", marginLeft: 2, backgroundColor: '#ECAF00', borderRadius: 20, padding: 8 }}>
        <Image source={require('./../../assets/requested_icn.png')} style={{ width: 15, height: 15, alignSelf: 'center' }} resizeMode='stretch' />
        <Text style={{ alignSelf: 'center', marginLeft: 5, color: '#FFFFFF', fontFamily: 'montserrat_regular', fontSize: 10 }}>REQUESTED</Text>
      </View>);
    } else {
      return (<View style={{ width: 200, marginTop: 15, flexDirection: "row", marginLeft: 2, backgroundColor: '#657CF4', borderRadius: 20, padding: 8 }}>
        <Image source={require('./../../assets/applyAgency_icn.png')} style={{ width: 15, height: 15, alignSelf: 'center' }} resizeMode='stretch' />
        <Text style={{ alignSelf: 'center', marginLeft: 5, color: '#FFFFFF', fontFamily: 'montserrat_regular', fontSize: 10 }}>APPLY FOR THE AGENCY</Text>
      </View>);
    }
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },

  tabHolder: {
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 30,
    flex: 1
  },
  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5


  },


});

