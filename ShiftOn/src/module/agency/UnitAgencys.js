/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert,
  SafeAreaView, Dimensions, TextInput, ToastAndroid

} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ReactNativeModal from 'react-native-modal';
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';
import { connect } from 'react-redux';


const mapStateToPrpos = (props) => {
  return {
    userid: props.userLoginDetails.id,
    imgUrl: props.defaultImageUrl.url,
  }
}

const { width } = Dimensions.get('window');

class UnitAgencys extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listingData: [],
      isLoading: false,

    };
    console.log(this.props.navigation);
  }

  componentDidMount() {
    this.redirectPage();
    this._unitAgencyListing();
  }

  redirectPage = async () => {
    try {
      await AsyncStorage.setItem('nextScreen', '');
    } catch (e) {
      console.log(e);
    }
  }

  _unitAgencyListing = () => {
    this.setState({ isLoading: true });

    var formData = new FormData();
    formData.append("unit_id", this.props.userid);

    Api._unitAgencyListings(formData)
      .then((response) => {

        console.log(response.data)
        var tempData = [];
        for (var i = 0; i < response.data.length; i++) {
          if (response.data[i].agency != null) {
            tempData.push(response.data[i])
          }
        }
        this.setState({ listingData: tempData, isLoading: false });
        //setToastMsg(response.message.toString());

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  isApproved = (item, status) => {
    this.setState({ isLoading: true });
    let rawData = {
      "agency_id": item.agency_id,
      "unit_id": this.props.userid,
      "status": status,
    }

    Api._unitAgencyListingApproval(rawData)
      .then((response) => {

        console.log(response);
        this._unitAgencyListing();
        //setToastMsg(response.message.toString());

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

          <View style={styles.mainContainer}>
            <View style={{ backgroundColor: '#00D5E1' }}>
              <TouchableOpacity
                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
              </TouchableOpacity>
              <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>AGENCIES</Text>

            </View>
            <Loader isLoading={this.state.isLoading} />
            <View style={{ marginLeft: 10, marginRight: 10 }}>

              <View style={{ marginTop: 10, marginBottom: 20 }}>

                {
                  (this.state.listingData.length > 0) ?

                    (<FlatList
                      style={{ width: '100%', marginBottom: '12%' }}
                      data={this.state.listingData}
                      keyExtractor={(item, index) => index.toString()}
                      //ItemSeparatorComponent={this.ItemSeparator}
                      renderItem={({ item, index }) =>

                        <TouchableOpacity
                          activeOpacity={0.9}
                          onPress={() => this.props.navigation.navigate('ViewAgencyDetail', { agencyId: item.agency.id })}
                        >
                          <View style={styles.rowContainer}>

                            {
                              (item.agency.image) ?
                                (<Image style={{ height: 120, width: 90, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={{ uri: AppConstants.IMAGE_URL + item.agency.image }} resizeMode='cover' />)
                                :
                                (<Image style={{ height: 120, width: 90, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={{ uri: this.props.imgUrl }} resizeMode='cover' />)
                            }

                            <View style={{ width: "57%", padding: 7, }}>
                              <Text numberOfLines={1}
                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                              >{item.agency.name}</Text>
                              <Text
                                numberOfLines={1}
                                style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12, }}
                              >{item.agency.trading_name} </Text>

                              <View style={{ marginTop: 6, flexDirection: "row" }}>
                                <Image source={require('./../../assets/email_icon.png')} style={{ width: 12, height: 12, alignSelf: 'center', }} resizeMode='stretch' />
                                <Text numberOfLines={1}
                                  style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10, }}
                                >{item.agency.email} </Text>
                              </View>

                              <View style={{ marginTop: 3, flexDirection: "row" }}>
                                <Image source={require('./../../assets/pin_icn.png')} style={{ width: 10, height: 15, alignSelf: 'center' }} resizeMode='stretch' />
                                <Text numberOfLines={1}
                                  style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 11, }}
                                >{item.agency.address} </Text>
                              </View>

                              <View style={{ marginTop: 3, flexDirection: "row" }}>
                                <Image source={require('./../../assets/pin_icn.png')} style={{ width: 10, height: 15, alignSelf: 'center' }} resizeMode='stretch' />
                                <Text numberOfLines={1}
                                  style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 11, }}
                                >{item.agency.landmark ? item.agency.landmark : ''}{item.agency.post_code ? ", " + item.agency.post_code : ''}</Text>
                              </View>

                            </View>
                            {
                              this.actionButtons(item, item.status)
                            }

                          </View>
                        </TouchableOpacity>
                      }
                    />)
                    :
                    (<View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                      <Text
                        numberOfLines={1}
                        style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                      >No Data Available!</Text>
                    </View>)
                }
              </View>

              <View>

              </View>

            </View>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  actionButtons = (item, status) => {
    if (item.agency_status.toString() == "1") {

      if (status.toString() == "0") {
        return (
          <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', marginBottom: 5, right: 0 }}>
            <TouchableOpacity
              style={{ padding: 7, backgroundColor: '#657CF4', borderRadius: 10, }}
              onPress={() => this.isApproved(item, '1')}
            >
              <Text style={{ color: '#FFFFFF', fontFamily: 'montserrat_semibold', fontSize: 10, }}>
                APPROVE
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{ padding: 7, backgroundColor: '#918e8e', borderRadius: 10, marginTop: 3, }}
              onPress={() => this.isApproved(item, '2')}
            >
              <Text style={{ color: '#FFFFFF', fontFamily: 'montserrat_semibold', fontSize: 10, }}>
                REJECT
              </Text>
            </TouchableOpacity>

          </View>
        )
      } else if (status.toString() == "1") {
        return (
          <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', marginBottom: 5, right: 0 }}>
            <TouchableOpacity
              style={{ padding: 7, backgroundColor: '#a1322a', borderRadius: 10, }}
              onPress={() => this.isApproved(item, '3')}
            >
              <Text style={{ color: '#FFFFFF', fontFamily: 'montserrat_semibold', fontSize: 10, }}>
                BLOCK
              </Text>
            </TouchableOpacity>

          </View>
        )
      } else if (status.toString() == "2") {
        return (
          <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', marginBottom: 11, right: 0 }}>
            <Text style={{ color: '#a1322a', fontSize: 11, fontFamily: 'montserrat_semibold' }}>
              REJECTED
            </Text>

          </View>
        )
      } else if (status.toString() == "3") {
        return (
          <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', marginBottom: 5, right: 0 }}>
            <TouchableOpacity
              style={{ padding: 7, backgroundColor: '#393d52', borderRadius: 10, }}
              onPress={() => this.isApproved(item, '1')}
            >
              <Text style={{ color: '#FFFFFF', fontFamily: 'montserrat_semibold', fontSize: 10, }}>
                UNBLOCK
              </Text>
            </TouchableOpacity>

          </View>
        )
      }
    } else {
      return (
        <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', marginBottom: 11 }}>
          <View>
            <Text style={{ color: '#dc3546', fontSize: 10, fontFamily: 'montserrat_regular', textAlign: 'center' }}>
              DISABLED
            </Text>
            <Text style={{ color: '#dc3546', fontSize: 10, fontFamily: 'montserrat_regular', textAlign: 'center' }}>
              BY AGENCY
            </Text>
          </View>

        </View>
      )
    }
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },

  tabHolder: {
    padding: 1,
    backgroundColor: "#00D5E1",
    borderRadius: 30,
    flex: 1
  },
  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5
  },

  headerContainer: {
    height: 100,
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderColor: '#EAEAEA',
    borderBottomWidth: 2,
    marginBottom: 5,
    padding: 10


  },
  loginHold: {
    flex: 1,
    padding: 5,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginRight: 5
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 15,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center',
    padding: 5

  },
  registerHold: {
    flex: 1,
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginLeft: 5
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 15,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center',
    padding: 5,

  },
  radioIcon: {
    width: 20,
    height: 20,
    marginRight: 5,
    alignSelf: 'center'

  },
  filterListItem: {
    padding: 5,
    flexDirection: 'row',
    marginLeft: 8,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  starStyle: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    margin: 2

  },
});

export default connect(mapStateToPrpos)(UnitAgencys);