/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView,
    Dimensions, TextInput, ToastAndroid, Platform
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import { setToastMsg } from '../../utils/ToastMessage';
import Loader from '../../utils/Loader';
import { connect } from 'react-redux';

const mapStateToPrpos = (props) => {
    return {
        userid: props.userLoginDetails.id,
        imgUrl: props.defaultImageUrl.url,
    }
}


const { width } = Dimensions.get('window');

class WorkerAddNewAgency extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchKeyword: '',
            searchResultMessage: 'No Data Available!',
            isLoading: false,
            agencyList: [],
            isJoiningOrNot: true,
        };
        //console.log(this.props.navigation);
    }


    componentDidMount() {

    }

    _workerSearchAgency = () => {
        this.setState({ isLoading: true });

        let formData = new FormData();
        formData.append('agency_name', this.state.searchKeyword);
        formData.append('worker_id', this.props.userid);

        Api._workerSearchAgency(formData)
            .then((response) => {

                console.log(response.data)

                if (response.status.toString() == "0") {
                    this.setState({ searchResultMessage: response.message.toString() });
                }
                this.setState({ agencyList: response.data, isLoading: false });
                if (response.data.length < 1) {
                    setToastMsg('No data found.');
                } else {
                    setToastMsg(response.message.toString());
                }


            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false })
            });
    }


    // checking for profile is complete or not...

    _checkWorkerProfileCompletion = (agencyId) => {
        this.setState({ isLoading: true })
        let rawData = {
            "worker_id": this.props.userid,
        }
        Api._checkWorkerProfileCompletion(rawData)
            .then((response) => {
                console.log(response.data)

                if (response.data.WorkerAcademic.length <= 0 || response.data.WorkerEmployment == null ||
                    response.data.WorkerProfessional.length <= 0 || response.data.WorkerNmsSkills == null ||
                    response.data.WorkerRefrences.length <= 0 || response.data.WorkerKins == null ||
                    response.data.WorkerCriminalRecords == null || response.data.WorkerEmploymentAct == null ||
                    response.data.WorkerEmploymentHistory.length <= 0) {

                    this.setState({ isLoading: false })
                    setToastMsg("Please complete your profile first.");
                } else {
                    this._workerJoinAgency(agencyId)
                }

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false });
            });
    }

    _workerJoinAgency = (agencyId) => {

        let formData = new FormData();
        formData.append('worker_id', this.props.userid);
        formData.append('agency_id', agencyId);

        Api._workerJoinAgency(formData)
            .then((response) => {

                console.log(response)
                this.setState({ isLoading: false })
                setToastMsg(response.message.toString());
                this.props.navigation.goBack()

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false })
            });
    }

    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

                    <View style={styles.mainContainer}>
                        <View style={{ backgroundColor: '#00D5E1' }}>
                            <TouchableOpacity
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                                onPress={() => this.props.navigation.goBack()}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                            </TouchableOpacity>
                            <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>ADD AGENCY</Text>
                        </View>

                        <Loader isLoading={this.state.isLoading} />

                        <View style={{ marginLeft: 10, marginRight: 10 }}>

                            <View style={{ flexDirection: 'row', marginTop: 20, backgroundColor: '#EAEAEA', borderRadius: 40 }}>

                                <TouchableOpacity style={{ padding: 5, position: 'absolute', end: 2, top: -1 }}
                                    onPress={() => this._workerSearchAgency()}>
                                    <Image style={{ height: 30, width: 30, }}
                                        source={require('./../../assets/search_icn.png')}
                                    />
                                </TouchableOpacity>

                                <TextInput
                                    style={{ width: '85%', paddingHorizontal: 8, paddingTop: 8, paddingBottom: 10, marginLeft: 10, marginRight: 40, color: '#969696', fontFamily: 'montserrat_regular', fontSize: 15 }}
                                    placeholder='Search by agency name'
                                    keyboardType='default'
                                    value={this.state.email}
                                    returnKeyType={'done'}
                                    onChangeText={(text) => {
                                        this.setState({
                                            searchKeyword: text,
                                        });
                                    }}
                                />


                            </View>
                            <View style={{ marginTop: 20, marginBottom: 20 }}>

                                {
                                    (this.state.agencyList.length > 0) ?

                                        (<FlatList
                                            style={{ width: '100%', marginBottom: 160 }}
                                            data={this.state.agencyList}
                                            keyExtractor={(item, index) => index.toString()}
                                            //ItemSeparatorComponent={this.ItemSeparator}
                                            renderItem={({ item, index }) =>
                                                <TouchableOpacity
                                                    activeOpacity={0.9}
                                                >
                                                    <View style={styles.rowContainer}>
                                                        {/* <Image style={{ height: 100, width: 100, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={require('./../../assets/hospital_green.jpg')} /> */}
                                                        {
                                                            (item.image) ?
                                                                (<Image style={{ height: 100, width: 100, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, }} source={{ uri: AppConstants.IMAGE_URL + item.image }} />)
                                                                :
                                                                (<Image style={{ height: 100, width: 100, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, }} source={{ uri: this.props.imgUrl }} />)
                                                        }
                                                        <View style={{ width: "74%", padding: 10, }}>
                                                            <Text
                                                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                                                            >{item.name}</Text>
                                                            {
                                                                item.already_added == "1" ?
                                                                    <TouchableOpacity >
                                                                        <View style={{ width: 200, marginTop: 20, flexDirection: "row", backgroundColor: '#ffffff', borderRadius: 20, padding: 8 }}>
                                                                            {/* <Image source={require('./../../assets/applyAgency_icn.png')} style={{ width: 15, height: 15, alignSelf: 'center' }} resizeMode='stretch' /> */}
                                                                            <Text style={{ alignSelf: 'center', marginLeft: 5, color: '#ff0000', fontFamily: 'montserrat_regular', fontSize: 10 }}>This worker is already associated with this agency.</Text>

                                                                        </View>
                                                                    </TouchableOpacity>
                                                                    :
                                                                    <TouchableOpacity
                                                                        onPress={() => this._checkWorkerProfileCompletion(item.id)}
                                                                    >
                                                                        <View style={{ width: 200, marginTop: 20, flexDirection: "row", backgroundColor: '#657CF4', borderRadius: 20, padding: 8 }}>
                                                                            <Image source={require('./../../assets/applyAgency_icn.png')} style={{ width: 15, height: 15, alignSelf: 'center' }} resizeMode='stretch' />
                                                                            <Text style={{ alignSelf: 'center', marginLeft: 5, color: '#FFFFFF', fontFamily: 'montserrat_regular', fontSize: 10 }}>APPLY FOR THE AGENCY</Text>

                                                                        </View>
                                                                    </TouchableOpacity>
                                                            }


                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                            }
                                        />)
                                        :
                                        (<View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                                            <Text
                                                numberOfLines={1}
                                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                                            >{this.state.searchResultMessage}</Text>
                                        </View>)
                                }
                            </View>

                            <View>

                            </View>

                        </View>

                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },

    tabHolder: {
        padding: 5,
        backgroundColor: "#00D5E1",
        borderRadius: 30,
        flex: 1
    },
    rowContainer: {
        flexDirection: 'row',
        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        borderColor: '#EAEAEA',
        borderWidth: 2,
        marginBottom: 5


    },


});

export default connect(mapStateToPrpos)(WorkerAddNewAgency);

