/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView,
  Dimensions, TextInput, ToastAndroid, Platform
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Api from '../../apis/Api';
import { setToastMsg } from '../../utils/ToastMessage';
import AppConstants from '../../apis/AppConstants';
import Loader from '../../utils/Loader';
import { connect } from 'react-redux';



const mapStateToPrpos = (props) => {

  return {
    userid: props.userLoginDetails.id,
    imgUrl: props.defaultImageUrl.url,
  }

}


const { width } = Dimensions.get('window');


class WorkerAgency extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchKeyword: '',
      selectedTab: '1',
      isLoading: true,
      agencyList: [],
      pendingAgencyList: [],
      acceptAgencyList: [],
      declinedAgencyList: [],
    };
    //console.log(this.props.navigation);
  }


  componentDidMount() {
    this.redirectPage();

    this.navFocusListener = this.props.navigation.addListener('didFocus', () => {
      this._workerAgencyListing();
    });
  }

  componentWillUnmount() {
    this.navFocusListener.remove();
  }

  redirectPage = async () => {
    try {
      await AsyncStorage.setItem('nextScreen', '');
    } catch (e) {
      console.log(e);
    }
  }

  _workerAgencyListing = () => {

    this.setState({ isLoading: true, selectedTab: "1" });
    var formData = new FormData();
    formData.append("worker_id", this.props.userid);

    Api._workerAgencyListing(formData)
      .then((response) => {

        console.log(response.data);

        if (response.status.toString() == "1") {
          this.pendingDataList(response.data);
          this.acceptDataList(response.data);
          this.declinedDataList(response.data);
        }
        //setToastMsg(response.message.toString());
        this.setState({ isLoading: false });

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  pendingDataList = (responseData) => {
    var tempData = [];
    for (var i = 0; i < responseData.length; i++) {
      if (responseData[i].status.toString() == "0") {
        if (responseData[i].agency != null) {
          tempData.push(responseData[i])
        }
      }
    }
    this.setState({
      agencyList: tempData,
      pendingAgencyList: tempData,
    })
  }

  acceptDataList = (responseData) => {
    var tempData = [];
    for (var i = 0; i < responseData.length; i++) {
      if (responseData[i].status.toString() == "1") {
        if (responseData[i].agency != null) {
          tempData.push(responseData[i])
        }
      }
    }
    this.setState({ acceptAgencyList: tempData })
  }

  declinedDataList = (responseData) => {
    var tempData = [];
    for (var i = 0; i < responseData.length; i++) {
      if (responseData[i].status.toString() == "2") {
        if (responseData[i].agency != null) {
          tempData.push(responseData[i])
        }
      }
    }
    this.setState({ declinedAgencyList: tempData })
  }

  _workerAgencyListUpdateStatus = (id, status) => {
    this.setState({ isLoading: true });

    var rawData = {
      "id": id,
      "status": status
    }
    Api._workerAgencyListUpdateStatus(rawData)
      .then((response) => {

        console.log(response);

        if (response.status.toString() == "1") {
          this._workerAgencyListing();
        }
        setToastMsg(response.message.toString());

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

          <View style={styles.mainContainer}>
            <View style={{ backgroundColor: '#00D5E1' }}>
              <TouchableOpacity
                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
              </TouchableOpacity>
              <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>AGENCY REQUEST</Text>
              <TouchableOpacity
                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20, right: 0 }}
                onPress={() => this.props.navigation.navigate('WorkerAddNewAgency')}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/more_icn.png')} />
              </TouchableOpacity>
            </View>

            <Loader isLoading={this.state.isLoading} />

            <View style={{ marginLeft: 10, marginRight: 10 }}>
              <View style={{ flexDirection: 'row', marginTop: 20, backgroundColor: '#626262', borderRadius: 40 }}>
                <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "1" ? "#00D5E1" : "#626262" }]}
                  activeOpacity={1}
                  onPress={() => this._onChangeTab("1")}
                >
                  <Text style={{ alignSelf: 'center', fontSize: 14, padding: 7, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "1" ? "#000000" : "#FFFFFF" }}>PENDING </Text>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "2" ? "#00D5E1" : "#626262" }]}
                  activeOpacity={1}
                  onPress={() => this._onChangeTab("2")}
                >
                  <Text style={{ alignSelf: 'center', fontSize: 14, padding: 7, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "2" ? "#000000" : "#FFFFFF" }}>ACCEPTED </Text>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "3" ? "#00D5E1" : "#626262" }]}
                  activeOpacity={1}
                  onPress={() => this._onChangeTab("3")}
                >
                  <Text style={{ alignSelf: 'center', fontSize: 14, padding: 7, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "3" ? "#000000" : "#FFFFFF" }}>DECLINED </Text>
                </TouchableOpacity>

              </View>

              <View style={{ marginTop: 10, marginBottom: 20 }}>

                {
                  (this.state.agencyList.length > 0) ?

                    (<FlatList
                      style={{ width: '100%', marginBottom: 170 }}
                      data={this.state.agencyList}
                      keyExtractor={(item, index) => index.toString()}
                      //ItemSeparatorComponent={this.ItemSeparator}
                      renderItem={({ item, index }) =>

                        <TouchableOpacity
                          activeOpacity={0.9}
                        >

                          <View style={styles.rowContainer}>

                            {
                              (item.agency.image) ?
                                (<Image style={{ height: 100, width: 90, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={{ uri: AppConstants.IMAGE_URL + item.agency.image }} />)
                                :
                                (<Image style={{ height: 100, width: 90, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={{ uri: this.props.imgUrl }} />)
                            }

                            <View style={{ width: "57%", padding: 10, }}>
                              <Text
                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                              >{item.agency.name}</Text>
                              <Text
                                numberOfLines={1}
                                style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 10, }}
                              >{item.agency.trading_name} </Text>
                              <Text
                                numberOfLines={1}
                                style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 10, }}
                              >{item.agency.address} </Text>
                              <Text
                                numberOfLines={1}
                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                              >{item.agency.landmark} ,{item.agency.post_code}</Text>
                            </View>

                            {
                              this.state.selectedTab == "1" ?
                                (
                                  item.requested_by == "agency" ?
                                    <View style={{ justifyContent: 'flex-end', alignItems: 'center', marginBottom: 5, }}>
                                      <TouchableOpacity
                                        style={{ padding: 7, backgroundColor: '#657CF4', borderRadius: 10, marginBottom: 3, }}
                                        onPress={() => this._workerAgencyListUpdateStatus(item.id, "1")}
                                      >
                                        <Text style={{ color: '#FFFFFF', fontFamily: 'montserrat_semibold', fontSize: 10, }}>
                                          ACCEPT
                                        </Text>
                                      </TouchableOpacity>

                                      <TouchableOpacity
                                        style={{ padding: 7, backgroundColor: '#cc0000', borderRadius: 10, }}
                                        onPress={() => this._workerAgencyListUpdateStatus(item.id, "2")}
                                      >
                                        <Text style={{ color: '#FFFFFF', fontFamily: 'montserrat_semibold', fontSize: 10, }}>
                                          CANCEL
                                        </Text>
                                      </TouchableOpacity>
                                    </View>
                                    :
                                    <View></View>
                                )
                                :
                                <View></View>
                            }

                          </View>
                        </TouchableOpacity>
                      }
                    />)
                    :
                    (<View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                      <Text
                        numberOfLines={1}
                        style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                      >No Data Available!</Text>
                    </View>)
                }
              </View>

            </View>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _onChangeTab(tab) {
    if (tab == "1") {
      this.setState({
        selectedTab: tab,
        agencyList: this.state.pendingAgencyList,
      });
    } else if (tab == "2") {
      this.setState({
        selectedTab: tab,
        agencyList: this.state.acceptAgencyList,
      });
    } else {
      this.setState({
        selectedTab: tab,
        agencyList: this.state.declinedAgencyList,
      });
    }

  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },

  tabHolder: {
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 30,
    flex: 1
  },
  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5


  },


});

export default connect(mapStateToPrpos)(WorkerAgency);

