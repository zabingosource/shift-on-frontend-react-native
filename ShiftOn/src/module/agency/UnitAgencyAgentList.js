/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput,

} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import ReactNativeModal from 'react-native-modal';




const { width } = Dimensions.get('window');

const Jobdata = [
  {
    storename: 'Store Item 1',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  }, {
    storename: 'Store Item 1',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 2',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 3',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 4',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 5',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 6',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 7',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
];


export default class UnitAgencyAgentList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      selectedTab: '1',
      selectedFilterTab: '1',
      modal_vissible: false,
      shortOptionDate: [],
      customStartDate: '',
      customEndDate: '',
      shortItem: '',
      selectedStar: [false, false, false, false, false]
    };
    console.log(this.props.navigation);
  }



  componentDidMount() {
    var shortOptionDate = [
      {
        option: 'All Time       ',
        isSelected: false
      }, {
        option: 'Last Month ',
        isSelected: false
      }, {
        option: 'Last Week  ',
        isSelected: false
      }, {
        option: 'Last 24 Hrs ',
        isSelected: false
      }, {
        option: 'Custom       ',
        isSelected: false
      },
    ];
    var temp = []
    this.setState({ shortOptionDate: [] });
    if (!this.state.shortOptionDate.length > 0) {
      for (var i = 0; i < shortOptionDate.length; i++) {
        temp.push(shortOptionDate[i])
      }
      this.setState({ shortOptionDate: temp });
    }

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

          <View style={styles.mainContainer}>
            <View style={{ backgroundColor: '#00D5E1' }}>
              <TouchableOpacity
                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
              </TouchableOpacity>
              <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>AVAILABILITY LIST</Text>

            </View>


            <TouchableOpacity
              activeOpacity={0.9}
            //onPress={() => this.props.navigation.navigate('Login')}
            >
              <View style={styles.headerContainer}>
                <Image style={{ height: 60, width: 60, padding: 10, alignSelf: 'center', marginLeft: 10 }} source={require('./../../assets/viewalljobs_a.png')} />
                <View style={{ marginLeft: 10, alignSelf: 'center', width: '58%' }}>
                  <Text style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 20, }}
                  >View All Jobs</Text>

                  <Text style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 15, }}
                  >Last Week</Text>

                </View>
                <TouchableOpacity
                  style={{ marginTop: 5, }}
                  onPress={() => this._showModal(true)}
                >
                  <Image style={{ height: 30, width: 80, padding: 5 }} source={require('./../../assets/filter_btn_a.png')} resizeMode='stretch' />
                </TouchableOpacity>


              </View>
            </TouchableOpacity>

            <ReactNativeModal
              style={{ justifyContent: 'center' }}
              isVisible={this.state.modal_vissible}
              onBackdropPress={() => this._showModal(!this.state.modal_vissible)}
              onBackButtonPress={() => this._showModal(!this.state.modal_vissible)}
            >
              <View style={{ backgroundColor: '#EBEBEB', marginLeft: 40, marginRight: 40, borderRadius: 10 }}>
                <Text style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 16, padding: 10, alignSelf: 'center' ,marginTop:10}}
                >Filter By</Text>

                <View style={{ flexDirection: 'row', margin: 10, backgroundColor: '#626262', borderRadius: 40 }}>
                  <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedFilterTab == "1" ? "#00D5E1" : "#626262" }]}
                    activeOpacity={1}
                    onPress={() => this._resetDateRatting("1")}
                  >
                    <Text style={{ alignSelf: 'center', fontSize: 16, padding: 10, fontFamily: 'montserrat_regular', color: this.state.selectedFilterTab == "1" ? "#000000" : "#FFFFFF" }}>DATE </Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedFilterTab == "2" ? "#00D5E1" : "#626262" }]}
                    activeOpacity={1}
                    onPress={() => this._resetDateRatting("2")}
                  >
                    <Text style={{ alignSelf: 'center', fontSize: 16, padding: 10, fontFamily: 'montserrat_regular', color: this.state.selectedFilterTab == "2" ? "#000000" : "#FFFFFF" }}>RATING</Text>
                  </TouchableOpacity>

                </View>
                {
                  (this.state.selectedFilterTab == 1) ? (<View>
                    <FlatList
                      style={{ width: '100%', marginTop: 5 }}
                      data={this.state.shortOptionDate}
                      keyExtractor={(item, index) => index.toString()}
                      ItemSeparatorComponent={this.ItemSeparator}
                      renderItem={({ item, index }) =>

                        <TouchableOpacity
                          activeOpacity={0.7}
                          onPress={() => this._optionDate(item)}
                        >
                          <View style={styles.filterListItem}>
                            {
                              (item.isSelected) ? (<Image style={styles.radioIcon} source={require('../../assets/selecttime_on_icn.png')} />) : (<Image style={styles.radioIcon} source={require('../../assets/selecttime_off_icn.png')} />)
                            }
                            <View style={{ justifyContent: 'center' }}>
                              <Text style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 15, padding: 10, }}>{item.option}</Text>

                            </View>


                          </View>

                        </TouchableOpacity>
                      }
                    />
                    {
                      (this.state.shortItem == 'Custom') ? (<View>
                        <TextInput
                          editable={true}
                          style={{
                            fontSize: 18,
                            padding: 2,
                            backgroundColor: '#FFFFFF',
                            borderColor: '#00D5E1',
                            borderRadius: 20,
                            borderWidth: 2,
                            paddingLeft: 30,
                            paddingRight: 30,
                            marginLeft: 20,
                            marginRight: 20,
                            color: '#464646',
                            fontFamily: "montserrat_regular"
                          }}
                          placeholder={'DD/MM/YYYY'}
                          value={this.state.customStartDate}
                          showSoftInputOnFocus={false}
                          onChangeText={(text) => {
                            this.setState({
                              customStartDate: text,
                            });
                          }}
                          underlineColorAndroid="transparent"
                          autoCorrect={false}
                          accessible={false}
                          onTouchEnd={() => this._setDate("start")}
                          caretHidden={true}
                        />
                        <Text style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 16, padding: 10, alignSelf: 'center' }}
                        >TO</Text>
                        <TextInput
                          editable={true}
                          style={{
                            fontSize: 18,
                            padding: 2,
                            backgroundColor: '#FFFFFF',
                            borderColor: '#00D5E1',
                            borderRadius: 20,
                            borderWidth: 2,
                            paddingLeft: 30,
                            paddingRight: 30,
                            color: '#464646',
                            marginLeft: 20,
                            marginRight: 20,
                            fontFamily: "montserrat_regular"
                          }}
                          placeholder={'DD/MM/YYYY'}
                          value={this.state.customEndDate}
                          showSoftInputOnFocus={false}
                          onChangeText={(text) => {
                            this.setState({
                              customStartDate: text,
                            });
                          }}
                          underlineColorAndroid="transparent"
                          autoCorrect={false}
                          accessible={false}
                          onTouchEnd={() => this._setDate("end")}
                          caretHidden={true}
                        />
                      </View>
                      ) : (<View></View>)
                    }


                  </View>
                  ) : (<View style={{ marginTop: 10, marginBottom: 10 }}>
                    <TouchableOpacity
                      style={{ alignSelf: "center", padding: 5 }}
                      onPress={() => this._selectStar(0)}
                    >
                      <View style={{ flexDirection: 'row' }}>
                        {
                          (this.state.selectedStar[0]) ? (<Image style={styles.radioIcon} source={require('../../assets/selecttime_on_icn.png')} />) : (<Image style={styles.radioIcon} source={require('../../assets/selecttime_off_icn.png')} />)
                        }
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{ alignSelf: "center", padding: 5 }}
                      onPress={() => this._selectStar(1)}
                    >
                      <View style={{ flexDirection: 'row' }}>
                        {
                          (this.state.selectedStar[1]) ? (<Image style={styles.radioIcon} source={require('../../assets/selecttime_on_icn.png')} />) : (<Image style={styles.radioIcon} source={require('../../assets/selecttime_off_icn.png')} />)
                        }
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{ alignSelf: "center", padding: 5 }}
                      onPress={() => this._selectStar(2)}
                    >
                      <View style={{ flexDirection: 'row' }}>
                        {
                          (this.state.selectedStar[2]) ? (<Image style={styles.radioIcon} source={require('../../assets/selecttime_on_icn.png')} />) : (<Image style={styles.radioIcon} source={require('../../assets/selecttime_off_icn.png')} />)
                        }
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{ alignSelf: "center", padding: 5 }}
                      onPress={() => this._selectStar(3)}
                    >
                      <View style={{ flexDirection: 'row' }}>
                        {
                          (this.state.selectedStar[3]) ? (<Image style={styles.radioIcon} source={require('../../assets/selecttime_on_icn.png')} />) : (<Image style={styles.radioIcon} source={require('../../assets/selecttime_off_icn.png')} />)
                        }
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{ alignSelf: "center", padding: 5 }}
                      onPress={() => this._selectStar(4)}
                    >
                      <View style={{ flexDirection: 'row' }}>
                        {
                          (this.state.selectedStar[4]) ? (<Image style={styles.radioIcon} source={require('../../assets/selecttime_on_icn.png')} />) : (<Image style={styles.radioIcon} source={require('../../assets/selecttime_off_icn.png')} />)
                        }
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                      </View>
                    </TouchableOpacity>

                  </View>

                    )
                }

                <View style={{ flexDirection: 'row', marginTop:20,marginBottom:30,marginRight:20,marginLeft:20 }}>
                  <TouchableOpacity style={styles.loginHold}
                    onPress={() => this._showModal(false)}
                  >
                    <Text style={styles.loginText}>CANCEL</Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={styles.registerHold}
                    onPress={() => this._showModal(false)}
                  >
                    <Text style={styles.registerText}>APPLY</Text>
                  </TouchableOpacity>

                </View>
              </View>
            </ReactNativeModal>








            <View style={{ marginLeft: 10, marginRight: 10 }}>

              <View style={{ flexDirection: 'row', marginTop: 10, backgroundColor: '#626262', borderRadius: 40 }}>
                <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "1" ? "#00D5E1" : "#626262" }]}
                  activeOpacity={1}
                  onPress={() => this._onChangeTab("1")}
                >
                  <Text style={{ alignSelf: 'center', fontSize: 16, padding: 10, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "1" ? "#000000" : "#FFFFFF" }}>AGENCY </Text>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "2" ? "#00D5E1" : "#626262" }]}
                  activeOpacity={1}
                  onPress={() => this._onChangeTab("2")}
                >
                  <Text style={{ alignSelf: 'center', fontSize: 16, padding: 10, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "2" ? "#000000" : "#FFFFFF" }}>WORKERS </Text>
                </TouchableOpacity>

              </View>


              {/* <View style={{ flexDirection: 'row', marginTop: 20, backgroundColor: '#EAEAEA', borderRadius: 40 }}>

                <Image style={{ height: 40, width: 40, padding: 10, position: 'absolute', end: 5, top: 3.5 }} source={require('./../../assets/search_icn.png')} />

                <TextInput
                  style={{ width: '85%', padding: 10, marginLeft: 10, marginRight: 40, color: '#969696', fontFamily: 'montserrat_regular', fontSize: 15 }}
                  placeholder='Search by job title'
                  keyboardType='default'
                  value={this.state.email}
                  returnKeyType={'done'}
                  onChangeText={(text) => {
                    this.setState({
                      email: text,
                    });
                  }}
                />


              </View> */}

              <View style={{ marginTop: 10, marginBottom: 20 }}>
                <FlatList
                  style={{ width: '100%', marginBottom: 260 }}
                  data={Jobdata}
                  keyExtractor={(item, index) => index.toString()}
                  //ItemSeparatorComponent={this.ItemSeparator}
                  renderItem={({ item, index }) =>

                    (this.state.selectedTab == '1') ? (<TouchableOpacity
                      activeOpacity={0.9}
                      onPress={() => this.props.navigation.navigate('MyOrderDetails', { order: item })}
                    >
                      <View style={styles.rowContainer}>
                        <Image style={{ height: 125, width: 90, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={require('./../../assets/hospital_green.jpg')} />
                        <View style={{ width: "65%", padding: 10, }}>
                          <Text
                            style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                          >Job Title abc</Text>
                          <Text
                            numberOfLines={1}
                            style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                          >The Royal London Hospital </Text>
                          <View style={{ marginTop: 5, flexDirection: "row" }}>
                            <Image source={require('./../../assets/pin_icn.png')} style={{ width: 10, height: 15, alignSelf: 'center' }} resizeMode='stretch' />
                            <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10, }}>White chaple Rd,Whitechaple,E1 1FR DD F1</Text>
                          </View>
                          <View >
                            <View style={{ marginTop: 5, flexDirection: "row", flex: 1.5, }}>
                              <Image source={require('./../../assets/calendar_icn_b.png')} style={{ width: 12, height: 12, alignSelf: 'center' }} resizeMode='stretch' />
                              <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>12 oct to 20 oct,2019</Text>
                            </View>
                            <View style={{ marginTop: 5, flexDirection: "row", flex: 1, }}>
                              <Image source={require('./../../assets/time_icn_a.png')} style={{ width: 12, height: 12, alignSelf: 'center' }} resizeMode='stretch' />
                              <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>9 AM to 6 PM</Text>
                            </View>

                          </View>

                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>

                          <Text style={{ color: '#FF7B68', fontFamily: 'montserrat_semibold', fontSize: 20, }}>
                            16
                                </Text>

                          <Text style={{ marginTop: -5, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 11, }}>
                            AGENT
                              </Text>
                        </View>

                      </View>
                    </TouchableOpacity>) : (<TouchableOpacity
                      activeOpacity={0.9}
                      onPress={() => this.props.navigation.navigate('MyOrderDetails', { order: item })}
                    >
                      <View style={styles.rowContainer}>


                        <Image style={{ height: 100, width: 90, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={require('./../../assets/woman_face.jpeg')} />
                        <View style={{ width: "65%", padding: 10, }}>
                          <Text
                            style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                          >Louie Potter</Text>
                          <Text
                            numberOfLines={1}
                            style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 10, }}
                          >Medical Assistants </Text>
                          <Text
                            numberOfLines={1}
                            style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 10, }}
                          >The Royal London Hospital </Text>
                          <Text
                            style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                          >ManPower Group LLC</Text>
                        </View>

                      </View>
                    </TouchableOpacity>)
                  }
                />
              </View>

              <View>



              </View>

            </View>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _resetDateRatting = (tab) => {
    this.setState({ selectedFilterTab: tab });
    if (tab == 1) {
      var starArray = [false, false, false, false, false]
      this.setState({ selectedStar: starArray });
    } else {
      var temp = []
      for (var i = 0; i < this.state.shortOptionDate.length; i++) {
        this.state.shortOptionDate[i].isSelected= false;
        temp.push(this.state.shortOptionDate[i])
      }
      this.setState({ shortOptionDate: temp ,shortItem:''});
    }

    
  }

  _selectStar = (statrno) => {

    if (statrno == 0) {
      var starArray = [true, false, false, false, false]
      this.setState({ selectedStar: starArray });
    } else if (statrno == 1) {
      var starArray = [false, true, false, false, false]
      this.setState({ selectedStar: starArray });
    } else if (statrno == 2) {
      var starArray = [false, false, true, false, false]
      this.setState({ selectedStar: starArray });
    } else if (statrno == 3) {
      var starArray = [false, false, false, true, false]
      this.setState({ selectedStar: starArray });
    } else if (statrno == 4) {
      var starArray = [false, false, false, false, true]
      this.setState({ selectedStar: starArray });
    }
  }

  _setDate = (type) => {
    if (type == 'start') {
      this.setState({ customStartDate: "15/09/2020" });
    } else {
      this.setState({ customEndDate: "20/09/2020" });
    }


  }

  _optionDate = (sortItem) => {
    var tempList = []
    for (var i = 0; i < this.state.shortOptionDate.length; i++) {
      if (sortItem.option == this.state.shortOptionDate[i].option) {
        this.state.shortOptionDate[i].isSelected = true;
      } else {
        this.state.shortOptionDate[i].isSelected = false;
      }
      tempList.push(this.state.shortOptionDate[i])
    }
    this.setState({ shortOptionDate: tempList, shortItem: sortItem.option.trim() });

  }


  _showModal(visible) {

    this.setState({ modal_vissible: visible });

  }


  _onChangeTab(tab) {
    this.setState({ selectedTab: tab });
  }

  

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },

  tabHolder: {
    padding: 1,
    backgroundColor: "#00D5E1",
    borderRadius: 30,
    flex: 1
  },
  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5
  },

  headerContainer: {
    height: 100,
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderColor: '#EAEAEA',
    borderBottomWidth: 2,
    marginBottom: 5,
    padding: 10


  },
  loginHold: {
    flex: 1,
    padding: 5,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginRight: 5
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 15,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center',
    padding:5

  },
  registerHold: {
    flex: 1,
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginLeft: 5
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 15,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center',
    padding:5,

  },
  radioIcon: {
    width: 20,
    height: 20,
    marginRight: 5,
    alignSelf: 'center'

  },
  filterListItem: {
    padding: 5,
    flexDirection: 'row',
    marginLeft: 8,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  starStyle: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    margin: 2

  },
});

