/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions,
    TextInput, ToastAndroid, Platform
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import Swipeout from 'react-native-swipeout';
import Api from '../../apis/Api';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
    return {
        userName: props.userLoginDetails.name,
        userid: props.userLoginDetails.id,
        userImage: props.userLoginDetails.image,
    }
}


class UnitPayRate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            apiData: [],
            unitName: ''
        };
        //console.log(this.props.navigation);
    }


    async componentDidMount() {
        let user_name = await AsyncStorage.getItem('user_name');
        this.setState({ unitName: user_name })
        this.navFocusListener = this.props.navigation.addListener('didFocus', () => {
            this._unitPayRatesListings();
        });
    }
    componentWillUnmount() {
        // this.navFocusListener.remove();
    }

    _unitPayRatesListings = () => {
        this.setState({ isLoading: true });

        var rawData = { "unit_id": this.props.userid };
        Api._unitPayRatesListings(rawData)
            .then((response) => {

                console.log(JSON.stringify(response.data));

                var tempArr = []
                for (let i = 0; i < response.data.length; i++) {
                    if (response.data[i].currencies) {
                        tempArr.push(response.data[i])
                    }
                }
                this.setState({ apiData: tempArr, isLoading: false });
                console.log('after filter', JSON.stringify(tempArr));

                if (response.status.toString() !== '1') {
                    setToastMsg(response.message.toString());
                }

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false });
            });
    }

    _active = (id, status) => {

        Alert.alert(
            '',
            'Are you sure?',
            [
                { text: 'NO', onPress: () => console.log('Close pressed') },
                {
                    text: 'YES', onPress: () => {
                        this.setState({ isLoading: true });

                        var rawData = {
                            "id": id,
                            "status": status,
                        }
                        Api._unitPayRatesStatusUpdate(rawData)
                            .then((response) => {

                                console.log(response.data);
                                this._unitPayRatesListings();
                                setToastMsg(response.message.toString());

                            })
                            .catch((err) => {
                                console.log(err);
                                setToastMsg("Somthing went wrong.");
                                this.setState({ isLoading: false });
                            });
                    }
                },
            ]
        )

    }


    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

                    <View style={styles.mainContainer}>
                        <View style={{ backgroundColor: '#00D5E1' }}>
                            <TouchableOpacity
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                                onPress={() => this.props.navigation.goBack()}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                            </TouchableOpacity>
                            <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>PAY RATES</Text>
                            <TouchableOpacity
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20, right: 0 }}
                                onPress={() => this.props.navigation.navigate('UnitAddPayRate', { payRateLists: this.state.apiData })}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/more_icn.png')} />
                            </TouchableOpacity>
                        </View>

                        <Loader isLoading={this.state.isLoading} />

                        <View style={{ margin: 20, }}>

                            {
                                (this.state.apiData.length > 0) ?

                                    (<FlatList
                                        style={{ width: '100%', marginBottom: '12%' }}
                                        data={this.state.apiData}
                                        keyExtractor={(item, index) => index.toString()}
                                        //ItemSeparatorComponent={this.ItemSeparator}
                                        renderItem={({ item, index }) =>
                                            <View style={{ borderRadius: 2, margin: 2 }}>
                                                <TouchableOpacity
                                                    activeOpacity={0.9}
                                                    onPress={() => { this.props.navigation.navigate('ViewPayRateDetail', { payRateData: item }) }}
                                                >
                                                    <View style={styles.rowContainer}>
                                                        {/* <View style={{ width: '14%', justifyContent: 'center', alignItems: 'center' }}>
                                                            <Image style={{ height: 38, width: 38, tintColor: '#000000' }} source={require('./../../assets/pay_rate_icn.png')} />
                                                        </View> */}
                                                        <View style={{ width: '72%', paddingStart: 5 }}>
                                                            <Text
                                                                numberOfLines={1}
                                                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                                                            >{item.role}</Text>

                                                            <Text
                                                                numberOfLines={1}
                                                                style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                                                            >
                                                                {item.currency ? item.currency + ' ' : ''}{item.rate_per_hour}
                                                            </Text>

                                                            <Text style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}>{this.state.unitName}</Text>

                                                            <Text
                                                                numberOfLines={2}
                                                                style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 14, }}
                                                            >
                                                                {item.currency ? item.currencies.address + ", " + item.currencies.landmark + ", " + item.currencies.post_code : ''}
                                                            </Text>

                                                        </View>
                                                        <View style={{ width: '28%', flexDirection: 'row', position: 'absolute', paddingRight: 10, right: 0, alignSelf: 'center' }}>
                                                            {
                                                                item.status == "1" ?
                                                                    <TouchableOpacity
                                                                        onPress={() => { this.props.navigation.navigate('UpdateUnitPayRate', { payRateData: item, payRateLists: this.state.apiData }) }}
                                                                    >
                                                                        <Image style={{ height: 15, width: 15, padding: 11, tintColor: '#000000' }} source={require('./../../assets/pencil.png')} />
                                                                    </TouchableOpacity>
                                                                    :
                                                                    <TouchableOpacity activeOpacity={1}>
                                                                        <Image style={{ height: 15, width: 15, padding: 11, tintColor: '#888888' }} source={require('./../../assets/pencil.png')} />
                                                                    </TouchableOpacity>
                                                            }
                                                            {
                                                                item.status == "1" ?
                                                                    <TouchableOpacity style={{ marginLeft: 15 }}
                                                                        onPress={() => { this._active(item.id, "0") }}
                                                                    >
                                                                        <Image style={{ height: 18, width: 40, padding: 10 }} source={require('./../../assets/toggle_icon_on.png')} />
                                                                    </TouchableOpacity>
                                                                    :
                                                                    <TouchableOpacity style={{ marginLeft: 15 }}
                                                                        onPress={() => { this._active(item.id, "1") }}
                                                                    >
                                                                        <Image style={{ height: 18, width: 40, padding: 10 }} source={require('./../../assets/toggle_icon_off.png')} />
                                                                    </TouchableOpacity>
                                                            }

                                                        </View>

                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        }
                                    />)
                                    :
                                    (<View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                                        <Text
                                            numberOfLines={1}
                                            style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                                        >No Data Available!</Text>
                                    </View>)

                            }
                        </View>

                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },

    tabHolder: {
        padding: 5,
        backgroundColor: "#00D5E1",
        borderRadius: 30,
        flex: 1
    },
    rowContainer: {
        flexDirection: 'row',
        padding: 10,
        backgroundColor: "#FFFFFF",
        borderTopLeftRadius: 2,
        borderBottomLeftRadius: 2,
        borderTopRightRadius: 2,
        borderBottomRightRadius: 2,
        borderColor: '#EAEAEA',
        borderWidth: 2,

    },


});

export default connect(mapStateToPrpos)(UnitPayRate);