/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions,
    TextInput, CheckBox, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import RadioGroup from 'react-native-radio-buttons-group';
import Api from '../../apis/Api';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';
import ModalSelector from '../../components/CustomModelSelector';
import { connect } from 'react-redux';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
    return {
        userName: props.userLoginDetails.name,
        userid: props.userLoginDetails.id,
        userImage: props.userLoginDetails.image,
    }
}


class UnitAddPayRate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedRole: '',
            selectedRoleId: '',
            currency: '',
            currencyId: '',
            pay_per_hour: '',
            roleData: [],

            locationData: [],
            locationId: '',
            full_location: '',
            unit_location: '',
            landmark: '',
            zip: '',
            currencyValue: '',
            isButtonEnable: false,

            isLoading: true,
            setErrors: {
                field: '',
                message: ''
            },

            allPayRateLists: this.props.navigation.getParam('payRateLists', 'NO-item'),
        };
        console.log(this.props.navigation);
    }


    componentDidMount() {
        this._unitRoleListing();
    }

    // This is for store role listing data in dropdown from unitRoleListing api...
    _unitRoleListing = () => {

        var formData = new FormData();
        formData.append("unit_id", this.props.userid);

        Api._unitRoleListing(formData)
            .then((response) => {
                console.log(response.data)

                var newArray = []
                response.data.map((item) => {
                    if (item.is_active.toString() == '1') {
                        var newObj = {}
                        newObj.key = item.id;
                        newObj.label = item.name;
                        newArray.push(newObj)
                    }
                })
                this.setState({ roleData: newArray });
                this.unitLocationListing();

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false });
            });
    }

    // This is for store unit locations data in dropdown from unitLocationListing api...
    unitLocationListing = () => {

        var rawData = { "unit_id": this.props.userid }

        Api._getUnitAllLocations(rawData)
            .then((response) => {
                console.log(response.data)

                var tempData = [];
                for (var i = 0; i < response.data.length; i++) {
                    if (response.data[i].status.toString() == "1") {
                        tempData.push(response.data[i])
                    }
                }

                let newArray = tempData.map((item) => {
                    return (
                        {
                            "key": item.id,
                            "label": item.address + ", " + item.landmark + ", " + item.post_code,
                            "address": item.address,
                            "landmark": item.landmark,
                            "zip": item.post_code,
                            "currencyId": item.currency,
                        }
                    )
                })
                this.setState({ locationData: newArray, isLoading: false })

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false });
            });
    }

    checkValidation = () => {
        let roleError = { field: '', message: '' };
        let locationError = { field: '', message: '' }
        let payRatesError = { field: '', message: '' };

        if (this.state.selectedRole == '') {
            roleError.field = "selectedRole";
            roleError.message = "Select role is required!";
            this.setState({ setErrors: roleError })
        } else if (this.state.full_location == '') {
            locationError.field = "unit_location";
            locationError.message = "Location is required!";
            this.setState({ setErrors: locationError })
        } else if (this.state.pay_per_hour == '') {
            payRatesError.field = "pay_per_hour";
            payRatesError.message = "Pay per hour is required!";
            this.setState({ setErrors: payRatesError })
        } else if (this.checkRoleLocatonInList(this.state.selectedRole, this.state.locationId)) {
            setToastMsg("This role and location already in payrate");
        } else {
            this.setState({ setErrors: { field: '', message: '' } })
            this._unitAddPayRates();
        }
    }

    _unitAddPayRates = () => {
        this.setState({ isLoading: true })

        var rawData = {
            "unit_id": this.props.userid,
            "role": this.state.selectedRole,
            "currency": this.state.locationId,
            "rate_per_hour": this.state.pay_per_hour,
            "status": "1"
        }
        Api._unitAddPayRates(rawData)
            .then((response) => {

                console.log(response.data)
                this.setState({ isLoading: false })
                setToastMsg(response.message.toString());
                this.props.navigation.goBack();
            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false })
            });

    }

    checkRoleLocatonInList = (role, locationId) => {
        var bool = false;
        var tempLocationData = [...this.state.allPayRateLists]

        for (var i = 0; i < tempLocationData.length; i++) {
            if (tempLocationData[i].role.toUpperCase() === role.toUpperCase() && tempLocationData[i].currencies.id === locationId) {
                bool = true;
            }
        }

        return bool;
    }


    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
                    <View style={styles.mainContainer}>


                        <View style={styles.mainContainer}>

                            <Loader isLoading={this.state.isLoading} />

                            <View style={{ backgroundColor: '#00D5E1' }}>
                                <TouchableOpacity
                                    style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                                    onPress={() => this.props.navigation.goBack()}
                                >
                                    <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                                </TouchableOpacity>
                                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>ADD PAY RATE</Text>

                            </View>
                            <ScrollView >
                                <View style={{ marginTop: 10, marginLeft: 30, marginRight: 30 }}>

                                    <ModalSelector
                                        data={this.state.roleData}
                                        initValue="SELECT ROLE"
                                        accessible={true}
                                        animationType="fade"
                                        search={false}
                                        scrollViewAccessibilityLabel={'Scrollable options'}
                                        cancelButtonAccessibilityLabel={'Cancel Button'}
                                        onChange={(option) => { this.setState({ selectedRole: option.label, selectedRoleId: option.key }) }}>
                                        <View style={{ marginTop: 5 }}>
                                            <CustomTextInput
                                                placeholder='SELECT ROLE'
                                                floatingText="SELECT ROLE"
                                                editable={false}
                                                onFocus={true}
                                                showSoftInputOnFocus={false}
                                                caretHidden={true}
                                                isVisibleDropDown={true}
                                                returnKeyType={'done'}
                                                value={this.state.selectedRole}
                                            />
                                        </View>
                                    </ModalSelector>
                                    {
                                        this.state.setErrors.field == "selectedRole" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                    {/* <ModalSelector
                                        data={this.state.currencyData}
                                        initValue="CURRENCY"
                                        accessible={true}
                                        animationType="fade"
                                        search={false}
                                        scrollViewAccessibilityLabel={'Scrollable options'}
                                        cancelButtonAccessibilityLabel={'Cancel Button'}
                                        onChange={(option) => { this.setState({ currency: option.label, currencyId: option.key }) }}>
                                        <View style={{ marginTop: 5 }}>
                                            <CustomTextInput
                                                placeholder='CURRENCY'
                                                floatingText="CURRENCY"
                                                editable={false}
                                                onFocus={true}
                                                showSoftInputOnFocus={false}
                                                caretHidden={true}
                                                isVisibleDropDown={true}
                                                returnKeyType={'done'}
                                                value={this.state.currency}
                                            />
                                        </View>
                                    </ModalSelector>
                                    {
                                        this.state.setErrors.field == "currency" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    } */}

                                    <View style={{ marginTop: 5 }}>
                                        <ModalSelector
                                            data={this.state.locationData}
                                            keyExtractor={(item, index) => item.key.toString()}
                                            initValue="UNIT/LOCATION"
                                            accessible={true}
                                            animationType="fade"
                                            search={false}
                                            scrollViewAccessibilityLabel={'Scrollable options'}
                                            cancelButtonAccessibilityLabel={'Cancel Button'}
                                            onChange={(option) => {
                                                this.setState({
                                                    locationId: option.key,
                                                    full_location: option.label,
                                                    unit_location: option.address,
                                                    landmark: option.landmark,
                                                    zip: option.zip,
                                                    currencyValue: option.currencyId,
                                                })
                                                {
                                                    option.currencyId ?
                                                        (
                                                            console.log("currency added"),
                                                            this.setState({ isButtonEnable: true })
                                                        )
                                                        :
                                                        (
                                                            console.log("currency not added"),
                                                            alert('Currency has not set yet in this location'),
                                                            this.setState({ isButtonEnable: false })
                                                        )
                                                }

                                            }}>
                                            <CustomTextInput
                                                placeholder='UNIT/LOCATION'
                                                keyboardType='default'
                                                floatingText="UNIT/LOCATION"
                                                editable={true}
                                                onFocus={false}
                                                showSoftInputOnFocus={false}
                                                caretHidden={true}
                                                isVisibleDropDown={true}
                                                value={this.state.full_location}
                                                returnKeyType={'done'}
                                            />
                                        </ModalSelector>
                                    </View>
                                    {
                                        this.state.setErrors.field == "unit_location" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                    <View style={{ marginTop: 5 }}>
                                        <CustomTextInput
                                            placeholder='RATE PER HOUR'
                                            keyboardType='numeric'
                                            floatingText="RATE PER HOUR"
                                            value={this.state.pay_per_hour}
                                            returnKeyType={'done'}
                                            onChangeText={(text) => {
                                                this.setState({ pay_per_hour: text });
                                            }}
                                        />
                                    </View>
                                    {
                                        this.state.setErrors.field == "pay_per_hour" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                </View>
                            </ScrollView>
                        </View>
                        <View >

                            <View style={styles.shadow}></View>
                            {
                                this.state.isButtonEnable ?
                                    <TouchableOpacity activeOpacity={0.7}
                                        style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                                        onPress={() => this.checkValidation()}
                                    >
                                        <Text style={styles.loginText}>SUBMIT</Text>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity activeOpacity={1}
                                        style={{ padding: 10, marginTop: 20, backgroundColor: "#c2c2c2", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                                        onPress={() => { }}
                                    >
                                        <Text style={styles.loginText}>SUBMIT</Text>
                                    </TouchableOpacity>
                            }

                        </View>


                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,
    },
    headerBg: {
        width: width,
        height: 150,
        alignSelf: 'center',

    },

    rowContainer: {
        flexDirection: 'row',
        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        borderColor: '#EAEAEA',
        borderWidth: 2,
        marginBottom: 5


    },
    floatingLabelText: {
        color: "#393FA0",
        marginTop: 10,
        backgroundColor: '#FFFFFF',
        paddingRight: 5,

        fontSize: 14,
        fontFamily: "montserrat_regular"
    },
    heading: {
        alignSelf: 'center',
        fontSize: 22,
        fontFamily: 'montserrat_semibold',
        color: '#353C9E',
        marginTop: 10

    },
    loginText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontFamily: "montserrat_semibold",
        alignSelf: 'center'

    },
    textInput_style: {
        flex: 1,
        fontSize: 20,
        padding: 15,
        backgroundColor: '#FFFFFF',
        borderColor: '#00D5E1',
        borderRadius: 20,
        borderWidth: 2,
        color: 'rgb(63, 82, 110)',
    },
    profileImage: {
        height: 150,
        width: 150,
        position: 'absolute',
        borderRadius: 100,
        alignSelf: 'center',
        marginTop: 60

    },
    shadow: {
        flexDirection: 'column-reverse',
        backgroundColor: "#F3F3F3",
        height: 0.2,
        elevation: 3,
        shadowColor: "#F3F3F3",
        shadowOpacity: 1,
        shadowRadius: 10,
        shadowOffset: {
            height: 10,
            width: 10
        }

    },
    errors: {
        color: 'red',
        fontSize: 14,
        marginLeft: 10,
    }

});

export default connect(mapStateToPrpos)(UnitAddPayRate);