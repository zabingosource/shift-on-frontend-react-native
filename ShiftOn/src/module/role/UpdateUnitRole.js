/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions,
    TextInput, CheckBox, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import RadioGroup from 'react-native-radio-buttons-group';
import Api from '../../apis/Api';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../components/CustomModelSelector';
import { connect } from 'react-redux';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
    return {
        userName: props.userLoginDetails.name,
        userid: props.userLoginDetails.id,
        userImage: props.userLoginDetails.image,
    }
}

class UpdateUnitRole extends Component {
    constructor(props) {
        super(props);
        this.state = {
            role: this.props.navigation.getParam('roleData', 'NO-item').name,
            description: this.props.navigation.getParam('roleData', 'NO-item').description,
            qualification: '',
            qualificationList: [],
            is_active: this.props.navigation.getParam('roleData', 'NO-item').is_active,
            isLoading: false,
            setErrors: {
                field: '',
                message: ''
            }
        };
        console.log(this.props.navigation);
    }


    componentDidMount() {
        var qualificationArray = this.props.navigation.getParam('roleData', 'NO-item').qualification.split(",");

        var list = [...this.state.qualificationList];
        for (let i = 0; i < qualificationArray.length; i++) {
            list.push(qualificationArray[i]);
        }
        this.setState({
            qualificationList: list,
            qualification: '',
        });
    }

    checkValidation = () => {
        let roleError = { field: '', message: '' }
        let descriptionError = { field: '', message: '' }
        let qualificationError = { field: '', message: '' }

        if (this.state.role == '') {
            roleError.field = "role";
            roleError.message = "Role is required!";
            this.setState({ setErrors: roleError })
        } else if (this.state.description == '') {
            descriptionError.field = "description";
            descriptionError.message = "Description is required!";
            this.setState({ setErrors: descriptionError })
        } else if (this.state.qualificationList.length <= 0) {
            qualificationError.field = "qualification";
            qualificationError.message = "Qualification is required!";
            this.setState({ setErrors: qualificationError })
        } else {
            this.setState({ setErrors: { field: '', message: '' } })
            this._unitUpdateRole();
        }
    }

    _unitUpdateRole = () => {
        this.setState({ isLoading: true })

        var formData = new FormData();
        formData.append("id", this.props.navigation.getParam('roleData', 'NO-item').id);
        formData.append("unit_id", this.props.userid);
        formData.append("name", this.state.role);
        formData.append("description", this.state.description);
        formData.append("qualification", this.state.qualificationList.toString());
        formData.append("is_active", this.state.is_active);

        Api._unitUpdateRole(formData)
            .then((response) => {

                console.log(response.data)
                this.setState({ isLoading: false })
                setToastMsg(response.message.toString());
                this.props.navigation.goBack();
            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false })
            });

    }

    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
                    <View style={styles.mainContainer}>


                        <View style={styles.mainContainer}>

                            <Loader isLoading={this.state.isLoading} />

                            <View style={{ backgroundColor: '#00D5E1' }}>
                                <TouchableOpacity
                                    style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                                    onPress={() => this.props.navigation.goBack()}
                                >
                                    <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                                </TouchableOpacity>
                                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>UPDATE ROLE</Text>

                            </View>
                            <ScrollView nestedScrollEnabled={true}>
                                <View style={{ marginTop: 10, marginLeft: 30, marginRight: 30 }}>
                                    <View style={{ marginTop: 5 }}>
                                        <CustomTextInput
                                            placeholder='ROLE'
                                            keyboardType='default'
                                            floatingText="ROLE"
                                            value={this.state.role}
                                            returnKeyType={'done'}
                                            onChangeText={(text) => {
                                                this.setState({ role: text });
                                            }}
                                        />
                                    </View>
                                    {
                                        this.state.setErrors.field == "role" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                    <View style={{ marginTop: 5 }}>
                                        <CustomTextInput
                                            placeholder='DESCRIPTION'
                                            keyboardType='default'
                                            floatingText="DESCRIPTION"
                                            value={this.state.description}
                                            returnKeyType={'done'}
                                            onChangeText={(text) => {
                                                this.setState({ description: text });
                                            }}
                                        />
                                    </View>
                                    {
                                        this.state.setErrors.field == "description" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                    <View style={{ width: '85%', marginTop: 5, flexDirection: 'row', alignItems: 'center' }}>

                                        <CustomTextInput
                                            style={{ width: 250 }}
                                            placeholder='QUALIFICATION'
                                            keyboardType='default'
                                            floatingText="QUALIFICATION"
                                            value={this.state.qualification}
                                            editable={true}
                                            onFocus={true}
                                            onChangeText={(text) => { this.setState({ qualification: text }) }}
                                        />

                                        <TouchableOpacity
                                            style={{ height: 50, width: 50, marginTop: 10, paddingLeft: 20, paddingRight: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFFFFF', borderRadius: 10, }}
                                            onPress={() => this._addQualification()}
                                        >
                                            <Image style={{ height: 30, width: 30, padding: 10 }} source={require('./../../assets/ic_addqualifucation.png')} />
                                        </TouchableOpacity>
                                    </View>
                                    {
                                        this.state.setErrors.field == "qualification" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                    <View style={{ marginTop: 10, marginBottom: 20 }}>

                                        <FlatList
                                            style={{ width: '100%', marginBottom: '12%' }}
                                            data={this.state.qualificationList}
                                            keyExtractor={(item, index) => index.toString()}
                                            //ItemSeparatorComponent={this.ItemSeparator}

                                            renderItem={({ item, index }) =>
                                                <View style={{ flexDirection: 'row', backgroundColor: "#FFFFFF", justifyContent: 'center', borderBottomWidth: 2, borderColor: '#EAEAEA' }}>
                                                    <View style={{ width: "85%", padding: 10, }}>
                                                        <Text
                                                            style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 17, }}
                                                        >{item}</Text>
                                                    </View>

                                                    <TouchableOpacity
                                                        style={{ height: 40, width: 40, padding: 10 }}
                                                        onPress={() => this._removeQualification(item)}
                                                    >
                                                        <Image style={{ height: 30, width: 30 }} source={require('./../../assets/ic_delete.png')} />

                                                    </TouchableOpacity>

                                                </View>
                                            }
                                        />
                                    </View>

                                </View>
                            </ScrollView>
                        </View>
                        <View >

                            <View style={styles.shadow}></View>
                            {/*  <Image style={{ width:'100%',height:40,}} source={require('./../../assets/border.png')} /> */}
                            <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                                onPress={() => this.checkValidation()}
                            >
                                <Text style={styles.loginText}>UPDATE</Text>
                            </TouchableOpacity>

                        </View>


                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }

    _addQualification = () => {
        var list = [...this.state.qualificationList];
        if (this.state.qualification) {
            list.push(this.state.qualification);
        }
        this.setState({
            qualificationList: list,
            qualification: '',
        });
    }

    _removeQualification = (e) => {

        var array = [...this.state.qualificationList]; // make a separate copy of the array
        var index = array.indexOf(e)
        if (index !== -1) {
            array.splice(index, 1);
            this.setState({ qualificationList: array });
        }
        console.log(this.state.qualificationList.indexOf(e));
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },
    headerBg: {
        width: width,
        height: 150,
        alignSelf: 'center',

    },

    rowContainer: {
        flexDirection: 'row',
        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        borderColor: '#EAEAEA',
        borderWidth: 2,
        marginBottom: 5


    },
    floatingLabelText: {
        color: "#393FA0",
        marginTop: 10,
        backgroundColor: '#FFFFFF',
        paddingRight: 5,

        fontSize: 14,
        fontFamily: "montserrat_regular"
    },
    heading: {
        alignSelf: 'center',
        fontSize: 22,
        fontFamily: 'montserrat_semibold',
        color: '#353C9E',
        marginTop: 10

    },
    loginText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontFamily: "montserrat_semibold",
        alignSelf: 'center'

    },
    textInput_style: {
        flex: 1,
        fontSize: 20,
        padding: 15,
        backgroundColor: '#FFFFFF',
        borderColor: '#00D5E1',
        borderRadius: 20,
        borderWidth: 2,
        color: 'rgb(63, 82, 110)',
    },
    profileImage: {
        height: 150,
        width: 150,
        position: 'absolute',
        borderRadius: 100,
        alignSelf: 'center',
        marginTop: 60

    },
    shadow: {
        flexDirection: 'column-reverse',
        backgroundColor: "#F3F3F3",
        height: 0.2,
        elevation: 3,
        shadowColor: "#F3F3F3",
        shadowOpacity: 1,
        shadowRadius: 10,
        shadowOffset: {
            height: 10,
            width: 10
        }

    },
    errors: {
        color: 'red',
        fontSize: 14,
        marginLeft: 10,
    }

});

export default connect(mapStateToPrpos)(UpdateUnitRole);