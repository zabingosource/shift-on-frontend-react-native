/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions,
    TextInput, ToastAndroid, Platform
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import Api from '../../../apis/Api';
import Loader from '../../../utils/Loader';
import { setToastMsg } from '../../../utils/ToastMessage';
import { connect } from 'react-redux';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
    return {
        userName: props.userLoginDetails.name,
        userid: props.userLoginDetails.id,
        userImage: props.userLoginDetails.image,
    }
}


class ShiftPayRateListing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            wardId: this.props.navigation.getParam('wardId', 'NO-item'),
            shiftId: this.props.navigation.getParam('shiftId', 'NO-item'),
            loactionId: this.props.navigation.getParam('loactionId', 'NO-item'),
            isLoading: true,
            apiData: [],
        };
        //console.log(this.props.navigation);
    }


    componentDidMount() {
        this.navFocusListener = this.props.navigation.addListener('didFocus', () => {
            this._unitWardShiftPayrateListings();
        });
    }
    componentWillUnmount() {
        this.navFocusListener.remove();
    }

    _unitWardShiftPayrateListings = () => {
        this.setState({ isLoading: true });

        var rawData = {
            "shift_id": this.state.shiftId,
            "ward_id": this.state.wardId,
            "unit_id": this.props.userid
        }
        Api._unitWardShiftPayrateListings(rawData)
            .then((response) => {

                console.log(JSON.stringify(response.data));

                var tempData = []
                response.data.map((item) => {
                    if (item.payrate.currencies.id.toString() == this.state.loactionId.toString()) {
                        tempData.push(item)
                    }
                })

                this.setState({ apiData: tempData, isLoading: false });
                console.log('tempData ', JSON.stringify(tempData));

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false });
            });
    }


    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

                    <View style={styles.mainContainer}>
                        <View style={{ backgroundColor: '#00D5E1' }}>
                            <TouchableOpacity
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                                onPress={() => {
                                    this.props.navigation.goBack()
                                }}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                            </TouchableOpacity>
                            <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>SELECTED PAY RATES</Text>
                            <TouchableOpacity
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20, right: 0 }}
                                onPress={() => this.props.navigation.navigate('ShiftAddPayRate', { wardId: this.state.wardId, shiftId: this.state.shiftId, loactionId: this.state.loactionId })}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/more_icn.png')} />
                            </TouchableOpacity>
                        </View>

                        <Loader isLoading={this.state.isLoading} />

                        <View style={{ marginLeft: 20, marginRight: 10 }}>

                            <View style={{ marginTop: 20, marginBottom: 20 }}>

                                {
                                    (this.state.apiData.length > 0) ?

                                        (<FlatList
                                            style={{ width: '100%', marginBottom: '12%' }}
                                            data={this.state.apiData}
                                            keyExtractor={(item, index) => index.toString()}
                                            //ItemSeparatorComponent={this.ItemSeparator}
                                            renderItem={({ item, index }) =>
                                                <View style={{ borderRadius: 2, margin: 2 }}>
                                                    <TouchableOpacity
                                                        activeOpacity={0.9}
                                                    >
                                                        <View style={styles.rowContainer}>
                                                            <View style={{ width: '100%', paddingLeft: 10 }}>
                                                                <Text
                                                                    numberOfLines={1}
                                                                    style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 15, }}
                                                                >{item.payrate.role}</Text>

                                                                <Text
                                                                    numberOfLines={1}
                                                                    style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                                                                >
                                                                    {item.payrate.currencies ? item.payrate.currency + ' ' : ''}{item.payrate.rate_per_hour}
                                                                </Text>

                                                                <Text
                                                                    numberOfLines={1}
                                                                    style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 14, }}
                                                                >
                                                                    {item.payrate.currency ? item.payrate.currencies.address + ", " + item.payrate.currencies.landmark + ", " + item.payrate.currencies.post_code : ''}
                                                                </Text>

                                                            </View>

                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            }
                                        />)
                                        :
                                        (<View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                                            <Text
                                                numberOfLines={1}
                                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                                            >No Data Available!</Text>
                                        </View>)

                                }
                            </View>

                        </View>

                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },

    tabHolder: {
        padding: 5,
        backgroundColor: "#00D5E1",
        borderRadius: 30,
        flex: 1
    },
    rowContainer: {
        flexDirection: 'row',
        padding: 10,
        backgroundColor: "#FFFFFF",
        borderTopLeftRadius: 2,
        borderBottomLeftRadius: 2,
        borderTopRightRadius: 2,
        borderBottomRightRadius: 2,
        borderColor: '#EAEAEA',
        borderWidth: 2,
        marginRight: 10

    },


});

export default connect(mapStateToPrpos)(ShiftPayRateListing);