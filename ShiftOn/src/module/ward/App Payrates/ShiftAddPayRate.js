/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions,
    TextInput, ToastAndroid, Platform
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import CheckBox from '@react-native-community/checkbox';
import Api from '../../../apis/Api';
import Loader from '../../../utils/Loader';
import { setToastMsg } from '../../../utils/ToastMessage';
import { connect } from 'react-redux';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
    return {
        userName: props.userLoginDetails.name,
        userid: props.userLoginDetails.id,
        userImage: props.userLoginDetails.image,
    }
}


class ShiftAddPayRate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            wardId: this.props.navigation.getParam('wardId', 'NO-item'),
            shiftId: this.props.navigation.getParam('shiftId', 'NO-item'),
            loactionId: this.props.navigation.getParam('loactionId', 'NO-item'),
            isLoading: true,
            apiData: [],
        };
        //console.log(this.props.navigation);
    }


    componentDidMount() {
        this._unitWardShiftPayrateListsWithLinkedStatus();
    }

    _unitWardShiftPayrateListsWithLinkedStatus = () => {
        this.setState({ isLoading: true });

        var rawData = {
            "unit_id": this.props.userid,
            "shift_id": this.state.shiftId,
            "ward_id": this.state.wardId,
        }
        Api._unitWardShiftPayrateListsWithLinkedStatus(rawData)
            .then((response) => {

                console.log(JSON.stringify(response.data));

                var tempArr = []
                response.data.map((item) => {
                    if (item.currencies && item.currencies.id.toString() == this.state.loactionId.toString()) {
                        tempArr.push(item)
                    }
                })

                this.setState({ apiData: tempArr, isLoading: false });
                console.log('after filter', JSON.stringify(tempArr));

                if (response.status == 0) {
                    setToastMsg(response.message.toString());
                }

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false });
            });
    }

    _unitWardShiftPayrateLinkingWithStatus = () => {
        this.setState({ isLoading: true });

        let linkedPayrateId = [];
        for (var i = 0; i < this.state.apiData.length; i++) {
            if (this.state.apiData[i].linked.toString() == "1") {
                linkedPayrateId.push(this.state.apiData[i].id);
            }

        }

        console.log(Object.values(linkedPayrateId))

        var rawData = {
            "unit_id": this.props.userid,
            "shift_id": this.state.shiftId,
            "ward_id": this.state.wardId,
            "location_id": this.state.loactionId,
            "payrate_id": Object.values(linkedPayrateId),
        }
        console.log(rawData)
        Api._unitWardShiftPayrateLinkingWithStatus(rawData)
            .then((response) => {

                console.log(response.data);

                this.setState({ isLoading: false });
                setToastMsg(response.message.toString());
                this.props.navigation.goBack();

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false });
            });
    }


    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

                    <View style={styles.mainContainer}>
                        <View style={styles.mainContainer}>

                            <View style={{ backgroundColor: '#00D5E1' }}>
                                <TouchableOpacity
                                    style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                                    onPress={() => this.props.navigation.goBack()}
                                >
                                    <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                                </TouchableOpacity>
                                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>PAY RATES</Text>

                            </View>

                            <Loader isLoading={this.state.isLoading} />

                            <View style={{ margin: 20 }}>

                                {
                                    (this.state.apiData.length > 0) ?

                                        (<FlatList
                                            style={{ width: '100%', marginBottom: '12%' }}
                                            data={this.state.apiData}
                                            keyExtractor={(item, index) => index.toString()}
                                            //ItemSeparatorComponent={this.ItemSeparator}
                                            renderItem={({ item, index }) =>
                                                <View style={{ borderRadius: 2, margin: 2 }}>
                                                    <TouchableOpacity
                                                        activeOpacity={0.9}
                                                    >
                                                        <View style={styles.rowContainer}>
                                                            <View style={{ width: '85%' }}>
                                                                <Text
                                                                    numberOfLines={1}
                                                                    style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 15, }}
                                                                >{item.role}</Text>

                                                                <Text
                                                                    numberOfLines={1}
                                                                    style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                                                                >
                                                                    {item.currencies ? item.currencies.currency_data.currency + ' ' : ''}{item.rate_per_hour}
                                                                </Text>

                                                                <Text
                                                                    numberOfLines={1}
                                                                    style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 14, }}
                                                                >
                                                                    {item.currency ? item.currencies.address + ", " + item.currencies.landmark + ", " + item.currencies.post_code : ''}
                                                                </Text>

                                                            </View>
                                                            <View style={{ width: '15%', position: 'absolute', right: 0, alignSelf: 'center' }}>

                                                                {
                                                                    item.status && item.status.toString() == "1" ?

                                                                        (item.linked && item.linked.toString() == "1" ?
                                                                            <TouchableOpacity style={styles.outerCheckBox}
                                                                                onPress={() => this.setCheckBoxFunc("0", index)}
                                                                            >
                                                                                <View style={[styles.innerCheckBox, { backgroundColor: '#000000' }]}></View>
                                                                            </TouchableOpacity>
                                                                            :
                                                                            <TouchableOpacity style={styles.outerCheckBox}
                                                                                onPress={() => this.setCheckBoxFunc("1", index)}
                                                                            >
                                                                                <View style={[styles.innerCheckBox, { backgroundColor: '#FFFFFF' }]}></View>
                                                                            </TouchableOpacity>)
                                                                        :
                                                                        (item.linked && item.linked.toString() == "1" ?
                                                                            <TouchableOpacity style={styles.outerCheckBox2} activeOpacity={1}>
                                                                                <View style={[styles.innerCheckBox2, { backgroundColor: '#888888' }]}></View>
                                                                            </TouchableOpacity>
                                                                            :
                                                                            <TouchableOpacity style={styles.outerCheckBox2} activeOpacity={1}>
                                                                                <View style={[styles.innerCheckBox2, { backgroundColor: '#FFFFFF' }]}></View>
                                                                            </TouchableOpacity>)

                                                                }

                                                            </View>

                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            }
                                        />)
                                        :
                                        (<View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                                            <Text
                                                numberOfLines={1}
                                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                                            >No Data Available!</Text>
                                        </View>)

                                }
                            </View>

                        </View>

                        <View >

                            <View style={styles.shadow}></View>
                            <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                                onPress={() => this._unitWardShiftPayrateLinkingWithStatus()}
                            >
                                <Text style={styles.loginText}>ADD</Text>
                            </TouchableOpacity>

                        </View>

                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }

    setCheckBoxFunc = (value, index) => {
        let tempList = [...this.state.apiData]
        tempList[index].linked = value
        this.setState({ apiData: tempList });
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },

    tabHolder: {
        padding: 5,
        backgroundColor: "#00D5E1",
        borderRadius: 30,
        flex: 1
    },
    rowContainer: {
        flexDirection: 'row',
        padding: 10,
        backgroundColor: "#FFFFFF",
        borderTopLeftRadius: 2,
        borderBottomLeftRadius: 2,
        borderTopRightRadius: 2,
        borderBottomRightRadius: 2,
        borderColor: '#EAEAEA',
        borderWidth: 2,
        marginRight: 10

    },
    shadow: {
        flexDirection: 'column-reverse',
        backgroundColor: "#F3F3F3",
        height: 0.2,
        elevation: 3,
        shadowColor: "#F3F3F3",
        shadowOpacity: 1,
        shadowRadius: 10,
        shadowOffset: {
            height: 10,
            width: 10
        }

    },
    loginText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontFamily: "montserrat_semibold",
        alignSelf: 'center'

    },
    outerCheckBox: {
        width: 17,
        height: 17,
        borderWidth: 1.3,
        borderRadius: 1,
        borderColor: '#000000',
        justifyContent: "center",
        alignItems: 'center'
    },
    innerCheckBox: {
        width: 9,
        height: 9,
    },
    outerCheckBox2: {
        width: 17,
        height: 17,
        borderWidth: 1.3,
        borderRadius: 1,
        borderColor: '#888888',
        justifyContent: "center",
        alignItems: 'center'
    },
    innerCheckBox2: {
        width: 9,
        height: 9,
    },

});

export default connect(mapStateToPrpos)(ShiftAddPayRate);