/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions,
    TextInput, ToastAndroid, Platform
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import Swipeout from 'react-native-swipeout';
import Api from '../../apis/Api';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';
import { connect } from 'react-redux';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
    return {
        userName: props.userLoginDetails.name,
        userid: props.userLoginDetails.id,
        userImage: props.userLoginDetails.image,
    }
}


class UnitWard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            apiData: [],
            shiftData: [],
        };
        //console.log(this.props.navigation);
    }


    componentDidMount() {
        this.navFocusListener = this.props.navigation.addListener('didFocus', () => {
            this._unitWardListing();
        });
    }
    componentWillUnmount() {
        this.navFocusListener.remove();
    }

    // _unitWardListing = () => {
    //     this.setState({ isLoading: true });

    //     var rawData = { "unit_id": this.props.userid }
    //     Api._unitWardListing(rawData)
    //         .then((response) => {

    //             console.log(response.data);

    //             this.setState({ apiData: response.data, isLoading: false });
    //             //setToastMsg(response.message.toString());

    //         })
    //         .catch((err) => {
    //             console.log(err);
    //             setToastMsg("Somthing went wrong.");
    //             this.setState({ isLoading: false });
    //         });
    // }

    _unitWardListing = async () => {
        try {
            this.setState({ isLoading: true });

            var rawData = { "unit_id": this.props.userid }
            const response = await Api._unitWardListing(rawData);

            var tempArrayData = []
            // Use Promise.all to handle multiple asynchronous calls
            const promises = response.data.map(async (item, index) => {
                const hasListings = await this.WardAddedShiftListings(item.id);

                tempArrayData.push({ ...response.data[index], isAdded: hasListings })

            });
            await Promise.all(promises);

            const sortedData = [...tempArrayData].sort((a, b) => b.id - a.id);
            console.log('tempSortedArrayData:----', sortedData);

            this.setState({ apiData: sortedData, isLoading: false });

        } catch (error) {
            // Handle errors
            console.error(error);
            setToastMsg("Something went wrong.");
            this.setState({ isLoading: false });
        }
    }

    WardAddedShiftListings = async (id) => {

        var rawData = {
            "unit_id": this.props.userid,
            "ward_id": id,
        }
        return Api._unitWardShiftListings(rawData)
            .then((response) => {

                // Assuming response.data is an array, you can check its length
                const hasListings = response.data && response.data.length > 0;

                return hasListings;
            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Something went wrong.");
                this.setState({ isLoading: false });
                return false; // Return false in case of an error
            });
    }

    _active = (id, status) => {

        Alert.alert(
            '',
            'Are you sure?',
            [
                { text: 'NO', onPress: () => console.log('Close pressed') },
                {
                    text: 'YES', onPress: () => {
                        this.setState({ isLoading: true });

                        var rawData = {
                            "id": id,
                            "is_active": status,
                        }
                        Api._unitWardStatusUpdate(rawData)
                            .then((response) => {

                                console.log(response.data);
                                setToastMsg(response.message.toString());
                                this._unitWardListing();

                            })
                            .catch((err) => {
                                console.log(err);
                                setToastMsg("Somthing went wrong.");
                                this.setState({ isLoading: false });
                            });
                    }
                },
            ]
        )

    }


    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

                    <View style={styles.mainContainer}>
                        <View style={{ backgroundColor: '#00D5E1' }}>
                            <TouchableOpacity
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                                onPress={() => this.props.navigation.goBack()}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                            </TouchableOpacity>
                            <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>WARDS</Text>
                            <TouchableOpacity
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20, right: 0 }}
                                onPress={() => this.props.navigation.navigate('UnitAddWard')}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/more_icn.png')} />
                            </TouchableOpacity>
                        </View>

                        <Loader isLoading={this.state.isLoading} />

                        <View style={{ margin: 15 }}>

                            {
                                (this.state.apiData.length > 0) ?

                                    (<FlatList
                                        style={{ width: '100%', marginBottom: '12%' }}
                                        data={this.state.apiData}
                                        keyExtractor={(item, index) => index.toString()}
                                        //ItemSeparatorComponent={this.ItemSeparator}
                                        renderItem={({ item, index }) =>
                                            <View style={{ borderRadius: 2, margin: 2 }}>
                                                <TouchableOpacity
                                                    activeOpacity={0.9}
                                                    onPress={() => { this.props.navigation.navigate('ViewWardDetail', { wardData: item }) }}
                                                >
                                                    <View style={styles.rowContainer}>
                                                        <View style={{ width: '10%', justifyContent: 'center' }}>
                                                            <Image style={{ height: 32, width: 32, tintColor: '#000000' }} source={require('./../../assets/ward_bed_icn.png')} resizeMode='contain' />
                                                        </View>
                                                        <View style={{ width: '46%', marginLeft: 5, }}>
                                                            <Text
                                                                numberOfLines={1}
                                                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 15, }}
                                                            >{item.name}</Text>

                                                            <Text
                                                                numberOfLines={2}
                                                                style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                                                            >{item.description}</Text>

                                                        </View>
                                                        <View style={{ width: '44%', flexDirection: 'row', position: 'absolute', right: 2, alignSelf: 'center' }}>
                                                            {
                                                                item.is_active == "1" ?
                                                                    <View style={{ flexDirection: 'row', }}>
                                                                        <TouchableOpacity
                                                                            style={{ borderColor: item.isAdded ? "#00D5E1" : '#000000', borderRadius: 20, borderWidth: 1, marginRight: 10, backgroundColor: item.isAdded ? "#00D5E1" : "#ffffff" }}
                                                                            onPress={() => { this.props.navigation.navigate('WardShiftListing', { wardData: item }) }}
                                                                        >
                                                                            <View>
                                                                                <Text style={{ color: item.isAdded ? "#ffffff" : '#000000', fontFamily: 'montserrat_ragular', fontSize: 13, paddingHorizontal: 7, paddingVertical: 2 }}>Shifts</Text>
                                                                            </View>
                                                                        </TouchableOpacity>

                                                                        <TouchableOpacity
                                                                            onPress={() => { this.props.navigation.navigate('UpdateUnitWard', { wardData: item }) }}
                                                                        >
                                                                            <Image style={{ height: 15, width: 14, padding: 11, tintColor: '#000000' }} source={require('./../../assets/pencil.png')} />
                                                                        </TouchableOpacity>
                                                                    </View>
                                                                    :
                                                                    <View style={{ flexDirection: 'row', }}>
                                                                        <TouchableOpacity activeOpacity={1}
                                                                            style={{ borderColor: '#888888', borderRadius: 20, borderWidth: 1, marginRight: 10, }}
                                                                        >
                                                                            <View>
                                                                                <Text style={{ color: '#888888', fontFamily: 'montserrat_ragular', fontSize: 13, paddingHorizontal: 7, paddingVertical: 2 }}>Shifts</Text>
                                                                            </View>
                                                                        </TouchableOpacity>
                                                                        <TouchableOpacity activeOpacity={1} >
                                                                            <Image style={{ height: 15, width: 14, padding: 11, tintColor: '#888888' }} source={require('./../../assets/pencil.png')} />
                                                                        </TouchableOpacity>
                                                                    </View>
                                                            }
                                                            {
                                                                item.is_active == "1" ?
                                                                    <TouchableOpacity style={{ marginLeft: 15 }}
                                                                        onPress={() => { this._active(item.id, "0") }}
                                                                    >
                                                                        <Image style={{ height: 18, width: 40, padding: 10 }} source={require('./../../assets/toggle_icon_on.png')} />
                                                                    </TouchableOpacity>
                                                                    :
                                                                    <TouchableOpacity style={{ marginLeft: 15 }}
                                                                        onPress={() => { this._active(item.id, "1") }}
                                                                    >
                                                                        <Image style={{ height: 18, width: 40, padding: 10 }} source={require('./../../assets/toggle_icon_off.png')} />
                                                                    </TouchableOpacity>
                                                            }

                                                        </View>

                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        }
                                    />)
                                    :
                                    (<View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                                        <Text
                                            numberOfLines={1}
                                            style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                                        >No Data Available!</Text>
                                    </View>)

                            }
                        </View>

                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },

    tabHolder: {
        padding: 5,
        backgroundColor: "#00D5E1",
        borderRadius: 30,
        flex: 1
    },
    rowContainer: {
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 5,
        backgroundColor: "#FFFFFF",
        borderTopLeftRadius: 2,
        borderBottomLeftRadius: 2,
        borderTopRightRadius: 2,
        borderBottomRightRadius: 2,
        borderColor: '#EAEAEA',
        borderWidth: 2,

    },


});

export default connect(mapStateToPrpos)(UnitWard);