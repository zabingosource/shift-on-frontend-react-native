/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions,
    TextInput, CheckBox, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import RadioGroup from 'react-native-radio-buttons-group';
import Api from '../../apis/Api';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../components/CustomModelSelector';
import { connect } from 'react-redux';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
    return {
        userName: props.userLoginDetails.name,
        userid: props.userLoginDetails.id,
        userImage: props.userLoginDetails.image,
    }
}

class UpdateUnitWard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.navigation.getParam('wardData', 'NO-item').id,
            ward: this.props.navigation.getParam('wardData', 'NO-item').name,
            description: this.props.navigation.getParam('wardData', 'NO-item').description,
            status: this.props.navigation.getParam('wardData', 'NO-item').is_active,
            isLoading: false,
            setErrors: {
                field: '',
                message: ''
            }
        };
        console.log(this.props.navigation);
    }


    componentDidMount() {

    }

    checkValidation = () => {
        let wardError = { field: '', message: '' }
        let descriptionError = { field: '', message: '' }

        if (this.state.ward == '') {
            wardError.field = "ward";
            wardError.message = "Ward is required!";
            this.setState({ setErrors: wardError })
        } else if (this.state.description == '') {
            descriptionError.field = "description";
            descriptionError.message = "Description is required!";
            this.setState({ setErrors: descriptionError })
        } else {
            this.setState({ setErrors: { field: '', message: '' } })
            this._unitUpdateWard();
        }
    }

    _unitUpdateWard = () => {
        this.setState({ isLoading: true })

        var rawData = {
            "unit_id": this.props.userid,
            "id": this.state.id,
            "name": this.state.ward,
            "description": this.state.description,
            "is_active": this.state.status,
        }
        Api._unitUpdateWard(rawData)
            .then((response) => {

                console.log(response.data)
                this.setState({ isLoading: false })
                setToastMsg(response.message.toString());
                this.props.navigation.goBack();
            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false })
            });

    }

    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
                    <View style={styles.mainContainer}>


                        <View style={styles.mainContainer}>

                            <Loader isLoading={this.state.isLoading} />

                            <View style={{ backgroundColor: '#00D5E1' }}>
                                <TouchableOpacity
                                    style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                                    onPress={() => this.props.navigation.goBack()}
                                >
                                    <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                                </TouchableOpacity>
                                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>UPDATE WARD</Text>

                            </View>
                            <ScrollView nestedScrollEnabled={true}>
                                <View style={{ marginTop: 10, marginLeft: 30, marginRight: 30 }}>
                                    <View style={{ marginTop: 5 }}>
                                        <CustomTextInput
                                            placeholder='WARD'
                                            keyboardType='default'
                                            floatingText="WARD"
                                            value={this.state.ward}
                                            returnKeyType={'done'}
                                            onChangeText={(text) => {
                                                this.setState({ ward: text });
                                            }}
                                        />
                                    </View>
                                    {
                                        this.state.setErrors.field == "ward" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                    <View style={{ marginTop: 5 }}>
                                        <CustomTextInput
                                            placeholder='DESCRIPTION'
                                            keyboardType='default'
                                            floatingText="DESCRIPTION"
                                            value={this.state.description}
                                            returnKeyType={'done'}
                                            onChangeText={(text) => {
                                                this.setState({ description: text });
                                            }}
                                        />
                                    </View>
                                    {
                                        this.state.setErrors.field == "description" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                </View>
                            </ScrollView>
                        </View>
                        <View >

                            <View style={styles.shadow}></View>
                            {/*  <Image style={{ width:'100%',height:40,}} source={require('./../../assets/border.png')} /> */}
                            <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                                onPress={() => this.checkValidation()}
                            >
                                <Text style={styles.loginText}>UPDATE</Text>
                            </TouchableOpacity>

                        </View>


                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,
    },
    headerBg: {
        width: width,
        height: 150,
        alignSelf: 'center',

    },

    rowContainer: {
        flexDirection: 'row',
        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        borderColor: '#EAEAEA',
        borderWidth: 2,
        marginBottom: 5


    },
    floatingLabelText: {
        color: "#393FA0",
        marginTop: 10,
        backgroundColor: '#FFFFFF',
        paddingRight: 5,

        fontSize: 14,
        fontFamily: "montserrat_regular"
    },
    heading: {
        alignSelf: 'center',
        fontSize: 22,
        fontFamily: 'montserrat_semibold',
        color: '#353C9E',
        marginTop: 10

    },
    loginText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontFamily: "montserrat_semibold",
        alignSelf: 'center'

    },
    textInput_style: {
        flex: 1,
        fontSize: 20,
        padding: 15,
        backgroundColor: '#FFFFFF',
        borderColor: '#00D5E1',
        borderRadius: 20,
        borderWidth: 2,
        color: 'rgb(63, 82, 110)',
    },
    profileImage: {
        height: 150,
        width: 150,
        position: 'absolute',
        borderRadius: 100,
        alignSelf: 'center',
        marginTop: 60

    },
    shadow: {
        flexDirection: 'column-reverse',
        backgroundColor: "#F3F3F3",
        height: 0.2,
        elevation: 3,
        shadowColor: "#F3F3F3",
        shadowOpacity: 1,
        shadowRadius: 10,
        shadowOffset: {
            height: 10,
            width: 10
        }

    },
    errors: {
        color: 'red',
        fontSize: 14,
        marginLeft: 10,
    }

});

export default connect(mapStateToPrpos)(UpdateUnitWard);