/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions,
    TextInput, ToastAndroid, Platform
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import Api from '../../../apis/Api';
import Loader from '../../../utils/Loader';
import { setToastMsg } from '../../../utils/ToastMessage';
import { connect } from 'react-redux';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
    return {
        userName: props.userLoginDetails.name,
        userid: props.userLoginDetails.id,
        userImage: props.userLoginDetails.image,
    }
}


class WardShiftListing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            wardId: this.props.navigation.getParam('wardData', 'NO-item').id,
            isLoading: true,
            apiData: [],
        };
        //console.log(this.props.navigation);
    }


    componentDidMount() {
        this.navFocusListener = this.props.navigation.addListener('didFocus', () => {
            this._unitWardShiftListings();
        });
    }
    componentWillUnmount() {
        this.navFocusListener.remove();
    }

    // _unitWardShiftListings = () => {
    //     this.setState({ isLoading: true });

    //     var rawData = {
    //         "unit_id": this.props.userid,
    //         "ward_id": this.state.wardId,
    //     }
    //     Api._unitWardShiftListings(rawData)
    //         .then((response) => {

    //             console.log(response.data);

    //             this.setState({ apiData: response.data, isLoading: false });
    //             //setToastMsg(response.message.toString());

    //         })
    //         .catch((err) => {
    //             console.log(err);
    //             setToastMsg("Somthing went wrong.");
    //             this.setState({ isLoading: false });
    //         });
    // }

    _unitWardShiftListings = async () => {
        try {
            this.setState({ isLoading: true });

            var rawData = { "unit_id": this.props.userid, "ward_id": this.state.wardId, }
            const response = await Api._unitWardShiftListings(rawData);

            var tempArrayData = []
            // Use Promise.all to handle multiple asynchronous calls
            const promises = response.data.map(async (item, index) => {
                const hasListings = await this._unitWardShiftPayrateListings(item.shift.id);

                tempArrayData.push({ ...response.data[index], isAdded: hasListings })

            });
            await Promise.all(promises);

            const sortedData = [...tempArrayData].sort((a, b) => b.id - a.id);
            console.log('tempSortedArrayData:----', sortedData);

            this.setState({ apiData: sortedData, isLoading: false });

        } catch (error) {
            // Handle errors
            console.error(error);
            setToastMsg("Something went wrong.");
            this.setState({ isLoading: false });
        }
    }

    _unitWardShiftPayrateListings = async (id) => {

        var rawData = {
            "shift_id": id,
            "ward_id": this.state.wardId,
            "unit_id": this.props.userid
        }
        return Api._unitWardShiftPayrateListings(rawData)
            .then((response) => {

                // Assuming response.data is an array, you can check its length
                const hasListings = response.data && response.data.length > 0;

                return hasListings;
            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Something went wrong.");
                this.setState({ isLoading: false });
                return false; // Return false in case of an error
            });
    }

    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

                    <View style={styles.mainContainer}>
                        <View style={{ backgroundColor: '#00D5E1' }}>
                            <TouchableOpacity
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                                onPress={() => this.props.navigation.goBack()}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                            </TouchableOpacity>
                            <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>SELECTED SHIFTS</Text>
                            <TouchableOpacity
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20, right: 0 }}
                                onPress={() => this.props.navigation.navigate('WardAddShift', { wardId: this.state.wardId })}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/more_icn.png')} />
                            </TouchableOpacity>
                        </View>

                        <Loader isLoading={this.state.isLoading} />

                        <View style={{ marginLeft: 20, marginRight: 10 }}>

                            <View style={{ marginTop: 20, marginBottom: 20 }}>

                                {
                                    (this.state.apiData.length > 0) ?

                                        (<FlatList
                                            style={{ width: '100%', marginBottom: '12%' }}
                                            data={this.state.apiData}
                                            keyExtractor={(item, index) => index.toString()}
                                            //ItemSeparatorComponent={this.ItemSeparator}
                                            renderItem={({ item, index }) =>
                                                <View style={{ borderRadius: 2, margin: 2 }}>
                                                    <TouchableOpacity
                                                        activeOpacity={0.9}
                                                    >
                                                        <View style={styles.rowContainer}>
                                                            <View style={{ width: '70%' }}>
                                                                <Text
                                                                    numberOfLines={1}
                                                                    style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 15, }}
                                                                >{item.shift.shift_title}</Text>

                                                                <Text
                                                                    numberOfLines={1}
                                                                    style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                                                                >{item.shift.shift_type}</Text>

                                                                <Text
                                                                    numberOfLines={1}
                                                                    style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                                                                >{item.shift.gender}</Text>

                                                            </View>
                                                            <View style={{ width: '30%', flexDirection: 'row', position: 'absolute', right: 0, alignSelf: 'center' }}>
                                                                {
                                                                    item.shift.status.toString() == "1" ?
                                                                        <TouchableOpacity
                                                                            style={{ borderColor: item.isAdded ? "#00D5E1" : '#000000', borderRadius: 20, borderWidth: 1, marginRight: 10, backgroundColor: item.isAdded ? "#00D5E1" : "#ffffff" }}
                                                                            onPress={() => { this.props.navigation.navigate('Locations', { wardId: this.state.wardId, shiftId: item.shift.id }) }}
                                                                        >
                                                                            <View>
                                                                                <Text style={{ color: item.isAdded ? "#ffffff" : '#000000', fontFamily: 'montserrat_ragular', fontSize: 13, paddingHorizontal: 7, paddingVertical: 2 }}>Pay Rates</Text>
                                                                            </View>
                                                                        </TouchableOpacity>
                                                                        :
                                                                        <TouchableOpacity activeOpacity={1}
                                                                            style={{ borderColor: '#888888', borderRadius: 20, borderWidth: 1, marginRight: 10, }}  >
                                                                            <View>
                                                                                <Text style={{ color: '#888888', fontFamily: 'montserrat_ragular', fontSize: 13, paddingHorizontal: 7, paddingVertical: 2 }}>Pay Rates</Text>
                                                                            </View>
                                                                        </TouchableOpacity>
                                                                }


                                                            </View>

                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            }
                                        />)
                                        :
                                        (<View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                                            <Text
                                                numberOfLines={1}
                                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                                            >No Data Available!</Text>
                                        </View>)

                                }
                            </View>

                        </View>

                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },

    tabHolder: {
        padding: 5,
        backgroundColor: "#00D5E1",
        borderRadius: 30,
        flex: 1
    },
    rowContainer: {
        flexDirection: 'row',
        padding: 10,
        backgroundColor: "#FFFFFF",
        borderTopLeftRadius: 2,
        borderBottomLeftRadius: 2,
        borderTopRightRadius: 2,
        borderBottomRightRadius: 2,
        borderColor: '#EAEAEA',
        borderWidth: 2,
        marginRight: 10

    },


});

export default connect(mapStateToPrpos)(WardShiftListing);