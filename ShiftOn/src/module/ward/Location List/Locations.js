/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions,
    TextInput, ToastAndroid, Platform
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import Swipeout from 'react-native-swipeout';
import Api from '../../../apis/Api';
import Loader from '../../../utils/Loader';
import { setToastMsg } from '../../../utils/ToastMessage';
import { connect } from 'react-redux';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
    return {
        userName: props.userLoginDetails.name,
        userid: props.userLoginDetails.id,
        userImage: props.userLoginDetails.image,
    }
}


class Locations extends Component {
    constructor(props) {
        super(props);
        this.state = {
            wardId: this.props.navigation.getParam('wardId', 'NO-item'),
            shiftId: this.props.navigation.getParam('shiftId', 'NO-item'),
            isLoading: true,
            locationData: [],
        };
        //console.log(this.props.navigation);
    }


    componentDidMount() {
        this.unitLocationListing();
    }

    // This is for store unit locations data in dropdown from unitLocationListing api...
    unitLocationListing = () => {

        var rawData = { "unit_id": this.props.userid }

        Api._getUnitAllLocations(rawData)
            .then((response) => {
                console.log(response.data)

                var tempData = [];
                for (var i = 0; i < response.data.length; i++) {
                    if (response.data[i].status.toString() == "1") {
                        tempData.push(response.data[i])
                    }
                }

                this.setState({ locationData: tempData, isLoading: false })
                console.log('Loaction List :', tempData)

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

                    <View style={styles.mainContainer}>
                        <View style={{ backgroundColor: '#00D5E1' }}>
                            <TouchableOpacity
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                                onPress={() => this.props.navigation.goBack()}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                            </TouchableOpacity>
                            <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>LOCATIONS</Text>
                            <TouchableOpacity activeOpacity={1}
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20, right: 0 }}
                            >
                                {/* <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/more_icn.png')} /> */}
                            </TouchableOpacity>
                        </View>

                        <Loader isLoading={this.state.isLoading} />

                        <View style={{ margin: 20, }}>

                            {
                                (this.state.locationData.length > 0) ?

                                    (<FlatList
                                        style={{ width: '100%', marginBottom: '12%' }}
                                        data={this.state.locationData}
                                        keyExtractor={(item, index) => index.toString()}
                                        //ItemSeparatorComponent={this.ItemSeparator}
                                        renderItem={({ item, index }) =>
                                            <View style={{ borderRadius: 2, margin: 2 }}>
                                                <TouchableOpacity
                                                    style={{ marginBottom: 10, }}
                                                    activeOpacity={0.5}
                                                    onPress={() => { this.props.navigation.navigate('ShiftPayRateListing', { wardId: this.state.wardId, shiftId: this.state.shiftId, loactionId: item.id }) }}
                                                >
                                                    <View style={styles.rowContainer}>
                                                        <View style={{ width: '100%', paddingStart: 5 }}>

                                                            <Text
                                                                numberOfLines={1}
                                                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 16, }}
                                                            >
                                                                {item.address + ", " + item.landmark + ", " + item.post_code}
                                                            </Text>

                                                        </View>

                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        }
                                    />)
                                    :
                                    (<View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                                        <Text
                                            numberOfLines={1}
                                            style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                                        >No Data Available!</Text>
                                    </View>)

                            }
                        </View>

                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },

    tabHolder: {
        padding: 5,
        backgroundColor: "#00D5E1",
        borderRadius: 30,
        flex: 1
    },
    rowContainer: {
        flexDirection: 'row',
        padding: 10,
        backgroundColor: "#FFFFFF",
        borderTopLeftRadius: 2,
        borderBottomLeftRadius: 2,
        borderTopRightRadius: 2,
        borderBottomRightRadius: 2,
        borderColor: '#EAEAEA',
        borderWidth: 2,

    },


});

export default connect(mapStateToPrpos)(Locations);