/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, ToastAndroid,
  SafeAreaView, Dimensions, TextInput
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import Api from '../../apis/Api';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';


const { width } = Dimensions.get('window');
const passwordHeading1 = "Forgot your password ?";
const passwordDesc1 = "Enter your e-mail address and we'll send you an OTP to reset your password.";
const passwordHeading2 = "Create new password.";
const passwordDesc2 = "Enter your e-mail address and we'll send you an OTP to creating new password.";

export default class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      forWhat: this.props.navigation.getParam('forWhat', 'NO-item'),
      email: '',
      otp: '',
      password: '',
      c_password: '',
      isOTPSend: false,
      isLoading: false,
      setErrors: {
        field: '',
        message: ''
      }

    };
    console.log(this.props.navigation);
  }

  componentDidMount() {
  }

  checkValidation = () => {
    const emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    let emailError = { field: '', message: '' }

    if (this.state.email == '') {
      emailError.field = "email";
      emailError.message = "Email is required!";
      this.setState({ setErrors: emailError })
    } else if (!emailRegEx.test(this.state.email)) {
      emailError.field = "email";
      emailError.message = "Invalid email!";
      this.setState({ setErrors: emailError })
    } else {
      this.setState({ setErrors: { field: '', message: '' } })
      this._sendOTP();
    }

  }

  _sendOTP = () => {
    this.setState({ isLoading: true })

    var formData = new FormData();
    formData.append("email", this.state.email);

    Api._generateOtpApi(formData)
      .then((response) => {

        console.log(response)
        if (response.status.toString() == "1") {
          this.setState({ isOTPSend: true })
        }
        this.setState({ isLoading: false, })
        setToastMsg(response.message.toString());

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false, })
      });
  }

  checkValidation2 = () => {
    const passCharecterRegex = /^(?=.*[a-z])(?=.*[A-Z])/;
    const passNumRegex = /^(?=.*[0-9])/;

    let otpError = { field: '', message: '' }
    let passwordError = { field: '', message: '' }
    let cPasswordError = { field: '', message: '' }

    if (this.state.otp == '') {
      emailError.field = "otp";
      emailError.message = "OTP is required!";
      this.setState({ setErrors: otpError })
    } else if (this.state.password == '') {
      passwordError.field = "password";
      passwordError.message = "Password is required!";
      this.setState({ setErrors: passwordError })
    } else if (!passCharecterRegex.test(this.state.password)) {
      passwordError.field = "password";
      passwordError.message = "Need atleast 1 upper and 1 lowercase.";
      this.setState({ setErrors: passwordError })
    } else if (!passNumRegex.test(this.state.password)) {
      passwordError.field = "password";
      passwordError.message = "Need atleast 1 digit.";
      this.setState({ setErrors: passwordError })
    } else if (this.state.password.length < 8) {
      passwordError.field = "password";
      passwordError.message = "Please enter at least 8 characters.";
      this.setState({ setErrors: passwordError })
    } else if (this.state.c_password == '') {
      cPasswordError.field = "c_password";
      cPasswordError.message = "Confirm Password is required!";
      this.setState({ setErrors: cPasswordError })
    } else if (this.state.password != this.state.c_password) {
      cPasswordError.field = "c_password";
      cPasswordError.message = "Password & Confirm Password should be equal!";
      this.setState({ setErrors: cPasswordError })
    } else {
      this.setState({ setErrors: { field: '', message: '' } })
      this._setNewPassword();
    }

  }

  _setNewPassword = () => {
    this.setState({ isLoading: true })

    var rawData = {
      "email": this.state.email,
      "otp": this.state.otp,
      "password": this.state.password,
    }

    Api._updatePasswordApi(rawData)
      .then((response) => {

        console.log(response)
        this.setState({ isLoading: false, })
        setToastMsg(response.message.toString());
        if (response.status.toString() == "1") {
          this.props.navigation.goBack();
        }

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false, })
      });
  }


  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <ScrollView>
            <View style={styles.mainContainer}>


              <Image style={styles.headerBg} source={require('./../../assets/login_bgtop.png')} resizeMode="stretch" />

              <Image style={styles.logoImage} source={require('./../../assets/logo_a.png')} resizeMode="stretch" />
              <TouchableOpacity
                style={{ position: 'absolute', marginTop: 20, marginLeft: 10 }}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
              </TouchableOpacity>
              <Loader isLoading={this.state.isLoading} />
              {
                this.state.isOTPSend ?

                  (<View style={{ marginLeft: 50, marginRight: 50 }}>
                    <Text style={styles.heading}>Update new password ?</Text>
                    <Text style={styles.forgotPassDescription}>
                      Enter your OTP and new password to set your new password.
                    </Text>
                    <Text style={styles.emailStyle}>[{this.state.email}]</Text>

                    <View style={{ marginTop: 30 }}>
                      <CustomTextInput
                        placeholder='OTP'
                        keyboardType='default'
                        floatingText="OTP"
                        value={this.state.otp}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({ otp: text });
                        }}
                      />
                      {
                        this.state.setErrors.field == "otp" && (
                          <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                        )
                      }

                      <CustomTextInput
                        placeholder='PASSWORD'
                        keyboardType='default'
                        floatingText="PASSWORD"
                        secureTextEntry={true}
                        returnKeyType={'done'}
                        value={this.state.password}
                        onChangeText={(text) => {
                          this.setState({ password: text });
                        }}
                      />
                      {
                        this.state.setErrors.field == "password" && (
                          <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                        )
                      }

                      <CustomTextInput
                        placeholder='CONFIRM PASSWORD'
                        keyboardType='default'
                        floatingText="CONFIRM PASSWORD"
                        secureTextEntry={true}
                        returnKeyType={'done'}
                        value={this.state.c_password}
                        onChangeText={(text) => {
                          this.setState({ c_password: text });
                        }}
                      />
                      {
                        this.state.setErrors.field == "c_password" && (
                          <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                        )
                      }

                      <View style={{ flexDirection: 'row', marginTop: 20 }}>

                        <TouchableOpacity style={styles.registerHold}
                          onPress={() => this.setState({ isOTPSend: false })}
                        >
                          <Text style={styles.registerText}>CANCEL</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.loginHold}
                          onPress={() => this.checkValidation2()}
                        >
                          <Text style={styles.loginText}>SUBMIT</Text>
                        </TouchableOpacity>

                      </View>


                    </View>
                  </View>)
                  :
                  (<View style={{ marginLeft: 50, marginRight: 50 }}>
                    <Text style={styles.heading}>{
                      this.state.forWhat == "forgotPassword" ?
                        passwordHeading1 : passwordHeading2
                    }</Text>
                    <Text style={styles.forgotPassDescription}>
                      {this.state.forWhat == "forgotPassword" ?
                        passwordDesc1 : passwordDesc2}
                    </Text>

                    <View style={{ marginTop: 30 }}>
                      <CustomTextInput
                        placeholder='EMAIL'
                        keyboardType='email-address'
                        floatingText="EMAIL"
                        value={this.state.email}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({ email: text });
                        }}
                      />
                      {
                        this.state.setErrors.field == "email" && (
                          <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                        )
                      }

                      <View style={{ flexDirection: 'row', marginTop: 20 }}>

                        <TouchableOpacity style={styles.registerHold}
                          onPress={() => this.props.navigation.goBack()}
                        >
                          <Text style={styles.registerText}>CANCEL</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.loginHold}
                          onPress={() => this.checkValidation()}
                        >
                          <Text style={styles.loginText}>SEND OTP</Text>
                        </TouchableOpacity>

                      </View>


                    </View>
                  </View>)
              }
            </View>
          </ScrollView>
        </SafeAreaView>
      </Fragment>
    );
  }


}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 250,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  emailStyle: {
    color: '#00D5E1',
    fontSize: 14,
    fontFamily: 'montserrat_semibold',
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }

});

