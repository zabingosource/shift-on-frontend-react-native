/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions, TextInput,
  ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
import { setToastMsg } from '../../utils/ToastMessage';
import Loader from '../../utils/Loader';
import { connect } from 'react-redux';
import { getDatesInRange } from '../../utils/DateRangList';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
  }
}

const { width } = Dimensions.get('window');

class UnitMyTimeSheet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: '',
      endDate: '',
      isLoading: true,
      apiData: [],
      filterApiData: [],
      isStartDatePickerVisible: false,
      isEndDatePickerVisible: false,
      compareDateValues: '',

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    this._unitTimeSheetList();
  }

  _unitTimeSheetList = () => {
    var rowData = {
      "unit_id": this.props.userid
    };

    Api._unitTimeSheetList(rowData)
      .then((response) => {

        console.log(response.data)

        this.setState({
          apiData: response.data,
          filterApiData: response.data,
          isLoading: false,
        })
        //setToastMsg(response.message.toString());

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false, })
      });
  }

  unitApproveTimeSheet = (id) => {
    this.setState({ isLoading: true })
    var rowData = {
      "unit_id": this.props.userid,
      "id": id,
    };

    Api._unitApproveTimeSheet(rowData)
      .then((response) => {

        console.log(response.data)
        this.setState({ isLoading: false })
        setToastMsg(response.message.toString());
      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false, })
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>

            <View style={styles.mainContainer}>

              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>MY TIME SHEET</Text>

              </View>
              <Loader isLoading={this.state.isLoading} />

              <View style={{ marginLeft: 10, marginRight: 10 }}>

                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginTop: 10, flex: 1 }}>
                    <TouchableOpacity
                    // onPress={() => this._setDate("start")}
                    >
                      <CustomTextInput
                        refer={"START_DATE"}
                        placeholder='START DATE'
                        keyboardType='null'
                        floatingText="START DATE"
                        editable={true}
                        onFocus={true}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        //style={{ fontSize: 16 }}
                        isVisibleCalendar={true}
                        value={this.state.startDate}
                        returnKeyType={'done'}
                        onTouchEnd={() => { this.setState({ isStartDatePickerVisible: true }) }}
                      />
                      <DateTimePickerModal
                        isVisible={this.state.isStartDatePickerVisible}
                        mode="date"
                        date={new Date()}
                        onConfirm={(date) => this.handleStartDateConfirm(date)}
                        onCancel={() => this.setState({ isStartDatePickerVisible: false })}
                      />

                    </TouchableOpacity>

                  </View>
                  <Text style={{ padding: 10, marginTop: 20, alignSelf: 'center', fontFamily: 'montserrat_semibold', fontSize: 16, color: '#000000' }}>To</Text>
                  <View style={{ marginTop: 10, flex: 1 }}>
                    <TouchableOpacity
                    // onPress={() => this._setDate("end")}
                    >
                      <CustomTextInput
                        placeholder='END DATE'
                        keyboardType='default'
                        floatingText="END DATE"
                        editable={true}
                        onFocus={true}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        // style={{ fontSize: 16,}}
                        isVisibleCalendar={true}
                        value={this.state.endDate}
                        returnKeyType={'done'}
                        onTouchEnd={() => { this.setState({ isEndDatePickerVisible: true, }) }}
                      />
                      <DateTimePickerModal
                        isVisible={this.state.isEndDatePickerVisible}
                        mode="date"
                        date={new Date()}
                        onConfirm={(date) => this.handleEndDateConfirm(date)}
                        onCancel={() => this.setState({ isEndDatePickerVisible: false, })}
                      />

                    </TouchableOpacity>

                  </View>
                </View>
                <View style={{ marginTop: 20, marginBottom: 10 }}>

                  {
                    this.state.apiData.length > 0 ?

                      <FlatList
                        style={{ width: '100%', }}
                        data={this.state.apiData}
                        keyExtractor={(item, index) => index.toString()}
                        //ItemSeparatorComponent={this.ItemSeparator}
                        renderItem={({ item, index }) =>
                          <TouchableOpacity
                            activeOpacity={0.9}
                            onPress={() => this.props.navigation.navigate('MyOrderDetails', { order: item })}
                          >
                            <View style={styles.rowContainer}>

                              <View style={{ backgroundColor: '#F1F1F1', height: 130, justifyContent: 'center', borderTopLeftRadius: 20, borderBottomLeftRadius: 20, alignItems: "center", padding: 20 }}>
                                <Text
                                  style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                                >{this.getDateName(item.date)}</Text>
                                <Text
                                  style={{ color: '#00E228', fontFamily: 'montserrat_semibold', fontSize: 35, }}
                                >{this.getDateValue(item.date)}</Text>
                              </View>
                              <View style={{ width: "60%", padding: 10, }}>
                                <Text
                                  style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                                >{item.job.job_title}</Text>
                                <Text
                                  numberOfLines={1}
                                  style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                                >{item.job.job_description} </Text>
                                <View style={{ marginTop: 5, flexDirection: "row" }}>
                                  <Image source={require('./../../assets/pin_icn.png')} style={{ width: 10, height: 15 }} resizeMode='stretch' />
                                  <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>
                                    {item.job.location}{item.job.landmark ? ", " + item.job.landmark : ''}{item.job.zip ? ", " + item.job.zip : ''}
                                  </Text>
                                </View>
                                <View >
                                  <View style={{ marginTop: 5, flexDirection: "row", flex: 1.5, }}>
                                    <Image source={require('./../../assets/calendar_icn_b.png')} style={{ width: 12, height: 12 }} resizeMode='stretch' />
                                    <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>
                                      {item.job.shift.start_date} to {item.job.shift.end_date}
                                    </Text>
                                  </View>
                                  <View style={{ marginTop: 5, flexDirection: "row", flex: 1, }}>
                                    <Image source={require('./../../assets/time_icn_a.png')} style={{ width: 12, height: 12 }} resizeMode='stretch' />
                                    <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>
                                      {item.job.shift.start_time} to {item.job.shift.end_time}
                                    </Text>
                                  </View>

                                </View>

                              </View>
                              <View style={{ justifyContent: 'center', alignItems: 'center' }}>

                                <Text style={{ color: '#FF7B68', fontFamily: 'montserrat_semibold', fontSize: 20, }}>
                                  {
                                    this.getTimeDifference(item.job.shift.start_time, item.job.shift.end_time)
                                  }
                                </Text>

                                <Text style={{ marginTop: -5, color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 12, }}>
                                  HRS
                                </Text>
                                <TouchableOpacity
                                  style={{ marginTop: 35, padding: 7, backgroundColor: '#657CF4', borderRadius: 10, }}
                                  onPress={() => this.unitApproveTimeSheet(item.id)}
                                >
                                  <Text style={{ color: '#FFFFFF', fontFamily: 'montserrat_semibold', fontSize: 10, }}>
                                    APPROVE
                                  </Text>
                                </TouchableOpacity>

                              </View>

                            </View>
                          </TouchableOpacity>
                        }
                      />
                      :
                      <View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                        <Text
                          numberOfLines={1}
                          style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                        >No Timesheet Available!</Text>
                      </View>
                  }
                </View>


              </View>

            </View>


          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _setDate = (type) => {
    if (type == 'start') {
      this.setState({ startDate: "12/09/2020" });
    } else {
      this.setState({ endDate: "30/09/2020" });
    }

  }

  getTimeDifference = (startTime, endTime) => {
    var now = moment(endTime, "H:mm");
    var prev = moment(startTime, "H:mm");
    var result = moment.utc(now.diff(prev)).format("HH:mm");

    return result;
  }

  getDateName = (date) => {
    var splitDate = date.split("/");
    var newDate = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];
    var a = new Date(newDate);
    const weekday = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var r = weekday[a.getDay()];
    return r;

  }

  getDateValue = (date) => {
    var splitDate = date.split("/");
    var newDate = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];
    return splitDate[0];

  }

  getDateInterval = (startDate, endDate) => {
    const days = getDatesInRange(new Date(startDate), new Date(endDate));

    var tempData = [];

    for (var i = 0; i < days.length; i++) {

      for (var j = 0; j < this.state.filterApiData.length; j++) {
        var secondSplitDate = this.state.filterApiData[j].date.split("/");
        var secondNewDate = secondSplitDate[2] + "-" + secondSplitDate[1] + "-" + secondSplitDate[0];

        if (secondNewDate == days[i]) {
          tempData.push(this.state.filterApiData[j])
        }
      }

    }

    console.log("Length= " + tempData.length);
    this.setState({ apiData: tempData });

  }

  // this is for start date picker...
  handleStartDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('YYYY-MM-DD');

    this.setState({
      startDate: dateTimeString,
      endDate: "",
      compareDateValues: date,
      isStartDatePickerVisible: false,
    })

    console.log(dateTimeString)
  };

  // this is for end date picker...
  handleEndDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('YYYY-MM-DD');

    var start = new Date(this.state.compareDateValues);
    var end = new Date(date);

    if (this.state.compareDateValues != '') {
      if (start.getTime() > end.getTime()) {
        this.setState({
          isEndDatePickerVisible: false,
          endDate: "",
        })
        setToastMsg("Date must be greater than start date.");
        console.log("end date is smaller")
      } else {
        this.setState({
          endDate: dateTimeString,
          isEndDatePickerVisible: false,
        })
        console.log("end date is greater")
        this.getDateInterval(this.state.startDate, dateTimeString);
      }
    } else {
      this.setState({
        isEndDatePickerVisible: false,
      })
      setToastMsg("Please select start date first.");
      console.log("start date not selected yet.")
    }
    console.log(dateTimeString)
  };

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center',

  },

  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5


  },

  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  profileImage: {
    height: 150,
    width: 150,
    position: 'absolute',
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: 60

  },

});

export default connect(mapStateToPrpos)(UnitMyTimeSheet);