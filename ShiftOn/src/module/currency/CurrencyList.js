/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions,
    TextInput, ToastAndroid, Platform
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import Swipeout from 'react-native-swipeout';
import Api from '../../apis/Api';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';
import { connect } from 'react-redux';

const mapStateToPrpos = (props) => {
    return {
        userid: props.userLoginDetails.id,
    }
}



class CurrencyList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            apiData: '',
        };
    }

    componentDidMount() {
        this.navFocusListener = this.props.navigation.addListener('didFocus', () => {
            this.unitLocationListing();
        });
    }

    componentWillUnmount() {
        this.navFocusListener.remove();
    }

    unitLocationListing = () => {
        this.setState({ isLoading: true })
        var rawData = { "unit_id": this.props.userid }

        Api._getUnitAllLocations(rawData)
            .then((response) => {
                console.log(response.data)

                var tempData = [];
                for (var i = 0; i < response.data.length; i++) {
                    if (response.data[i].status.toString() == "1") {
                        tempData.push(response.data[i])
                    }
                }

                this.setState({ apiData: tempData, isLoading: false })

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

                    <View style={styles.mainContainer}>
                        <View style={{ backgroundColor: '#00D5E1' }}>
                            <TouchableOpacity
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                                onPress={() => this.props.navigation.goBack()}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                            </TouchableOpacity>
                            <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>ADDED CURRENCIES</Text>
                            <TouchableOpacity
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20, right: 0 }}
                                onPress={() => this.props.navigation.navigate('Currency')}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/more_icn.png')} />
                            </TouchableOpacity>
                        </View>

                        <Loader isLoading={this.state.isLoading} />

                        <View style={{ margin: 20, }}>

                            {
                                (this.state.apiData.length > 0) ?

                                    (<FlatList
                                        style={{ width: '100%', marginBottom: '12%' }}
                                        data={this.state.apiData}
                                        keyExtractor={(item, index) => index.toString()}
                                        //ItemSeparatorComponent={this.ItemSeparator}
                                        renderItem={({ item, index }) =>
                                            <View style={{ borderRadius: 2, margin: 2 }}>
                                                <TouchableOpacity activeOpacity={1} >
                                                    <View style={styles.rowContainer}>
                                                        <View style={{ width: '15%', justifyContent: 'center' }}>
                                                            <Image style={{ height: 38, width: 38, }} source={require('./../../assets/currency_ic4.png')} />
                                                        </View>
                                                        <View style={{ width: '70%' }}>
                                                            <Text
                                                                numberOfLines={2}
                                                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                                                            >{item.address}, {item.landmark}, {item.post_code}</Text>

                                                            <Text
                                                                numberOfLines={1}
                                                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, marginTop: 5, }}
                                                            >
                                                                {
                                                                    item.currency_data ?
                                                                        'Currency: ' + item.currency_data.currency
                                                                        :
                                                                        ''
                                                                }
                                                            </Text>

                                                        </View>
                                                        <View style={{ width: '15%', flexDirection: 'row', position: 'absolute', paddingRight: 10, right: 0, alignSelf: 'center' }}>
                                                            <TouchableOpacity
                                                                onPress={() => { this.props.navigation.navigate('UpdateCurrency', { backData: item }) }}
                                                            >
                                                                <Image style={{ height: 15, width: 15, padding: 11, tintColor: '#000000' }} source={require('./../../assets/pencil.png')} />
                                                            </TouchableOpacity>

                                                        </View>

                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        }
                                    />)
                                    :
                                    (<View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                                    </View>)

                            }
                        </View>

                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },

    tabHolder: {
        padding: 5,
        backgroundColor: "#00D5E1",
        borderRadius: 30,
        flex: 1
    },
    rowContainer: {
        flexDirection: 'row',
        padding: 10,
        backgroundColor: "#FFFFFF",
        borderTopLeftRadius: 2,
        borderBottomLeftRadius: 2,
        borderTopRightRadius: 2,
        borderBottomRightRadius: 2,
        borderColor: '#EAEAEA',
        borderWidth: 2,

    },


});

export default connect(mapStateToPrpos)(CurrencyList);