/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity,
    ScrollView,
    Alert,
    Modal,
    SafeAreaView,
    Dimensions,
    TextInput,
    CheckBox, ToastAndroid
} from 'react-native';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import { setToastMsg } from '../../utils/ToastMessage';
import Api from '../../apis/Api';
import Loader from '../../utils/Loader';
import ModalSelector from '../../components/CustomModelSelector';
import { connect } from 'react-redux';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
    return {
        userid: props.userLoginDetails.id,
    }
}

class Currency extends Component {
    constructor(props) {
        super(props);
        this.state = {

            isLoading: false,
            full_location: '',
            locationId: '',
            currencyId: '',
            currency: '',
            locationData: [],
            currencyData: [],

            setErrors: {
                field: '',
                message: ''
            }

        };
        console.log(this.props.navigation);
    }

    componentDidMount() {
        this.unitLocationListing()
    }

    // This is for store unit locations data in dropdown from unitLocationListing api...
    unitLocationListing = () => {
        this.setState({ isLoading: true })
        var rawData = { "unit_id": this.props.userid }

        Api._getUnitAllLocations(rawData)
            .then((response) => {
                console.log(response.data)

                var tempData = [];
                for (var i = 0; i < response.data.length; i++) {
                    if (response.data[i].status.toString() == "1") {
                        tempData.push(response.data[i])
                    }
                }

                let newArray = tempData.map((item) => {
                    return (
                        {
                            "key": item.id,
                            "label": item.address + ", " + item.landmark + ", " + item.post_code,
                        }
                    )
                })
                this.setState({ locationData: newArray })
                this.getCurrencyListing()

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false });
            });
    }

    // This is for store unit locations data in dropdown from unitLocationListing api...
    getCurrencyListing = () => {

        Api._getCurrencyListApi()
            .then((response) => {
                console.log(response.data)

                let newArray = response.data.map((item) => {
                    return (
                        {
                            "key": item.id,
                            "label": item.currency,
                        }
                    )
                })
                this.setState({ currencyData: newArray, isLoading: false })

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false });
            });
    }


    checkValidation = () => {
        let locationError = { field: '', message: '' }
        let currencyError = { field: '', message: '' }

        if (this.state.full_location == '') {
            locationError.field = "location";
            locationError.message = "Location is required!";
            this.setState({ setErrors: locationError })
        } else if (this.state.currency == '') {
            currencyError.field = "currency";
            currencyError.message = "Currency is required!";
            this.setState({ setErrors: currencyError })
        } else {
            this.setState({ setErrors: { field: '', message: '' } })
            this.checkLocation();
        }

    }

    checkLocation = () => {
        this.setState({ isLoading: true })
        var rawData = { "unit_id": this.props.userid }

        Api._getUnitAllLocations(rawData)
            .then((response) => {
                console.log(response.data)

                for (var i = 0; i < response.data.length; i++) {
                    if (response.data[i].id == this.state.locationId) {
                        if (response.data[i].currency_data) {
                            this.setState({ isLoading: false });
                            setToastMsg("This location is already tagged with another currency.");
                        } else {
                            this.addLocationToCurrency();
                        }
                    }
                }


            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false });
            });
    }

    // Call AddJob Api after click submit button...
    addLocationToCurrency = () => {

        this.setState({ isLoading: true })

        var rawData = {
            "location_id": this.state.locationId,
            "currency_id": this.state.currencyId
        }

        Api._addCurrencyToLocationApi(rawData)
            .then((response) => {

                console.log(response)
                this.setState({ isLoading: false })
                setToastMsg(response.message.toString());
                this.props.navigation.goBack();

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false })
            });
    }

    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
                    <View style={styles.mainContainer}>

                        <View style={styles.mainContainer}>

                            <View style={{ backgroundColor: '#00D5E1' }}>
                                <TouchableOpacity
                                    style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                                    onPress={() => this.props.navigation.goBack()}
                                >
                                    <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                                </TouchableOpacity>
                                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>ADD NEW CURRENCY</Text>

                            </View>
                            <Loader isLoading={this.state.isLoading} />
                            <ScrollView>
                                <View style={{ marginTop: 10, marginLeft: 30, marginRight: 30 }}>

                                    <View style={{ marginTop: 5 }}>
                                        <ModalSelector
                                            data={this.state.locationData}
                                            keyExtractor={(item, index) => item.key.toString()}
                                            initValue="UNIT/LOCATION"
                                            accessible={true}
                                            animationType="fade"
                                            search={false}
                                            scrollViewAccessibilityLabel={'Scrollable options'}
                                            cancelButtonAccessibilityLabel={'Cancel Button'}
                                            onChange={(option) => {
                                                this.setState({
                                                    locationId: option.key,
                                                    full_location: option.label,
                                                })
                                            }}>
                                            <CustomTextInput
                                                placeholder='UNIT/LOCATION'
                                                keyboardType='default'
                                                floatingText="UNIT/LOCATION"
                                                editable={true}
                                                onFocus={false}
                                                showSoftInputOnFocus={false}
                                                caretHidden={true}
                                                isVisibleDropDown={true}
                                                value={this.state.full_location}
                                                returnKeyType={'done'}
                                            />
                                        </ModalSelector>
                                    </View>
                                    {
                                        this.state.setErrors.field == "location" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                    <View style={{ marginTop: 5 }}>
                                        <ModalSelector
                                            data={this.state.currencyData}
                                            keyExtractor={(item, index) => item.key.toString()}
                                            initValue="CURRENCY"
                                            accessible={true}
                                            animationType="fade"
                                            search={false}
                                            scrollViewAccessibilityLabel={'Scrollable options'}
                                            cancelButtonAccessibilityLabel={'Cancel Button'}
                                            onChange={(option) => {
                                                this.setState({
                                                    currencyId: option.key,
                                                    currency: option.label,
                                                })
                                            }}>
                                            <CustomTextInput
                                                placeholder='CURRENCY'
                                                keyboardType='default'
                                                floatingText="CURRENCY"
                                                editable={true}
                                                onFocus={false}
                                                showSoftInputOnFocus={false}
                                                caretHidden={true}
                                                isVisibleDropDown={true}
                                                value={this.state.currency}
                                                returnKeyType={'done'}
                                            />
                                        </ModalSelector>
                                    </View>
                                    {
                                        this.state.setErrors.field == "currency" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                </View>
                            </ScrollView>
                        </View>
                        <View >

                            <View style={styles.shadow}></View>
                            {/*  <Image style={{ width:'100%',height:40,}} source={require('./../../assets/border.png')} /> */}
                            <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                                onPress={() => this.checkValidation()}
                            >
                                <Text style={styles.loginText}>ADD</Text>
                            </TouchableOpacity>

                        </View>


                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },
    headerBg: {
        width: width,
        height: 150,
        alignSelf: 'center',

    },

    rowContainer: {
        flexDirection: 'row',
        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        borderColor: '#EAEAEA',
        borderWidth: 2,
        marginBottom: 5


    },
    floatingLabelText: {
        color: "#393FA0",
        marginTop: 10,
        backgroundColor: '#FFFFFF',
        paddingRight: 5,

        fontSize: 14,
        fontFamily: "montserrat_regular"
    },
    heading: {
        alignSelf: 'center',
        fontSize: 22,
        fontFamily: 'montserrat_semibold',
        color: '#353C9E',
        marginTop: 10

    },
    loginText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontFamily: "montserrat_semibold",
        alignSelf: 'center'

    },
    textInput_style: {
        flex: 1,
        fontSize: 20,
        padding: 15,
        backgroundColor: '#FFFFFF',
        borderColor: '#00D5E1',
        borderRadius: 20,
        borderWidth: 2,
        color: 'rgb(63, 82, 110)',
    },
    profileImage: {
        height: 150,
        width: 150,
        position: 'absolute',
        borderRadius: 100,
        alignSelf: 'center',
        marginTop: 60

    },
    shadow: {
        flexDirection: 'column-reverse',
        backgroundColor: "#F3F3F3",
        height: 0.2,
        elevation: 3,
        shadowColor: "#F3F3F3",
        shadowOpacity: 1,
        shadowRadius: 10,
        shadowOffset: {
            height: 10,
            width: 10
        }

    },
    errors: {
        color: 'red',
        fontSize: 14,
        marginLeft: 10,
    }

});

export default connect(mapStateToPrpos)(Currency);