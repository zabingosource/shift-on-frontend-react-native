/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';




const { width } = Dimensions.get('window');

export default class RegistrationFirst extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',

    };
    console.log(this.props.navigation);
  }



  componentDidMount() {

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <ScrollView>
            <View style={styles.mainContainer}>



              <Image style={styles.headerBg} source={require('./../../assets/login_bgtop.png')} resizeMode="stretch" />

              <Image style={styles.logoImage} source={require('./../../assets/logo_a.png')} resizeMode="stretch" />
              <TouchableOpacity
                style={{ position: 'absolute', marginTop: 20, marginLeft: 10 }}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
              </TouchableOpacity>

              <View style={{ marginLeft: 50, marginRight: 50 }}>
                <Text style={styles.heading}>Registration : Step 1</Text>

                <View style={{ marginTop: 30 }}>
                  <CustomTextInput
                    placeholder='EMAIL'
                    keyboardType='email-address'
                    floatingText="EMAIL"
                    value={this.state.email}
                    returnKeyType={'done'}
                    autoCapitalize = 'none'
                    onChangeText={(text) => {
                      this.setState({
                        email: text,
                      });
                    }}
                  />

                  <View style={{ flexDirection: 'row', marginTop: 20 }}>
                    <TouchableOpacity style={styles.loginHold}
                      onPress={() => this.props.navigation.navigate('RegistrationSecond')}
                    >
                      <Text style={styles.loginText}>SUBMIT</Text>
                    </TouchableOpacity>

                  </View>


                </View>
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </Fragment>
    );
  }


}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 250,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },

});

