/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../../components/CustomTextInput';




const { width } = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});

export default class WorkerStepSix extends Component {
  constructor(props) {
    super(props);
    this.state = {
      employerame: '',
      address1: '',
      address2: '',
      mobile: '',
      managername: '',
      datestarted: '',
      position: '',
      leavereason: '',
      selectedTab: '1',

    };
    console.log(this.props.navigation);
  }



  componentDidMount() {

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() =>  Alert.alert(
                    "Alert",
                    'If you go back you will be lost your all data',
                    [
                      { text: 'NO', onPress: () => console.log('NO pressed') },
                      { text: 'YES', onPress: () => this.props.navigation.navigate('Login') },
                    
                    ],
                    { cancelable: false },
                  )}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>APPLICATION FORM</Text>

              </View>
              <ScrollView>
                <View>
                  <Text style={styles.heading}> Step 6</Text>
                  <Text style={styles.heading_mail}>Professional References</Text>


                  <View style={{ flexDirection: 'row', marginTop: 10, backgroundColor: '#000000', borderRadius: 40 ,marginLeft:10,marginRight:10}}>
                    <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "1" ? "#00D5E1" : "#000000" }]}
                      activeOpacity={1}
                      onPress={() => this._onChangeTab("1")}
                    >
                      <Text style={{ alignSelf: 'center', fontSize: 12, padding: 7, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "1" ? "#000000" : "#FFFFFF" }}>REFERENCE-1</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "2" ? "#00D5E1" : "#000000" }]}
                      activeOpacity={1}
                      onPress={() => this._onChangeTab("2")}
                    >
                      <Text style={{ alignSelf: 'center', fontSize: 12, padding: 7, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "2" ? "#000000" : "#FFFFFF" }}>REFERENCE-2</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "3" ? "#00D5E1" : "#000000" }]}
                      activeOpacity={1}
                      onPress={() => this._onChangeTab("3")}
                    >
                      <Text style={{ alignSelf: 'center', fontSize: 12, padding: 7, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "3" ? "#000000" : "#FFFFFF" }}>REFERENCE-3</Text>
                    </TouchableOpacity>

                  </View>

                  <View style={{ marginLeft: 50, marginRight: 50 }}>

                    <View style={{ marginTop: 20 }}>
                      <CustomTextInput
                        placeholder='NAME'
                        keyboardType='default'
                        floatingText="NAME"
                        value={this.state.employerame}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({
                            employerame: text,
                          });
                        }}
                      />
                    </View>

                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='JOB TITLE'
                        keyboardType='default'
                        floatingText="JOB TITLE"
                        value={this.state.address1}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({
                            address1: text,
                          });
                        }}
                      />
                    </View>
                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='HOW DO YOU KNOW THIS PERSON?'
                        keyboardType='default'
                        multiline= {true}
                        style={{ textAlignVertical: "top", height: 150, }}
                        floatingText="HOW DO YOU KNOW THIS PERSON?"
                        value={this.state.address2}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({
                            address2: text,
                          });
                        }}
                      />
                    </View>
                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='COMPANY'
                        keyboardType='default'
                        floatingText="COMPANY"
                        value={this.state.mobile}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({
                            mobile: text,
                          });
                        }}
                      />
                    </View>
                    <View style={{ marginTop: 5 }}>
                      <TouchableOpacity>
                        <CustomTextInput
                          placeholder='ADDRESS'
                          keyboardType='default'
                          floatingText="ADDRESS"
                          value={this.state.managername}
                          returnKeyType={'done'}
                          onChangeText={(text) => {
                            this.setState({
                              managername: text,
                            });
                          }}
                        />
                      </TouchableOpacity>

                    </View>



                    <View style={{ marginTop: 5 }}>
                      <TouchableOpacity>
                        <CustomTextInput
                          placeholder='CONTACT NUMBER'
                          keyboardType='numeric'
                          floatingText="CONTACT NUMBER"
                          value={this.state.position}
                          returnKeyType={'done'}
                          onChangeText={(text) => {
                            this.setState({
                              position: text,
                            });
                          }}
                        />

                      </TouchableOpacity>

                    </View>
                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='EMAIL'
                        keyboardType='email-address'
                        floatingText="EMAIL"
                        value={this.state.leavereason}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({
                            leavereason: text,
                          });
                        }}
                      />
                    </View>

                  
                  </View>
                  <Text style={{
                    fontSize: 12,
                    fontFamily: 'montserrat_light',
                    color: '#000000',
                    marginLeft: 20,
                    marginRight: 20,
                    marginTop: 20,
                    borderRadius: 10,
                    padding: 20,
                    backgroundColor: '#CAE8A2'
                  }}>Note:Please provide full names and address of <Text style={{ fontWeight: 'bold', }}>three professional employment referees.</Text>
                  your <Text style={{ fontWeight: 'bold', }}>first reference</Text> must be from your
                  <Text style={{ fontWeight: 'bold', }}>current or last place of work </Text>
                   and addressed to your line manager.ABCD Corp.Cannot use friends or relatives for any
                  reference,employment reference can not be sent to private or personal home addresses.
                  You must provide workplace addresses and the referee must be a higher grade of staff than you yourself
                  i.e your line
                  </Text>
                  <View style={{ flexDirection: 'row',marginRight:20,marginLeft:20 }}>
                      <TouchableOpacity style={{ flex: 1, padding: 10, marginTop: 20, backgroundColor: "#7F7F7F", borderRadius: 20, marginBottom: 10 }}
                        onPress={() => this.props.navigation.goBack()}
                      >
                        <Text style={styles.loginText}>BACK</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={{ flex: 3, padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 10, marginBottom: 10 }}
                        onPress={() => this.props.navigation.navigate('WorkerStepSeven')}
                      >
                        <Text style={styles.loginText}>NEXT</Text>
                      </TouchableOpacity>

                    </View>

                </View>

              </ScrollView>
            </View>



          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _setDate = (type) => {
    if (type == 'datestarted') {
      this.setState({ datestarted: "15/09/2020" });
    } else if (type == 'finish_date') {
      this.setState({ finish_date: "20/09/2020" });
    }
  }

  _onChangeTab(tab) {
    this.setState({ selectedTab: tab });
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#585858',
    textAlign: 'center',
    marginTop: 10
  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  tabHolder: {
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 30,
    flex: 1
  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  }

});

