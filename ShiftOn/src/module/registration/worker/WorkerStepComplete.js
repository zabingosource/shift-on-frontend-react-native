/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../../components/CustomTextInput';




const { width } = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});

export default class WorkerStepComplete extends Component {
  constructor(props) {
    super(props);
    this.state = {
      criminalDetails: '',
      clinicalDetails: '',
      isCriminal: false,
      isClinical: false,
      date: '',

    };
    console.log(this.props.navigation);
  }



  componentDidMount() {

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() =>  Alert.alert(
                    "Alert",
                    'If you go back you will be lost your all data',
                    [
                      { text: 'NO', onPress: () => console.log('NO pressed') },
                      { text: 'YES', onPress: () => this.props.navigation.navigate('Login') },
                    
                    ],
                    { cancelable: false },
                  )}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>APPLICATION FORM</Text>

              </View>
              <ScrollView>
                <View style={{ }}>
                  <Text style={styles.heading}> Complete!</Text>
                  <Text style={styles.heading_mail}>Employment Act and Sign</Text>

                  <View style={{ marginLeft: 50, marginRight: 50 }}>
                    <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 20, marginTop: 20 }}>

                      <View>
                        <Text
                          style={{
                            color: "#393FA0",
                            marginLeft: 15,
                            paddingRight: 15,
                            paddingLeft: 5,
                            padding: 5,
                            fontSize: 13,
                            fontFamily: "montserrat_regular"
                          }}
                        >Do you wish to work more than 48 hrs per week?</Text>


                        <View style={{ flexDirection: 'row' }}>
                          <TouchableOpacity style={{ alignItems: 'center', marginLeft: 15, marginBottom: 10 }}
                            onPress={() => this.setState({ isCriminal: !this.state.isCriminal })}
                          >
                            <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                              {
                                (this.state.isCriminal) ? (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_on_icn.png')} />) : (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_off_icn.png')} />)
                              }
                              <Text
                                style={{
                                  color: "#000000",
                                  fontSize: 15,
                                  fontFamily: "montserrat_regular",
                                  padding: 10
                                }}
                              >I WISH</Text>
                            </View>

                          </TouchableOpacity>
                          <TouchableOpacity style={{ alignItems: 'center', marginLeft: 15, marginBottom: 10 }}
                            onPress={() => this.setState({ isCriminal: !this.state.isCriminal })}
                          >
                            <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                              {
                                (this.state.isCriminal) ? (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_off_icn.png')} />) : (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_on_icn.png')} />)
                              }
                              <Text
                                style={{
                                  color: "#000000",
                                  fontSize: 15,
                                  fontFamily: "montserrat_regular",
                                  padding: 10
                                }}
                              >NOT WISH</Text>
                            </View>

                          </TouchableOpacity>
                        </View>

                      </View>

                    </View>

                    <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 20, marginTop: 20 }}>

                      <View>
                        <Text
                          style={{
                            color: "#393FA0",
                            marginLeft: 15,
                            paddingRight: 15,
                            paddingLeft: 5,
                            padding: 5,
                            fontSize: 13,
                            fontFamily: "montserrat_regular"
                          }}
                        >How did you hear about ABCD Corp?{"\n"} Please provide detail:</Text>
                        <TextInput
                          style={{ height: 140, textAlignVertical: "top", color: '#000000', fontSize: 15, marginLeft: 15, padding: 5 }}
                          multiline={true}
                          placeholder="Input Detail"
                          //onChangeText={text => setText(text)}
                          //defaultValue={text}
                          placeholderTextColor='#585858'
                        />


                      </View>

                    </View>

                    <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 20, marginTop: 20 }}>

                      <View>
                        <Text
                          style={{
                            color: "#393FA0",
                            marginLeft: 15,
                            paddingRight: 15,
                            paddingLeft: 5,
                            padding: 5,
                            fontSize: 13,
                            fontFamily: "montserrat_regular"
                          }}
                        >Signed</Text>

                        <TouchableOpacity style={{ alignSelf: 'center', alignItems: 'center' }}>
                          <View>
                            <Image style={{ height: 50, width: 80, padding: 10, alignSelf: 'center' }} source={require('./../../../assets/upload_img.png')} resizeMode='stretch' />
                            <Text
                              style={{ color: "#000000", fontSize: 15, fontFamily: "montserrat_regular", padding: 10, marginTop: 10 }}
                            >Upload Signature</Text>
                          </View>

                        </TouchableOpacity>
                      </View>

                    </View>

                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='ANY ADDITIONAL INFORMATION'
                        keyboardType='default'
                        floatingText="ANY ADDITIONAL INFORMATION"
                        style={{ textAlignVertical: "top", height: 150, }}
                        value={this.state.criminalDetails}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({
                            criminalDetails: text,
                          });
                        }}
                      />
                    </View>

                    <View style={{ marginTop: 5, }}>
                      <CustomTextInput
                        placeholder='DATE'
                        keyboardType='default'
                        onFocus={true}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        isVisibleCalendar={true}
                        floatingText="DATE"
                        value={this.state.date}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({
                            date: text,
                          });
                        }}
                        onTouchEnd={() => this._setDate("date")}
                      />
                    </View>

                  </View>

                  <Text style={{
                    fontSize: 12,
                    fontFamily: 'montserrat_light',
                    color: '#000000',
                    marginLeft: 20,
                    marginRight: 20,
                    marginTop: 20,
                    borderRadius: 10,
                    padding: 20,
                    backgroundColor: '#CAE8A2'
                  }}>Note:This employment is not exempt from the provisioms of the rehabilition of young
                  offenders Act 1947 you are not therefore entailed to with hold information requested by the
                  company about any previous convictions in this country or abroad you may have, even if in
                  other circumstances these would appear spent.I confirm that the information I have given
                  is true. I understand that if information given on the application from is found to be false.. 
                  </Text>


                  <View style={{ flexDirection: 'row', marginLeft: 30,marginRight: 30,}}>
                    <TouchableOpacity style={{ flex: 1, padding: 10, marginTop: 20, backgroundColor: "#7F7F7F", borderRadius: 20, marginBottom: 10 }}
                      onPress={() => this.props.navigation.goBack()}
                    >
                      <Text style={styles.loginText}>BACK</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 3, padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 10, marginBottom: 10 }}
                      onPress={() => this.props.navigation.navigate('Login')}
                    >
                      <Text style={styles.loginText}>SUBMIT</Text>
                    </TouchableOpacity>

                  </View>
                </View>

              </ScrollView>
            </View>



          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _setDate = (type) => {
    if (type == 'date') {
      this.setState({ date: "15/09/2020" });
    } else if (type == 'finish_date') {
      this.setState({ finish_date: "20/09/2020" });
    }
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#585858',
    textAlign: 'center',
    marginTop: 10
  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  }

});

