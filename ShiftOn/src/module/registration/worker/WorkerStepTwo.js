/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../../components/CustomTextInput';




const { width } = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});

const Jobdata = [
  {
    storename: 'Store Item 0',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  }, {
    storename: 'Store Item 1',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 2',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 3',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
]

export default class WorkerStepTwo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      employerame: '',
      address1: '',
      address2: '',
      academicQualification: [],
      professionalQualification: []
    };
    console.log(this.props.navigation);
  }



  componentDidMount() {

    var acqdemicQualification={}
    var tempList= []
    acqdemicQualification.qualification =''
    acqdemicQualification.date ='DATE'
    tempList.push(acqdemicQualification)
    this.setState({ academicQualification: tempList }); 
    
    var professionalQualification={}
    var tempList1= []
    professionalQualification.qualification =''
    professionalQualification.date ='DATE'
    tempList1.push(professionalQualification)
    this.setState({ professionalQualification: tempList1 });

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => Alert.alert(
                    "Alert",
                    'If you go back you will be lost your all data',
                    [
                      { text: 'NO', onPress: () => console.log('NO pressed') },
                      { text: 'YES', onPress: () => this.props.navigation.navigate('Login') },
                    
                    ],
                    { cancelable: false },
                  )}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>APPLICATION FORM</Text>

              </View>
              <ScrollView>
                <View style={{}}>
                  <Text style={styles.heading}> Step 2</Text>
                  <Text style={styles.heading_mail}>Current Employment {"\n"} information</Text>

                  <View style={{ marginTop: 20, marginLeft: 50, marginRight: 50 }}>


                    {/* <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 10, }}>
                      <TextInput
                        style={{ height: 40, color: '#000000', fontSize: 15, marginLeft: 15, padding: 5 }}
                        placeholder="Input qualification"
                        //onChangeText={text => setText(text)}
                        //defaultValue={text}
                        placeholderTextColor='#585858'
                      />

                      <Text style={{ height: 1, backgroundColor: '#00D5E1' }}> </Text>
                      <TouchableOpacity
                        onPress={() => alert('aaa')}
                      >
                        <View>
                          <Text
                            style={{
                              color: "#393FA0",
                              marginLeft: 15,
                              backgroundColor: '#FFFFFF',
                              paddingRight: 5,
                              paddingLeft: 5,
                              fontSize: 10,
                              fontFamily: "montserrat_regular"
                            }}
                          > DATE</Text>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text
                              style={{
                                color: "#000000",
                                marginLeft: 15,
                                backgroundColor: '#FFFFFF',
                                paddingRight: 5,
                                paddingLeft: 5,
                                fontSize: 15,
                                fontFamily: "montserrat_regular"
                              }}
                            > Date</Text>
                            <Image style={{ height: 20, width: 20, padding: 10, marginRight: 10, marginBottom: 10 }} source={require('./../../../assets/calendar_icn_c.png')} />
                          </View>


                        </View>

                      </TouchableOpacity>

                    </View>
                    <Text 
                      style={{
                        color: "#393FA0",
                        marginLeft: 15,
                        position: 'absolute',
                        marginTop: -8,
                        backgroundColor: '#FFFFFF',
                        paddingRight: 5,
                        paddingLeft: 5,
                        fontSize: 10,
                        fontFamily: "montserrat_regular"
                      }}
                    > QUALIFICATION</Text>*/}
                    <FlatList
                      style={{ width: '100%', marginBottom: '12%' }}
                      data={this.state.academicQualification}
                      keyExtractor={(item, index) => index.toString()}
                      //ItemSeparatorComponent={this.ItemSeparator}
                      renderItem={({ item, index }) =>
                        <View style={{ marginTop: 10 }}>
                          <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 10, }}>
                            <TextInput
                              style={{ height: 40, color: '#000000', fontSize: 15, marginLeft: 15, padding: 5 }}
                              placeholder="Input qualification"
                              onChangeText={(text) => {
                                this._oNChangeQualification(text,index,'academic')
                              }}
                              defaultValue={item.qualification}
                              placeholderTextColor='#585858'
                            />

                            <Text style={{ height: 1, backgroundColor: '#00D5E1' }}> </Text>
                            <TouchableOpacity
                              onPress={() =>  this._oNChangeDate("12/11/2020",index,'academic')}
                            >
                              <View>
                                <Text
                                  style={{
                                    color: "#393FA0",
                                    marginLeft: 15,
                                    backgroundColor: '#FFFFFF',
                                    paddingRight: 5,
                                    paddingLeft: 5,
                                    fontSize: 10,
                                    fontFamily: "montserrat_regular"
                                  }}
                                > DATE</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                  <Text
                                    style={{
                                      color: "#000000",
                                      marginLeft: 15,
                                      backgroundColor: '#FFFFFF00',
                                      paddingRight: 5,
                                      paddingLeft: 5,
                                      fontSize: 15,
                                      fontFamily: "montserrat_regular"
                                    }}
                                  > {item.date}</Text>
                                  <Image style={{ height: 20, width: 20, padding: 10, marginRight: 10, marginBottom: 10 }} source={require('./../../../assets/calendar_icn_c.png')} />
                                </View>


                              </View>

                            </TouchableOpacity>

                          </View>
                          <Text
                            style={{
                              color: "#393FA0",
                              marginLeft: 15,
                              position: 'absolute',
                              marginTop: -8,
                              backgroundColor: '#FFFFFF',
                              paddingRight: 5,
                              paddingLeft: 5,
                              fontSize: 10,
                              fontFamily: "montserrat_regular"
                            }}
                          > QUALIFICATION</Text>


                        </View>
                      }
                    />
                    <TouchableOpacity style={{ marginTop: -30, alignSelf: 'center', padding: 5, }}
                      onPress={() => this._addAcademicQualification()}
                    >
                      <Text
                        style={{
                          color: "#000000",
                          fontSize: 12,
                          fontFamily: "montserrat_regular"
                        }}
                      >+ ADD QUALIFICATION</Text>
                    </TouchableOpacity>


                  </View>
                  <Text style={{ height: 1, backgroundColor: '#585858', marginTop: 10 }}> </Text>


                  <Text style={styles.heading_mail}>Professional & Clinical {"\n"} Training & Qualifications</Text>

                  <View style={{ marginTop: 20, marginLeft: 50, marginRight: 50 }}>


                    <FlatList
                      style={{ width: '100%', marginBottom: '12%' }}
                      data={this.state.professionalQualification}
                      keyExtractor={(item, index) => index.toString()}
                      //ItemSeparatorComponent={this.ItemSeparator}
                      renderItem={({ item, index }) =>
                        <View style={{ marginTop: 10 }}>
                          <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 10, }}>
                            <TextInput
                              style={{ height: 40, color: '#000000', fontSize: 15, marginLeft: 15, padding: 5 }}
                              placeholder="Professional & Clinical Training "
                              onChangeText={(text) => {
                                this._oNChangeQualification(text,index,'professional')
                              }}
                              defaultValue={item.qualification}
                              placeholderTextColor='#585858'
                            />

                            <Text style={{ height: 1, backgroundColor: '#00D5E1' }}> </Text>
                            <TouchableOpacity
                              onPress={() =>  this._oNChangeDate("14/11/2020",index,'professional')}
                            >
                              <View>
                                <Text
                                  style={{
                                    color: "#393FA0",
                                    marginLeft: 15,
                                    backgroundColor: '#FFFFFF',
                                    paddingRight: 5,
                                    paddingLeft: 5,
                                    fontSize: 10,
                                    fontFamily: "montserrat_regular"
                                  }}
                                > DATE</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                  <Text
                                    style={{
                                      color: "#000000",
                                      marginLeft: 15,
                                      backgroundColor: '#FFFFFF00',
                                      paddingRight: 5,
                                      paddingLeft: 5,
                                      fontSize: 15,
                                      fontFamily: "montserrat_regular"
                                    }}
                                  > {item.date}</Text>
                                  <Image style={{ height: 20, width: 20, padding: 10, marginRight: 10, marginBottom: 10 }} source={require('./../../../assets/calendar_icn_c.png')} />
                                </View>


                              </View>

                            </TouchableOpacity>

                          </View>
                          <Text
                            style={{
                              color: "#393FA0",
                              marginLeft: 15,
                              position: 'absolute',
                              marginTop: -8,
                              backgroundColor: '#FFFFFF',
                              paddingRight: 5,
                              paddingLeft: 5,
                              fontSize: 10,
                              fontFamily: "montserrat_regular"
                            }}
                          > PROFESSIONAL & CLINICAL TRAINING</Text>


                        </View>
                      }
                    />
                    <TouchableOpacity style={{ marginTop: -30, alignSelf: 'center', padding: 5, }}
                     onPress={() => this._addProffesionalQualification()}
                    >
                      <Text
                        style={{
                          color: "#000000",
                          fontSize: 12,
                          fontFamily: "montserrat_regular"
                        }}
                      >+ ADD MORE</Text>
                    </TouchableOpacity>


                  </View>
                  <Text style={{ height: 1, backgroundColor: '#585858', marginTop: 10 }}> </Text>

                  <Text style={{
                    fontSize: 12,
                    fontFamily: 'montserrat_light',
                    color: '#000000',
                    marginLeft: 10,
                    marginRight: 10,
                    marginTop: 10,
                    borderRadius: 10,
                    padding: 20,
                    backgroundColor: '#CAE8A2'
                  }}>Note:Please bring with you original certificates of all relevant qualifications and certificates you have obtained</Text>

                  <View style={{flexDirection:'row'}}>
                  <TouchableOpacity style={{flex:1, padding: 10, marginTop: 20, backgroundColor: "#7F7F7F", borderRadius: 20, marginLeft: 40, marginBottom: 10 }}
                    onPress={() => this.props.navigation.goBack()}
                  >
                    <Text style={styles.loginText}>BACK</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{flex:3, padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginRight: 40,marginLeft:10, marginBottom: 10 }}
                    onPress={() => this.props.navigation.navigate('WorkerStepThree')}
                  >
                    <Text style={styles.loginText}>NEXT</Text>
                  </TouchableOpacity>

                  </View>
                 
                </View>




              </ScrollView>
            </View>



          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _oNChangeQualification = (text,pos,type) =>{

    if(type== 'academic'){
      let tempList = [...this.state.academicQualification]
      tempList[pos].qualification = text
      this.setState({ academicQualification: tempList });
    }else{
      let tempList = [...this.state.professionalQualification]
      tempList[pos].qualification = text
      this.setState({ professionalQualification: tempList });
    }
  }
     _oNChangeDate = (text,pos,type) =>{

    if(type== 'academic'){
      let tempList = [...this.state.academicQualification]
      tempList[pos].date = text
      this.setState({ academicQualification: tempList });
    }else{
      let tempList1 = [...this.state.professionalQualification]
      tempList1[pos].date = text
      this.setState({ professionalQualification: tempList1 });
    }
  } 
   _addAcademicQualification = () =>{
    var acqdemicQualification={}
    let tempList = [...this.state.academicQualification]
    acqdemicQualification.qualification =''
    acqdemicQualification.date ='DATE'
    tempList.push(acqdemicQualification)
    this.setState({ academicQualification: tempList });

  } 
    _addProffesionalQualification = () =>{
    var professionalcQualification={}
    let tempList = [...this.state.professionalQualification]
    professionalcQualification.qualification =''
    professionalcQualification.date ='DATE'
    tempList.push(professionalcQualification)
    this.setState({ professionalQualification: tempList });

  }



}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#585858',
    textAlign: 'center',
    marginTop: 10
  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  }

});

