/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../../components/CustomTextInput';




const { width } = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});

const Jobdata = [
  {
    storename: 'Store Item 0',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  }, {
    storename: 'Store Item 1',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  
]

export default class WorkerStepFour extends Component {
  constructor(props) {
    super(props);
    this.state = {
      employerame: '',
      address1: '',
      address2: '',
      employmentHistory: [],
    };
    console.log(this.props.navigation);
  }



  componentDidMount() {

    var employmentHistory={}
    var tempList= []
    employmentHistory.employeerName =''
    employmentHistory.address =''
    employmentHistory.position =''
    employmentHistory.startDate =''
    employmentHistory.endDate =''
    employmentHistory.reason =''
    tempList.push(employmentHistory)
    this.setState({ employmentHistory: tempList }); 

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => Alert.alert(
                    "Alert",
                    'If you go back you will be lost your all data',
                    [
                      { text: 'NO', onPress: () => console.log('NO pressed') },
                      { text: 'YES', onPress: () => this.props.navigation.navigate('Login') },
                    
                    ],
                    { cancelable: false },
                  )}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>APPLICATION FORM</Text>

              </View>
              <ScrollView>
                <View style={{}}>
                  <Text style={styles.heading}> Step 4</Text>
                  <Text style={styles.heading_mail}>Full Employment History</Text>
                  <Text style={{ alignSelf: 'center', fontSize: 10, fontFamily: 'montserrat_regular', color: '#585858', textAlign: 'center', }}>(including any gaps - most recent first)</Text>

                  <View style={{ marginTop: 20, }}>
                   
                    <FlatList
                      style={{ width: '100%', marginBottom: '12%' }}
                      data={this.state.employmentHistory}
                      keyExtractor={(item, index) => index.toString()}
                      //ItemSeparatorComponent={this.ItemSeparator}
                      renderItem={({ item, index }) =>
                      <View >
                      <View style={{ marginLeft: 50, marginRight: 50 }}>

                        <View style={{ marginTop: 5 }}>
                          <CustomTextInput
                            placeholder='NAME OF EMPLOYER'
                            keyboardType='default'
                            floatingText="NAME OF EMPLOYER"
                            value={this.state.employmentHistory[index].employeer}
                            returnKeyType={'done'}
                            onChangeText={(text) => {
                             this._oNChangeDetails(text,index,'employeer')
                            }}
                          />
                        </View>

                        <View style={{ marginTop: 5 }}>
                            <CustomTextInput
                            placeholder='ADDRESS'
                            keyboardType='default'
                            floatingText="ADDRESS"
                            value={this.state.employmentHistory[index].address}
                            returnKeyType={'done'}
                            onChangeText={(text) => {
                              this._oNChangeDetails(text,index,'address')
                             }}
                          />
                        </View>
                        <View style={{ marginTop: 5 }}>
                          <TouchableOpacity>
                            <CustomTextInput
                              placeholder='POSITION HELD'
                              keyboardType='default'
                              floatingText="POSITION HELD"
                              value={this.state.employmentHistory[index].position}
                              returnKeyType={'done'}
                              onChangeText={(text) => {
                                this._oNChangeDetails(text,index,'position')
                               }}
                            />

                          </TouchableOpacity>

                        </View>

                        <View style={{ flexDirection: 'row' }}>
                          <View style={{ marginTop: 5, flex: 1, marginRight: 5 }}>
                            <TouchableOpacity
                            // onPress={() => this._setDate("start")}
                            >
                              <CustomTextInput
                                refer={"START_DATE"}
                                placeholder='START DATE'
                                keyboardType='null'
                                floatingText="START DATE"
                                editable={true}
                                onFocus={true}
                                showSoftInputOnFocus={false}
                                caretHidden={true}
                                style={{ paddingLeft: 10 }}
                                isVisibleCalendar={true}
                                value={this.state.employmentHistory[index].startDate}
                                returnKeyType={'done'}
                               
                                onTouchEnd={() =>  this._oNChangeDetails('15/03/2015',index,'startDate')}
                              />

                            </TouchableOpacity>

                          </View>
                          <View style={{ marginTop: 5, flex: 1, marginLeft: 5 }}>
                            <TouchableOpacity
                            // onPress={() => this._setDate("end")}
                            >
                              <CustomTextInput
                                placeholder='END DATE'
                                keyboardType='default'
                                floatingText="END DATE"
                                editable={true}
                                onFocus={true}
                                showSoftInputOnFocus={false}
                                caretHidden={true}
                                style={{ paddingLeft: 10 }}
                                isVisibleCalendar={true}
                                value={this.state.employmentHistory[index].endDate}
                                returnKeyType={'done'}
                               
                                onTouchEnd={() =>  this._oNChangeDetails('12/11/2020',index,'endDate')}
                              />

                            </TouchableOpacity>

                          </View>
                        </View>

                        <View style={{ marginTop: 5 }}>
                          <CustomTextInput
                            placeholder='REASON FOR LEAVING'
                            keyboardType='default'
                            style={{ textAlignVertical: "top", height: 150, }}
                            floatingText="REASON FOR LEAVING"
                            value={this.state.employmentHistory[index].reason}
                              returnKeyType={'done'}
                            returnKeyType={'done'}
                            onChangeText={(text) => {
                              this._oNChangeDetails(text,index,'reason')
                             }}
                          />
                        </View>

                      </View>
                      <Text style={{ height: 1, backgroundColor: '#585858', marginTop: 10 }}> </Text>
                    </View>

                         }
                    />
                    <TouchableOpacity style={{ marginTop: -45, alignSelf: 'center', padding: 5, }}
                      onPress={() =>this._addEmployeementHistory()}
                    >
                      <Text
                        style={{
                          color: "#000000",
                          fontSize: 12,
                          fontFamily: "montserrat_regular"
                        }}
                      >+ ADD ANOTHER</Text>
                    </TouchableOpacity>


                  </View>

                  <Text style={{ height: 1, backgroundColor: '#585858', marginTop: 10 }}> </Text>

                  <Text style={{
                    fontSize: 12,
                    fontFamily: 'montserrat_light',
                    color: '#000000',
                    marginLeft: 10,
                    marginRight: 10,
                    marginTop: 20,
                    borderRadius: 10,
                    padding: 20,
                    backgroundColor: '#CAE8A2'
                  }}>Note:Please note a full 10 years employment history must be provided, 
                  or your employment history since leaving full time education if less than 10 years.
                  All gaps over 2 months in employment history must be detailed with a note of explanation</Text>

                  <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity style={{ flex: 1, padding: 10, marginTop: 20, backgroundColor: "#7F7F7F", borderRadius: 20, marginLeft: 40, marginBottom: 10 }}
                      onPress={() => this.props.navigation.goBack()}
                    >
                      <Text style={styles.loginText}>BACK</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 3, padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginRight: 40, marginLeft: 10, marginBottom: 10 }}
                      onPress={() => this.props.navigation.navigate('WorkerStepFive')}
                    >
                      <Text style={styles.loginText}>NEXT</Text>
                    </TouchableOpacity>

                  </View>

                </View>




              </ScrollView>
            </View>



          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _addEmployeementHistory = () =>{

    var employmentHistory={}
    let tempList = [...this.state.employmentHistory]
    employmentHistory.employeerName =''
    employmentHistory.address =''
    employmentHistory.position =''
    employmentHistory.startDate =''
    employmentHistory.endDate =''
    employmentHistory.reason =''
    tempList.push(employmentHistory)
    this.setState({ employmentHistory: tempList }); 

  } 

  _oNChangeDetails = (text,pos,type) =>{

    if(type== 'employeer'){
      let tempList = [...this.state.employmentHistory]
      tempList[pos].employeerName = text
      this.setState({ employmentHistory: tempList });
    }else if(type== 'address'){
      let tempList = [...this.state.employmentHistory]
      tempList[pos].address = text
      this.setState({ employmentHistory: tempList });
    }else if(type== 'position'){
      let tempList = [...this.state.employmentHistory]
      tempList[pos].position = text
      this.setState({ employmentHistory: tempList });
    }else if(type== 'startDate'){
      let tempList = [...this.state.employmentHistory]
      tempList[pos].startDate = text
      this.setState({ employmentHistory: tempList });
    }else if(type== 'endDate'){
      let tempList = [...this.state.employmentHistory]
      tempList[pos].endDate = text
      this.setState({ employmentHistory: tempList });
    }else if(type== 'reason'){
      let tempList = [...this.state.employmentHistory]
      tempList[pos].reason = text
      this.setState({ employmentHistory: tempList });
    }
    
  }


}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#585858',
    textAlign: 'center',
    marginTop: 10
  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  }

});

