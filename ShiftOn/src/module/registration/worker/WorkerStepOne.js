/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions, TextInput,
  ToastAndroid, Platform, Modal
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../../components/CustomTextInput';
import AppConstants from '../../../apis/AppConstants';
import Api from '../../../apis/Api';
import ImagePicker from 'react-native-image-crop-picker';
import { androidCameraPermission } from '../../../../permissions';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Loader from '../../../utils/Loader';
import { setToastMsg } from '../../../utils/ToastMessage';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../../components/CustomModelSelector';


const { width } = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});

const titleData = [
  {
    id: "1",
    label: 'Mr.',
  },
  {
    id: "2",
    label: 'Mrs.',
  },

]


class WorkerStepOne extends Component {
  constructor(props) {
    super(props);
    this.state = {
      position: '',
      surname: '',
      forename: '',
      title: '',
      maidenname: '',
      telephone: '',
      mobile: '',
      email: '',
      password: '',
      c_password: '',
      dob: '',
      nationality: '',
      address1: '',
      address2: '',
      postcode: '',
      image: '',
      isDatePickerVisible: false,
      isLoading: false,

      countryData: [],
      stateData: [],
      cityData: [],
      countryName: '',
      stateName: '',
      cityName: '',
      countryId: '',
      stateId: '',
      cityId: '',

      successModelPopup: false,
      setErrors: {
        field: '',
        message: ''
      }
    };
    //console.log(this.props.navigation);
  }

  componentDidMount() {
    this.countryHandle();
  }

  onSelectImage = async () => {
    const permissionStatus = await androidCameraPermission()
    if (permissionStatus || Platform.OS == 'ios') {
      Alert.alert(
        'Profile Image',
        'Choose an option',
        [
          { text: 'Close', onPress: () => console.log('Close pressed') },
          { text: 'Camera', onPress: () => this.onCamera() },
          { text: 'Gallery', onPress: () => this.onGallery() },
        ]
      )
    }
  }

  onCamera = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 300,
      cropping: true,
      //freeStyleCropEnabled: true,
      includeBase64: false,
    }).then(image => {
      console.log(image);
      this.setState({
        image: image.path
      })
    }).catch((err) => {
      console.log("openCamera erroe" + err.toString())
      setToastMsg(err.toString());
    });
  }

  onGallery = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      //freeStyleCropEnabled: true,
      includeBase64: false,
    }).then(image => {
      console.log(image);
      this.setState({
        image: image.path,
      })
    }).catch((err) => {
      console.log("openGallery erroe" + err.toString())
      setToastMsg(err.toString());
    });
  }


  handleConfirm = (date) => {
    let dateTimeString = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
    this.setState({
      dob: dateTimeString, isDatePickerVisible: false,
    })

    console.log(dateTimeString)

  };

  checkValidation = () => {
    const emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const passCharecterRegex = /^(?=.*[a-z])(?=.*[A-Z])/;
    const passNumRegex = /^(?=.*[0-9])/;

    let titleError = { field: '', message: '' }
    let surenameError = { field: '', message: '' }
    let forenameError = { field: '', message: '' }
    let hTelephoneError = { field: '', message: '' }
    let mobileError = { field: '', message: '' }
    let emailError = { field: '', message: '' }
    let passwordError = { field: '', message: '' }
    let cPasswordError = { field: '', message: '' }
    let dobError = { field: '', message: '' }
    let nationalityError = { field: '', message: '' }
    let address1Error = { field: '', message: '' }
    let address2Error = { field: '', message: '' }
    let countryError = { field: '', message: '' }
    let stateError = { field: '', message: '' }
    let cityError = { field: '', message: '' }
    let postCodeError = { field: '', message: '' }


    if (this.state.title == '') {
      titleError.field = "title";
      titleError.message = "Title is required!";
      this.setState({ setErrors: titleError })
    } else if (this.state.surname == '') {
      surenameError.field = "surename";
      surenameError.message = "Sure Name is required!";
      this.setState({ setErrors: surenameError })
    } else if (this.state.forename == '') {
      forenameError.field = "forename";
      forenameError.message = "Fore Name is required!";
      this.setState({ setErrors: forenameError })
    } else if (this.state.telephone == '') {
      hTelephoneError.field = "h_telephone";
      hTelephoneError.message = "Telephone number is required!";
      this.setState({ setErrors: hTelephoneError })
    } else if (this.state.telephone.length < 10) {
      hTelephoneError.field = "h_telephone";
      hTelephoneError.message = "Telephone number is invalid!";
      this.setState({ setErrors: hTelephoneError })
    } else if (this.state.mobile == '') {
      mobileError.field = "mobile";
      mobileError.message = "Mobile Number is required!";
      this.setState({ setErrors: mobileError })
    } else if (this.state.mobile.length < 10) {
      mobileError.field = "mobile";
      mobileError.message = "Mobile Number is invalid!";
      this.setState({ setErrors: mobileError })
    } else if (this.state.email == '') {
      emailError.field = "email";
      emailError.message = "Email is required!";
      this.setState({ setErrors: emailError })
    } else if (!emailRegEx.test(this.state.email)) {
      emailError.field = "email";
      emailError.message = "Invalid email!";
      this.setState({ setErrors: emailError })
    } else if (this.state.dob == '') {
      dobError.field = "dob";
      dobError.message = "Date of birth is required!";
      this.setState({ setErrors: dobError })
    } else if (this.state.nationality == '') {
      nationalityError.field = "nationality";
      nationalityError.message = "Nationality is required!";
      this.setState({ setErrors: nationalityError })
    } else if (this.state.address1 == '') {
      address1Error.field = "address1";
      address1Error.message = "Address is required!";
      this.setState({ setErrors: address1Error })
    } else if (this.state.address2 == '') {
      address2Error.field = "address2";
      address2Error.message = "Address is required!";
      this.setState({ setErrors: address2Error })
    } else if (this.state.countryName == '') {
      countryError.field = "country";
      countryError.message = "Country is required!";
      this.setState({ setErrors: countryError })
    } else if (this.state.stateName == '') {
      stateError.field = "state";
      stateError.message = "State is required!";
      this.setState({ setErrors: stateError })
    } else if (this.state.cityName == '') {
      cityError.field = "city";
      cityError.message = "City is required!";
      this.setState({ setErrors: cityError })
    } else if (this.state.postcode == '') {
      postCodeError.field = "post_code";
      postCodeError.message = "Post code is required!";
      this.setState({ setErrors: postCodeError })
    } else if (this.state.password == '') {
      passwordError.field = "password";
      passwordError.message = "Password is required!";
      this.setState({ setErrors: passwordError })
    } else if (!passCharecterRegex.test(this.state.password)) {
      passwordError.field = "password";
      passwordError.message = "Need atleast 1 upper and 1 lowercase.";
      this.setState({ setErrors: passwordError })
    } else if (!passNumRegex.test(this.state.password)) {
      passwordError.field = "password";
      passwordError.message = "Need atleast 1 digit.";
      this.setState({ setErrors: passwordError })
    } else if (this.state.password.length < 8) {
      passwordError.field = "password";
      passwordError.message = "Please enter at least 8 characters.";
      this.setState({ setErrors: passwordError })
    } else if (this.state.c_password == '') {
      cPasswordError.field = "c_password";
      cPasswordError.message = "Confirm Password is required!";
      this.setState({ setErrors: cPasswordError })
    } else if (this.state.password != this.state.c_password) {
      cPasswordError.field = "c_password";
      cPasswordError.message = "Password & Confirm Password should be equal!";
      this.setState({ setErrors: cPasswordError })
    } else {
      this.setState({ setErrors: { field: '', message: '' } })
      this._workerResitration();
    }
  }

  _workerResitration = () => {

    this.setState({ isLoading: true });

    let formData = new FormData();
    formData.append('sur_name', this.state.surname);
    formData.append('fore_name', this.state.forename);
    formData.append('title', this.state.title);
    formData.append('maiden_name', this.state.maidenname);
    formData.append('telephone', this.state.telephone);
    formData.append('mobile', this.state.mobile);
    formData.append('email', this.state.email);
    formData.append('password', this.state.password);
    formData.append('dob', this.state.dob);
    formData.append('nationality', this.state.nationality);
    formData.append('applied_for', this.state.position);
    formData.append('address1', this.state.address1);
    formData.append('address2', this.state.address2);
    formData.append('country_id', this.state.countryId);
    formData.append('state_id', this.state.stateId);
    formData.append('city_id', this.state.cityId);
    formData.append('post_code', this.state.postcode);
    if (this.state.image != '') {
      formData.append("image",
        {
          uri: this.state.image,
          name: 'photo.jpg',
          type: 'image/jpeg',
        }
      );
    }

    Api._workerResitration(formData)
      .then((response) => {
        console.log(response.data)

        this.setState({ isLoading: false, })
        setToastMsg(response.message.toString());
        if (response.status.toString() == "1") {
          this.setState({ successModelPopup: true, })
        }


      })
      .catch((err) => {
        console.log(err);
        this.setState({ isLoading: false, })
        setToastMsg(err.toString);
      });

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>

            <View style={styles.mainContainer}>
              <View style={{ backgroundColor: '#00D5E1' }}>

                <Modal
                  visible={this.state.successModelPopup}
                  transparent={true}
                  animationType="none"
                  style={{ zIndex: 1100 }}>
                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => { }}
                    style={styles.modalBackground}>
                    <View style={styles.activityIndicatorWrapper}>
                      <View style={{ alignItems: 'center', alignSelf: 'center', padding: 20 }}>

                        <Text style={{ color: "#000000", fontSize: 18, fontFamily: 'montserrat_bold' }}>Congratulations!</Text>
                        <Text style={{ color: '#666666', fontSize: 16, fontFamily: 'montserrat_semibold', marginTop: 10, textAlign: 'center' }}>Thanks you for registering with us.</Text>

                        <TouchableOpacity style={styles.popupConView}
                          activeOpacity={0.7}
                          onPress={() => {
                            this.setState({ successModelPopup: false });
                            this.props.navigation.navigate("Login");
                          }}
                        >
                          <Text style={styles.popupConText}>Login now</Text>
                        </TouchableOpacity>

                      </View>
                    </View>
                  </TouchableOpacity>
                </Modal>

                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() =>
                    Alert.alert(
                      "Alert",
                      'If you go back you will be lost your all data',
                      [
                        { text: 'NO', onPress: () => console.log('NO pressed') },
                        { text: 'YES', onPress: () => this.props.navigation.navigate('Login') },

                      ],
                      { cancelable: false },
                    )
                  }
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>APPLICATION FORM</Text>

              </View>
              <ScrollView>
                <Loader isLoading={this.state.isLoading} />

                <View style={{ marginLeft: 50, marginRight: 50 }}>

                  <TouchableOpacity style={{ alignItems: 'center', height: 120, width: 120, alignSelf: 'center', marginTop: 20 }}
                    onPress={() => this.onSelectImage()}>
                    <Image style={{ height: 120, width: 120, borderRadius: 25 }} source={
                      this.state.image && this.state.image !== "" ? { uri: this.state.image } :
                        require('./../../../assets/woman_face.jpeg')
                    } resizeMode='stretch' />

                  </TouchableOpacity>

                  <Text style={styles.heading_mail}>ATTACH A SELF IMAGE</Text>


                  {/* <View style={{ marginTop: 20 }}>
                    <CustomTextInput
                      placeholder='POSITION APPLIED FOR'
                      keyboardType='default'
                      //floatingStyle={{ color: '#AA0606' }}
                      floatingText="POSITION APPLIED FOR"
                      value={this.state.position}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          position: text,
                        });
                      }}
                    />
                  </View> */}

                  <ModalSelector
                    data={titleData}
                    initValue="TITLE"
                    accessible={true}
                    search={false}
                    animationType={"fade"}
                    keyExtractor={item => item.id}
                    labelExtractor={item => item.label}
                    onChange={(option) => { this.setState({ title: option.label }) }}>

                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='TITLE'
                        keyboardType='default'
                        floatingText="TITLE"
                        value={this.state.title}
                        editable={true}
                        onFocus={true}
                        onSubmitEditing={true}
                        showSoftInputOnFocus={true}
                        caretHidden={false}
                        isVisibleDropDown={true}
                        returnKeyType={'done'}
                      />
                    </View>

                  </ModalSelector>
                  {
                    this.state.setErrors.field == "title" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='SURNAME'
                      keyboardType='default'
                      floatingText="SURNAME"
                      value={this.state.surname}
                      returnKeyType={'next'}
                      onSubmitEditing={() => { this.secondTextInput.focus(); }}
                      blurOnSubmit={false}
                      onChangeText={(text) => {
                        this.setState({
                          surname: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "surename" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='FORENAME'
                      keyboardType='default'
                      floatingText="FORENAME"
                      value={this.state.forename}
                      returnKeyType={'next'}
                      inputRef={(input) => { this.secondTextInput = input; }}
                      onSubmitEditing={() => { this.thirdTextInput.focus(); }}
                      blurOnSubmit={false}
                      onChangeText={(text) => {
                        this.setState({
                          forename: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "forename" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='MAIDEN NAME (OPTIONAL)'
                      keyboardType='default'
                      floatingText="MAIDEN NAME"
                      value={this.state.maidenname}
                      returnKeyType={'next'}
                      inputRef={(input) => { this.thirdTextInput = input; }}
                      onSubmitEditing={() => { this.forthTextInput.focus(); }}
                      blurOnSubmit={false}
                      onChangeText={(text) => {
                        this.setState({
                          maidenname: text,
                        });
                      }}
                    />
                  </View>

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='HOME TELEPHONE'
                      keyboardType='numeric'
                      floatingText="HOME TELEPHONE"
                      value={this.state.telephone}
                      maxLength={10}
                      returnKeyType={'next'}
                      inputRef={(input) => { this.forthTextInput = input; }}
                      onSubmitEditing={() => { this.fifthTextInput.focus(); }}
                      blurOnSubmit={false}
                      onChangeText={(text) => {
                        this.setState({
                          telephone: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "h_telephone" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='MOBILE'
                      keyboardType='numeric'
                      floatingText="MOBILE"
                      value={this.state.mobile}
                      maxLength={10}
                      returnKeyType={'next'}
                      inputRef={(input) => { this.fifthTextInput = input; }}
                      onSubmitEditing={() => { this.sixthTextInput.focus(); }}
                      blurOnSubmit={false}
                      onChangeText={(text) => {
                        this.setState({
                          mobile: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "mobile" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='EMAIL'
                      keyboardType='email-address'
                      floatingText="EMAIL"
                      value={this.state.email}
                      returnKeyType={'next'}
                      inputRef={(input) => { this.sixthTextInput = input; }}
                      onSubmitEditing={() => { this.ninthTextInput.focus(); }}
                      blurOnSubmit={false}
                      onChangeText={(text) => {
                        this.setState({
                          email: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "email" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5, }}>
                    <CustomTextInput
                      placeholder='DATE OF BIRTH'
                      keyboardType='default'
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleCalendar={true}
                      floatingText="DATE OF BIRTH"
                      value={this.state.dob.toString()}
                      returnKeyType={'done'}
                      onTouchEnd={() => {
                        this.setState({ isDatePickerVisible: true, })
                      }}
                    />

                    <DateTimePickerModal
                      isVisible={this.state.isDatePickerVisible}
                      mode="date"
                      date={new Date()}
                      onConfirm={(date) => this.handleConfirm(date)}
                      onCancel={() => {
                        this.setState({ isDatePickerVisible: false, })
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "dob" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='NATIONALITY'
                      keyboardType='default'
                      floatingText="NATIONALITY"
                      value={this.state.nationality}
                      returnKeyType={'next'}
                      inputRef={(input) => { this.ninthTextInput = input; }}
                      onSubmitEditing={() => { this.tenthTextInput.focus(); }}
                      blurOnSubmit={false}
                      onChangeText={(text) => {
                        this.setState({
                          nationality: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "nationality" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='ADDRESS-1'
                      keyboardType='default'
                      floatingText="ADDRESS-1"
                      value={this.state.address1}
                      returnKeyType={'next'}
                      inputRef={(input) => { this.tenthTextInput = input; }}
                      onSubmitEditing={() => { this.eleventhTextInput.focus(); }}
                      blurOnSubmit={false}
                      onChangeText={(text) => {
                        this.setState({
                          address1: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "address1" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='ADDRESS-2'
                      keyboardType='default'
                      floatingText="ADDRESS-2"
                      value={this.state.address2}
                      returnKeyType={'next'}
                      inputRef={(input) => { this.eleventhTextInput = input; }}
                      onSubmitEditing={() => { this.twelvethTextInput.focus(); }}
                      blurOnSubmit={false}
                      onChangeText={(text) => {
                        this.setState({
                          address2: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "address2" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='POST CODE'
                      keyboardType='default'
                      floatingText="POST CODE"
                      value={this.state.postcode}
                      returnKeyType={'done'}
                      inputRef={(input) => { this.twelvethTextInput = input; }}
                      onChangeText={(text) => {
                        this.setState({
                          postcode: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "post_code" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <ModalSelector
                    data={this.state.countryData}
                    initValue="COUNTRY"
                    accessible={true}
                    search={true}
                    animationType={"fade"}
                    keyExtractor={item => item.id}
                    labelExtractor={item => item.label}
                    onChange={(option) => {
                      this.setState({
                        countryName: option.label,
                        countryId: option.id,
                        stateName: '',
                        stateId: '',
                        cityName: '',
                        cityId: '',
                        isLoading: true,
                      });
                      this.handleState(option.id);
                    }}>

                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='COUNTRY'
                        keyboardType='default'
                        floatingText="COUNTRY"
                        value={this.state.countryName}
                        editable={false}
                        onFocus={true}
                        showSoftInputOnFocus={true}
                        caretHidden={false}
                        isVisibleDropDown={true}
                        returnKeyType={'done'}
                      />
                    </View>
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "country" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <ModalSelector
                    data={this.state.stateData}
                    initValue="STATE"
                    accessible={true}
                    search={true}
                    animationType={"fade"}
                    keyExtractor={item => item.id}
                    labelExtractor={item => item.label}
                    onChange={(option) => {
                      this.setState({
                        stateName: option.label,
                        stateId: option.id,
                        cityName: '',
                        cityId: '',
                        isLoading: true,
                      });
                      this.handleCity(option.id);
                    }}>

                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='STATE'
                        keyboardType='default'
                        floatingText="STATE"
                        value={this.state.stateName}
                        editable={false}
                        onFocus={true}
                        showSoftInputOnFocus={true}
                        caretHidden={false}
                        isVisibleDropDown={true}
                        returnKeyType={'done'}
                      />
                    </View>
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "state" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <ModalSelector
                    data={this.state.cityData}
                    initValue="CITY"
                    accessible={true}
                    search={true}
                    animationType={"fade"}
                    keyExtractor={item => item.id}
                    labelExtractor={item => item.label}
                    onChange={(option) => {
                      this.setState({
                        cityName: option.label, cityId: option.id,
                      });
                    }}>
                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='CITY'
                        keyboardType='default'
                        floatingText="CITY"
                        value={this.state.cityName}
                        editable={false}
                        onFocus={true}
                        showSoftInputOnFocus={true}
                        caretHidden={false}
                        isVisibleDropDown={true}
                        returnKeyType={'done'}
                      />
                    </View>
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "city" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 10 }}>
                    <CustomTextInput
                      placeholder='PASSWORD'
                      keyboardType='default'
                      floatingText="PASSWORD"
                      secureTextEntry={true}
                      //isVisibleCalendar={true}
                      returnKeyType={'next'}
                      inputRef={(input) => { this.seventhTextInput = input; }}
                      onSubmitEditing={() => { this.eighthTextInput.focus(); }}
                      blurOnSubmit={false}
                      value={this.state.password}
                      onChangeText={(text) => {
                        this.setState({
                          password: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "password" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 10 }}>
                    <CustomTextInput
                      placeholder='CONFIRM PASSWORD'
                      keyboardType='default'
                      floatingText="CONFIRM PASSWORD"
                      secureTextEntry={true}
                      //isVisibleCalendar={true}
                      returnKeyType={'done'}
                      inputRef={(input) => { this.eighthTextInput = input; }}
                      value={this.state.c_password}
                      onChangeText={(text) => {
                        this.setState({
                          c_password: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "c_password" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                    //onPress={() => this.props.navigation.goBack()}
                    onPress={() => this.checkValidation()}
                  >
                    <Text style={styles.loginText}>SUBMIT</Text>
                  </TouchableOpacity>
                </View>

              </ScrollView>
            </View>



          </View>
        </SafeAreaView>
      </Fragment>
    );
  }


  countryHandle = () => {

    this.setState({ isLoading: true });

    Api._countryListApi()
      .then((response) => {

        console.log("COUNTRY:- ", response.data);
        var count = Object.keys(response.data).length;
        let countryArray = [];
        for (var i = 0; i < count; i++) {
          countryArray.push({
            id: response.data[i].id,
            label: response.data[i].country_name,
          });
        }
        this.setState({ countryData: countryArray, isLoading: false, });

      })
      .catch((err) => {
        console.log("country error: " + err);
        this.setState({ isLoading: false, });
      });
  }

  handleState = (countryCode) => {
    let rawData = { "country_id": countryCode, }

    Api._stateListApi(rawData)
      .then((response) => {

        console.log("STATE:- ", response.data);
        var count = Object.keys(response.data).length;
        let stateArray = [];
        for (var i = 0; i < count; i++) {
          stateArray.push({
            id: response.data[i].id,
            label: response.data[i].state_name,
          });
        }
        this.setState({ stateData: stateArray, isLoading: false, });

      })
      .catch((err) => {
        console.log("state error: " + err);
        this.setState({ isLoading: false, });
      });
  }

  handleCity = (stateCode) => {
    let rawData = { "state_id": stateCode, }

    Api._cityListApi(rawData)
      .then((response) => {

        console.log("CITY:- ", response.data);
        var count = Object.keys(response.data).length;
        let cityArray = [];
        for (var i = 0; i < count; i++) {
          cityArray.push({
            id: response.data[i].city_id,
            label: response.data[i].city_name,
          });
        }
        this.setState({ cityData: cityArray, isLoading: false, });

      })
      .catch((err) => {
        console.log("state error: " + err);
        this.setState({ isLoading: false, });
      });
  }

}


const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75
  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10
  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 12,
    fontFamily: 'montserrat_regular',
    color: '#585858',
    marginTop: 10
  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7
  },
  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20
  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'
  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'
  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'
  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }
  },
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#rgba(0, 0, 0, 0.5)',
    zIndex: 1000
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    width: '85%',
    borderRadius: 10,
    display: 'flex',
    justifyContent: 'space-around'
  },
  popupConView: {
    paddingVertical: 14,
    paddingHorizontal: 25,
    marginTop: 30,
    backgroundColor: '#00D5E1',
    borderRadius: 10,
  },
  popupConText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_bold",
    alignSelf: 'center'
  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }

});

export default WorkerStepOne;