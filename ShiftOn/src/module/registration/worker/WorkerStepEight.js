/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../../components/CustomTextInput';




const { width } = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});

export default class WorkerStepEight extends Component {
  constructor(props) {
    super(props);
    this.state = {
      criminalDetails: '',
      clinicalDetails: '',
      isCriminal: false,
      isClinical: false,

    };
    console.log(this.props.navigation);
  }



  componentDidMount() {

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() =>  Alert.alert(
                    "Alert",
                    'If you go back you will be lost your all data',
                    [
                      { text: 'NO', onPress: () => console.log('NO pressed') },
                      { text: 'YES', onPress: () => this.props.navigation.navigate('Login') },
                    
                    ],
                    { cancelable: false },
                  )}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>APPLICATION FORM</Text>

              </View>
              <ScrollView>
                <View style={{ marginLeft: 50, marginRight: 50 }}>
                  <Text style={styles.heading}> Step 8</Text>
                  <Text style={styles.heading_mail}>Criminal Records</Text>


                  <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 20, marginTop: 20 }}>

                    <View>
                      <Text
                        style={{
                          color: "#393FA0",
                          marginLeft: 15,
                          paddingRight: 5,
                          paddingLeft: 5,
                          fontSize: 13,
                          fontFamily: "montserrat_regular"
                        }}
                      > CRIMINAL CONVICTIONS!</Text>
                      <Text
                        style={{
                          color: "#000000",
                          marginLeft: 15,
                          backgroundColor: '#FFFFFF',
                          paddingRight: 5,
                          paddingLeft: 5,
                          fontSize: 15,
                          fontFamily: "montserrat_regular",
                          padding: 10

                        }}
                      > Do you have any criminal convictions in the UK or abroad?(wheather related to work or not)</Text>

                      <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity style={{ alignItems: 'center', marginLeft: 15, marginBottom: 10 }}
                          onPress={() => this.setState({ isCriminal: !this.state.isCriminal })}
                        >
                          <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                            {
                              (this.state.isCriminal) ? (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_on_icn.png')} />) : (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_off_icn.png')} />)
                            }
                            <Text
                              style={{
                                color: "#000000",
                                fontSize: 15,
                                fontFamily: "montserrat_regular",
                                padding: 10
                              }}
                            >YES</Text>
                          </View>

                        </TouchableOpacity>
                        <TouchableOpacity style={{ alignItems: 'center', marginLeft: 15, marginBottom: 10 }}
                          onPress={() => this.setState({ isCriminal: !this.state.isCriminal })}
                        >
                          <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                            {
                              (this.state.isCriminal) ? (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_off_icn.png')} />) : (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_on_icn.png')} />)
                            }
                            <Text
                              style={{
                                color: "#000000",
                                fontSize: 15,
                                fontFamily: "montserrat_regular",
                                padding: 10
                              }}
                            >NO</Text>
                          </View>

                        </TouchableOpacity>


                      </View>

                    </View>

                  </View>
                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='IF YES PLEASE DETAIL BELOW'
                      multiline ={true}
                      keyboardType='default'
                      floatingText="IF YES PLEASE DETAIL BELOW"
                      style={{ textAlignVertical: "top", height: 150, }}
                      value={this.state.criminalDetails}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          criminalDetails: text,
                        });
                      }}
                    />
                  </View>

                  <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 20, marginTop: 15 }}>

                    <View>
                      <Text
                        style={{
                          color: "#393FA0",
                          marginLeft: 15,
                          paddingRight: 5,
                          paddingLeft: 5,
                          fontSize: 13,
                          fontFamily: "montserrat_regular"
                        }}
                      > CLINICAL INVESTIGATION OR SUSPENSION!</Text>
                      <Text
                        style={{
                          color: "#000000",
                          marginLeft: 15,
                          backgroundColor: '#FFFFFF',
                          paddingRight: 5,
                          paddingLeft: 5,
                          fontSize: 15,
                          fontFamily: "montserrat_regular",
                          padding: 10

                        }}
                      > Are you / have you been under / undergoing any clinical investigation or suspension?</Text>

                      <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity style={{ alignItems: 'center', marginLeft: 15, marginBottom: 10 }}
                          onPress={() => this.setState({ isClinical: !this.state.isClinical })}
                        >
                          <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                            {
                              (this.state.isClinical) ? (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_on_icn.png')} />) : (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_off_icn.png')} />)
                            }
                            <Text
                              style={{
                                color: "#000000",
                                fontSize: 15,
                                fontFamily: "montserrat_regular",
                                padding: 10
                              }}
                            >YES</Text>
                          </View>

                        </TouchableOpacity>
                        <TouchableOpacity style={{ alignItems: 'center', marginLeft: 15, marginBottom: 10 }}
                          onPress={() => this.setState({ isClinical: !this.state.isClinical })}
                        >
                          <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                            {
                              (this.state.isClinical) ? (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_off_icn.png')} />) : (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_on_icn.png')} />)
                            }
                            <Text
                              style={{
                                color: "#000000",
                                fontSize: 15,
                                fontFamily: "montserrat_regular",
                                padding: 10
                              }}
                            >NO</Text>
                          </View>

                        </TouchableOpacity>


                      </View>

                    </View>

                  </View>



                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='IF YES PLEASE DETAIL BELOW'
                      multiline ={true}
                      keyboardType='default'
                      style={{ textAlignVertical: "top", height: 150, }}
                      floatingText="IF YES PLEASE DETAIL BELOW"
                      value={this.state.clinicalDetails}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          clinicalDetails: text,
                        });
                      }}
                    />
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity style={{ flex: 1, padding: 10, marginTop: 20, backgroundColor: "#7F7F7F", borderRadius: 20, marginBottom: 10 }}
                      onPress={() => this.props.navigation.goBack()}
                    >
                      <Text style={styles.loginText}>BACK</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: 3, padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 10, marginBottom: 10 }}
                      onPress={() => this.props.navigation.navigate('WorkerStepComplete')}
                    >
                      <Text style={styles.loginText}>NEXT</Text>
                    </TouchableOpacity>

                  </View>
                </View>

              </ScrollView>
            </View>



          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _setDate = (type) => {
    if (type == 'datestarted') {
      this.setState({ datestarted: "15/09/2020" });
    } else if (type == 'finish_date') {
      this.setState({ finish_date: "20/09/2020" });
    }
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#585858',
    textAlign: 'center',
    marginTop: 10
  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  }

});

