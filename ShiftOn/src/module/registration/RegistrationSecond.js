/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView,
  Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import Loader from '../../utils/Loader';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../components/CustomModelSelector';
import ImagePicker from 'react-native-image-crop-picker';
import { androidCameraPermission } from '../../../permissions';
import { setToastMsg } from '../../utils/ToastMessage';
import moment from 'moment';
import { connect } from 'react-redux';
import { loginUserDetails } from '../../redux/actions/actions';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
  return {
    userid: props.userLoginDetails.id,
    userName: props.userLoginDetails.name,
    userEmail: props.userLoginDetails.email,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userDetails: (txt1, txt2, txt3, txt4, txt5) => dispatch(loginUserDetails(txt1, txt2, txt3, txt4, txt5)),
  }
}


class RegistrationSecond extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: this.props.navigation.getParam('fullProfileData').email,
      firstName: this.props.navigation.getParam('fullProfileData').first_name,
      lastName: this.props.navigation.getParam('fullProfileData').last_name,
      postCode: this.props.navigation.getParam('fullProfileData').post_code,
      address: this.props.navigation.getParam('fullProfileData').address,
      landmark: this.props.navigation.getParam('fullProfileData').landmark,
      phone: this.props.navigation.getParam('fullProfileData').phone,
      description: this.props.navigation.getParam('fullProfileData').description,
      image: this.props.navigation.getParam('fullProfileData').profile_image,
      isImageUpdate: '0',
      isLoading: false,

      countryData: [],
      stateData: [],
      cityData: [],
      countryName: this.props.navigation.getParam('fullProfileData').country.country_name,
      stateName: this.props.navigation.getParam('fullProfileData').state.state_name,
      cityName: this.props.navigation.getParam('fullProfileData').city.city_name,
      countryId: this.props.navigation.getParam('fullProfileData').country.id,
      stateId: this.props.navigation.getParam('fullProfileData').state.id,
      cityId: this.props.navigation.getParam('fullProfileData').city.city_id,

      setErrors: {
        field: '',
        message: ''
      }

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    //this.countryHandle();
  }

  onSelectImage = async () => {
    const permissionStatus = await androidCameraPermission()
    if (permissionStatus || Platform.OS == 'ios') {
      Alert.alert(
        'Profile Image',
        'Choose an option',
        [
          { text: 'Close', onPress: () => console.log('Close pressed') },
          { text: 'Camera', onPress: () => this.onCamera() },
          { text: 'Gallery', onPress: () => this.onGallery() },
        ]
      )
    }
  }

  onCamera = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 300,
      cropping: true,
      //freeStyleCropEnabled: true,
      includeBase64: false,
    }).then(image => {
      console.log(image);
      this.setState({
        image: image.path,
        isImageUpdate: '1',
      })
    }).catch((err) => {
      console.log("openCamera erroe" + err.toString())
      setToastMsg(err.toString());
    });

  }

  onGallery = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      //freeStyleCropEnabled: true,
      includeBase64: false,
    }).then(image => {
      console.log(image);
      this.setState({
        image: image.path,
        isImageUpdate: '1',
      })
    }).catch((err) => {
      console.log("openGallery erroe" + err.toString())
      setToastMsg(err.toString());
    });

  }

  checkValidation = () => {
    let firstNameError = { field: '', message: '' }
    let lastNameError = { field: '', message: '' }
    let countryError = { field: '', message: '' }
    let stateError = { field: '', message: '' }
    let cityError = { field: '', message: '' }
    let postCodeError = { field: '', message: '' }
    let landmarkError = { field: '', message: '' }
    let descriptionError = { field: '', message: '' }
    let addressError = { field: '', message: '' }
    let mobileError = { field: '', message: '' }

    if (this.state.firstName == '') {
      firstNameError.field = "first_name";
      firstNameError.message = "First name is required!";
      this.setState({ setErrors: firstNameError })
    } else if (this.state.lastName == '') {
      lastNameError.field = "last_name";
      lastNameError.message = "Last name is required!";
      this.setState({ setErrors: lastNameError })
    } else if (this.state.countryName == '') {
      countryError.field = "country";
      countryError.message = "Country is required!";
      this.setState({ setErrors: countryError })
    } else if (this.state.stateName == '') {
      stateError.field = "state";
      stateError.message = "State is required!";
      this.setState({ setErrors: stateError })
    } else if (this.state.cityName == '') {
      cityError.field = "city";
      cityError.message = "City is required!";
      this.setState({ setErrors: cityError })
    } else if (this.state.postCode == '') {
      postCodeError.field = "postcode";
      postCodeError.message = "Post Code is required!";
      this.setState({ setErrors: postCodeError })
    } else if (this.state.address == '') {
      addressError.field = "address";
      addressError.message = "Address is required!";
      this.setState({ setErrors: addressError })
    } else if (this.state.landmark == '') {
      landmarkError.field = "landmark";
      landmarkError.message = "Landmark is required!";
      this.setState({ setErrors: landmarkError })
    } else if (this.state.description == '') {
      descriptionError.field = "description";
      descriptionError.message = "Description is required!";
      this.setState({ setErrors: descriptionError })
    } else if (this.state.phone == '') {
      mobileError.field = "mobile";
      mobileError.message = "Phone number is required!";
      this.setState({ setErrors: mobileError })
    } else if (this.state.phone.length < 10) {
      mobileError.field = "mobile";
      mobileError.message = "Phone number is invalid!";
      this.setState({ setErrors: mobileError })
    } else {
      this.setState({ setErrors: { field: '', message: '' } })
      this._controllerUpdateProfile();
    }

  }

  _controllerUpdateProfile = () => {
    this.setState({ isLoading: true });

    let formData = new FormData();
    formData.append('id', this.props.userid);
    formData.append('first_name', this.state.firstName);
    formData.append('last_name', this.state.lastName);
    formData.append('phone', this.state.phone);
    formData.append('address', this.state.address);
    formData.append('landmark', this.state.landmark);
    formData.append('description', this.state.description);
    formData.append('post_code', this.state.postCode);
    formData.append('city', this.state.cityId);
    formData.append('state', this.state.stateId);
    formData.append('country', this.state.countryId);
    if (this.state.isImageUpdate == '1') {
      formData.append('profile_image', {
        name: 'photo.jpg',
        type: 'image/jpg',
        uri: this.state.image,
      });
    }

    console.log(formData)

    Api._controllerUpdateProfile(formData)
      .then((response) => {

        console.log(response)
        // if (response.status.toString() == "1") {
        //   this.props.userDetails(this.props.userid, this.state.firstName + " " + this.state.lastName, this.props.userEmail, "", "controller");
        //   this.storeLogDetailsToAsyncStorage(this.props.userid, this.state.firstName + " " + this.state.lastName, this.props.userEmail, "", "controller");
        // }
        this.setState({ isLoading: false, })
        setToastMsg(response.message.toString());
        this.props.navigation.goBack();
      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false, })
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>
            <ScrollView>

              <View style={styles.mainContainer}>


                <Image style={styles.headerBg} source={require('./../../assets/regis_step-bg.png')} resizeMode="stretch" />

                <TouchableOpacity
                  style={{ position: 'absolute', marginTop: 20, marginLeft: 10 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Loader isLoading={this.state.isLoading} />
                <View style={{ marginLeft: 50, marginRight: 50 }}>
                  <Text style={styles.heading}>Edit Profile Details</Text>

                  <Text style={styles.heading_mail}>{this.props.userName}</Text>
                  <Text style={styles.heading_type}>[{this.props.userEmail}]</Text>

                  <TouchableOpacity style={{ alignItems: 'center', height: 120, width: 120, alignSelf: 'center', marginTop: 20 }}
                    onPress={() => this.onSelectImage()}>
                    <Image style={{ height: 120, width: 120, borderRadius: 25 }}
                      source={this.state.image ?
                        (this.state.isImageUpdate == '1' ? { uri: this.state.image } : { uri: AppConstants.IMAGE_URL + this.state.image }) :
                        require('./../../assets/woman_face.jpeg')
                      } resizeMode='contain' />

                  </TouchableOpacity>

                  <View style={{ marginTop: 20 }}>
                    <CustomTextInput
                      placeholder='FIRST NAME'
                      keyboardType='default'
                      floatingText="FIRST NAME"
                      value={this.state.firstName}
                      editable={false}
                      style={{ backgroundColor: '#f5f5f5' }}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          firstName: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "first_name" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='LAST NAME'
                      keyboardType='default'
                      floatingText="FIRST NAME"
                      value={this.state.lastName}
                      editable={false}
                      style={{ backgroundColor: '#f5f5f5' }}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          lastName: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "last_name" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  {/* <ModalSelector
                    data={this.state.countryData}
                    initValue="COUNTRY"
                    accessible={true}
                    search={true}
                    animationType={"fade"}
                    keyExtractor={item => item.id}
                    labelExtractor={item => item.label}
                    onChange={(option) => {
                      this.setState({
                        countryName: option.label,
                        countryId: option.id,
                        stateId: '',
                        stateName: '',
                        cityId: '',
                        cityName: '',
                        isLoading: true,
                      });
                      this.handleState(option.id);
                    }}>

                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='COUNTRY'
                        keyboardType='default'
                        floatingText="COUNTRY"
                        value={this.state.countryName}
                        editable={false}
                        style={{ backgroundColor: '#f5f5f5' }}
                        onFocus={true}
                        showSoftInputOnFocus={true}
                        caretHidden={false}
                        isVisibleDropDown={true}
                        returnKeyType={'done'}
                      />
                    </View>
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "country" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  } */}

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='COUNTRY'
                      keyboardType='default'
                      floatingText="COUNTRY"
                      value={this.state.countryName}
                      editable={false}
                      style={{ backgroundColor: '#f5f5f5' }}
                      onFocus={true}
                      showSoftInputOnFocus={true}
                      caretHidden={false}
                      isVisibleDropDown={true}
                      returnKeyType={'done'}
                    />
                  </View>

                  {/* <ModalSelector
                    data={this.state.stateData}
                    initValue="STATE"
                    accessible={true}
                    search={true}
                    animationType={"fade"}
                    keyExtractor={item => item.id}
                    labelExtractor={item => item.label}
                    onChange={(option) => {
                      this.setState({
                        stateName: option.label,
                        stateId: option.id,
                        cityId: '',
                        cityName: '',
                        isLoading: true,
                      });
                      this.handleCity(option.id);
                    }}>

                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='STATE'
                        keyboardType='default'
                        floatingText="STATE"
                        value={this.state.stateName}
                        editable={false}
                        onFocus={true}
                        showSoftInputOnFocus={true}
                        caretHidden={false}
                        isVisibleDropDown={true}
                        returnKeyType={'done'}
                      />
                    </View>
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "state" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  } */}

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='STATE'
                      keyboardType='default'
                      floatingText="STATE"
                      value={this.state.stateName}
                      editable={false}
                      style={{ backgroundColor: '#f5f5f5' }}
                      onFocus={true}
                      showSoftInputOnFocus={true}
                      caretHidden={false}
                      isVisibleDropDown={true}
                      returnKeyType={'done'}
                    />
                  </View>

                  {/* <ModalSelector
                    data={this.state.cityData}
                    initValue="CITY"
                    accessible={true}
                    search={true}
                    animationType={"fade"}
                    keyExtractor={item => item.id}
                    labelExtractor={item => item.label}
                    onChange={(option) => {
                      this.setState({
                        cityName: option.label,
                        cityId: option.id,
                      });
                    }}>

                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='CITY'
                        keyboardType='default'
                        floatingText="CITY"
                        value={this.state.cityName}
                        editable={false}
                        onFocus={true}
                        showSoftInputOnFocus={true}
                        caretHidden={false}
                        isVisibleDropDown={true}
                        returnKeyType={'done'}
                      />
                    </View>
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "city" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  } */}

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='CITY'
                      keyboardType='default'
                      floatingText="CITY"
                      value={this.state.cityName}
                      editable={false}
                      style={{ backgroundColor: '#f5f5f5' }}
                      onFocus={true}
                      showSoftInputOnFocus={true}
                      caretHidden={false}
                      isVisibleDropDown={true}
                      returnKeyType={'done'}
                    />
                  </View>

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='ZIP/POST CODE'
                      keyboardType='default'
                      floatingText="ZIP/POST CODE"
                      value={this.state.postCode}
                      editable={false}
                      style={{ backgroundColor: '#f5f5f5' }}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          postCode: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "postcode" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='ADDRESS'
                      keyboardType='default'
                      floatingText="ADDRESS"
                      value={this.state.address}
                      editable={false}
                      style={{ backgroundColor: '#f5f5f5' }}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          address: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "address" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='NEAREST LANDMARK'
                      keyboardType='default'
                      floatingText="LANDMARK"
                      value={this.state.landmark}
                      editable={false}
                      style={{ backgroundColor: '#f5f5f5' }}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          landmark: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "landmark" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='DESCRIPTION'
                      keyboardType='default'
                      floatingText="DESCRIPTION"
                      value={this.state.description}
                      editable={false}
                      style={{ backgroundColor: '#f5f5f5' }}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          description: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "description" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5, marginBottom: 10 }}>
                    <CustomTextInput
                      placeholder='PHONE NUMBER'
                      keyboardType='numeric'
                      floatingText="PHONE NUMBER"
                      value={this.state.phone}
                      editable={false}
                      style={{ backgroundColor: '#f5f5f5' }}
                      maxLength={10}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          phone: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "mobile" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                </View>
              </View>
            </ScrollView>

            <View >

              <View style={styles.shadow}></View>
              {/*  <Image style={{ width:'100%',height:40,}} source={require('./../../assets/border.png')} /> */}
              <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                onPress={() => this.checkValidation()}
              >
                <Text style={styles.loginText}>SUBMIT</Text>
              </TouchableOpacity>

            </View>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  countryHandle = () => {
    this.setState({ isLoading: true });

    Api._countryListApi()
      .then((response) => {

        console.log("COUNTRY:- ", response.data);
        var count = Object.keys(response.data).length;
        let countryArray = [];
        for (var i = 0; i < count; i++) {
          countryArray.push({
            id: response.data[i].id,
            label: response.data[i].country_name,
          });
        }
        this.setState({ countryData: countryArray, isLoading: false });

      })
      .catch((err) => {
        console.log("country error: " + err);
        this.setState({ isLoading: false });
      });
  }

  handleState = (countryCode) => {
    let rawData = { "country_id": countryCode }

    Api._stateListApi(rawData)
      .then((response) => {

        console.log("STATE:- ", response.data);
        var count = Object.keys(response.data).length;
        let stateArray = [];
        for (var i = 0; i < count; i++) {
          stateArray.push({
            id: response.data[i].id,
            label: response.data[i].state_name,
          });
        }
        this.setState({ stateData: stateArray, isLoading: false });

      })
      .catch((err) => {
        console.log("state error: " + err);
        this.setState({ isLoading: false });
      });
  }

  handleCity = (stateCode) => {
    let rawData = { "state_id": stateCode }

    Api._cityListApi(rawData)
      .then((response) => {

        console.log("CITY:- ", response.data);
        var count = Object.keys(response.data).length;
        let cityArray = [];
        for (var i = 0; i < count; i++) {
          cityArray.push({
            id: response.data[i].city_id,
            label: response.data[i].city_name,
          });
        }
        this.setState({ cityData: cityArray, isLoading: false });

      })
      .catch((err) => {
        console.log("state error: " + err);
        this.setState({ isLoading: false });
      });
  }

  storeLogDetailsToAsyncStorage = async (id, name, email, image, role) => {
    try {
      await AsyncStorage.setItem('user_id', id);
      await AsyncStorage.setItem('user_name', name);
      await AsyncStorage.setItem('user_email', email);
      await AsyncStorage.setItem('user_image', image);
      await AsyncStorage.setItem('user_role', role);

    } catch (e) {
      console.log(e);
    }
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 20,
    fontFamily: 'montserrat_regular',
    color: '#00B800',
    marginTop: 20

  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }
  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }

});

export default connect(mapStateToPrpos, mapDispatchToProps)(RegistrationSecond);