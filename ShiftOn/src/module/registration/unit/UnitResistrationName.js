/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView,
    Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import { StackActions, NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Api from '../../../apis/Api';
import AppConstants from '../../../apis/AppConstants';
import Loader from '../../../utils/Loader';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../../components/CustomModelSelector';
import ImagePicker from 'react-native-image-crop-picker';
import { androidCameraPermission } from '../../../../permissions';
import { setToastMsg } from '../../../utils/ToastMessage';
import moment from 'moment';

const { width } = Dimensions.get('window');

class UnitResistrationName extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            isSubmitEnable: false,
            setErrors: {
                field: '',
                message: ''
            },
        };
    }

    componentDidMount() {

    }

    _checkUnitExistName = (text) => {
        this.setState({ firstName: text });

        if (text !== '') {

            let formData = new FormData();
            formData.append('val', text);

            Api._checkUnitExistName(formData)
                .then((response) => {
                    console.log(response.data)

                    if (response.status.toString() == "1") {
                        this.setState({ setErrors: { field: 'name', message: response.message }, isSubmitEnable: false })
                    } else {
                        this.setState({ setErrors: { field: '', message: '' }, isSubmitEnable: true })
                    }

                })
                .catch((err) => {
                    console.log(err);
                    setToastMsg("Somthing went wrong.");
                    this.setState({ isLoading: false, })
                });
        } else {
            this.setState({ isSubmitEnable: false })
        }



    }

    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
                    <View style={styles.mainContainer}>
                        <View style={styles.mainContainer}>

                            <Image style={styles.headerBg} source={require('./../../../assets/regis_step-bg.png')} resizeMode="stretch" />

                            <TouchableOpacity
                                style={{ position: 'absolute', marginTop: 20, marginLeft: 10 }}
                                onPress={() => this.props.navigation.goBack()}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                            </TouchableOpacity>

                            <View style={{ justifyContent: 'center' }}>

                                <View style={{ margin: 50, }}>

                                    <View style={{ marginTop: 20 }}>
                                        <CustomTextInput
                                            placeholder='ENTER UNIT NAME'
                                            keyboardType='default'
                                            floatingText="ENTER UNIT NAME"
                                            onFocus={true}
                                            value={this.state.firstName}
                                            returnKeyType={'done'}
                                            onChangeText={(text) => {
                                                this._checkUnitExistName(text);
                                            }}
                                        />
                                    </View>
                                    {
                                        this.state.setErrors.field == "name" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                </View>


                                {this.state.isSubmitEnable ?
                                    <TouchableOpacity
                                        activeOpacity={0.7}
                                        style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                                        onPress={() => {
                                            this.state.firstName ?
                                                this.props.navigation.navigate('UnitRegistration', { unitName: this.state.firstName })
                                                :
                                                setToastMsg("Unit name is required.");
                                        }}
                                    >
                                        <Text style={styles.loginText}>NEXT</Text>
                                    </TouchableOpacity>
                                    :
                                    // <TouchableOpacity
                                    //     activeOpacity={0.7}
                                    //     style={{ padding: 10, marginTop: 20, backgroundColor: "#858585", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                                    // >
                                    //     <Text style={styles.loginText}>NEXT</Text>
                                    // </TouchableOpacity>
                                    <View></View>
                                }

                            </View>

                        </View>

                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },
    headerBg: {
        width: width,
        height: 150,
        alignSelf: 'center'
    },
    logoImage: {
        width: 230,
        height: 80,
        alignSelf: 'center',
        position: 'absolute',
        top: 75


    },
    heading: {
        alignSelf: 'center',
        fontSize: 22,
        fontFamily: 'montserrat_semibold',
        color: '#353C9E',
        marginTop: 10

    },
    heading_mail: {
        alignSelf: 'center',
        fontSize: 20,
        fontFamily: 'montserrat_regular',
        color: '#00B800',
        marginTop: 20

    },
    heading_type: {
        alignSelf: 'center',
        fontSize: 15,
        fontFamily: 'montserrat_regular',
        color: '#000000',
        marginTop: 7

    },

    textInput_style: {
        flex: 1,
        fontSize: 20,
        padding: 15,
        backgroundColor: '#FFFFFF',
        borderColor: '#00D5E1',
        borderRadius: 20,
        borderWidth: 2,
        color: 'rgb(63, 82, 110)',
    },
    forgotPassHold: {
        paddingTop: 10,
        paddingBottom: 30,
        alignSelf: 'flex-end',
        marginRight: 20

    },
    forgotPassDescription: {
        color: '#585858',
        fontSize: 13,
        fontFamily: "montserrat_regular",
        textAlign: 'center'

    },
    loginHold: {
        padding: 10,
        marginTop: 20,
        backgroundColor: "#657CF4",
        borderRadius: 20,
        marginLeft: 3,
        flex: 1
    },
    loginText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontFamily: "montserrat_semibold",
        alignSelf: 'center'

    },
    registerHold: {
        padding: 10,
        marginTop: 20,
        backgroundColor: "#00D5E1",
        borderRadius: 20,
        marginRight: 3,
        flex: 1
    },
    registerText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontFamily: "montserrat_regular",
        alignSelf: 'center'

    },
    shadow: {
        flexDirection: 'column-reverse',
        backgroundColor: "#F3F3F3",
        height: 0.2,
        elevation: 3,
        shadowColor: "#F3F3F3",
        shadowOpacity: 1,
        shadowRadius: 10,
        shadowOffset: {
            height: 10,
            width: 10
        }
    },
    errors: {
        color: 'red',
        fontSize: 14,
        marginLeft: 10,
    }

});

export default UnitResistrationName;