/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';




const { width } = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});

export default class RegistrationSelection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',

    };
    console.log(this.props.navigation);
  }



  componentDidMount() {

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>
            <ScrollView>

              <View style={styles.mainContainer}>



                <Image style={styles.headerBg} source={require('./../../assets/regis_step-bg.png')} resizeMode="stretch" />

                <TouchableOpacity
                  style={{ position: 'absolute', marginTop: 20, marginLeft: 10 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                </TouchableOpacity>

                <View style={{ marginLeft: 50, marginRight: 50 }}>
                  <Text style={styles.heading}>Select type of user</Text>
                </View>

                <TouchableOpacity style={{height:100, padding: 10, marginTop: 20,borderColor:'#00D5E1',borderWidth:2, borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10,alignItems:'center',justifyContent:'center', }}
                  onPress={() => this.props.navigation.navigate('WorkerStepOne')}
                >
                  <Text style={styles.loginText1}>WORKER</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{height:100, padding: 10, marginTop: 20,  borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10,alignItems:'center',justifyContent:'center',borderColor:'#00D5E1',borderWidth:2 }}
                  onPress={() => this.props.navigation.dispatch(resetAction)}
                >
                  <Text style={styles.loginText1}>CONTROLLER</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{height:100, padding: 10, marginTop: 20,  borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10,alignItems:'center',justifyContent:'center',borderColor:'#00D5E1',borderWidth:2 }}
                  onPress={() => this.props.navigation.dispatch(resetAction)}
                >
                  <Text style={styles.loginText1}>UNIT</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>


          </View>
        </SafeAreaView>
      </Fragment>
    );
  }


}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 20,
    fontFamily: 'montserrat_regular',
    color: '#00B800',
    marginTop: 20

  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",

  },
  loginText1: {
    color: '#585858',
    fontSize: 16,
    fontFamily: "montserrat_semibold",

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  }

});

