/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView,
    Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import { StackActions, NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import Loader from '../../utils/Loader';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../components/CustomModelSelector';
import ImagePicker from 'react-native-image-crop-picker';
import { androidCameraPermission } from '../../../permissions';
import { setToastMsg } from '../../utils/ToastMessage';
import moment from 'moment';
import { connect } from 'react-redux';
import { loginUserDetails } from '../../redux/actions/actions';


const { width } = Dimensions.get('window');

const parkingInfoData = [
    {
        id: "1", label: 'Yes',
    },
    {
        id: "2", label: 'No',
    },
]


const tempData = []

const mapStateToPrpos = (props) => {
    return {
        userid: props.userLoginDetails.id,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userDetails: (txt1, txt2, txt3, txt4, txt5) => dispatch(loginUserDetails(txt1, txt2, txt3, txt4, txt5)),
    }
}

class RegistrationThird extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: this.props.navigation.getParam('fullProfileData').email,
            firstName: this.props.navigation.getParam('fullProfileData').name,
            postCode: this.props.navigation.getParam('fullProfileData').post_code,
            address: this.props.navigation.getParam('fullProfileData').address,
            landmark: this.props.navigation.getParam('fullProfileData').landmark,
            phone: this.props.navigation.getParam('fullProfileData').phone,
            person_incharge: this.props.navigation.getParam('fullProfileData').person_incharge,
            parking_info: this.props.navigation.getParam('fullProfileData').parking_info,
            description: this.props.navigation.getParam('fullProfileData').description,
            image: this.props.navigation.getParam('fullProfileData').profile_image,
            isImageUpdate: '0',
            isLoading: false,
            isDeleteAddress: false,

            addressData: [],
            countryData: [],
            stateData: [],
            cityData: [],
            previousData: this.props.navigation.getParam('fullProfileData').unit_locations,

            setErrors: {
                field: '',
                message: ''
            },

        };
        console.log(this.props.navigation);
    }

    componentDidMount() {

        var addressData = {}
        var tempList = []
        addressData.location_id = ''
        addressData.address = ''
        addressData.landmark = ''
        addressData.post_code = ''
        addressData.countryName = ''
        addressData.stateName = ''
        addressData.cityName = ''
        addressData.countryId = ''
        addressData.stateId = ''
        addressData.cityId = ''
        addressData.isDelete = false
        tempList.push(addressData)
        this.setState({ addressData: tempList });

        let newTempList = [];
        for (let i = 0; i < this.state.previousData.length; i++) {
            var newAddressData = {};
            newAddressData.location_id = this.state.previousData[i].id;
            newAddressData.status = this.state.previousData[i].status;
            newAddressData.address = this.state.previousData[i].address;
            newAddressData.landmark = this.state.previousData[i].landmark;
            newAddressData.post_code = this.state.previousData[i].post_code;
            newAddressData.countryName = this.state.previousData[i].country_data.country_name;
            newAddressData.stateName = this.state.previousData[i].state_data.state_name;
            newAddressData.cityName = this.state.previousData[i].city_data.city_name;
            newAddressData.countryId = this.state.previousData[i].country_data.id;
            newAddressData.stateId = this.state.previousData[i].state_data.id;
            newAddressData.cityId = this.state.previousData[i].city_data.city_id;
            newAddressData.isDelete = false;

            newTempList.push(newAddressData)
        }
        this.setState({ addressData: newTempList });

        this.countryHandle();
    }

    onSelectImage = async () => {
        const permissionStatus = await androidCameraPermission()
        if (permissionStatus || Platform.OS == 'ios') {
            Alert.alert(
                'Profile Image',
                'Choose an option',
                [
                    { text: 'Close', onPress: () => console.log('Close pressed') },
                    { text: 'Camera', onPress: () => this.onCamera() },
                    { text: 'Gallery', onPress: () => this.onGallery() },
                ]
            )
        }
    }

    onCamera = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 300,
            cropping: true,
            //freeStyleCropEnabled: true,
            includeBase64: false,
        }).then(image => {
            console.log(image);
            this.setState({
                image: image.path,
                isImageUpdate: '1',
            })
        }).catch((err) => {
            console.log("openCamera erroe" + err.toString())
            setToastMsg(err.toString());
        });

    }

    onGallery = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 300,
            cropping: true,
            //freeStyleCropEnabled: true,
            includeBase64: false,
        }).then(image => {
            console.log(image);
            this.setState({
                image: image.path,
                isImageUpdate: '1',
            })
        }).catch((err) => {
            console.log("openGallery erroe" + err.toString())
            setToastMsg(err.toString());
        });

    }

    checkValidation = () => {
        const emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        let nameError = { field: '', message: '' }
        let emailError = { field: '', message: '' }
        let countryError = { field: '', message: '' }
        let stateError = { field: '', message: '' }
        let cityError = { field: '', message: '' }
        let postCodeError = { field: '', message: '' }
        let landmarkError = { field: '', message: '' }
        let addressError = { field: '', message: '' }
        let inchargeError = { field: '', message: '' }
        let mobileError = { field: '', message: '' }
        let parkingError = { field: '', message: '' }
        let descError = { field: '', message: '' }

        if (this.state.firstName == '') {
            nameError.field = "name";
            nameError.message = "Name is required!";
            this.setState({ setErrors: nameError })
        } else if (this.state.email == '') {
            emailError.field = "email";
            emailError.message = "Email is required!";
            this.setState({ setErrors: emailError })
        } else if (!emailRegEx.test(this.state.email)) {
            emailError.field = "email";
            emailError.message = "Invalid email!";
            this.setState({ setErrors: emailError })
        } else if (this.state.phone == '') {
            mobileError.field = "mobile";
            mobileError.message = "Phone number is required!";
            this.setState({ setErrors: mobileError })
        } else if (this.state.phone.length < 10) {
            mobileError.field = "mobile";
            mobileError.message = "Phone number is invalid!";
            this.setState({ setErrors: mobileError })
        } else if (this.state.person_incharge == '') {
            inchargeError.field = "person_incharge";
            inchargeError.message = "Person in charge is required!";
            this.setState({ setErrors: inchargeError })
        } else if (this.state.parking_info == '') {
            parkingError.field = "parking_info";
            parkingError.message = "Parking info is required!";
            this.setState({ setErrors: parkingError })
        } else if (this.state.description == '') {
            descError.field = "description";
            descError.message = "Description is required!";
            this.setState({ setErrors: descError })
        } else {
            this.setState({ setErrors: { field: '', message: '' } })
            this._unitUpdateProfile();
        }

    }

    _unitUpdateProfile = () => {
        this.setState({ isLoading: true })

        let addressDataLocationId = [];
        let addressDataAddress = [];
        let addressDataLandmark = [];
        let addressDataPostCode = [];
        let addressDataCountryName = [];
        let addressDataStateName = [];
        let addressDataCityName = [];
        let addressDataCountryId = [];
        let addressDataStateId = [];
        let addressDataCityId = [];

        for (var i = 0; i < this.state.addressData.length; i++) {
            addressDataLocationId.push(this.state.addressData[i].location_id.toString())
            addressDataAddress.push(this.state.addressData[i].address.toString())
            addressDataLandmark.push(this.state.addressData[i].landmark.toString())
            addressDataPostCode.push(this.state.addressData[i].post_code.toString())
            addressDataCountryName.push(this.state.addressData[i].countryName.toString())
            addressDataStateName.push(this.state.addressData[i].stateName.toString())
            addressDataCityName.push(this.state.addressData[i].cityName.toString())
            addressDataCountryId.push(this.state.addressData[i].countryId.toString())
            addressDataStateId.push(this.state.addressData[i].stateId.toString())
            addressDataCityId.push(this.state.addressData[i].cityId.toString())
        }

        console.log("-------------------");
        console.log(Object.values(addressDataLocationId));
        console.log(Object.values(addressDataAddress));
        console.log(Object.values(addressDataLandmark));
        console.log(Object.values(addressDataPostCode));
        console.log(Object.values(addressDataCountryName));
        console.log(Object.values(addressDataStateName));
        console.log(Object.values(addressDataCityName));
        console.log(Object.values(addressDataCountryId));
        console.log(Object.values(addressDataStateId));
        console.log(Object.values(addressDataCityId));
        console.log("///////////////////////////");

        var countryIdValue = "1";
        var stateIdValue = "1";
        var cityIdValue = "1";

        for (var i = 0; i < addressDataCountryId.length; i++) {
            if (addressDataCountryId[i] == '') {
                countryIdValue = "0";
            }
        }

        for (var i = 0; i < addressDataStateId.length; i++) {
            if (addressDataStateId[i] == '') {
                stateIdValue = "0";
            }
        }

        for (var i = 0; i < addressDataCityId.length; i++) {
            if (addressDataCityId[i] == '') {
                cityIdValue = "0";
            }
        }

        if (countryIdValue == "0" || stateIdValue == "0" || cityIdValue == "0") {
            this.setState({ isLoading: false });
            setToastMsg("Please fill all address fields.");
        } else {

            var rawData = {
                "id": this.props.userid,
                "name": this.state.firstName,
                "email": this.state.email,
                "phone": this.state.phone,
                "location_id": Object.values(addressDataLocationId),
                "address": Object.values(addressDataAddress),
                "landmark": Object.values(addressDataLandmark),
                "post_code": Object.values(addressDataPostCode),
                "city": Object.values(addressDataCityId),
                "state": Object.values(addressDataStateId),
                "country": Object.values(addressDataCountryId),
                "person_incharge": this.state.person_incharge,
                "parking_info": this.state.parking_info,
                "description": this.state.description,
            }

            Api._unitUpdateProfile(rawData)
                .then((response) => {

                    console.log(response)

                    if (response.status.toString() == "1") {
                        // this.props.userDetails(this.props.userid, this.state.firstName, this.state.email, "", "unit");
                        // this.storeLogDetailsToAsyncStorage(this.props.userid, this.state.firstName, this.state.email, "", "unit");

                        if (this.state.isImageUpdate == '1') {
                            this._unitUpdateProfileImage();
                        } else {
                            this.setState({ isLoading: false })
                            this.props.navigation.goBack();
                        }
                    } else {
                        this.setState({ isLoading: false });
                    }
                    setToastMsg(response.message.toString());

                })
                .catch((err) => {
                    console.log(err);
                    setToastMsg("Somthing went wrong.");
                    this.setState({ isLoading: false });
                });
        }
    }

    _unitUpdateProfileImage = () => {

        let formData = new FormData();
        formData.append('id', this.props.userid);
        if (this.state.isImageUpdate == '1') {
            formData.append('profile_image', {
                name: 'photo.jpg',
                type: 'image/jpg',
                uri: this.state.image,
            });
        }

        Api._unitUpdateProfileImage(formData)
            .then((response) => {

                console.log(response)

                this.setState({ isLoading: false })
                //setToastMsg(response.message.toString());
                this.props.navigation.goBack();

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false, })
            });
    }

    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
                    <View style={styles.mainContainer}>
                        <ScrollView>

                            <View style={styles.mainContainer}>


                                <Image style={styles.headerBg} source={require('./../../assets/regis_step-bg.png')} resizeMode="stretch" />

                                <TouchableOpacity
                                    style={{ position: 'absolute', marginTop: 20, marginLeft: 10 }}
                                    onPress={() => this.props.navigation.goBack()}
                                >
                                    <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                                </TouchableOpacity>
                                <Loader isLoading={this.state.isLoading} />
                                <View style={{ marginLeft: 50, marginRight: 50 }}>
                                    <Text style={styles.heading}>Edit Profile Details</Text>
                                    <Text style={styles.heading_mail}>{this.state.firstName}</Text>
                                    <Text style={styles.heading_type}>[{this.props.navigation.getParam('fullProfileData').email}]</Text>


                                    <TouchableOpacity style={{ alignItems: 'center', height: 120, width: 120, alignSelf: 'center', marginTop: 20 }}
                                        onPress={() => this.onSelectImage()}>
                                        <Image style={{ height: 120, width: 120, borderRadius: 25 }}
                                            source={this.state.image ?
                                                (this.state.isImageUpdate == '1' ? { uri: this.state.image } : { uri: AppConstants.IMAGE_URL + this.state.image }) :
                                                require('./../../assets/woman_face.jpeg')
                                            } resizeMode='stretch' />

                                    </TouchableOpacity>

                                    <View style={{ marginTop: 20 }}>
                                        <CustomTextInput
                                            placeholder='NAME'
                                            keyboardType='default'
                                            floatingText="NAME"
                                            onFocus={true}
                                            value={this.state.firstName}
                                            returnKeyType={'done'}
                                            onChangeText={(text) => {
                                                this.setState({ firstName: text });
                                            }}
                                        />
                                    </View>
                                    {
                                        this.state.setErrors.field == "name" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                    <View style={{ marginTop: 5 }}>
                                        <CustomTextInput
                                            placeholder='EMAIL'
                                            keyboardType='email-address'
                                            floatingText="EMAIL"
                                            //style={{ backgroundColor: '#e6e6e6' }}
                                            editable={true}
                                            onFocus={true}
                                            value={this.state.email}
                                            returnKeyType={'done'}
                                            onChangeText={(text) => {
                                                this.setState({ email: text });
                                            }}
                                        />
                                    </View>
                                    {
                                        this.state.setErrors.field == "email" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                    <View style={{ marginTop: 5 }}>
                                        <CustomTextInput
                                            placeholder='PHONE NUMBER'
                                            keyboardType='numeric'
                                            floatingText="PHONE NUMBER"
                                            onFocus={true}
                                            value={this.state.phone}
                                            maxLength={10}
                                            returnKeyType={'done'}
                                            onChangeText={(text) => {
                                                this.setState({ phone: text });
                                            }}
                                        />
                                    </View>
                                    {
                                        this.state.setErrors.field == "mobile" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                    <View style={{ marginTop: 5 }}>
                                        <CustomTextInput
                                            placeholder='PERSON IN CHARGE'
                                            keyboardType='default'
                                            floatingText="PERSON IN CHARGE"
                                            onFocus={true}
                                            value={this.state.person_incharge}
                                            returnKeyType={'done'}
                                            onChangeText={(text) => {
                                                this.setState({ person_incharge: text });
                                            }}
                                        />
                                    </View>
                                    {
                                        this.state.setErrors.field == "person_incharge" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                    <View style={{ marginTop: 5 }}>
                                        <ModalSelector
                                            data={parkingInfoData}
                                            initValue="PARKING INFORMATION"
                                            accessible={true}
                                            search={false}
                                            animationType={"fade"}
                                            keyExtractor={item => item.id}
                                            labelExtractor={item => item.label}
                                            onChange={(option) => { this.setState({ parking_info: option.label }) }}>
                                            <CustomTextInput
                                                placeholder='PARKING INFORMATION'
                                                keyboardType='default'
                                                floatingText="PARKING INFORMATION"
                                                value={this.state.parking_info}
                                                editable={true}
                                                onFocus={true}
                                                showSoftInputOnFocus={true}
                                                caretHidden={false}
                                                isVisibleDropDown={true}
                                                returnKeyType={'done'}
                                            />
                                        </ModalSelector>
                                    </View>
                                    {
                                        this.state.setErrors.field == "parking_info" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                    <View style={{ marginTop: 5 }}>
                                        <CustomTextInput
                                            placeholder='DESCRIPTION'
                                            keyboardType='default'
                                            floatingText="DESCRIPTION"
                                            onFocus={true}
                                            value={this.state.description}
                                            returnKeyType={'done'}
                                            onChangeText={(text) => {
                                                this.setState({ description: text });
                                            }}
                                        />
                                    </View>
                                    {
                                        this.state.setErrors.field == "description" && (
                                            <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                        )
                                    }

                                </View>
                                <View style={{ marginTop: 10 }}>

                                    <FlatList
                                        style={{ width: '100%', marginBottom: '12%' }}
                                        data={this.state.addressData}
                                        keyExtractor={(item, index) => index.toString()}
                                        //ItemSeparatorComponent={this.ItemSeparator}
                                        renderItem={({ item, index }) =>

                                            <View >
                                                <View style={{ marginLeft: 50, marginRight: 35, flexDirection: 'row', marginBottom: 5 }}>

                                                    <View style={{
                                                        width: '84%', borderWidth: 1,
                                                        borderColor: '#00D5E1', borderRadius: 20, padding: 10,
                                                    }}>

                                                        <View style={{ marginTop: 5 }}>
                                                            <CustomTextInput
                                                                placeholder='ADDRESS'
                                                                keyboardType='default'
                                                                floatingText="ADDRESS"
                                                                onFocus={true}
                                                                value={this.state.addressData[index].address}
                                                                returnKeyType={'done'}
                                                                onChangeText={(text) => {
                                                                    this._oNChangeDetails(text, index, 'address')
                                                                }}
                                                            />
                                                        </View>

                                                        <View style={{ marginTop: 5 }}>
                                                            <CustomTextInput
                                                                placeholder='NEAREST LANDMARK'
                                                                keyboardType='default'
                                                                floatingText="NEAREST LANDMARK"
                                                                onFocus={true}
                                                                value={this.state.addressData[index].landmark}
                                                                returnKeyType={'done'}
                                                                onChangeText={(text) => {
                                                                    this._oNChangeDetails(text, index, 'landmark')
                                                                }}
                                                            />
                                                        </View>

                                                        <View style={{ marginTop: 5 }}>
                                                            <CustomTextInput
                                                                placeholder='ZIP/POST CODE'
                                                                keyboardType='default'
                                                                floatingText="ZIP/POST CODE"
                                                                onFocus={true}
                                                                value={this.state.addressData[index].post_code}
                                                                returnKeyType={'done'}
                                                                onChangeText={(text) => {
                                                                    this._oNChangeDetails(text, index, 'post_code')
                                                                }}
                                                            />
                                                        </View>

                                                        <ModalSelector
                                                            data={this.state.countryData}
                                                            initValue="COUNTRY"
                                                            accessible={true}
                                                            search={true}
                                                            animationType={"fade"}
                                                            keyExtractor={item => item.id}
                                                            labelExtractor={item => item.label}
                                                            onChange={(option) => {
                                                                this.setState({ isLoading: true });
                                                                this._oNChangeDetails(option, index, 'country');
                                                                this.handleState(option.id);
                                                            }}
                                                        >
                                                            <View style={{ marginTop: 5 }}>
                                                                <CustomTextInput
                                                                    placeholder='COUNTRY'
                                                                    keyboardType='default'
                                                                    floatingText="COUNTRY"
                                                                    value={this.state.addressData[index].countryName}
                                                                    editable={false}
                                                                    onFocus={true}
                                                                    showSoftInputOnFocus={true}
                                                                    caretHidden={false}
                                                                    isVisibleDropDown={true}
                                                                    returnKeyType={'done'}
                                                                />
                                                            </View>
                                                        </ModalSelector>

                                                        {
                                                            this.state.addressData[index].countryName ?
                                                                (<ModalSelector
                                                                    data={this.state.stateData}
                                                                    initValue="STATE"
                                                                    accessible={true}
                                                                    search={true}
                                                                    animationType={"fade"}
                                                                    keyExtractor={item => item.id}
                                                                    labelExtractor={item => item.label}
                                                                    onChange={(option) => {
                                                                        this.setState({ isLoading: true });
                                                                        this._oNChangeDetails(option, index, 'state')
                                                                        this.handleCity(option.id);
                                                                    }}>
                                                                    <View style={{ marginTop: 5 }}>
                                                                        <CustomTextInput
                                                                            placeholder='STATE'
                                                                            keyboardType='default'
                                                                            floatingText="STATE"
                                                                            value={this.state.addressData[index].stateName}
                                                                            editable={false}
                                                                            onFocus={true}
                                                                            showSoftInputOnFocus={true}
                                                                            caretHidden={false}
                                                                            isVisibleDropDown={true}
                                                                            returnKeyType={'done'}
                                                                        />
                                                                    </View>
                                                                </ModalSelector>)
                                                                :
                                                                (<ModalSelector
                                                                    data={tempData}
                                                                    initValue="STATE"
                                                                    accessible={true}
                                                                    search={true}
                                                                    animationType={"fade"}
                                                                    keyExtractor={item => item.id}
                                                                    labelExtractor={item => item.label}
                                                                >
                                                                    <View style={{ marginTop: 5 }}>
                                                                        <CustomTextInput
                                                                            placeholder='STATE'
                                                                            keyboardType='default'
                                                                            floatingText="STATE"
                                                                            value={this.state.addressData[index].stateName}
                                                                            editable={false}
                                                                            onFocus={true}
                                                                            showSoftInputOnFocus={true}
                                                                            caretHidden={false}
                                                                            isVisibleDropDown={true}
                                                                            returnKeyType={'done'}
                                                                        />
                                                                    </View>
                                                                </ModalSelector>)
                                                        }

                                                        {
                                                            this.state.addressData[index].stateName ?
                                                                (<ModalSelector
                                                                    data={this.state.cityData}
                                                                    initValue="CITY"
                                                                    accessible={true}
                                                                    search={true}
                                                                    animationType={"fade"}
                                                                    keyExtractor={item => item.id}
                                                                    labelExtractor={item => item.label}
                                                                    onChange={(option) => {
                                                                        this._oNChangeDetails(option, index, 'city')
                                                                    }}>
                                                                    <View style={{ marginTop: 5 }}>
                                                                        <CustomTextInput
                                                                            placeholder='CITY'
                                                                            keyboardType='default'
                                                                            floatingText="CITY"
                                                                            value={this.state.addressData[index].cityName}
                                                                            editable={false}
                                                                            onFocus={true}
                                                                            showSoftInputOnFocus={true}
                                                                            caretHidden={false}
                                                                            isVisibleDropDown={true}
                                                                            returnKeyType={'done'}
                                                                        />
                                                                    </View>
                                                                </ModalSelector>)
                                                                :
                                                                (<ModalSelector
                                                                    data={tempData}
                                                                    initValue="CITY"
                                                                    accessible={true}
                                                                    search={true}
                                                                    animationType={"fade"}
                                                                    keyExtractor={item => item.id}
                                                                    labelExtractor={item => item.label}
                                                                >
                                                                    <View style={{ marginTop: 5 }}>
                                                                        <CustomTextInput
                                                                            placeholder='CITY'
                                                                            keyboardType='default'
                                                                            floatingText="CITY"
                                                                            value={this.state.addressData[index].cityName}
                                                                            editable={false}
                                                                            onFocus={true}
                                                                            showSoftInputOnFocus={true}
                                                                            caretHidden={false}
                                                                            isVisibleDropDown={true}
                                                                            returnKeyType={'done'}
                                                                        />
                                                                    </View>
                                                                </ModalSelector>)
                                                        }


                                                    </View>

                                                    {
                                                        this.state.addressData[index].isDelete ?
                                                            (<TouchableOpacity style={{ width: '16%', marginTop: 10, height: 28, width: 28, end: 0 }} activeOpacity={0.7}
                                                                onPress={() => { this.setState({ isDeleteAddress: true }), this.deleteAddressData(item, index) }}>
                                                                <Image style={{ height: 28, width: 28, padding: 12, tintColor: '#00D5E1' }} source={require('./../../assets/trash_icon.png')} />
                                                            </TouchableOpacity>)
                                                            :
                                                            (
                                                                this.state.addressData[index].status.toString() == "1" ?
                                                                    <TouchableOpacity style={{ width: '16%', marginTop: 10, height: 28, width: 28, end: -6 }}
                                                                        onPress={() => { this._active(this.state.addressData[index].location_id, "0") }}
                                                                    >
                                                                        {/* <View style={{ height: 22, width: 22, backgroundColor: '#92c949', borderRadius: 100 }}></View> */}
                                                                        <Image style={{ height: 18, width: 40, padding: 10 }} source={require('./../../assets/toggle_icon_on.png')} />
                                                                    </TouchableOpacity>
                                                                    :
                                                                    <TouchableOpacity style={{ width: '16%', marginTop: 10, height: 28, width: 28, end: -6 }}
                                                                        onPress={() => { this._active(this.state.addressData[index].location_id, "1") }}
                                                                    >
                                                                        {/* <View style={{ height: 22, width: 22, backgroundColor: '#bbbbbb', borderRadius: 100 }}></View> */}
                                                                        <Image style={{ height: 18, width: 40, padding: 10 }} source={require('./../../assets/toggle_icon_off.png')} />
                                                                    </TouchableOpacity>
                                                            )

                                                    }

                                                </View>

                                            </View>

                                        }
                                    />
                                    <TouchableOpacity style={{ marginTop: -35, marginBottom: 5, alignSelf: 'center', padding: 8, }}
                                        activeOpacity={0.9}
                                        onPress={() => this._addAddressData()}
                                    >
                                        <Text style={{ color: "#000000", fontSize: 12, fontFamily: "montserrat_regular" }} >
                                            + ADD MORE ADDRESS
                                        </Text>
                                    </TouchableOpacity>


                                </View>
                            </View>
                        </ScrollView>

                        <View >

                            <View style={styles.shadow}></View>
                            {/*  <Image style={{ width:'100%',height:40,}} source={require('./../../assets/border.png')} /> */}
                            <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                                onPress={() => this.checkValidation()}
                            >
                                <Text style={styles.loginText}>SUBMIT</Text>
                            </TouchableOpacity>

                        </View>

                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }

    _addAddressData = () => {

        var addressData = {}
        let tempList = [...this.state.addressData]
        addressData.location_id = ''
        addressData.address = ''
        addressData.landmark = ''
        addressData.post_code = ''
        addressData.countryName = ''
        addressData.stateName = ''
        addressData.cityName = ''
        addressData.countryId = ''
        addressData.stateId = ''
        addressData.cityId = ''
        addressData.isDelete = true;
        tempList.push(addressData)
        this.setState({ addressData: tempList });

    }

    _active = (id, status) => {

        Alert.alert(
            '',
            'Are you sure?',
            [
                { text: 'NO', onPress: () => console.log('Close pressed') },
                {
                    text: 'YES', onPress: () => {
                        this.setState({ isLoading: true });

                        var rawData = {
                            "id": id,
                            "status": status,
                        }
                        Api._unitUpdateLocationStatus(rawData)
                            .then((response) => {

                                console.log(response);
                                setToastMsg(response.message.toString());
                                this.props.navigation.goBack();

                            })
                            .catch((err) => {
                                console.log(err);
                                setToastMsg("Somthing went wrong.");
                                this.setState({ isLoading: false });
                            });
                    }
                },
            ]
        )

    }

    deleteAddressData = (e, pos) => {

        Alert.alert(
            "Comfirm to Delete?",
            'Are you sure you want to delete this address?',
            [
                { text: 'NO', onPress: () => { console.log('No pressed') } },
                {
                    text: 'YES', onPress: () => {

                        var array = [...this.state.addressData];
                        var index = array.indexOf(e)
                        array.splice(index, 1);

                        console.log(index);

                        this.setState({ addressData: array });
                        setTimeout(() => {
                            this.setState({ isDeleteAddress: false })
                        }, 1000)

                    }
                },

            ],
            { cancelable: false },
        )

    }

    _oNChangeDetails = (text, pos, type) => {

        if (!this.state.isDeleteAddress) {
            if (type == 'address') {
                let tempList = [...this.state.addressData]
                tempList[pos].address = text
                this.setState({ addressData: tempList });
            } else if (type == 'landmark') {
                let tempList = [...this.state.addressData]
                tempList[pos].landmark = text
                this.setState({ addressData: tempList });
            } else if (type == 'post_code') {
                let tempList = [...this.state.addressData]
                tempList[pos].post_code = text
                this.setState({ addressData: tempList });
            } else if (type == 'country') {
                let tempList = [...this.state.addressData]
                tempList[pos].countryName = text.label
                tempList[pos].countryId = text.id
                tempList[pos].stateName = ''
                tempList[pos].stateId = ''
                tempList[pos].cityName = ''
                tempList[pos].cityId = ''
                this.setState({ addressData: tempList });
            } else if (type == 'state') {
                let tempList = [...this.state.addressData]
                tempList[pos].stateName = text.label
                tempList[pos].stateId = text.id
                tempList[pos].cityName = ''
                tempList[pos].cityId = ''
                this.setState({ addressData: tempList });
            } else if (type == 'city') {
                let tempList = [...this.state.addressData]
                tempList[pos].cityName = text.label
                tempList[pos].cityId = text.id
                this.setState({ addressData: tempList });
            }
        }

    }

    countryHandle = () => {
        this.setState({ isLoading: true });

        Api._countryListApi()
            .then((response) => {

                console.log("COUNTRY:- ", response.data);
                var count = Object.keys(response.data).length;
                let countryArray = [];
                for (var i = 0; i < count; i++) {
                    countryArray.push({
                        id: response.data[i].id,
                        label: response.data[i].country_name,
                    });
                }
                this.setState({ countryData: countryArray, isLoading: false });

            })
            .catch((err) => {
                console.log("country error: " + err);
                this.setState({ isLoading: false, });
            });
    }

    handleState = (countryCode) => {
        let rawData = { "country_id": countryCode, }

        Api._stateListApi(rawData)
            .then((response) => {

                console.log("STATE:- ", response.data);
                var count = Object.keys(response.data).length;
                let stateArray = [];
                for (var i = 0; i < count; i++) {
                    stateArray.push({
                        id: response.data[i].id,
                        label: response.data[i].state_name,
                    });
                }
                this.setState({
                    stateData: stateArray,
                    isLoading: false,
                });

            })
            .catch((err) => {
                console.log("state error: " + err);
                this.setState({ isLoading: false, });
            });
    }

    handleCity = (stateCode) => {
        let rawData = { "state_id": stateCode }

        Api._cityListApi(rawData)
            .then((response) => {

                console.log("CITY:- ", response.data);
                var count = Object.keys(response.data).length;
                let cityArray = [];
                for (var i = 0; i < count; i++) {
                    cityArray.push({
                        id: response.data[i].city_id,
                        label: response.data[i].city_name,
                    });
                }
                this.setState({
                    cityData: cityArray,
                    isLoading: false,
                });

            })
            .catch((err) => {
                console.log("state error: " + err);
                this.setState({ isLoading: false });
            });
    }


    storeLogDetailsToAsyncStorage = async (id, name, email, image, role) => {
        try {
            await AsyncStorage.setItem('user_id', id);
            await AsyncStorage.setItem('user_name', name);
            await AsyncStorage.setItem('user_email', email);
            await AsyncStorage.setItem('user_image', image);
            await AsyncStorage.setItem('user_role', role);

        } catch (e) {
            console.log(e);
        }
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },
    headerBg: {
        width: width,
        height: 150,
        alignSelf: 'center'
    },
    logoImage: {
        width: 230,
        height: 80,
        alignSelf: 'center',
        position: 'absolute',
        top: 75


    },
    heading: {
        alignSelf: 'center',
        fontSize: 22,
        fontFamily: 'montserrat_semibold',
        color: '#353C9E',
        marginTop: 10

    },
    heading_mail: {
        alignSelf: 'center',
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'montserrat_regular',
        color: '#00B800',
        marginTop: 20

    },
    heading_type: {
        alignSelf: 'center',
        textAlign: 'center',
        fontSize: 15,
        fontFamily: 'montserrat_regular',
        color: '#000000',
        marginTop: 7

    },

    textInput_style: {
        flex: 1,
        fontSize: 20,
        padding: 15,
        backgroundColor: '#FFFFFF',
        borderColor: '#00D5E1',
        borderRadius: 20,
        borderWidth: 2,
        color: 'rgb(63, 82, 110)',
    },
    forgotPassHold: {
        paddingTop: 10,
        paddingBottom: 30,
        alignSelf: 'flex-end',
        marginRight: 20

    },
    forgotPassDescription: {
        color: '#585858',
        fontSize: 13,
        fontFamily: "montserrat_regular",
        textAlign: 'center'

    },
    loginHold: {
        padding: 10,
        marginTop: 20,
        backgroundColor: "#657CF4",
        borderRadius: 20,
        marginLeft: 3,
        flex: 1
    },
    loginText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontFamily: "montserrat_semibold",
        alignSelf: 'center'

    },
    registerHold: {
        padding: 10,
        marginTop: 20,
        backgroundColor: "#00D5E1",
        borderRadius: 20,
        marginRight: 3,
        flex: 1
    },
    registerText: {
        color: '#FFFFFF',
        fontSize: 16,
        fontFamily: "montserrat_regular",
        alignSelf: 'center'

    },
    shadow: {
        flexDirection: 'column-reverse',
        backgroundColor: "#F3F3F3",
        height: 0.2,
        elevation: 3,
        shadowColor: "#F3F3F3",
        shadowOpacity: 1,
        shadowRadius: 10,
        shadowOffset: {
            height: 10,
            width: 10
        }
    },
    errors: {
        color: 'red',
        fontSize: 14,
        marginLeft: 10,
    }

});

export default connect(mapStateToPrpos, mapDispatchToProps)(RegistrationThird);