/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet, View, Image, Dimensions, Modal, Platform, Text, TouchableOpacity, Linking, ImageBackground
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { StackActions, NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DeviceInfo, { getBuildNumber, getVersion } from 'react-native-device-info';
import { removeLogDetailsToAsyncStorage, logoutApi } from '../../utils/Logout';
import messaging from '@react-native-firebase/messaging';
import Api from '../../apis/Api';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';


const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 260;

const resetToLogin = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});
const resetToWorker = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'WorkerHome' })],
});
const resetToUnit = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'UnitHome' })],
});
const resetToController = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'ControlerHome' })],
});

const deviceType = Platform.OS == 'ios' ? 'ios' : 'android'
const sortDeviceType = Platform.OS == 'ios' ? 'I' : 'A'

export default class Splash extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      nextPage: "",
      isResponseModalVisible: false,
      responseData: {},
      responseModalUpdates: '',
      responseModalNameString: '',
      responseModalNumberString: '',
      responseModalHeaderString: '',
    }
  }

  componentDidMount() {
    this.setState({ isLoading: true })
    this.redirectPage();

    // Register background handler
    messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log('Message handled in the background!', remoteMessage);
    });

    messaging().onNotificationOpenedApp(async remoteMessage => {
      console.log("background notification: ", remoteMessage);
      try {
        await AsyncStorage.setItem('nextScreen', remoteMessage.data.screen);
      } catch (e) {
        console.log(e);
      }

      // for notification click event.
      if (remoteMessage.data.screen == 'WorkerAgency') {
        this.setState({ nextPage: 'WorkerHome' });
      } else {
        this.setState({ nextPage: 'UnitHome' });
      }


    });

    messaging().onMessage(async remoteMessage => {
      console.log("foreground notification: ", remoteMessage);

    });

    messaging().getInitialNotification().then(async remoteMessage => {
      if (remoteMessage) {
        console.log("Notification caused app to open from quite state: ", remoteMessage);
        try {
          await AsyncStorage.setItem('nextScreen', remoteMessage.data.screen);
        } catch (e) {
          console.log(e);
        }

        // for notification click event.
        if (remoteMessage.data.screen == 'WorkerAgency') {
          this.setState({ nextPage: 'WorkerHome' });
        } else {
          this.setState({ nextPage: 'UnitHome' });
        }

      }
    })

    this.getCurrentVersionApi()
  }

  getCurrentVersionApi = async () => {
    var VersionIgnoreValue = await AsyncStorage.getItem('VersionIgnore')

    var formData = new FormData();
    formData.append('version_id', getBuildNumber());
    formData.append('device_type', sortDeviceType);

    Api.getCurrentVersionApi(formData)
      .then(response => {
        console.log(response);

        if (response.status.toString() == "1") {
          if (response.data == null) {
            this.userLogedinFunc()
          } else {
            if (this.checkVersionFromResponse(response.data.version, VersionIgnoreValue, getVersion())) {
              this.setState({
                isResponseModalVisible: true, isLoading: false,
                responseModalHeaderString: response.data.header_text, responseModalNameString: response.data.name,
                responseModalUpdates: response.data.updates, responseModalNumberString: response.data.version, responseData: response.data
              })
            } else {
              this.userLogedinFunc()
            }
          }
        } else {
          this.userLogedinFunc()
        }
      })
      .catch(err => {
        console.log(err);
        this.setState({ isLoading: false })
      });
  }

  userLogedinFunc = async () => {
    this.setState({ isLoading: true })
    var id = await AsyncStorage.getItem('user_id');
    var role = await AsyncStorage.getItem('user_role');
    var device_token = await AsyncStorage.getItem('device_token');

    var formData = new FormData();
    formData.append("user_id", id);
    formData.append("role", role);
    formData.append("device", device_token);
    console.log(formData);

    Api._isUserLoggedIn(formData)
      .then((response) => {
        console.log(response.data)

        if (response.status.toString() == "1") {
          if (this.state.nextPage !== '' && this.state.nextPage == "WorkerHome") {
            if (id !== null && role == 'worker') {
              this.props.navigation.dispatch(resetToWorker);
            } else {
              logoutApi(this.props.navigation);
            }
          } else if (this.state.nextPage !== '' && this.state.nextPage == "UnitHome") {
            if (id !== null && role == 'unit') {
              this.props.navigation.dispatch(resetToUnit)
            } else {
              logoutApi(this.props.navigation);
            }
          } else {
            this.redirectFunc();
          }
        } else {
          logoutApi(this.props.navigation);
        }
        this.setState({ isLoading: false })

      })
      .catch((err) => {
        console.log(err);
        this.setState({ isLoading: false })
        setToastMsg("Somthing went wrong.");
        logoutApi(this.props.navigation);
      });
  }

  redirectPage = async () => {
    try {
      await AsyncStorage.setItem('nextScreen', '');
    } catch (e) {
      console.log(e);
    }
  }

  redirectFunc = async () => {
    var id = await AsyncStorage.getItem('user_id');
    var role = await AsyncStorage.getItem('user_role');

    if (id !== null) {
      if (role == 'worker') {
        this.props.navigation.dispatch(resetToWorker)
      } else if (role == 'controller') {
        this.props.navigation.dispatch(resetToController)
      } else if (role == 'unit') {
        this.props.navigation.dispatch(resetToUnit)
      } else {
        //this.props.navigation.dispatch(resetToLogin)
        logoutApi(this.props.navigation);
      }
    } else {
      //this.props.navigation.dispatch(resetToLogin)
      logoutApi(this.props.navigation);
    }
  }


  render() {

    return (
      <View style={styles.mainContainer}>
        <Loader isLoading={this.state.isLoading} />

        <View>

          <Modal
            transparent={true}
            visible={this.state.isResponseModalVisible}
            animationType="none">
            <View style={styles.versionModalContainer}>
              <View style={styles.versionModalContent}>
                <ImageBackground source={require('../../assets/version_popup.png')} style={{ width: '100%', }} resizeMode='cover'>
                  <View
                    style={{
                      width: '100%',
                      paddingHorizontal: 10,
                      paddingTop: 10,
                      flexDirection: 'row',
                    }}>
                    <View style={{ alignItems: 'flex-start', width: '50%', marginTop: 40 }}>
                      <Image source={require('../../assets/group_img.png')} style={{ height: 60, width: 60 }} resizeMode='contain' />
                    </View>
                    <View style={{ alignItems: 'center', width: '50%' }}>
                      <Image source={require('../../assets/rocket.png')} style={{ height: 90, width: 90 }} resizeMode='contain' />
                    </View>
                  </View>
                  <View style={{ paddingHorizontal: 25, }}>
                    <Text style={{ fontSize: 19, fontFamily: 'montserrat_semibold', color: '#666666' }}>Update your application to the latest version</Text>
                    {/* <Text style={{ fontSize: 14, fontFamily: 'montserrat_regular', marginTop: 10, color: '#666666' }}>For more features and a better user experience, please upgrade this app.</Text> */}

                    <Text style={{ fontSize: 14, fontFamily: 'montserrat_semibold', marginTop: 10, color: '#666666' }}>Version: {this.state.responseModalNumberString}</Text>

                    <View style={{ marginTop: 5, }}>
                      <Text style={{ fontSize: 14, fontFamily: 'montserrat_regular', color: '#666666' }}>{this.state.responseModalUpdates}</Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      width: '90%',
                      justifyContent: 'space-between',
                      alignSelf: 'center',
                      marginTop: 30,
                      marginBottom: 15
                    }}>
                    {
                      this.state.responseData.is_update !== "1" ?
                        (
                          <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={() => { this.versionIgnore(this.state.responseData.version, this.state.responseData.is_update) }}
                            style={styles.versionNoButton}>
                            <Text
                              style={{
                                color: 'white',
                                fontSize: 15,
                                fontFamily: 'montserrat_regular',
                              }}>
                              NOT NOW
                            </Text>
                          </TouchableOpacity>
                        )
                        :
                        null
                    }

                    <TouchableOpacity
                      activeOpacity={0.8}
                      onPress={() => { this.versionUpdate() }}
                      style={[styles.versionYesButton, { width: this.state.responseData.is_update == '1' ? '100%' : '48%' }]}>
                      <Text
                        style={{
                          color: 'white',
                          fontSize: 15,
                          fontFamily: 'montserrat_regular',
                        }}>
                        UPGRADE
                      </Text>
                    </TouchableOpacity>
                  </View>
                </ImageBackground>
              </View>


            </View>
          </Modal>

        </View>

        <Image style={styles.smilyImg} source={require('../../assets/logo_b.png')} resizeMode='stretch' />


      </View>
    );
  }

  versionIgnore = async (versionCode, is_update) => {
    if (is_update == '1') {
      setToastMsg("You cannot ignore this update, Please upgrade your app.");
    } else {
      this.setState({ isResponseModalVisible: false })
      try {
        console.log(versionCode)
        await AsyncStorage.setItem('VersionIgnore', versionCode ? versionCode : '');
      } catch (error) {
        console.error('Error getting version ID:', error);
      }

      this.userLogedinFunc()
    }
  }

  versionUpdate = () => {
    // this.setState({ isResponseModalVisible: false })
    if (deviceType == 'ios') {
      const link = 'itms-apps://apps.apple.com/id/app/halojasa/id1492671277?l=id';
      Linking.canOpenURL(link).then(supported => {
        supported && Linking.openURL(link);
      }, (err) => console.log(err));
    } else {
      Linking.openURL(
        'http://play.google.com/store/apps/details?id=com.zabingo.blooddonate'
      );
    }
  }

  checkVersionFromResponse = (versionCode, VersionIgnoreValue, getVersion) => {
    if (VersionIgnoreValue != null && VersionIgnoreValue) {
      if (VersionIgnoreValue == versionCode) {
        return false;
      }
      else {
        return true;
      }
    } else {
      if (getVersion === versionCode) {
        return false;
      } else {
        return true;
      }
    }
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#00D5E1',
    justifyContent: 'center',
    alignItems: 'center'
  },


  smilyImg: {
    width: 250,
    height: 100,

  },

  versionModalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    flexDirection: 'column',
  },
  versionModalContent: {
    backgroundColor: '#ececec',
    borderRadius: 10,
    width: '80%',
    overflow: 'hidden',
    paddingVertical: 10
  },
  versionNoButton: {
    width: '48%',
    marginTop: 20,
    backgroundColor: '#32d4e0',
    alignItems: 'center',
    padding: 10,
    borderRadius: 20,
  },
  versionYesButton: {
    width: '48%',
    marginTop: 20,
    backgroundColor: '#469cef',
    alignItems: 'center',
    padding: 10,
    borderRadius: 20,
  },

});

