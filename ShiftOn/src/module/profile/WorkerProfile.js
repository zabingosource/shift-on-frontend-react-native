/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../components/CustomTextInput';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import AppConstants from '../../apis/AppConstants';
import { removeLogDetailsToAsyncStorage } from '../../utils/Logout';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
  }
}

const { width } = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});


class WorkerProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
    console.log(this.props.navigation);
  }

  componentDidMount() {

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>
            <ScrollView>

              <View style={styles.mainContainer}>

                <Image style={styles.headerBg} source={require('./../../assets/header_bg.png')} resizeMode="stretch" />

                <TouchableOpacity
                  style={{ position: 'absolute', marginTop: 20, marginLeft: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                </TouchableOpacity>

                {this.props.userImage ?
                  <Image style={styles.profileImage} source={{ uri: AppConstants.IMAGE_URL + this.props.userImage }} resizeMode="stretch" />
                  :
                  <Image style={styles.profileImage} source={require('./../../assets/woman_face.jpeg')} resizeMode="stretch" />
                }

                <View style={{ marginLeft: 50, marginRight: 50, marginTop: 60 }}>
                  <Text style={styles.heading}>{(this.props.userName).trim() != "" ? (this.props.userName) : "User"}</Text>

                  <View style={{ marginTop: 15 }}>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('EditWorkerProfileOption')}
                    >
                      <CustomTextInput
                        placeholder='Edit Profile'
                        keyboardType='default'
                        //floatingText="Edit Profile"
                        editable={false}
                        isVisibleRightArrow={true}
                        value={'Edit Profile'}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({
                            email: text,
                          });
                        }}
                      />

                    </TouchableOpacity>

                  </View>

                  <View >
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('Settings')}
                    >
                      <CustomTextInput
                        placeholder='Settings'
                        keyboardType='default'
                        //floatingText="Settings"
                        editable={false}
                        isVisibleRightArrow={true}
                        value={'Settings'}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({
                            email: text,
                          });
                        }}
                      />

                    </TouchableOpacity>

                  </View>

                  <View >
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('ForgotPassword', { forWhat: "forgotPassword" })}
                    >
                      <CustomTextInput
                        placeholder='Reset Password'
                        keyboardType='default'
                        //floatingText="Edit Profile"
                        editable={false}
                        isVisibleRightArrow={true}
                        value={'Reset Password'}
                        returnKeyType={'done'}
                      />

                    </TouchableOpacity>

                  </View>


                  <TouchableOpacity style={{ padding: 10, backgroundColor: "#657CF4", borderRadius: 20, marginTop: 50 }}
                    onPress={() => removeLogDetailsToAsyncStorage(this.props.navigation)}
                  >
                    <Text style={styles.loginText}>LOGOUT</Text>
                  </TouchableOpacity>

                </View>
              </View>
            </ScrollView>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }


}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center',

  },

  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10,
    textAlign: 'center',

  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  profileImage: {
    height: 120,
    width: 120,
    position: 'absolute',
    borderRadius: 25,
    alignSelf: 'center',
    marginTop: 60

  },

});

export default connect(mapStateToPrpos)(WorkerProfile);