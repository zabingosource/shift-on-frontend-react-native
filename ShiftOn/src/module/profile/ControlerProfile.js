/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { loginUserDetails } from '../../redux/actions/actions';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';
import { removeLogDetailsToAsyncStorage } from '../../utils/Logout';

const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userId: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
    userEmail: props.userLoginDetails.email,
    imgUrl: props.defaultImageUrl.url,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userDetails: (txt1, txt2, txt3, txt4, txt5) => dispatch(loginUserDetails(txt1, txt2, txt3, txt4, txt5)),
  }
}

const { width } = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});


class ControlerProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: this.props.userImage,
      name: this.props.userName,
      profileDetails: '',

      isLoading: true,

    };
    //console.log(this.props.navigation);
  }

  componentDidMount() {
    this.navFocusListener = this.props.navigation.addListener('didFocus', () => {
      this._controllerGetProfileData();
    });
  }

  componentWillUnmount() {
    this.navFocusListener.remove();
  }

  _controllerGetProfileData = () => {

    let formData = new FormData();
    formData.append('id', this.props.userId);

    Api._controllerGetProfileData(formData)
      .then((response) => {

        console.log(response)
        this.setState({ isLoading: false })
        if (response.status.toString() == "1") {
          let firstName = '';
          let lastName = '';
          let imageProfile = '';

          if (response.data.first_name) {
            firstName = response.data.first_name;
          }
          if (response.data.last_name) {
            lastName = response.data.last_name;
          }
          if (response.data.profile_image) {
            imageProfile = response.data.profile_image;
          }


          this.props.userDetails(this.props.userId, firstName + " " + lastName, response.data.email, imageProfile, "controller");
          this.storeLogDetailsToAsyncStorage(this.props.userId, firstName + " " + lastName, response.data.email, imageProfile, "controller");
          this.setState({ profileDetails: response.data, name: firstName + " " + lastName, image: imageProfile });
        } else {
          setToastMsg(response.message.toString());
        }

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false, })
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>
            <ScrollView>

              <View style={styles.mainContainer}>



                <Image style={styles.headerBg} source={require('./../../assets/header_bg.png')} resizeMode="stretch" />
                <Loader isLoading={this.state.isLoading} />
                <TouchableOpacity
                  style={{ position: 'absolute', marginTop: 20, marginLeft: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                </TouchableOpacity>

                {this.props.userImage ?
                  <Image style={styles.profileImage} source={{ uri: AppConstants.IMAGE_URL + this.props.userImage }} resizeMode="stretch" />
                  :
                  <Image style={styles.profileImage} source={{ uri: this.props.imgUrl }} resizeMode="stretch" />
                }

                <View style={{ marginLeft: 50, marginRight: 50, marginTop: 60 }}>
                  <Text style={styles.heading}>{this.props.userName}</Text>

                  <View style={{ marginTop: 15 }}>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('RegistrationSecond', { userId: this.props.userId, fullProfileData: this.state.profileDetails })}
                    >
                      <CustomTextInput
                        placeholder='Edit Profile'
                        keyboardType='default'
                        //floatingText="Edit Profile"
                        editable={false}
                        isVisibleRightArrow={true}
                        value={'Edit Profile'}
                        returnKeyType={'done'}
                      />

                    </TouchableOpacity>

                  </View>
                  <View>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('ControllerSettings')}
                    >
                      <CustomTextInput
                        placeholder='Settings'
                        keyboardType='default'
                        //floatingText="Settings"
                        editable={false}
                        isVisibleRightArrow={true}
                        value={'Settings'}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({
                            email: text,
                          });
                        }}
                      />

                    </TouchableOpacity>

                  </View>

                  <View >
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('ForgotPassword', { forWhat: "forgotPassword" })}
                    >
                      <CustomTextInput
                        placeholder='Reset Password'
                        keyboardType='default'
                        //floatingText="Edit Profile"
                        editable={false}
                        isVisibleRightArrow={true}
                        value={'Reset Password'}
                        returnKeyType={'done'}
                      />

                    </TouchableOpacity>

                  </View>

                  <TouchableOpacity style={{ padding: 10, backgroundColor: "#657CF4", borderRadius: 20, marginTop: 50 }}
                    onPress={() => removeLogDetailsToAsyncStorage(this.props.navigation)}
                  >
                    <Text style={styles.loginText}>LOGOUT</Text>
                  </TouchableOpacity>

                </View>
              </View>
            </ScrollView>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  storeLogDetailsToAsyncStorage = async (id, name, email, image, role) => {
    try {
      await AsyncStorage.setItem('user_id', id);
      await AsyncStorage.setItem('user_name', name);
      await AsyncStorage.setItem('user_email', email);
      await AsyncStorage.setItem('user_image', image);
      await AsyncStorage.setItem('user_role', role);

    } catch (e) {
      console.log(e);
    }
  }


}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center',

  },

  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10,
    textAlign: 'center',

  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  profileImage: {
    height: 120,
    width: 120,
    position: 'absolute',
    borderRadius: 25,
    alignSelf: 'center',
    marginTop: 60

  },

});

export default connect(mapStateToPrpos, mapDispatchToProps)(ControlerProfile);