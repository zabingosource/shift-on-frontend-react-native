/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert,
  SafeAreaView, Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../../components/CustomTextInput';
import Loader from '../../../utils/Loader';
import Api from '../../../apis/Api';
import { setToastMsg } from '../../../utils/ToastMessage';
import { connect } from 'react-redux';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
  }
}

const { width } = Dimensions.get('window');


class WorkerCriminalRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      criminalDetails: '',
      criminalDetailEnable: false,
      clinicalDetails: '',
      clinicalDetailEnable: false,
      isCriminal: false,
      isClinical: false,
      isLoading: false,

      previousData: this.props.navigation.getParam('previousData', 'NO-item'),
      isUpdate: this.props.navigation.getParam('isUpdate', 'NO-item'),

      setErrors: {
        field: '',
        message: ''
      },

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    if (this.state.isUpdate == "1") {
      this.setState({
        criminalDetails: this.state.previousData.conviction_details,
        clinicalDetails: this.state.previousData.suspension_details,
      })
      if (this.state.previousData.is_conviction.toString() == "YES") {
        this.setState({ isCriminal: true, criminalDetailEnable: true })
      }
      if (this.state.previousData.is_suspension.toString() == "YES") {
        this.setState({ isClinical: true, clinicalDetailEnable: true })
      }
    }
  }

  checkValidation = () => {
    let criminalDetailError = { field: '', message: '' }
    let clinicalDetailError = { field: '', message: '' }

    if (this.state.isCriminal && (this.state.criminalDetails == null || this.state.criminalDetails == '')) {
      criminalDetailError.field = "criminal_details";
      criminalDetailError.message = "Criminal details are required!";
      this.setState({ setErrors: criminalDetailError })
    } else if (this.state.isClinical && (this.state.clinicalDetails == null || this.state.clinicalDetails == '')) {
      clinicalDetailError.field = "clinical_details";
      clinicalDetailError.message = "Clinical details are required!";
      this.setState({ setErrors: clinicalDetailError })
    } else {
      this.setState({ setErrors: { field: '', message: '' } })
      this.onSaveBtn();
    }

  }

  onSaveBtn = () => {
    this.setState({ isLoading: true, })

    let isCriminalRec = "NO";
    let isCriminalRecDetails = "";
    let isClinicalRec = "NO";
    let isClinicalRecDetails = "";
    if (this.state.isCriminal) {
      isCriminalRec = "YES";
      isCriminalRecDetails = this.state.criminalDetails.toString();
    }
    if (this.state.isClinical) {
      isClinicalRec = "YES";
      isClinicalRecDetails = this.state.clinicalDetails.toString();
    }

    let rawData = {
      "worker_id": this.props.userid,
      "user_id": this.props.userid,
      "is_conviction": isCriminalRec,
      "conviction_details": isCriminalRecDetails,
      "is_suspension": isClinicalRec,
      "suspension_details": isClinicalRecDetails,
    }

    Api._workerCriminalRecordsUpdate(rawData)
      .then((response) => {
        console.log(response)

        this.setState({ isLoading: false, })
        setToastMsg(response.message.toString());
        this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false, })
      });

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15, textTransform: 'uppercase' }}>Criminal Records</Text>

              </View>
              <Loader isLoading={this.state.isLoading} />
              <ScrollView>
                <View style={{ marginLeft: 50, marginRight: 50 }}>
                  <Text style={styles.heading_mail}>Criminal Records</Text>


                  <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 20, marginTop: 20 }}>

                    <View>
                      <Text
                        style={{
                          color: "#393FA0",
                          marginLeft: 15,
                          paddingRight: 5,
                          paddingLeft: 5,
                          fontSize: 13,
                          fontFamily: "montserrat_regular"
                        }}
                      > CRIMINAL CONVICTIONS!</Text>
                      <Text
                        style={{
                          color: "#000000",
                          marginLeft: 15,
                          backgroundColor: '#FFFFFF',
                          paddingRight: 5,
                          paddingLeft: 5,
                          fontSize: 15,
                          fontFamily: "montserrat_regular",
                          padding: 10

                        }}
                      > Do you have any criminal convictions in the UK or abroad?(wheather related to work or not)</Text>

                      <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity style={{ alignItems: 'center', marginLeft: 15, marginBottom: 10 }}
                          onPress={() => this.setState({ isCriminal: true, criminalDetailEnable: true, })}
                        >
                          <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                            {
                              (this.state.isCriminal) ?
                                (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_on_icn.png')} />)
                                :
                                (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_off_icn.png')} />)
                            }
                            <Text
                              style={{
                                color: "#000000",
                                fontSize: 15,
                                fontFamily: "montserrat_regular",
                                padding: 10
                              }}
                            >YES</Text>
                          </View>

                        </TouchableOpacity>
                        <TouchableOpacity style={{ alignItems: 'center', marginLeft: 15, marginBottom: 10 }}
                          onPress={() => this.setState({ isCriminal: false, criminalDetailEnable: false, criminalDetails: '' })}
                        >
                          <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                            {
                              (this.state.isCriminal) ?
                                (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_off_icn.png')} />)
                                :
                                (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_on_icn.png')} />)
                            }
                            <Text
                              style={{
                                color: "#000000",
                                fontSize: 15,
                                fontFamily: "montserrat_regular",
                                padding: 10
                              }}
                            >NO</Text>
                          </View>

                        </TouchableOpacity>


                      </View>

                    </View>

                  </View>
                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='IF YES PLEASE DETAIL BELOW'
                      multiline={true}
                      keyboardType='default'
                      floatingText="IF YES PLEASE DETAIL BELOW"
                      onFocus={true}
                      editable={this.state.criminalDetailEnable}
                      style={{ textAlignVertical: "top", height: 150, }}
                      value={this.state.criminalDetails}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          criminalDetails: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "criminal_details" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 20, marginTop: 15 }}>

                    <View>
                      <Text
                        style={{
                          color: "#393FA0",
                          marginLeft: 15,
                          paddingRight: 5,
                          paddingLeft: 5,
                          fontSize: 13,
                          fontFamily: "montserrat_regular"
                        }}
                      > CLINICAL INVESTIGATION OR SUSPENSION!</Text>
                      <Text
                        style={{
                          color: "#000000",
                          marginLeft: 15,
                          backgroundColor: '#FFFFFF',
                          paddingRight: 5,
                          paddingLeft: 5,
                          fontSize: 15,
                          fontFamily: "montserrat_regular",
                          padding: 10

                        }}
                      > Are you / have you been under / undergoing any clinical investigation or suspension?</Text>

                      <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity style={{ alignItems: 'center', marginLeft: 15, marginBottom: 10 }}
                          onPress={() => this.setState({ isClinical: true, clinicalDetailEnable: true })}
                        >
                          <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                            {
                              (this.state.isClinical) ?
                                (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_on_icn.png')} />)
                                :
                                (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_off_icn.png')} />)
                            }
                            <Text
                              style={{
                                color: "#000000",
                                fontSize: 15,
                                fontFamily: "montserrat_regular",
                                padding: 10
                              }}
                            >YES</Text>
                          </View>

                        </TouchableOpacity>
                        <TouchableOpacity style={{ alignItems: 'center', marginLeft: 15, marginBottom: 10 }}
                          onPress={() => this.setState({ isClinical: false, clinicalDetailEnable: false, clinicalDetails: '' })}
                        >
                          <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                            {
                              (this.state.isClinical) ?
                                (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_off_icn.png')} />)
                                :
                                (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_on_icn.png')} />)
                            }
                            <Text
                              style={{
                                color: "#000000",
                                fontSize: 15,
                                fontFamily: "montserrat_regular",
                                padding: 10
                              }}
                            >NO</Text>
                          </View>

                        </TouchableOpacity>


                      </View>

                    </View>

                  </View>



                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='IF YES PLEASE DETAIL BELOW'
                      multiline={true}
                      keyboardType='default'
                      style={{ textAlignVertical: "top", height: 150, }}
                      floatingText="IF YES PLEASE DETAIL BELOW"
                      onFocus={true}
                      editable={this.state.clinicalDetailEnable}
                      value={this.state.clinicalDetails}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          clinicalDetails: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "clinical_details" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                    onPress={() => this.checkValidation()}
                  >
                    <Text style={styles.loginText}>SAVE</Text>
                  </TouchableOpacity>
                </View>

              </ScrollView>
            </View>



          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#585858',
    textAlign: 'center',
    marginTop: 10
  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }

});

export default connect(mapStateToPrpos)(WorkerCriminalRecord);