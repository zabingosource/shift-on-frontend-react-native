/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert,
  SafeAreaView, Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../../components/CustomTextInput';
import Loader from '../../../utils/Loader';
import Api from '../../../apis/Api';
import { setToastMsg } from '../../../utils/ToastMessage';
import { connect } from 'react-redux';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../../components/CustomModelSelector';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
  }
}

const titleData = [
  {
    id: "1",
    label: 'Mr.',
  },
  {
    id: "2",
    label: 'Mrs.',
  },

]

const relationData = [
  {
    id: "1",
    label: 'Single',
  }, {
    id: "2",
    label: 'Married',
  }, {
    id: "3",
    label: 'Divorced',
  }, {
    id: "4",
    label: 'Separated',
  }, {
    id: "5",
    label: 'Widowed',
  },
]

const { width } = Dimensions.get('window');


class WorkerEmergencyContact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      surname: '',
      forename: '',
      title: '',
      relationship: '',
      contactnumber: '',
      workcontactnumber: '',
      address1: '',
      address2: '',
      postcode: '',
      isLoading: false,

      previousData: this.props.navigation.getParam('previousData', 'NO-item'),
      isUpdate: this.props.navigation.getParam('isUpdate', 'NO-item'),
      setErrors: {
        field: '',
        message: ''
      },

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {

    if (this.state.isUpdate == "1") {
      this.setState({
        surname: this.state.previousData.surname,
        forename: this.state.previousData.forename,
        title: this.state.previousData.title,
        relationship: this.state.previousData.relationship,
        contactnumber: this.state.previousData.phone,
        workcontactnumber: this.state.previousData.work_phone,
        address1: this.state.previousData.address1,
        address2: this.state.previousData.address2,
        postcode: this.state.previousData.post_code,
      })
    }
  }

  checkValidation = () => {
    let surnameError = { field: '', message: '' }
    let forenameError = { field: '', message: '' }
    let titleError = { field: '', message: '' }
    let relationalError = { field: '', message: '' }
    let numberError = { field: '', message: '' }
    let workNumberError = { field: '', message: '' }
    let address1Error = { field: '', message: '' }
    let address2Error = { field: '', message: '' }
    let postcodeError = { field: '', message: '' }

    if (this.state.surname == '') {
      surnameError.field = "surname";
      surnameError.message = "Surname is required!";
      this.setState({ setErrors: surnameError })
    } else if (this.state.forename == '') {
      forenameError.field = "forename";
      forenameError.message = "Forename is required!";
      this.setState({ setErrors: forenameError })
    } else if (this.state.title == '') {
      titleError.field = "title";
      titleError.message = "Title is required!";
      this.setState({ setErrors: titleError })
    } else if (this.state.relationship == '') {
      relationalError.field = "relationship";
      relationalError.message = "Relationship is required!";
      this.setState({ setErrors: relationalError })
    } else if (this.state.contactnumber == '') {
      numberError.field = "number";
      numberError.message = "Contact number is required!";
      this.setState({ setErrors: numberError })
    } else if (this.state.contactnumber.length < 10) {
      numberError.field = "number";
      numberError.message = "Contact number is invalid!";
      this.setState({ setErrors: numberError })
    } else if (this.state.workcontactnumber == '') {
      workNumberError.field = "worknumber";
      workNumberError.message = "Work contact number is required!";
      this.setState({ setErrors: workNumberError })
    } else if (this.state.workcontactnumber.length < 10) {
      workNumberError.field = "worknumber";
      workNumberError.message = "Work contact number is invalid!";
      this.setState({ setErrors: workNumberError })
    } else if (this.state.address1 == '') {
      address1Error.field = "address1";
      address1Error.message = "Address is required!";
      this.setState({ setErrors: address1Error })
    } else if (this.state.address2 == '') {
      address2Error.field = "address2";
      address2Error.message = "Address is required!";
      this.setState({ setErrors: address2Error })
    } else if (this.state.postcode == '') {
      postcodeError.field = "postcode";
      postcodeError.message = "Postcode is required!";
      this.setState({ setErrors: postcodeError })
    } else {
      this.setState({ setErrors: { field: '', message: '' } })
      this.onSaveBtn();
    }
  }

  onSaveBtn = () => {
    this.setState({ isLoading: true })

    let rawData = {
      "worker_id": this.props.userid,
      "user_id": this.props.userid,
      "surname": this.state.surname,
      "forename": this.state.forename,
      "title": this.state.title,
      "relationship": this.state.relationship,
      "phone": this.state.contactnumber,
      "work_phone": this.state.workcontactnumber,
      "address1": this.state.address1,
      "address2": this.state.address2,
      "post_code": this.state.postcode,
    }

    Api._workerKinDetailsUpdate(rawData)
      .then((response) => {
        console.log(response)

        this.setState({ isLoading: false })
        setToastMsg(response.message.toString());
        this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>

            <View style={styles.mainContainer}>

              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15, textTransform: 'uppercase' }}>Kin Details</Text>

              </View>
              <Loader isLoading={this.state.isLoading} />
              <ScrollView>
                <View style={{ marginLeft: 50, marginRight: 50 }}>
                  <Text style={styles.heading_mail}>Next of kin details.who you would {"\n"} want ABCD Corp.tocontact in the {"\n"} event of an emergency</Text>


                  <View style={{ marginTop: 20 }}>
                    <CustomTextInput
                      placeholder='SURNAME'
                      keyboardType='default'
                      floatingText="SURNAME"
                      onFocus={true}
                      value={this.state.surname}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          surname: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "surname" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='FORENAME'
                      keyboardType='default'
                      floatingText="FORENAME"
                      onFocus={true}
                      value={this.state.forename}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          forename: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "forename" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <ModalSelector
                      data={titleData}
                      initValue="TITLE"
                      accessible={true}
                      search={false}
                      animationType={"fade"}
                      keyExtractor={item => item.id}
                      labelExtractor={item => item.label}
                      onChange={(option) => { this.setState({ title: option.label }) }}>

                      <CustomTextInput
                        placeholder='TITLE'
                        keyboardType='default'
                        floatingText="TITLE"
                        onFocus={true}
                        value={this.state.title}
                        returnKeyType={'done'}
                        editable={true}
                        showSoftInputOnFocus={true}
                        caretHidden={false}
                        isVisibleDropDown={true}
                      />
                    </ModalSelector>
                  </View>
                  {
                    this.state.setErrors.field == "title" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <ModalSelector
                      data={relationData}
                      initValue="RELATIONSHIP"
                      accessible={true}
                      search={false}
                      animationType={"fade"}
                      keyExtractor={item => item.id}
                      labelExtractor={item => item.label}
                      onChange={(option) => { this.setState({ relationship: option.label }) }}>

                      <CustomTextInput
                        placeholder='RELATIONSHIP'
                        keyboardType='default'
                        floatingText="RELATIONSHIP"
                        value={this.state.relationship}
                        returnKeyType={'done'}
                        editable={true}
                        onFocus={true}
                        showSoftInputOnFocus={true}
                        caretHidden={false}
                        isVisibleDropDown={true}
                      />
                    </ModalSelector>
                  </View>
                  {
                    this.state.setErrors.field == "relationship" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='CONTACT NUMBER'
                      keyboardType='numeric'
                      floatingText="CONTACT NUMBER"
                      onFocus={true}
                      value={this.state.contactnumber}
                      maxLength={10}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          contactnumber: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "number" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='WORK CONTACT NUMBER'
                      keyboardType='numeric'
                      floatingText="WORK CONTACT NUMBER"
                      onFocus={true}
                      value={this.state.workcontactnumber}
                      maxLength={10}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          workcontactnumber: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "worknumber" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='ADDRESS-1'
                      keyboardType='default'
                      floatingText="ADDRESS-1"
                      onFocus={true}
                      value={this.state.address1}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          address1: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "address1" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='ADDRESS-2'
                      keyboardType='default'
                      floatingText="ADDRESS-2"
                      onFocus={true}
                      value={this.state.address2}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          address2: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "address2" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <TouchableOpacity>
                      <CustomTextInput
                        placeholder='POST CODE'
                        keyboardType='default'
                        floatingText="POST CODE"
                        onFocus={true}
                        value={this.state.postcode}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({
                            postcode: text,
                          });
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                  {
                    this.state.setErrors.field == "postcode" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                    onPress={() => this.checkValidation()}
                  >
                    <Text style={styles.loginText}>SAVE</Text>
                  </TouchableOpacity>
                </View>

              </ScrollView>
            </View>



          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _setDate = (type) => {
    if (type == 'datestarted') {
      this.setState({ datestarted: "15/09/2020" });
    } else if (type == 'finish_date') {
      this.setState({ finish_date: "20/09/2020" });
    }
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#585858',
    textAlign: 'center',
    marginTop: 10
  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }

});

export default connect(mapStateToPrpos)(WorkerEmergencyContact);