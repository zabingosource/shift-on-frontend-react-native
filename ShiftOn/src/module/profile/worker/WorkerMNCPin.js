/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView,
  Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../../components/CustomTextInput';
import Loader from '../../../utils/Loader';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Api from '../../../apis/Api';
import { setToastMsg } from '../../../utils/ToastMessage';
import moment from 'moment';
import { connect } from 'react-redux';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
  }
}

const { width } = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});

class WorkerMNCPin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resistration_number: '',
      expiry_date: '',
      skill_experience: '',
      isLoading: false,
      isDatePickerVisible: false,
      previousData: this.props.navigation.getParam('previousData', 'NO-item'),
      isUpdate: this.props.navigation.getParam('isUpdate', 'NO-item'),

      setErrors: {
        field: '',
        message: ''
      },

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {

    if (this.state.isUpdate == "1") {
      this.setState({
        resistration_number: this.state.previousData.nmc_number,
        expiry_date: this.state.previousData.expiry_date,
        skill_experience: this.state.previousData.skills,
      })
    }

  }

  checkValidation = () => {
    let resistrationError = { field: '', message: '' }
    let dateError = { field: '', message: '' }
    let skillError = { field: '', message: '' }

    if (this.state.resistration_number == '') {
      resistrationError.field = "resistration";
      resistrationError.message = "NMC resistration number is required!";
      this.setState({ setErrors: resistrationError })
    } else if (this.state.expiry_date == '') {
      dateError.field = "date";
      dateError.message = "Expiry date is required!";
      this.setState({ setErrors: dateError })
    } else if (this.state.skill_experience == '') {
      skillError.field = "skills";
      skillError.message = "Skill & experience are required!";
      this.setState({ setErrors: skillError })
    } else {
      this.setState({ setErrors: { field: '', message: '' } })
      this.onSaveBtn();
    }
  }

  onSaveBtn = () => {
    this.setState({ isLoading: true })

    let rawData = {
      "worker_id": this.props.userid,
      "user_id": this.props.userid,
      "nmc_number": this.state.resistration_number,
      "expiry_date": this.state.expiry_date,
      "skills": this.state.skill_experience,
    }

    Api._workerMncAndSkillUpdate(rawData)
      .then((response) => {
        console.log(response)

        this.setState({ isLoading: false, })
        setToastMsg(response.message.toString());
        this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false, })
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15, textTransform: 'uppercase' }}> NMC & Skill</Text>

              </View>
              <Loader isLoading={this.state.isLoading} />
              <ScrollView>
                <View >
                  <Text style={styles.heading_mail}>NMC Pin (if applicable) and {"\n"} your Skill & Experiance</Text>

                  <View style={{ marginLeft: 50, marginRight: 50 }}>
                    <View style={{ marginTop: 20 }}>
                      <CustomTextInput
                        placeholder='NMC REGISTRATION NUMBER'
                        keyboardType='default'
                        floatingText="NMC REGISTRATION NUMBER"
                        value={this.state.resistration_number}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({ resistration_number: text, });
                        }}
                      />
                    </View>
                    {
                      this.state.setErrors.field == "resistration" && (
                        <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                      )
                    }


                    <View style={{ marginTop: 5, }}>
                      <CustomTextInput
                        placeholder='EXPIRY DATE'
                        keyboardType='default'
                        onFocus={true}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        isVisibleCalendar={true}
                        floatingText="EXPIRY DATE"
                        value={this.state.expiry_date}
                        returnKeyType={'done'}
                        onTouchEnd={() => this.setState({
                          isDatePickerVisible: true
                        })}
                      />
                      <DateTimePickerModal
                        isVisible={this.state.isDatePickerVisible}
                        mode="date"
                        locale="en_GB"
                        date={new Date()}
                        onConfirm={(date) => this.handleFirstDateConfirm(date)}
                        onCancel={() => this.setState({
                          isDatePickerVisible: false
                        })}
                      />
                    </View>
                    {
                      this.state.setErrors.field == "date" && (
                        <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                      )
                    }

                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='YOUR SKILL & EXPERIENCE'
                        keyboardType='default'
                        style={{ textAlignVertical: "top", height: 150, }}
                        floatingText="YOUR SKILL & EXPERIENCE"
                        value={this.state.skill_experience}
                        returnKeyType={'done'}
                        onFocus={true}
                        onChangeText={(text) => {
                          this.setState({
                            skill_experience: text,
                          });
                        }}
                      />
                    </View>
                    {
                      this.state.setErrors.field == "skills" && (
                        <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                      )
                    }

                  </View>

                  <Text style={{
                    fontSize: 12,
                    fontFamily: 'montserrat_light',
                    color: '#000000',
                    marginLeft: 20,
                    marginRight: 20,
                    marginTop: 20,
                    borderRadius: 10,
                    padding: 20,
                    backgroundColor: '#CAE8A2'
                  }}>Note:In support of your application, please detail your relevent skills,
                    experiences and personal qualities which you believe are relevant to the position you're
                    applying for.</Text>


                  <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                    onPress={() => this.checkValidation()}
                  >
                    <Text style={styles.loginText}>SAVE</Text>
                  </TouchableOpacity>
                </View>

              </ScrollView>
            </View>



          </View>
        </SafeAreaView>
      </Fragment>
    );
  }


  handleFirstDateConfirm = (date) => {

    let dateTimeString = moment(date).local().format('DD/MM/YYYY');
    this.setState({
      isDatePickerVisible: false,
      expiry_date: dateTimeString
    })

  };

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#585858',
    textAlign: 'center',
    marginTop: 10
  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }

});

export default connect(mapStateToPrpos)(WorkerMNCPin);