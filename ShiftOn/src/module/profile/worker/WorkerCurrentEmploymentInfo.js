/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert,
  SafeAreaView, Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../../components/CustomTextInput';
import Loader from '../../../utils/Loader';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Api from '../../../apis/Api';
import { setToastMsg } from '../../../utils/ToastMessage';
import AppConstants from '../../../apis/AppConstants';
import moment from 'moment';
import { connect } from 'react-redux';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
  }
}

const { width } = Dimensions.get('window');


class WorkerCurrentEmploymentInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      employername: '',
      address1: '',
      address2: '',
      mobile: '',
      managername: '',
      datestarted: '',
      position: '',
      leavereason: '',
      isLoading: false,
      isDatePickerVisible: false,

      setErrors: {
        field: '',
        message: ''
      },

      previousData: this.props.navigation.getParam('previousData', 'NO-item'),
      isUpdate: this.props.navigation.getParam('isUpdate', 'NO-item'),

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    if (this.state.isUpdate == "1") {
      this.setState({
        employername: this.state.previousData.employer_name,
        address1: this.state.previousData.address1,
        address2: this.state.previousData.address2,
        mobile: this.state.previousData.contact_number,
        managername: this.state.previousData.manager_name,
        datestarted: this.state.previousData.date_started,
        position: this.state.previousData.position_held,
        leavereason: this.state.previousData.reason_for_leaving,
      })
    }
  }

  checkValidation = () => {
    let employerNameError = { field: '', message: '' }
    let address1Error = { field: '', message: '' }
    let address2Error = { field: '', message: '' }
    let mobileError = { field: '', message: '' }
    let managerNameError = { field: '', message: '' }
    let dateStartedError = { field: '', message: '' }
    let positionError = { field: '', message: '' }
    let leaveReasonError = { field: '', message: '' }

    if (this.state.employername == '') {
      employerNameError.field = "employername";
      employerNameError.message = "Employer name is required!";
      this.setState({ setErrors: employerNameError })
    } else if (this.state.address1 == '') {
      address1Error.field = "address1";
      address1Error.message = "Address is required!";
      this.setState({ setErrors: address1Error })
    } else if (this.state.address2 == '') {
      address2Error.field = "address2";
      address2Error.message = "Address is required!";
      this.setState({ setErrors: address2Error })
    } else if (this.state.mobile == '') {
      mobileError.field = "mobile";
      mobileError.message = "Contact number is required!";
      this.setState({ setErrors: mobileError })
    } else if (this.state.mobile.length < 10) {
      mobileError.field = "mobile";
      mobileError.message = "Contact number is invalid!";
      this.setState({ setErrors: mobileError })
    } else if (this.state.managername == '') {
      managerNameError.field = "managername";
      managerNameError.message = "Manager name is required!";
      this.setState({ setErrors: managerNameError })
    } else if (this.state.datestarted == '') {
      dateStartedError.field = "datestarted";
      dateStartedError.message = "Started date is required!";
      this.setState({ setErrors: dateStartedError })
    } else if (this.state.position == '') {
      positionError.field = "position";
      positionError.message = "Position is required!";
      this.setState({ setErrors: positionError })
    } else if (this.state.leavereason == '') {
      leaveReasonError.field = "leavereason";
      leaveReasonError.message = "Reason for leaving is required!";
      this.setState({ setErrors: leaveReasonError })
    } else {
      this.setState({ setErrors: { field: '', message: '' } })
      this.onSaveBtn();
    }

  }

  onSaveBtn = () => {
    this.setState({ isLoading: true })

    let rawData = {
      "worker_id": this.props.userid,
      "user_id": this.props.userid,
      "employer_name": this.state.employername,
      "address1": this.state.address1,
      "address2": this.state.address2,
      "contact_number": this.state.mobile,
      "manager_name": this.state.managername,
      "date_started": this.state.datestarted,
      "position_held": this.state.position,
      "reason_for_leaving": this.state.leavereason,
    }

    Api._workerCurrentEmployerUpdate(rawData)
      .then((response) => {
        console.log(response.data)

        this.setState({ isLoading: false })
        setToastMsg(response.message.toString());
        this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15, textTransform: 'uppercase' }}>Current Employer</Text>

              </View>
              <Loader isLoading={this.state.isLoading} />
              <ScrollView>
                <View style={{ marginLeft: 50, marginRight: 50 }}>
                  <Text style={styles.heading_mail}>Current Employment {"\n"} information</Text>


                  <View style={{ marginTop: 20 }}>
                    <CustomTextInput
                      placeholder='NAME OF EMPLOYER'
                      keyboardType='default'
                      floatingText="NAME OF EMPLOYER"
                      onFocus={true}
                      value={this.state.employername}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          employername: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "employername" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='ADDRESS-1'
                      keyboardType='default'
                      floatingText="ADDRESS-1"
                      onFocus={true}
                      value={this.state.address1}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          address1: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "address1" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='ADDRESS-2'
                      keyboardType='default'
                      floatingText="ADDRESS-2"
                      onFocus={true}
                      value={this.state.address2}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          address2: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "address2" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='CONTACT NUMBER'
                      keyboardType='numeric'
                      floatingText="CONTACT NUMBER"
                      onFocus={true}
                      maxLength={10}
                      value={this.state.mobile}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          mobile: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "mobile" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <TouchableOpacity>
                      <CustomTextInput
                        placeholder='MANAGER NAME'
                        keyboardType='default'
                        floatingText="MANAGER NAME"
                        onFocus={true}
                        value={this.state.managername}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({
                            managername: text,
                          });
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                  {
                    this.state.setErrors.field == "managername" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5, }}>
                    <CustomTextInput
                      placeholder='DATE STARTED'
                      keyboardType='default'
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleCalendar={true}
                      floatingText="DATE STARTED"
                      value={this.state.datestarted}
                      returnKeyType={'done'}
                      onTouchEnd={() => this.setState({ isDatePickerVisible: true })}
                    />

                    <DateTimePickerModal
                      isVisible={this.state.isDatePickerVisible}
                      mode="date"
                      locale="en_GB"
                      date={new Date()}
                      onConfirm={(date) => this.handleFirstDateConfirm(date)}
                      onCancel={() => this.setState({ isDatePickerVisible: false })}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "datestarted" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }


                  <View style={{ marginTop: 5 }}>
                    <TouchableOpacity>
                      <CustomTextInput
                        placeholder='POSITION HELD'
                        keyboardType='default'
                        floatingText="POSITION HELD"
                        onFocus={true}
                        value={this.state.position}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({
                            position: text,
                          });
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                  {
                    this.state.setErrors.field == "position" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='REASON FOR LEAVING'
                      keyboardType='default'
                      style={{ textAlignVertical: "top", height: 150, }}
                      floatingText="REASON FOR LEAVING"
                      onFocus={true}
                      value={this.state.leavereason}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          leavereason: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "leavereason" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                    onPress={() => this.checkValidation()}
                  >
                    <Text style={styles.loginText}>SAVE</Text>
                  </TouchableOpacity>
                </View>

              </ScrollView>
            </View>



          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  handleFirstDateConfirm = (date) => {

    let dateTimeString = moment(date).local().format('DD/MM/YYYY');
    this.setState({
      isDatePickerVisible: false,
      datestarted: dateTimeString
    })

  };

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75
  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10
  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#585858',
    textAlign: 'center',
    marginTop: 10
  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7
  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20
  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'
  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'
  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'
  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }
  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }

});

export default connect(mapStateToPrpos)(WorkerCurrentEmploymentInfo);