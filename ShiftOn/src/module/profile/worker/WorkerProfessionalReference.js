/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert,
  SafeAreaView, Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../../components/CustomTextInput';
import Loader from '../../../utils/Loader';
import Api from '../../../apis/Api';
import { setToastMsg } from '../../../utils/ToastMessage';
import { connect } from 'react-redux';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
  }
}

const { width } = Dimensions.get('window');


class WorkerProfessionalReference extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name1: '',
      name2: '',
      name3: '',

      job_title1: '',
      job_title2: '',
      job_title3: '',

      know_this_person1: '',
      know_this_person2: '',
      know_this_person3: '',

      company1: '',
      company2: '',
      company3: '',

      address1: '',
      address2: '',
      address3: '',

      mobile1: '',
      mobile2: '',
      mobile3: '',

      email1: '',
      email2: '',
      email3: '',

      selectedTab: '1',
      isLoading: false,

      previousData: this.props.navigation.getParam('previousData', 'NO-item'),
      isUpdate: this.props.navigation.getParam('isUpdate', 'NO-item'),

      setErrors: {
        field: '',
        message: ''
      },

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    if (this.state.isUpdate == "1") {
      for (let i = 0; i < this.state.previousData.length; i++) {
        if (this.state.previousData[i].field1 == "1") {
          this.setState({
            name1: this.state.previousData[i].name,
            job_title1: this.state.previousData[i].job_title,
            know_this_person1: this.state.previousData[i].how_know,
            company1: this.state.previousData[i].company,
            address1: this.state.previousData[i].address,
            mobile1: this.state.previousData[i].contact_number,
            email1: this.state.previousData[i].email,
          })
        } else if (this.state.previousData[i].field1 == "2") {
          this.setState({
            name2: this.state.previousData[i].name,
            job_title2: this.state.previousData[i].job_title,
            know_this_person2: this.state.previousData[i].how_know,
            company2: this.state.previousData[i].company,
            address2: this.state.previousData[i].address,
            mobile2: this.state.previousData[i].contact_number,
            email2: this.state.previousData[i].email,
          })
        } else {
          this.setState({
            name3: this.state.previousData[i].name,
            job_title3: this.state.previousData[i].job_title,
            know_this_person3: this.state.previousData[i].how_know,
            company3: this.state.previousData[i].company,
            address3: this.state.previousData[i].address,
            mobile3: this.state.previousData[i].contact_number,
            email3: this.state.previousData[i].email,
          })
        }
      }

    }
  }

  checkValidation = () => {
    const emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    let nameError = { field: '', message: '' }
    let jobTitleError = { field: '', message: '' }
    let howKnowError = { field: '', message: '' }
    let companyError = { field: '', message: '' }
    let addressError = { field: '', message: '' }
    let mobileError = { field: '', message: '' }
    let emailError = { field: '', message: '' }

    if (this.state.selectedTab == "1") {
      if (this.state.name1 == '') {
        nameError.field = "name1";
        nameError.message = "Name is required!";
        this.setState({ setErrors: nameError })
      } else if (this.state.job_title1 == '') {
        jobTitleError.field = "job_title1";
        jobTitleError.message = "Job title is required!";
        this.setState({ setErrors: jobTitleError })
      } else if (this.state.know_this_person1 == '') {
        howKnowError.field = "how_know1";
        howKnowError.message = "How to know field is required!";
        this.setState({ setErrors: howKnowError })
      } else if (this.state.company1 == '') {
        companyError.field = "company1";
        companyError.message = "Company is required!";
        this.setState({ setErrors: companyError })
      } else if (this.state.address1 == '') {
        addressError.field = "address1";
        addressError.message = "Address is required!";
        this.setState({ setErrors: addressError })
      } else if (this.state.mobile1 == '') {
        mobileError.field = "mobile1";
        mobileError.message = "Contact number is required!";
        this.setState({ setErrors: mobileError })
      } else if (this.state.mobile1.length < 10) {
        mobileError.field = "mobile1";
        mobileError.message = "Contact number is invalid!";
        this.setState({ setErrors: mobileError })
      } else if (this.state.email1 == '') {
        emailError.field = "email1";
        emailError.message = "Email is required!";
        this.setState({ setErrors: emailError })
      } else if (!emailRegEx.test(this.state.email1)) {
        emailError.field = "email1";
        emailError.message = "Invalid email!";
        this.setState({ setErrors: emailError })
      } else {
        this.setState({ setErrors: { field: '', message: '' } })
        this.onSaveBtn();
      }
    } else if (this.state.selectedTab == "2") {
      if (this.state.name2 == '') {
        nameError.field = "name2";
        nameError.message = "Name is required!";
        this.setState({ setErrors: nameError })
      } else if (this.state.job_title2 == '') {
        jobTitleError.field = "job_title2";
        jobTitleError.message = "Job title is required!";
        this.setState({ setErrors: jobTitleError })
      } else if (this.state.know_this_person2 == '') {
        howKnowError.field = "how_know2";
        howKnowError.message = "How to know field is required!";
        this.setState({ setErrors: howKnowError })
      } else if (this.state.company2 == '') {
        companyError.field = "company2";
        companyError.message = "Company is required!";
        this.setState({ setErrors: companyError })
      } else if (this.state.address2 == '') {
        addressError.field = "address2";
        addressError.message = "Address is required!";
        this.setState({ setErrors: addressError })
      } else if (this.state.mobile2 == '') {
        mobileError.field = "mobile2";
        mobileError.message = "Contact number is required!";
        this.setState({ setErrors: mobileError })
      } else if (this.state.mobile2.length < 10) {
        mobileError.field = "mobile2";
        mobileError.message = "Contact number is invalid!";
        this.setState({ setErrors: mobileError })
      } else if (this.state.email2 == '') {
        emailError.field = "email2"
        emailError.message = "Email is required!";
        this.setState({ setErrors: emailError })
      } else if (!emailRegEx.test(this.state.email2)) {
        emailError.field = "email2";
        emailError.message = "Invalid email!";
        this.setState({ setErrors: emailError })
      } else {
        this.setState({ setErrors: { field: '', message: '' } })
        this.onSaveBtn();
      }
    } else {
      if (this.state.name3 == '') {
        nameError.field = "name3";
        nameError.message = "Name is required!";
        this.setState({ setErrors: nameError })
      } else if (this.state.job_title3 == '') {
        jobTitleError.field = "job_title3";
        jobTitleError.message = "Job title is required!";
        this.setState({ setErrors: jobTitleError })
      } else if (this.state.know_this_person3 == '') {
        howKnowError.field = "how_know3";
        howKnowError.message = "How to know field is required!";
        this.setState({ setErrors: howKnowError })
      } else if (this.state.company3 == '') {
        companyError.field = "company3";
        companyError.message = "Company is required!";
        this.setState({ setErrors: companyError })
      } else if (this.state.address3 == '') {
        addressError.field = "address3";
        addressError.message = "Address is required!";
        this.setState({ setErrors: addressError })
      } else if (this.state.mobile3 == '') {
        mobileError.field = "mobile3";
        mobileError.message = "Contact number is required!";
        this.setState({ setErrors: mobileError })
      } else if (this.state.mobile3.length < 10) {
        mobileError.field = "mobile3";
        mobileError.message = "Contact number is invalid!";
        this.setState({ setErrors: mobileError })
      } else if (this.state.email3 == '') {
        emailError.field = "email3";
        emailError.message = "Email is required!";
        this.setState({ setErrors: emailError })
      } else if (!emailRegEx.test(this.state.email3)) {
        emailError.field = "email3";
        emailError.message = "Invalid email!";
        this.setState({ setErrors: emailError })
      } else {
        this.setState({ setErrors: { field: '', message: '' } })
        this.onSaveBtn();
      }
    }


  }

  onSaveBtn = () => {
    this.setState({ isLoading: true });

    let rawData = ''

    if (this.state.selectedTab == "1") {
      rawData = {
        "worker_id": this.props.userid,
        "user_id": this.props.userid,
        "tab": 1,
        "name": this.state.name1,
        "job_title": this.state.job_title1,
        "how_know": this.state.know_this_person1,
        "company": this.state.company1,
        "address": this.state.address1,
        "contact_number": this.state.mobile1,
        "email": this.state.email1,
      }
    } else if (this.state.selectedTab == "2") {
      rawData = {
        "worker_id": this.props.userid,
        "user_id": this.props.userid,
        "tab": 2,
        "name": this.state.name2,
        "job_title": this.state.job_title2,
        "how_know": this.state.know_this_person2,
        "company": this.state.company2,
        "address": this.state.address2,
        "contact_number": this.state.mobile2,
        "email": this.state.email2,
      }
    } else {
      rawData = {
        "worker_id": this.props.userid,
        "user_id": this.props.userid,
        "tab": 3,
        "name": this.state.name3,
        "job_title": this.state.job_title3,
        "how_know": this.state.know_this_person3,
        "company": this.state.company3,
        "address": this.state.address3,
        "contact_number": this.state.mobile3,
        "email": this.state.email3,
      }
    }

    console.log(rawData);

    Api._workerReferencesUpdate(rawData)
      .then((response) => {
        console.log(response)

        this.setState({ isLoading: false })
        setToastMsg(response.message.toString());
        this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>


              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15, textTransform: 'uppercase' }}>References</Text>

              </View>

              <Loader isLoading={this.state.isLoading} />

              <ScrollView>
                <View>
                  <Text style={styles.heading_mail}>Professional References</Text>


                  <View style={{ flexDirection: 'row', marginTop: 10, backgroundColor: '#000000', borderRadius: 40, marginLeft: 10, marginRight: 10 }}>
                    <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "1" ? "#00D5E1" : "#000000" }]}
                      activeOpacity={1}
                      onPress={() => this._onChangeTab("1")}
                    >
                      <Text style={{ alignSelf: 'center', fontSize: 12, padding: 7, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "1" ? "#000000" : "#FFFFFF" }}>REFERENCE-1</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "2" ? "#00D5E1" : "#000000" }]}
                      activeOpacity={1}
                      onPress={() => this._onChangeTab("2")}
                    >
                      <Text style={{ alignSelf: 'center', fontSize: 12, padding: 7, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "2" ? "#000000" : "#FFFFFF" }}>REFERENCE-2</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "3" ? "#00D5E1" : "#000000" }]}
                      activeOpacity={1}
                      onPress={() => this._onChangeTab("3")}
                    >
                      <Text style={{ alignSelf: 'center', fontSize: 12, padding: 7, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "3" ? "#000000" : "#FFFFFF" }}>REFERENCE-3</Text>
                    </TouchableOpacity>

                  </View>

                  {
                    this._referencePage()
                  }

                  <Text style={{
                    fontSize: 12,
                    fontFamily: 'montserrat_light',
                    color: '#000000',
                    marginLeft: 20,
                    marginRight: 20,
                    marginTop: 20,
                    borderRadius: 10,
                    padding: 20,
                    backgroundColor: '#CAE8A2'
                  }}>Note:Please provide full names and address of <Text style={{ fontWeight: 'bold', }}>three professional employment referees.</Text>
                    your <Text style={{ fontWeight: 'bold', }}>first reference</Text> must be from your
                    <Text style={{ fontWeight: 'bold', }}>current or last place of work </Text>
                    and addressed to your line manager.ABCD Corp.Cannot use friends or relatives for any
                    reference,employment reference can not be sent to private or personal home addresses.
                    You must provide workplace addresses and the referee must be a higher grade of staff than you yourself
                    i.e your line
                  </Text>
                  <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                    onPress={() => this.checkValidation()}
                  >
                    <Text style={styles.loginText}>SAVE</Text>
                  </TouchableOpacity>

                </View>

              </ScrollView>
            </View>



          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _onChangeTab(tab) {
    this.setState({ selectedTab: tab });
  }

  _referencePage = () => {

    if (this.state.selectedTab == "1") {

      return (
        <View style={{ marginLeft: 50, marginRight: 50 }}>

          <View style={{ marginTop: 20 }}>
            <CustomTextInput
              placeholder='NAME'
              keyboardType='default'
              floatingText="NAME"
              onFocus={true}
              value={this.state.name1}
              returnKeyType={'done'}
              onChangeText={(text) => {
                this.setState({ name1: text });
              }}
            />
          </View>
          {
            this.state.setErrors.field == "name1" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <CustomTextInput
              placeholder='JOB TITLE'
              keyboardType='default'
              floatingText="JOB TITLE"
              onFocus={true}
              value={this.state.job_title1}
              returnKeyType={'done'}
              onChangeText={(text) => {
                this.setState({ job_title1: text });
              }}
            />
          </View>
          {
            this.state.setErrors.field == "job_title1" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <CustomTextInput
              placeholder='HOW DO YOU KNOW THIS PERSON?'
              keyboardType='default'
              multiline={true}
              style={{ textAlignVertical: "top", height: 150, }}
              floatingText="HOW DO YOU KNOW THIS PERSON?"
              onFocus={true}
              value={this.state.know_this_person1}
              returnKeyType={'done'}
              onChangeText={(text) => {
                this.setState({ know_this_person1: text });
              }}
            />
          </View>
          {
            this.state.setErrors.field == "how_know1" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <CustomTextInput
              placeholder='COMPANY'
              keyboardType='default'
              floatingText="COMPANY"
              onFocus={true}
              value={this.state.company1}
              returnKeyType={'done'}
              onChangeText={(text) => {
                this.setState({ company1: text });
              }}
            />
          </View>
          {
            this.state.setErrors.field == "company1" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <TouchableOpacity>
              <CustomTextInput
                placeholder='ADDRESS'
                keyboardType='default'
                floatingText="ADDRESS"
                onFocus={true}
                value={this.state.address1}
                returnKeyType={'done'}
                onChangeText={(text) => {
                  this.setState({ address1: text });
                }}
              />
            </TouchableOpacity>
          </View>
          {
            this.state.setErrors.field == "address1" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <TouchableOpacity>
              <CustomTextInput
                placeholder='CONTACT NUMBER'
                keyboardType='numeric'
                floatingText="CONTACT NUMBER"
                onFocus={true}
                maxLength={10}
                value={this.state.mobile1}
                returnKeyType={'done'}
                onChangeText={(text) => {
                  this.setState({ mobile1: text });
                }}
              />
            </TouchableOpacity>
          </View>
          {
            this.state.setErrors.field == "mobile1" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <CustomTextInput
              placeholder='EMAIL'
              keyboardType='email-address'
              floatingText="EMAIL"
              onFocus={true}
              value={this.state.email1}
              returnKeyType={'done'}
              onChangeText={(text) => {
                this.setState({ email1: text });
              }}
            />
          </View>
          {
            this.state.setErrors.field == "email1" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

        </View>
      )

    } else if (this.state.selectedTab == "2") {

      return (
        <View style={{ marginLeft: 50, marginRight: 50 }}>

          <View style={{ marginTop: 20 }}>
            <CustomTextInput
              placeholder='NAME'
              keyboardType='default'
              floatingText="NAME"
              onFocus={true}
              value={this.state.name2}
              returnKeyType={'done'}
              onChangeText={(text) => {
                this.setState({ name2: text });
              }}
            />
          </View>
          {
            this.state.setErrors.field == "name2" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <CustomTextInput
              placeholder='JOB TITLE'
              keyboardType='default'
              floatingText="JOB TITLE"
              onFocus={true}
              value={this.state.job_title2}
              returnKeyType={'done'}
              onChangeText={(text) => {
                this.setState({ job_title2: text });
              }}
            />
          </View>
          {
            this.state.setErrors.field == "job_title2" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <CustomTextInput
              placeholder='HOW DO YOU KNOW THIS PERSON?'
              keyboardType='default'
              multiline={true}
              style={{ textAlignVertical: "top", height: 150, }}
              floatingText="HOW DO YOU KNOW THIS PERSON?"
              onFocus={true}
              value={this.state.know_this_person2}
              returnKeyType={'done'}
              onChangeText={(text) => {
                this.setState({ know_this_person2: text });
              }}
            />
          </View>
          {
            this.state.setErrors.field == "how_know2" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <CustomTextInput
              placeholder='COMPANY'
              keyboardType='default'
              floatingText="COMPANY"
              onFocus={true}
              value={this.state.company2}
              returnKeyType={'done'}
              onChangeText={(text) => {
                this.setState({ company2: text });
              }}
            />
          </View>
          {
            this.state.setErrors.field == "company2" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <TouchableOpacity>
              <CustomTextInput
                placeholder='ADDRESS'
                keyboardType='default'
                floatingText="ADDRESS"
                onFocus={true}
                value={this.state.address2}
                returnKeyType={'done'}
                onChangeText={(text) => {
                  this.setState({ address2: text });
                }}
              />
            </TouchableOpacity>
          </View>
          {
            this.state.setErrors.field == "address2" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <TouchableOpacity>
              <CustomTextInput
                placeholder='CONTACT NUMBER'
                keyboardType='numeric'
                floatingText="CONTACT NUMBER"
                onFocus={true}
                value={this.state.mobile2}
                maxLength={10}
                returnKeyType={'done'}
                onChangeText={(text) => {
                  this.setState({ mobile2: text });
                }}
              />
            </TouchableOpacity>
          </View>
          {
            this.state.setErrors.field == "mobile2" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <CustomTextInput
              placeholder='EMAIL'
              keyboardType='email-address'
              floatingText="EMAIL"
              onFocus={true}
              value={this.state.email2}
              returnKeyType={'done'}
              onChangeText={(text) => {
                this.setState({ email2: text });
              }}
            />
          </View>
          {
            this.state.setErrors.field == "email2" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

        </View>
      )

    } else {

      return (
        <View style={{ marginLeft: 50, marginRight: 50 }}>

          <View style={{ marginTop: 20 }}>
            <CustomTextInput
              placeholder='NAME'
              keyboardType='default'
              floatingText="NAME"
              onFocus={true}
              value={this.state.name3}
              returnKeyType={'done'}
              onChangeText={(text) => {
                this.setState({ name3: text });
              }}
            />
          </View>
          {
            this.state.setErrors.field == "name3" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <CustomTextInput
              placeholder='JOB TITLE'
              keyboardType='default'
              floatingText="JOB TITLE"
              onFocus={true}
              value={this.state.job_title3}
              returnKeyType={'done'}
              onChangeText={(text) => {
                this.setState({ job_title3: text });
              }}
            />
          </View>
          {
            this.state.setErrors.field == "job_title3" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <CustomTextInput
              placeholder='HOW DO YOU KNOW THIS PERSON?'
              keyboardType='default'
              multiline={true}
              style={{ textAlignVertical: "top", height: 150, }}
              floatingText="HOW DO YOU KNOW THIS PERSON?"
              onFocus={true}
              value={this.state.know_this_person3}
              returnKeyType={'done'}
              onChangeText={(text) => {
                this.setState({ know_this_person3: text });
              }}
            />
          </View>
          {
            this.state.setErrors.field == "how_know3" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <CustomTextInput
              placeholder='COMPANY'
              keyboardType='default'
              floatingText="COMPANY"
              onFocus={true}
              value={this.state.company3}
              returnKeyType={'done'}
              onChangeText={(text) => {
                this.setState({ company3: text });
              }}
            />
          </View>
          {
            this.state.setErrors.field == "company3" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <TouchableOpacity>
              <CustomTextInput
                placeholder='ADDRESS'
                keyboardType='default'
                floatingText="ADDRESS"
                onFocus={true}
                value={this.state.address3}
                returnKeyType={'done'}
                onChangeText={(text) => {
                  this.setState({ address3: text });
                }}
              />
            </TouchableOpacity>
          </View>
          {
            this.state.setErrors.field == "address3" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <TouchableOpacity>
              <CustomTextInput
                placeholder='CONTACT NUMBER'
                keyboardType='numeric'
                floatingText="CONTACT NUMBER"
                onFocus={true}
                value={this.state.mobile3}
                maxLength={10}
                returnKeyType={'done'}
                onChangeText={(text) => {
                  this.setState({ mobile3: text });
                }}
              />
            </TouchableOpacity>
          </View>
          {
            this.state.setErrors.field == "mobile3" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

          <View style={{ marginTop: 5 }}>
            <CustomTextInput
              placeholder='EMAIL'
              keyboardType='email-address'
              floatingText="EMAIL"
              onFocus={true}
              value={this.state.email3}
              returnKeyType={'done'}
              onChangeText={(text) => {
                this.setState({ email3: text });
              }}
            />
          </View>
          {
            this.state.setErrors.field == "email3" && (
              <Text style={styles.errors}>{this.state.setErrors.message}</Text>
            )
          }

        </View>
      )

    }

  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#585858',
    textAlign: 'center',
    marginTop: 10
  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  tabHolder: {
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 30,
    flex: 1
  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }

});

export default connect(mapStateToPrpos)(WorkerProfessionalReference);