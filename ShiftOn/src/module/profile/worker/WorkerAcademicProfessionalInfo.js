/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput, ToastAndroid
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../../components/CustomTextInput';
import Loader from '../../../utils/Loader';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Api from '../../../apis/Api';
import { setToastMsg } from '../../../utils/ToastMessage';
import AppConstants from '../../../apis/AppConstants';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../../components/CustomModelSelector';
import moment from 'moment';
import { connect } from 'react-redux';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
  }
}

const { width } = Dimensions.get('window');


class WorkerAcademicProfessionalInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      employerame: '',
      address1: '',
      address2: '',
      academicQualification: [],
      professionalQualification: [],
      isLoading: false,
      dateValue: '',
      isFirstDatePickerVisible: false,
      isSecondDatePickerVisible: false,
      isDeleteOn: false,
      isDeleteOn2: false,
      listPos: 0,
      listPos2: 0,
      qualificationData: [],

      previousDataTop: this.props.navigation.getParam('previousDataTop', 'NO-item'),
      isUpdateTop: this.props.navigation.getParam('isUpdateTop', 'NO-item'),
      previousDataBottom: this.props.navigation.getParam('previousDataBottom', 'NO-item'),
      isUpdateBottom: this.props.navigation.getParam('isUpdateBottom', 'NO-item'),

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {

    var academicQualification = {}
    var tempList = []
    academicQualification.qualification = ''
    academicQualification.date = ''
    tempList.push(academicQualification)
    this.setState({ academicQualification: tempList });

    var professionalQualification = {}
    var tempList1 = []
    professionalQualification.qualification = ''
    professionalQualification.date = ''
    tempList1.push(professionalQualification)
    this.setState({ professionalQualification: tempList1 });

    if (this.state.isUpdateTop == "1") {
      let newTempList = [];
      for (let i = 0; i < this.state.previousDataTop.length; i++) {
        var academicQualification = {};

        if (this.state.previousDataTop[i].qualification != null) {
          academicQualification.qualification = this.state.previousDataTop[i].qualification;
        } else {
          academicQualification.qualification = "";
        }

        if (this.state.previousDataTop[i].date != null) {
          academicQualification.date = this.state.previousDataTop[i].date;
        } else {
          academicQualification.date = "";
        }

        newTempList.push(academicQualification)
      }
      this.setState({ academicQualification: newTempList });
      if (newTempList.length == 1) {
        this.setState({ isDeleteOn: false });
      } else {
        this.setState({ isDeleteOn: true });
      }
    }

    if (this.state.isUpdateBottom == "1") {
      let newTempList = [];
      for (let i = 0; i < this.state.previousDataBottom.length; i++) {
        var professionalQualification = {};

        if (this.state.previousDataBottom[i].training != null) {
          professionalQualification.qualification = this.state.previousDataBottom[i].training;
        } else {
          professionalQualification.qualification = "";
        }

        if (this.state.previousDataBottom[i].date != null) {
          professionalQualification.date = this.state.previousDataBottom[i].date;
        } else {
          professionalQualification.date = "";
        }

        newTempList.push(professionalQualification)
      }
      this.setState({ professionalQualification: newTempList });
      if (newTempList.length == 1) {
        this.setState({ isDeleteOn2: false });
      } else {
        this.setState({ isDeleteOn2: true });
      }
    }

  }

  saveQualificationBtn = () => {
    this.setState({ isLoading: true });

    let academicQualificationInput = [];
    let academicQualificationDate = [];
    for (var i = 0; i < this.state.academicQualification.length; i++) {
      academicQualificationInput.push(this.state.academicQualification[i].qualification.toString());
      academicQualificationDate.push(this.state.academicQualification[i].date.toString());
    }

    let professionalQualificationInput = [];
    let professionalQualificationDate = [];
    for (var i = 0; i < this.state.professionalQualification.length; i++) {
      professionalQualificationInput.push(this.state.professionalQualification[i].qualification.toString())
      professionalQualificationDate.push(this.state.professionalQualification[i].date.toString())
    }

    console.log("-------------------");
    console.log(Object.values(academicQualificationInput));
    console.log(Object.values(academicQualificationDate));
    console.log("///////////////////////////");
    console.log(Object.values(professionalQualificationInput));
    console.log(Object.values(professionalQualificationDate));
    console.log("-------------------");

    var topValue1 = "1";
    for (var i = 0; i < academicQualificationInput.length; i++) {
      if (academicQualificationInput[i] == '') {
        topValue1 = "0";
      }
    }

    var topValue2 = "1";
    for (var i = 0; i < academicQualificationDate.length; i++) {
      if (academicQualificationDate[i] == '') {
        topValue2 = "0";
      }
    }

    var bottomValue1 = "1";
    for (var i = 0; i < professionalQualificationInput.length; i++) {
      if (professionalQualificationInput[i] == '') {
        bottomValue1 = "0";
      }
    }

    var bottomValue2 = "1";
    for (var i = 0; i < professionalQualificationDate.length; i++) {
      if (professionalQualificationDate[i] == '') {
        bottomValue2 = "0";
      }
    }

    if (topValue1 == "0" || topValue2 == "0") {
      this.setState({ isLoading: false });
      setToastMsg("Please fill all Qualification fields.");

    } else if (bottomValue1 == "0" || bottomValue2 == "0") {
      this.setState({ isLoading: false });
      setToastMsg("Please fill all Professional fields.");

    } else {

      let rowData = {
        "worker_id": this.props.userid,
        "user_id": this.props.userid,
        "qualification": Object.values(academicQualificationInput),
        "q_date": Object.values(academicQualificationDate),
        "training": Object.values(professionalQualificationInput),
        "t_date": Object.values(professionalQualificationDate)
      }

      Api._workerAcademicProfessionalUpdate(rowData)
        .then((response) => {
          console.log(response)

          this.setState({ isLoading: false });
          setToastMsg(response.message.toString());
          this.props.navigation.goBack();

        })
        .catch((err) => {
          console.log(err);
          this.setState({ isLoading: false });
          setToastMsg("Somthing went wrong.");
        });
    }

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15, textTransform: 'uppercase' }}> Academic & Professional</Text>

              </View>

              <Loader isLoading={this.state.isLoading} />

              <ScrollView>
                <View style={{}}>
                  <Text style={styles.heading_mail}>Academic Qualification {"\n"} Information</Text>

                  <View style={{ marginTop: 20, marginLeft: 50, marginRight: 40 }}>


                    {/* <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 10, }}>
                      <TextInput
                        style={{ height: 40, color: '#000000', fontSize: 15, marginLeft: 15, padding: 5 }}
                        placeholder="Input qualification"
                        //onChangeText={text => setText(text)}
                        //defaultValue={text}
                        placeholderTextColor='#585858'
                      />

                      <Text style={{ height: 1, backgroundColor: '#00D5E1' }}> </Text>
                      <TouchableOpacity
                        onPress={() => alert('aaa')}
                      >
                        <View>
                          <Text
                            style={{
                              color: "#393FA0",
                              marginLeft: 15,
                              backgroundColor: '#FFFFFF',
                              paddingRight: 5,
                              paddingLeft: 5,
                              fontSize: 10,
                              fontFamily: "montserrat_regular"
                            }}
                          > DATE</Text>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text
                              style={{
                                color: "#000000",
                                marginLeft: 15,
                                backgroundColor: '#FFFFFF',
                                paddingRight: 5,
                                paddingLeft: 5,
                                fontSize: 15,
                                fontFamily: "montserrat_regular"
                              }}
                            > Date</Text>
                            <Image style={{ height: 20, width: 20, padding: 10, marginRight: 10, marginBottom: 10 }} source={require('./../../../assets/calendar_icn_c.png')} />
                          </View>


                        </View>

                      </TouchableOpacity>

                    </View>
                    <Text 
                      style={{
                        color: "#393FA0",
                        marginLeft: 15,
                        position: 'absolute',
                        marginTop: -8,
                        backgroundColor: '#FFFFFF',
                        paddingRight: 5,
                        paddingLeft: 5,
                        fontSize: 10,
                        fontFamily: "montserrat_regular"
                      }}
                    > QUALIFICATION</Text>*/}
                    <FlatList
                      style={{ width: '100%', marginBottom: '12%', }}
                      data={this.state.academicQualification}
                      keyExtractor={(item, index) => index.toString()}
                      //ItemSeparatorComponent={this.ItemSeparator}
                      renderItem={({ item, index }) =>
                        <View style={{ flexDirection: 'row' }}>

                          <View style={{ marginTop: 20, width: '95%' }}>

                            <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 10, marginRight: 10, }}>

                              <TextInput
                                style={{ height: 40, color: '#000000', fontSize: 15, marginLeft: 15, padding: 5 }}
                                placeholder="Enter data "
                                onChangeText={(text) => {
                                  this._oNChangeQualification(text, index, 'academic')
                                }}
                                defaultValue={item.qualification}
                                placeholderTextColor='#585858'
                              />

                              <Text style={{ height: 1, backgroundColor: '#00D5E1' }}> </Text>
                              <TouchableOpacity
                                onPress={() => this.setState({ isFirstDatePickerVisible: true, listPos: index })}
                              >

                                <DateTimePickerModal
                                  isVisible={this.state.isFirstDatePickerVisible}
                                  mode="date"
                                  locale="en_GB"
                                  date={new Date()}
                                  onConfirm={(date) => this.handleFirstDateConfirm(date)}
                                  onCancel={() => this.setState({ isFirstDatePickerVisible: false })}
                                />

                                <View>
                                  <Text
                                    style={{
                                      color: "#393FA0",
                                      marginLeft: 15,
                                      backgroundColor: '#FFFFFF',
                                      paddingRight: 5,
                                      paddingLeft: 5,
                                      fontSize: 10,
                                      fontFamily: "montserrat_regular"
                                    }}
                                  > DATE</Text>
                                  <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text
                                      style={{
                                        color: "#000000",
                                        marginLeft: 15,
                                        backgroundColor: '#FFFFFF00',
                                        paddingRight: 5,
                                        paddingLeft: 5,
                                        fontSize: 15,
                                        fontFamily: "montserrat_regular"
                                      }}
                                    > {item.date}</Text>
                                    <Image style={{ height: 20, width: 20, padding: 10, marginRight: 10, marginBottom: 10 }} source={require('./../../../assets/calendar_icn_c.png')} />
                                  </View>


                                </View>

                              </TouchableOpacity>

                            </View>
                            <Text
                              style={styles.qualificationTextStyle}
                            > QUALIFICATION</Text>

                          </View>
                          {
                            this.state.isDeleteOn ?
                              <TouchableOpacity style={{ width: '5%', marginTop: 50, end: 7 }}
                                onPress={() => this.deleteAcadmicQualification(item)}>
                                <Image style={{ height: 20, width: 20, padding: 12 }} source={require('./../../../assets/trash_icon.png')} />
                              </TouchableOpacity>
                              :
                              <View></View>
                          }

                        </View>
                      }
                    />
                    <TouchableOpacity style={{ marginTop: -30, alignSelf: 'center', padding: 5, }}
                      onPress={() => this._addAcademicQualification()}
                    >
                      <Text
                        style={{
                          color: "#000000",
                          fontSize: 12,
                          fontFamily: "montserrat_regular"
                        }}
                      >+ ADD QUALIFICATION</Text>
                    </TouchableOpacity>


                  </View>
                  <Text style={{ height: 1, backgroundColor: '#585858', marginTop: 10 }}> </Text>


                  <Text style={styles.heading_mail}>Professional & Clinical {"\n"} Training</Text>

                  <View style={{ marginTop: 20, marginLeft: 50, marginRight: 40 }}>


                    <FlatList
                      style={{ width: '100%', marginBottom: '12%' }}
                      data={this.state.professionalQualification}
                      keyExtractor={(item, index) => index.toString()}
                      //ItemSeparatorComponent={this.ItemSeparator}
                      renderItem={({ item, index }) =>

                        <View style={{ flexDirection: 'row' }}>

                          <View style={{ marginTop: 20, width: '90%' }}>

                            <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 10, }}>
                              <TextInput
                                style={{ height: 40, color: '#000000', fontSize: 15, marginLeft: 15, padding: 5 }}
                                placeholder="Enter data  "
                                onChangeText={(text) => {
                                  this._oNChangeQualification(text, index, 'professional')
                                }}
                                defaultValue={item.qualification}
                                placeholderTextColor='#585858'
                              />

                              <Text style={{ height: 1, backgroundColor: '#00D5E1' }}> </Text>
                              <TouchableOpacity
                                onPress={() => this.setState({ isSecondDatePickerVisible: true, listPos2: index })}
                              >

                                <DateTimePickerModal
                                  isVisible={this.state.isSecondDatePickerVisible}
                                  mode="date"
                                  locale="en_GB"
                                  date={new Date()}
                                  onConfirm={(date) => this.handleSecondDateConfirm(date)}
                                  onCancel={() => this.setState({ isSecondDatePickerVisible: false })}
                                />

                                <View>
                                  <Text
                                    style={{
                                      color: "#393FA0",
                                      marginLeft: 15,
                                      backgroundColor: '#FFFFFF',
                                      paddingRight: 5,
                                      paddingLeft: 5,
                                      fontSize: 10,
                                      fontFamily: "montserrat_regular"
                                    }}
                                  > DATE</Text>
                                  <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text
                                      style={{
                                        color: "#000000",
                                        marginLeft: 15,
                                        backgroundColor: '#FFFFFF00',
                                        paddingRight: 5,
                                        paddingLeft: 5,
                                        fontSize: 15,
                                        fontFamily: "montserrat_regular"
                                      }}
                                    > {item.date}</Text>
                                    <Image style={{ height: 20, width: 20, padding: 10, marginRight: 10, marginBottom: 10 }} source={require('./../../../assets/calendar_icn_c.png')} />
                                  </View>


                                </View>

                              </TouchableOpacity>

                            </View>
                            <Text
                              style={styles.qualificationTextStyle}
                            > PROFESSIONAL & CLINICAL TRAINING</Text>

                          </View>
                          {
                            this.state.isDeleteOn2 ?
                              <TouchableOpacity style={{ width: '10%', marginTop: 50, end: -3 }}
                                onPress={() => this.deleteProfessionalTranning(item)}>
                                <Image style={{ height: 20, width: 20, padding: 12 }} source={require('./../../../assets/trash_icon.png')} />
                              </TouchableOpacity>
                              :
                              <View></View>
                          }


                        </View>

                      }
                    />
                    <TouchableOpacity style={{ marginTop: -30, alignSelf: 'center', padding: 5, }}
                      onPress={() => this._addProffesionalQualification()}
                    >
                      <Text
                        style={{
                          color: "#000000",
                          fontSize: 12,
                          fontFamily: "montserrat_regular"
                        }}
                      >+ ADD MORE</Text>
                    </TouchableOpacity>


                  </View>
                  <Text style={{ height: 1, backgroundColor: '#585858', marginTop: 10 }}> </Text>

                  <Text style={{
                    fontSize: 12,
                    fontFamily: 'montserrat_light',
                    color: '#000000',
                    marginLeft: 10,
                    marginRight: 10,
                    marginTop: 10,
                    borderRadius: 10,
                    padding: 20,
                    backgroundColor: '#CAE8A2'
                  }}>Note:Please bring with you original certificates of all relevant qualifications and certificates you have obtained</Text>

                  <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                    onPress={() => this.saveQualificationBtn()}
                  >
                    <Text style={styles.loginText}>SAVE</Text>
                  </TouchableOpacity>

                </View>

              </ScrollView>
            </View>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _oNChangeQualification = (text, pos, type) => {

    if (type == 'academic') {
      let tempList = [...this.state.academicQualification];
      tempList[pos].qualification = text;
      this.setState({ academicQualification: tempList });
    } else {
      let tempList = [...this.state.professionalQualification]
      tempList[pos].qualification = text
      this.setState({ professionalQualification: tempList });
    }
  }

  handleFirstDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    let tempList = [...this.state.academicQualification]
    tempList[this.state.listPos].date = dateTimeString
    this.setState({
      academicQualification: tempList,
      isFirstDatePickerVisible: false,
    });

  };

  handleSecondDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    let tempList = [...this.state.professionalQualification]
    tempList[this.state.listPos2].date = dateTimeString
    this.setState({
      professionalQualification: tempList,
      isSecondDatePickerVisible: false,
    });

  };

  _addAcademicQualification = () => {
    var academicQualification = {}
    let tempList = [...this.state.academicQualification]
    academicQualification.qualification = ''
    academicQualification.date = ''
    tempList.push(academicQualification)
    this.setState({ academicQualification: tempList, isDeleteOn: true });

  }
  _addProffesionalQualification = () => {
    var professionalQualification = {}
    let tempList = [...this.state.professionalQualification]
    professionalQualification.qualification = ''
    professionalQualification.date = ''
    tempList.push(professionalQualification)
    this.setState({ professionalQualification: tempList, isDeleteOn2: true });

  }

  deleteAcadmicQualification = (e) => {

    Alert.alert(
      "Confirm to Delete?",
      'Are you sure you want to delete this qualification.',
      [
        { text: 'NO', onPress: () => { console.log('No pressed') } },
        {
          text: 'YES', onPress: () => {
            var array = [...this.state.academicQualification];
            var index = array.indexOf(e)
            if (index !== -1) {
              array.splice(index, 1);
              this.setState({ academicQualification: array });
            }
            console.log(this.state.academicQualification.indexOf(e));

            if (this.state.academicQualification.length == 1) {
              this.setState({ isDeleteOn: false });
            }
          }
        },

      ],
      { cancelable: false },
    )

  }

  deleteProfessionalTranning = (e) => {

    Alert.alert(
      "Confirm to Delete?",
      'Are you sure you want to delete this professional tranning.',
      [
        { text: 'NO', onPress: () => { console.log('No pressed') } },
        {
          text: 'YES', onPress: () => {
            var array = [...this.state.professionalQualification];
            var index = array.indexOf(e)
            if (index !== -1) {
              array.splice(index, 1);
              this.setState({ professionalQualification: array });
            }
            console.log(this.state.professionalQualification.indexOf(e));

            if (this.state.professionalQualification.length == 1) {
              this.setState({ isDeleteOn2: false });
            }
          }
        },

      ],
      { cancelable: false },
    )

  }


}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#585858',
    textAlign: 'center',
    marginTop: 10
  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  },
  qualificationTextStyle: {
    color: "#393FA0",
    marginLeft: 15,
    position: 'absolute',
    marginTop: -8,
    backgroundColor: '#FFFFFF',
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 10,
    fontFamily: "montserrat_regular"
  }

});

export default connect(mapStateToPrpos)(WorkerAcademicProfessionalInfo);