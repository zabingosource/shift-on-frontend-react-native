/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView, Dimensions,
  TextInput, ToastAndroid, Platform
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../../components/CustomTextInput';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Api from '../../../apis/Api';
import { androidCameraPermission } from '../../../../permissions';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import ImagePicker from 'react-native-image-crop-picker';
import Loader from '../../../utils/Loader';
import { setToastMsg } from '../../../utils/ToastMessage';
import moment from 'moment';
import AppConstants from '../../../apis/AppConstants';
import { connect } from 'react-redux';
import { loginUserDetails } from '../../../redux/actions/actions';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../../components/CustomModelSelector';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userDetails: (txt1, txt2, txt3, txt4, txt5) => dispatch(loginUserDetails(txt1, txt2, txt3, txt4, txt5)),
  }
}

const titleData = [
  {
    id: "1",
    label: 'Mr.',
  },
  {
    id: "2",
    label: 'Mrs.',
  },

]

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});

class WorkerPersonalInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      surname: '',
      forename: '',
      title: '',
      maidenname: '',
      telephone: '',
      mobile: '',
      email: '',
      dob: '',
      nationality: '',
      address1: '',
      address2: '',
      postcode: '',
      image: '',
      isImageUpdate: '0',
      fileName: '',
      isDatePickerVisible: false,
      isLoading: true,
      countryData: [],
      stateData: [],
      cityData: [],
      countryName: '',
      stateName: '',
      cityName: '',
      countryId: '',
      stateId: '',
      cityId: '',
      setErrors: {
        field: '',
        message: ''
      },
    };
    //console.log(this.props.navigation);
  }


  componentDidMount() {
    this._getWorkerPersonalInfo();
  }

  onSelectImage = async () => {
    const permissionStatus = await androidCameraPermission()
    if (permissionStatus || Platform.OS == 'ios') {
      Alert.alert(
        'Profile Image',
        'Choose an option',
        [
          { text: 'Close', onPress: () => console.log('Close pressed') },
          { text: 'Camera', onPress: () => this.onCamera() },
          { text: 'Gallery', onPress: () => this.onGallery() },
        ]
      )
    }
  }

  onCamera = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 300,
      cropping: true,
      //freeStyleCropEnabled: true,
      includeBase64: false,
    }).then(image => {
      console.log(image);
      this.setState({
        image: image.path,
        isImageUpdate: '1',
      })
    }).catch((err) => {
      console.log("openCamera erroe" + err.toString())
      setToastMsg(err.toString());
    });

  }

  onGallery = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      //freeStyleCropEnabled: true,
      includeBase64: false,
    }).then(image => {
      console.log(image);
      this.setState({
        image: image.path,
        isImageUpdate: '1',
      })
    }).catch((err) => {
      console.log("openGallery erroe" + err.toString())
      setToastMsg(err.toString());
    });

  }


  handleConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    this.setState({
      dob: dateTimeString,
      isDatePickerVisible: false,
    })

    console.log(dateTimeString)
  };

  _getWorkerPersonalInfo = () => {
    this.setState({ isLoading: true });

    let formData = new FormData();
    formData.append('id', this.props.userid);

    Api._getWorkerPersonalInfo(formData)
      .then((response) => {

        console.log(response.data)

        if (response.status == 1) {
          response.data.sur_name != null ? this.setState({ surname: response.data.sur_name }) : this.setState({ surname: "" })
          response.data.fore_name != null ? this.setState({ forename: response.data.fore_name }) : this.setState({ forename: "" })
          response.data.title != null ? this.setState({ title: response.data.title }) : this.setState({ title: "" })
          response.data.maiden_name != null ? this.setState({ maidenname: response.data.maiden_name }) : this.setState({ maidenname: "" })
          response.data.telephone != null ? this.setState({ telephone: response.data.telephone }) : this.setState({ telephone: "" })
          response.data.mobile != null ? this.setState({ mobile: response.data.mobile }) : this.setState({ mobile: "" })
          response.data.email != null ? this.setState({ email: response.data.email }) : this.setState({ email: "" })
          response.data.dob != null ? this.setState({ dob: response.data.dob }) : this.setState({ dob: "" })
          response.data.nationality != null ? this.setState({ nationality: response.data.nationality }) : this.setState({ nationality: "" })
          response.data.address1 != null ? this.setState({ address1: response.data.address1 }) : this.setState({ address1: "" })
          response.data.address2 != null ? this.setState({ address2: response.data.address2 }) : this.setState({ address2: "" })

          response.data.country != null ? this.setState({ countryName: response.data.country.country_name }) : this.setState({ countryName: "" })
          response.data.country != null ? this.setState({ countryId: response.data.country.id }) : this.setState({ countryId: "" })
          response.data.state != null ? this.setState({ stateName: response.data.state.state_name }) : this.setState({ stateName: "" })
          response.data.state != null ? this.setState({ stateId: response.data.state.id }) : this.setState({ stateId: "" })
          response.data.city != null ? this.setState({ cityName: response.data.city.city_name }) : this.setState({ cityName: "" })
          response.data.city != null ? this.setState({ cityId: response.data.city.city_id }) : this.setState({ cityId: "" })

          response.data.post_code != null ? this.setState({ postcode: response.data.post_code }) : this.setState({ postcode: "" })
          response.data.image != null ? this.setState({ image: response.data.image }) : this.setState({ image: "" })
        } else {
          setToastMsg(response.message.toString());
        }
        //this.setState({ isLoading: false, });
        this.countryHandle();


      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });
  }

  checkValidation = () => {

    let titleError = { field: '', message: '' }
    let surenameError = { field: '', message: '' }
    let forenameError = { field: '', message: '' }
    let hTelephoneError = { field: '', message: '' }
    let mobileError = { field: '', message: '' }
    let dobError = { field: '', message: '' }
    let nationalityError = { field: '', message: '' }
    let address1Error = { field: '', message: '' }
    let address2Error = { field: '', message: '' }
    let countryError = { field: '', message: '' }
    let stateError = { field: '', message: '' }
    let cityError = { field: '', message: '' }
    let postCodeError = { field: '', message: '' }


    if (this.state.title == '') {
      titleError.field = "title";
      titleError.message = "Title is required!";
      this.setState({ setErrors: titleError })
    } else if (this.state.surname == '') {
      surenameError.field = "surename";
      surenameError.message = "Sure Name is required!";
      this.setState({ setErrors: surenameError })
    } else if (this.state.forename == '') {
      forenameError.field = "forename";
      forenameError.message = "Fore Name is required!";
      this.setState({ setErrors: forenameError })
    } else if (this.state.telephone == '') {
      hTelephoneError.field = "h_telephone";
      hTelephoneError.message = "Telephone number is required!";
      this.setState({ setErrors: hTelephoneError })
    } else if (this.state.telephone.length < 10) {
      hTelephoneError.field = "h_telephone";
      hTelephoneError.message = "Telephone number is invalid!";
      this.setState({ setErrors: hTelephoneError })
    } else if (this.state.mobile == '') {
      mobileError.field = "mobile";
      mobileError.message = "Mobile Number is required!";
      this.setState({ setErrors: mobileError })
    } else if (this.state.mobile.length < 10) {
      mobileError.field = "mobile";
      mobileError.message = "Mobile Number is invalid!";
      this.setState({ setErrors: mobileError })
    } else if (this.state.dob == '') {
      dobError.field = "dob";
      dobError.message = "Date of birth is required!";
      this.setState({ setErrors: dobError })
    } else if (this.state.nationality == '') {
      nationalityError.field = "nationality";
      nationalityError.message = "Nationality is required!";
      this.setState({ setErrors: nationalityError })
    } else if (this.state.address1 == '') {
      address1Error.field = "address1";
      address1Error.message = "Address is required!";
      this.setState({ setErrors: address1Error })
    } else if (this.state.address2 == '') {
      address2Error.field = "address2";
      address2Error.message = "Address is required!";
      this.setState({ setErrors: address2Error })
    } else if (this.state.countryName == '') {
      countryError.field = "country";
      countryError.message = "Country is required!";
      this.setState({ setErrors: countryError })
    } else if (this.state.stateName == '') {
      stateError.field = "state";
      stateError.message = "State is required!";
      this.setState({ setErrors: stateError })
    } else if (this.state.cityName == '') {
      cityError.field = "city";
      cityError.message = "City is required!";
      this.setState({ setErrors: cityError })
    } else if (this.state.postcode == '') {
      postCodeError.field = "post_code";
      postCodeError.message = "Post code is required!";
      this.setState({ setErrors: postCodeError })
    } else {
      this.setState({ setErrors: { field: '', message: '' } })
      this._workerPersonalInfoUpdate();
    }
  }

  _workerPersonalInfoUpdate = () => {
    this.setState({ isLoading: true });

    let formData = new FormData();
    formData.append('id', this.props.userid);
    formData.append('sur_name', this.state.surname);
    formData.append('fore_name', this.state.forename);
    formData.append('title', this.state.title);
    formData.append('maiden_name', this.state.maidenname);
    formData.append('telephone', this.state.telephone);
    formData.append('mobile', this.state.mobile);
    formData.append('email', this.state.email);
    formData.append('dob', this.state.dob);
    formData.append('nationality', this.state.nationality);
    formData.append('address1', this.state.address1);
    formData.append('address2', this.state.address2);
    formData.append('country_id', this.state.countryId);
    formData.append('state_id', this.state.stateId);
    formData.append('city_id', this.state.cityId);
    formData.append('post_code', this.state.postcode);
    if (this.state.isImageUpdate == '1') {
      formData.append('image', {
        name: 'photo.jpg',
        type: 'image/jpg',
        uri: this.state.image,
      });
    }


    Api._workerPersonalInfoUpdate(formData)
      .then((response) => {

        console.log(response)

        if (response.status == "1") {
          var imageValue = "";
          var sureNameValue = "";
          var foreNameValue = "";

          if (response.data.image != null) {
            imageValue = response.data.image.toString()
          }
          if (response.data.sur_name != null) {
            sureNameValue = response.data.sur_name.toString()
          }
          if (response.data.fore_name != null) {
            foreNameValue = response.data.fore_name.toString()
          }

          this.props.userDetails(
            this.props.userid, foreNameValue + " " + sureNameValue,
            response.data.email.toString(), imageValue, "worker");

          this.storeLogDetailsToAsyncStorage(
            this.props.userid, foreNameValue + " " + sureNameValue,
            response.data.email.toString(), imageValue, "worker");
        }

        this.setState({ isLoading: false })
        setToastMsg(response.message.toString());
        this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false, })
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>

              <Loader isLoading={this.state.isLoading} />

              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() =>
                    this.props.navigation.goBack()
                  }
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15, textTransform: 'uppercase' }}>Personal Info</Text>

              </View>
              <ScrollView>
                <View style={{ marginLeft: 50, marginRight: 50 }}>

                  <TouchableOpacity style={{ alignItems: 'center', height: 120, width: 120, alignSelf: 'center', marginTop: 20 }}
                    onPress={() => this.onSelectImage()}>
                    <Image style={{ height: 120, width: 120, borderRadius: 25 }}
                      source={this.state.image ?
                        (this.state.isImageUpdate == '1' ? { uri: this.state.image } : { uri: AppConstants.IMAGE_URL + this.state.image }) :
                        require('./../../../assets/woman_face.jpeg')
                      } resizeMode='stretch' />

                  </TouchableOpacity>

                  <Text style={styles.heading_mail}>ATTACH A SELF IMAGE</Text>


                  {/* <View style={{ marginTop: 20 }}>
                    <CustomTextInput
                      placeholder='POSITION APPLIED FOR'
                      keyboardType='default'
                      floatingStyle={{ color: '#AA0606' }}
                      floatingText="POSITION APPLIED FOR"
                      editable={true}
                      onFocus={true}
                      value={this.state.position}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          position: text,
                        });
                      }}
                    />
                  </View> */}

                  <ModalSelector
                    data={titleData}
                    initValue="TITLE"
                    accessible={true}
                    search={false}
                    animationType={"fade"}
                    keyExtractor={item => item.id}
                    labelExtractor={item => item.label}
                    onChange={(option) => { this.setState({ title: option.label }) }}>
                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='TITLE'
                        keyboardType='default'
                        floatingText="TITLE"
                        value={this.state.title}
                        editable={true}
                        onFocus={true}
                        showSoftInputOnFocus={true}
                        caretHidden={false}
                        isVisibleDropDown={true}
                        returnKeyType={'done'}
                      />
                    </View>
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "title" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='SURNAME'
                      keyboardType='default'
                      floatingText="SURNAME"
                      editable={true}
                      onFocus={true}
                      onSubmitEditing={true}
                      value={this.state.surname}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          surname: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "surename" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='FORENAME'
                      keyboardType='default'
                      floatingText="FORENAME"
                      editable={true}
                      onFocus={true}
                      value={this.state.forename}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          forename: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "forename" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='MAIDEN NAME (OPTIONAL)'
                      keyboardType='default'
                      floatingText="MAIDEN NAME"
                      editable={true}
                      onFocus={true}
                      value={this.state.maidenname}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          maidenname: text,
                        });
                      }}
                    />
                  </View>

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='HOME TELEPHONE'
                      keyboardType='numeric'
                      floatingText="HOME TELEPHONE"
                      editable={true}
                      onFocus={true}
                      maxLength={10}
                      value={this.state.telephone}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          telephone: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "h_telephone" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='MOBILE'
                      keyboardType='numeric'
                      floatingText="MOBILE"
                      editable={true}
                      onFocus={true}
                      maxLength={10}
                      value={this.state.mobile}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          mobile: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "mobile" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='EMAIL'
                      keyboardType='email-address'
                      floatingText="EMAIL"
                      style={{ backgroundColor: '#e6e6e6' }}
                      editable={false}
                      onFocus={true}
                      value={this.state.email}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          email: text,
                        });
                      }}
                    />
                  </View>

                  <View style={{ marginTop: 5, }}>
                    <CustomTextInput
                      placeholder='DATE OF BIRTH'
                      keyboardType='default'
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleCalendar={true}
                      floatingText="DATE OF BIRTH"
                      value={this.state.dob}
                      returnKeyType={'done'}
                      onTouchEnd={() => {
                        this.setState({ isDatePickerVisible: true, })
                      }}
                    />
                    <DateTimePickerModal
                      isVisible={this.state.isDatePickerVisible}
                      mode="date"
                      date={new Date()}
                      onConfirm={(date) => this.handleConfirm(date)}
                      onCancel={() => this.setState({ isDatePickerVisible: false, })}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "dob" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='NATIONALITY'
                      keyboardType='default'
                      floatingText="NATIONALITY"
                      editable={true}
                      onFocus={true}
                      value={this.state.nationality}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          nationality: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "nationality" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='ADDRESS-1'
                      keyboardType='default'
                      floatingText="ADDRESS-1"
                      editable={true}
                      onFocus={true}
                      value={this.state.address1}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          address1: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "address1" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='ADDRESS-2'
                      keyboardType='default'
                      floatingText="ADDRESS-2"
                      editable={true}
                      onFocus={true}
                      value={this.state.address2}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          address2: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "address2" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <ModalSelector
                    data={this.state.countryData}
                    initValue="COUNTRY"
                    accessible={true}
                    search={true}
                    animationType={"fade"}
                    keyExtractor={item => item.id}
                    labelExtractor={item => item.label}
                    onChange={(option) => {
                      this.setState({
                        countryName: option.label,
                        countryId: option.id,
                        stateName: '',
                        stateId: '',
                        cityName: '',
                        cityId: '',
                        isLoading: true,
                      });
                      this.handleState(option.id);
                    }}>

                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='COUNTRY'
                        keyboardType='default'
                        floatingText="COUNTRY"
                        value={this.state.countryName}
                        editable={false}
                        onFocus={true}
                        showSoftInputOnFocus={true}
                        caretHidden={false}
                        isVisibleDropDown={true}
                        returnKeyType={'done'}
                      />
                    </View>
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "country" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <ModalSelector
                    data={this.state.stateData}
                    initValue="STATE"
                    accessible={true}
                    search={true}
                    animationType={"fade"}
                    keyExtractor={item => item.id}
                    labelExtractor={item => item.label}
                    onChange={(option) => {
                      this.setState({
                        stateName: option.label,
                        stateId: option.id,
                        cityName: '',
                        cityId: '',
                        isLoading: true,
                      });
                      this.handleCity(option.id);
                    }}>

                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='STATE'
                        keyboardType='default'
                        floatingText="STATE"
                        value={this.state.stateName}
                        editable={false}
                        onFocus={true}
                        showSoftInputOnFocus={true}
                        caretHidden={false}
                        isVisibleDropDown={true}
                        returnKeyType={'done'}
                      />
                    </View>
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "state" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <ModalSelector
                    data={this.state.cityData}
                    initValue="CITY"
                    accessible={true}
                    search={true}
                    animationType={"fade"}
                    keyExtractor={item => item.id}
                    labelExtractor={item => item.label}
                    onChange={(option) => {
                      this.setState({
                        cityName: option.label, cityId: option.id,
                      });
                    }}>
                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='CITY'
                        keyboardType='default'
                        floatingText="CITY"
                        value={this.state.cityName}
                        editable={false}
                        onFocus={true}
                        showSoftInputOnFocus={true}
                        caretHidden={false}
                        isVisibleDropDown={true}
                        returnKeyType={'done'}
                      />
                    </View>
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "city" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='POST CODE'
                      keyboardType='default'
                      floatingText="POST CODE"
                      editable={true}
                      onFocus={true}
                      value={this.state.postcode}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          postcode: text,
                        });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "post_code" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                    onPress={() => this.checkValidation()}
                  >
                    <Text style={styles.loginText}>SAVE</Text>
                  </TouchableOpacity>
                </View>

              </ScrollView>
            </View>



          </View>
        </SafeAreaView>
      </Fragment>
    );
  }


  countryHandle = () => {

    this.setState({ isLoading: true });

    Api._countryListApi()
      .then((response) => {

        console.log("COUNTRY:- ", response.data);
        var count = Object.keys(response.data).length;
        let countryArray = [];
        for (var i = 0; i < count; i++) {
          countryArray.push({
            id: response.data[i].id,
            label: response.data[i].country_name,
          });
        }
        this.setState({ countryData: countryArray, isLoading: false, });

      })
      .catch((err) => {
        console.log("country error: " + err);
        this.setState({ isLoading: false, });
      });
  }

  handleState = (countryCode) => {
    let rawData = { "country_id": countryCode, }

    Api._stateListApi(rawData)
      .then((response) => {

        console.log("STATE:- ", response.data);
        var count = Object.keys(response.data).length;
        let stateArray = [];
        for (var i = 0; i < count; i++) {
          stateArray.push({
            id: response.data[i].id,
            label: response.data[i].state_name,
          });
        }
        this.setState({ stateData: stateArray, isLoading: false, });

      })
      .catch((err) => {
        console.log("state error: " + err);
        this.setState({ isLoading: false, });
      });
  }

  handleCity = (stateCode) => {
    let rawData = { "state_id": stateCode, }

    Api._cityListApi(rawData)
      .then((response) => {

        console.log("CITY:- ", response.data);
        var count = Object.keys(response.data).length;
        let cityArray = [];
        for (var i = 0; i < count; i++) {
          cityArray.push({
            id: response.data[i].city_id,
            label: response.data[i].city_name,
          });
        }
        this.setState({ cityData: cityArray, isLoading: false, });

      })
      .catch((err) => {
        console.log("state error: " + err);
        this.setState({ isLoading: false, });
      });
  }

  storeLogDetailsToAsyncStorage = async (id, name, email, image, role) => {
    try {
      await AsyncStorage.setItem('user_id', id);
      await AsyncStorage.setItem('user_name', name);
      await AsyncStorage.setItem('user_email', email);
      await AsyncStorage.setItem('user_image', image);
      await AsyncStorage.setItem('user_role', role);

    } catch (e) {
      console.log(e);
    }
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 12,
    fontFamily: 'montserrat_regular',
    color: '#585858',
    marginTop: 10
  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }

});

export default connect(mapStateToPrpos, mapDispatchToProps)(WorkerPersonalInfo);