/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView,
  Dimensions, TextInput, ToastAndroid, RefreshControl
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import Api from '../../../apis/Api';
import { setToastMsg } from '../../../utils/ToastMessage';
import Loader from '../../../utils/Loader';
import { connect } from 'react-redux';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
  }
}

const { width } = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});

class EditWorkerProfileOption extends Component {
  constructor(props) {
    super(props);
    this.state = {

      isLoading: true,
      isRefresh: false,

      WerkerPersonalInfo: false,
      WorkerAcademic: false,
      WorkerCurrentEmployment: false,
      WorkerEmploymentHistory: false,
      WorkerNmsSkills: false,
      WorkerRefrences: false,
      WorkerKins: false,
      WorkerCriminalRecords: false,
      WorkerEmploymentAct: false,

      WorkerAcademicData: [],
      WorkerAcademicIsUpdate: "0",

      WorkerProfessionalData: [],
      WorkerProfessionalIsUpdate: "0",

      WorkerEmploymentHistoryData: [],
      WorkerEmploymentHistoryIsUpdate: '0',

      WorkerCurrentEmploymentData: '',
      WorkerCurrentEmploymentIsUpdate: '0',

      WorkerNmsSkillsData: '',
      WorkerNmsSkillsIsUpdate: '0',

      WorkerRefrencesData: '',
      WorkerRefrencesIsUpdate: '0',

      WorkerKinsData: '',
      WorkerKinsIsUpdate: '0',

      WorkerCriminalRecordsData: '',
      WorkerCriminalRecordsIsUpdate: '0',

      WorkerEmploymentActData: '',
      WorkerEmploymentActIsUpdate: '0',

      isDisplayicon: false,

    };
    //console.log(this.props.navigation);
  }


  componentDidMount() {
    this.navFocusListener = this.props.navigation.addListener('didFocus', () => {
      this._checkWorkerProfileCompletion();
    });
  }

  componentWillUnmount() {
    this.navFocusListener.remove();
  }

  // onRefresh = () => {
  //   this.setState({ isRefresh: true, isLoading: true })
  //   this._checkWorkerProfileCompletion();
  //   setTimeout(() => { this.setState({ isRefresh: false }) }, 1000)
  // }

  _checkWorkerProfileCompletion = () => {
    this.setState({ isLoading: true });
    let rawData = {
      "worker_id": this.props.userid,
    }

    Api._checkWorkerProfileCompletion(rawData)
      .then((response) => {

        console.log(response.data)

        if (response.data.WorkerAcademic != null) {
          if (response.data.WorkerAcademic.length > 0) {
            this.setState({
              WorkerAcademic: true,
              WorkerAcademicIsUpdate: "1",
              WorkerAcademicData: response.data.WorkerAcademic,
            })
          }
        }
        if (response.data.WorkerProfessional != null) {
          if (response.data.WorkerProfessional.length > 0) {
            this.setState({
              WorkerAcademic: true,
              WorkerProfessionalIsUpdate: "1",
              WorkerProfessionalData: response.data.WorkerProfessional,
            })
          }
        }
        if (response.data.WorkerEmployment != null) {
          this.setState({
            WorkerCurrentEmployment: true,
            WorkerCurrentEmploymentIsUpdate: "1",
            WorkerCurrentEmploymentData: response.data.WorkerEmployment,
          })
        }
        if (response.data.WorkerEmploymentHistory != null) {
          if (response.data.WorkerEmploymentHistory.length > 0) {
            this.setState({
              WorkerEmploymentHistory: true,
              WorkerEmploymentHistoryIsUpdate: "1",
              WorkerEmploymentHistoryData: response.data.WorkerEmploymentHistory,
            })
          }
        }
        if (response.data.WorkerNmsSkills != null) {
          this.setState({
            WorkerNmsSkills: true,
            WorkerNmsSkillsIsUpdate: "1",
            WorkerNmsSkillsData: response.data.WorkerNmsSkills,
          })
        }
        if (response.data.WorkerRefrences != null) {
          if (response.data.WorkerRefrences.length > 0) {
            this.setState({
              WorkerRefrences: true,
              WorkerRefrencesIsUpdate: "1",
              WorkerRefrencesData: response.data.WorkerRefrences,
            })
          }
        }
        if (response.data.WorkerKins != null) {
          this.setState({
            WorkerKins: true,
            WorkerKinsIsUpdate: "1",
            WorkerKinsData: response.data.WorkerKins,
          })
        }
        if (response.data.WorkerCriminalRecords != null) {
          this.setState({
            WorkerCriminalRecords: true,
            WorkerCriminalRecordsIsUpdate: "1",
            WorkerCriminalRecordsData: response.data.WorkerCriminalRecords,
          })
        }
        if (response.data.WorkerEmploymentAct != null) {
          this.setState({
            WorkerEmploymentAct: true,
            WorkerEmploymentActIsUpdate: "1",
            WorkerEmploymentActData: response.data.WorkerEmploymentAct,
          })
        }

        this.setState({
          isLoading: false,
          isDisplayicon: true,
          WerkerPersonalInfo: true,
        })
        //setToastMsg("User details fetched.");

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({
          isLoading: false,
        })
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>
            <ScrollView
            // refreshControl={
            //   <RefreshControl
            //     refreshing={this.state.isRefresh}
            //     onRefresh={() => this.onRefresh()}
            //   />
            // }
            >

              <View style={styles.mainContainer}>

                <Image style={styles.headerBg} source={require('./../../../assets/regis_step-bg.png')} resizeMode="stretch" />

                <TouchableOpacity
                  style={{ position: 'absolute', marginTop: 20, marginLeft: 10 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>

                <Loader isLoading={this.state.isLoading} />

                <View style={{ marginLeft: 50, marginRight: 50 }}>
                  <Text style={styles.heading}>EDIT PROFILE</Text>
                </View>

                <TouchableOpacity style={[styles.optionButtonStyle, { backgroundColor: this.state.WerkerPersonalInfo ? '#d6f6f9' : '#ffffff' }]}
                  onPress={() => this.props.navigation.navigate('WorkerPersonalInfo')}
                >
                  <Text style={styles.loginText1}>Personal Info</Text>
                  {
                    this.state.isDisplayicon ?
                      (this.state.WerkerPersonalInfo ?
                        <Image style={{ height: 15, width: 15, padding: 10, }} source={require('./../../../assets/profile_done.png')} />
                        :
                        <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/profile_undone.png')} />)
                      :
                      <View></View>
                  }
                </TouchableOpacity>

                <TouchableOpacity style={[styles.optionButtonStyle, { backgroundColor: this.state.WorkerAcademic ? '#d6f6f9' : '#ffffff' }]}
                  onPress={() => this.props.navigation.navigate('WorkerAcademicProfessionalInfo',
                    { previousDataTop: this.state.WorkerAcademicData, isUpdateTop: this.state.WorkerAcademicIsUpdate, previousDataBottom: this.state.WorkerProfessionalData, isUpdateBottom: this.state.WorkerProfessionalIsUpdate })}
                >
                  <Text style={styles.loginText1}> Academic & Professional</Text>
                  {
                    this.state.isDisplayicon ?
                      (this.state.WorkerAcademic ?
                        <Image style={{ height: 15, width: 15, padding: 10, }} source={require('./../../../assets/profile_done.png')} />
                        :
                        <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/profile_undone.png')} />)
                      :
                      <View></View>
                  }
                </TouchableOpacity>

                <TouchableOpacity style={[styles.optionButtonStyle, { backgroundColor: this.state.WorkerCurrentEmployment ? '#d6f6f9' : '#ffffff' }]}
                  onPress={() => this.props.navigation.navigate('WorkerCurrentEmploymentInfo', { previousData: this.state.WorkerCurrentEmploymentData, isUpdate: this.state.WorkerCurrentEmploymentIsUpdate })}
                >
                  <Text style={styles.loginText1}> Current Employer</Text>
                  {this.state.isDisplayicon ?
                    (this.state.WorkerCurrentEmployment ?
                      <Image style={{ height: 15, width: 15, padding: 10, }} source={require('./../../../assets/profile_done.png')} />
                      :
                      <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/profile_undone.png')} />)
                    :
                    <View></View>
                  }
                </TouchableOpacity>

                <TouchableOpacity style={[styles.optionButtonStyle, { backgroundColor: this.state.WorkerEmploymentHistory ? '#d6f6f9' : '#ffffff' }]}
                  onPress={() => this.props.navigation.navigate('WorkerEmploymentHistory', { previousData: this.state.WorkerEmploymentHistoryData, isUpdate: this.state.WorkerEmploymentHistoryIsUpdate })}
                >
                  <Text style={styles.loginText1}>Employment History</Text>
                  {this.state.isDisplayicon ?
                    (this.state.WorkerEmploymentHistory ?
                      <Image style={{ height: 15, width: 15, padding: 10, }} source={require('./../../../assets/profile_done.png')} />
                      :
                      <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/profile_undone.png')} />)
                    :
                    <View></View>
                  }
                </TouchableOpacity>

                <TouchableOpacity style={[styles.optionButtonStyle, { backgroundColor: this.state.WorkerNmsSkills ? '#d6f6f9' : '#ffffff' }]}
                  onPress={() => this.props.navigation.navigate('WorkerMNCPin', { previousData: this.state.WorkerNmsSkillsData, isUpdate: this.state.WorkerNmsSkillsIsUpdate })}
                >
                  <Text style={styles.loginText1}>NMC & Skill</Text>
                  {this.state.isDisplayicon ?
                    (this.state.WorkerNmsSkills ?
                      <Image style={{ height: 15, width: 15, padding: 10, }} source={require('./../../../assets/profile_done.png')} />
                      :
                      <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/profile_undone.png')} />)
                    :
                    <View></View>
                  }
                </TouchableOpacity>

                <TouchableOpacity style={[styles.optionButtonStyle, { backgroundColor: this.state.WorkerRefrences ? '#d6f6f9' : '#ffffff' }]}
                  onPress={() => this.props.navigation.navigate('WorkerProfessionalReference', { previousData: this.state.WorkerRefrencesData, isUpdate: this.state.WorkerRefrencesIsUpdate })}
                >
                  <Text style={styles.loginText1}>References</Text>
                  {this.state.isDisplayicon ?
                    (this.state.WorkerRefrences ?
                      <Image style={{ height: 15, width: 15, padding: 10, }} source={require('./../../../assets/profile_done.png')} />
                      :
                      <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/profile_undone.png')} />)
                    :
                    <View></View>
                  }
                </TouchableOpacity>

                <TouchableOpacity style={[styles.optionButtonStyle, { backgroundColor: this.state.WorkerKins ? '#d6f6f9' : '#ffffff' }]}
                  onPress={() => this.props.navigation.navigate('WorkerEmergencyContact', { previousData: this.state.WorkerKinsData, isUpdate: this.state.WorkerKinsIsUpdate })}
                >
                  <Text style={styles.loginText1}>Kin Details</Text>
                  {this.state.isDisplayicon ?
                    (this.state.WorkerKins ?
                      <Image style={{ height: 15, width: 15, padding: 10, }} source={require('./../../../assets/profile_done.png')} />
                      :
                      <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/profile_undone.png')} />)
                    :
                    <View></View>
                  }
                </TouchableOpacity>

                <TouchableOpacity style={[styles.optionButtonStyle, { backgroundColor: this.state.WorkerCriminalRecords ? '#d6f6f9' : '#ffffff' }]}
                  onPress={() => this.props.navigation.navigate('WorkerCriminalRecord', { previousData: this.state.WorkerCriminalRecordsData, isUpdate: this.state.WorkerCriminalRecordsIsUpdate })}
                >
                  <Text style={styles.loginText1}>Criminal Records</Text>
                  {this.state.isDisplayicon ?
                    (this.state.WorkerCriminalRecords ?
                      <Image style={{ height: 15, width: 15, padding: 10, }} source={require('./../../../assets/profile_done.png')} />
                      :
                      <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/profile_undone.png')} />)
                    :
                    <View></View>
                  }
                </TouchableOpacity>

                <TouchableOpacity style={[styles.optionButtonStyle, { backgroundColor: this.state.WorkerEmploymentAct ? '#d6f6f9' : '#ffffff' }]}
                  onPress={() => this.props.navigation.navigate('WorkerEmploymentAct', { previousData: this.state.WorkerEmploymentActData, isUpdate: this.state.WorkerEmploymentActIsUpdate })}
                >
                  <Text style={styles.loginText1}>Employment Act</Text>
                  {this.state.isDisplayicon ?
                    (this.state.WorkerEmploymentAct ?
                      <Image style={{ height: 15, width: 15, padding: 10, }} source={require('./../../../assets/profile_done.png')} />
                      :
                      <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/profile_undone.png')} />)
                    :
                    <View></View>
                  }
                </TouchableOpacity>
              </View>
            </ScrollView>


          </View>
        </SafeAreaView>
      </Fragment>
    );
  }


}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 18,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 20,
    fontFamily: 'montserrat_regular',
    color: '#00B800',
    marginTop: 20

  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",

  },
  loginText1: {
    color: '#585858',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    textAlign: 'center',
    marginLeft: 10,

  },
  optionButtonStyle: {
    padding: 10,
    marginTop: 10,
    //backgroundColor: '',
    borderColor: '#00D5E1',
    borderWidth: 1.5,
    borderRadius: 15,
    marginLeft: 40,
    marginRight: 40,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  }

});

export default connect(mapStateToPrpos)(EditWorkerProfileOption);