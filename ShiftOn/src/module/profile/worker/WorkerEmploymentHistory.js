/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView,
  Alert, SafeAreaView, Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../../components/CustomTextInput';
import Loader from '../../../utils/Loader';
import { setToastMsg } from '../../../utils/ToastMessage';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Api from '../../../apis/Api';
import AppConstants from '../../../apis/AppConstants';
import moment from 'moment';
import { connect } from 'react-redux';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
  }
}

const { width } = Dimensions.get('window');


class WorkerEmploymentHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      employeerName: '',
      address1: '',
      address2: '',
      employmentHistory: [],
      isLoading: false,
      isStartDatePickerVisible: false,
      isEndDatePickerVisible: false,
      listPos: 0,
      isDeleteOn: false,
      previousData: this.props.navigation.getParam('previousData', 'NO-item'),
      isUpdate: this.props.navigation.getParam('isUpdate', 'NO-item'),
      setErrors: {
        field: '',
        message: ''
      }

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {

    var employmentHistory = {}
    var tempList = []
    employmentHistory.employeerName = ''
    employmentHistory.address = ''
    employmentHistory.position = ''
    employmentHistory.startDate = ''
    employmentHistory.endDate = ''
    employmentHistory.reason = ''
    tempList.push(employmentHistory)
    this.setState({ employmentHistory: tempList });

    if (this.state.isUpdate == "1") {

      let newTempList = [];
      for (let i = 0; i < this.state.previousData.length; i++) {
        var newEmploymentHistory = {};
        newEmploymentHistory.employeerName = this.state.previousData[i].employer_name;
        newEmploymentHistory.address = this.state.previousData[i].address1;
        newEmploymentHistory.position = this.state.previousData[i].position_held;
        newEmploymentHistory.startDate = this.state.previousData[i].date_started;
        newEmploymentHistory.endDate = this.state.previousData[i].date_end;
        newEmploymentHistory.reason = this.state.previousData[i].reason_for_leaving;
        newTempList.push(newEmploymentHistory)

      }

      this.setState({ employmentHistory: newTempList });
      if (newTempList.length == 1) {
        this.setState({ isDeleteOn: false });
      } else {
        this.setState({ isDeleteOn: true });
      }

    }

  }

  checkValidation = () => {
    for (var i = 0; i < this.state.employmentHistory.length; i++) {

      if (this.state.employmentHistory[i].employeerName.toString() == '') {
        console.error('Empolyer name is required!');
      } else if (this.state.employmentHistory[i].address.toString() == '') {
        console.error('Address is required!')
      } else if (this.state.employmentHistory[i].position.toString() == '') {
        console.error('Position is required!')
      } else if (this.state.employmentHistory[i].reason.toString() == '') {
        console.error('Reason for leaving is required!')
      }

    }
    //this.onSaveBtn();


  }

  onSaveBtn = () => {
    this.setState({ isLoading: true })

    let employmentHistoryName = [];
    let employmentHistoryAddress = [];
    let employmentHistoryPosition = [];
    let employmentHistorystartDate = [];
    let employmentHistoryEndDate = [];
    let employmentHistoryReason = [];

    for (var i = 0; i < this.state.employmentHistory.length; i++) {
      employmentHistoryName.push(this.state.employmentHistory[i].employeerName.toString())
      employmentHistoryAddress.push(this.state.employmentHistory[i].address.toString())
      employmentHistoryPosition.push(this.state.employmentHistory[i].position.toString())
      employmentHistorystartDate.push(this.state.employmentHistory[i].startDate.toString())
      employmentHistoryEndDate.push(this.state.employmentHistory[i].endDate.toString())
      employmentHistoryReason.push(this.state.employmentHistory[i].reason.toString())
    }

    console.log("-------------------");
    console.log(Object.values(employmentHistoryName));
    console.log(Object.values(employmentHistoryAddress));
    console.log(Object.values(employmentHistorystartDate));
    console.log(Object.values(employmentHistoryEndDate));
    console.log(Object.values(employmentHistoryPosition));
    console.log(Object.values(employmentHistoryReason));
    console.log("///////////////////////////");

    let rowData = {
      "worker_id": this.props.userid,
      "user_id": this.props.userid,
      "employer_name": Object.values(employmentHistoryName),
      "address1": Object.values(employmentHistoryAddress),
      "date_started": Object.values(employmentHistorystartDate),
      "date_end": Object.values(employmentHistoryEndDate),
      "position_held": Object.values(employmentHistoryPosition),
      "reason_for_leaving": Object.values(employmentHistoryReason),
    }

    Api._workerEmploymentHistoryUpdate(rowData)
      .then((response) => {
        console.log(response)

        this.setState({ isLoading: false })
        setToastMsg(response.message.toString());
        this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15, textTransform: 'uppercase' }}>Employment History</Text>

              </View>
              <Loader isLoading={this.state.isLoading} />
              <ScrollView>
                <View style={{}}>
                  <Text style={styles.heading_mail}>Full Employment History</Text>
                  <Text style={{ alignSelf: 'center', fontSize: 10, fontFamily: 'montserrat_regular', color: '#585858', textAlign: 'center', }}>(including any gaps - most recent first)</Text>

                  <View style={{ marginTop: 20, }}>

                    <FlatList
                      style={{ width: '100%', marginBottom: '12%' }}
                      data={this.state.employmentHistory}
                      keyExtractor={(item, index) => index.toString()}
                      //ItemSeparatorComponent={this.ItemSeparator}
                      renderItem={({ item, index }) =>

                        <View >

                          <View style={{ marginLeft: 40, marginRight: 30, flexDirection: 'row' }}>

                            <View style={{ marginTop: 10, width: '95%' }}>

                              <View style={{ marginTop: 5 }}>
                                <CustomTextInput
                                  placeholder='NAME OF EMPLOYER'
                                  keyboardType='default'
                                  floatingText="NAME OF EMPLOYER"
                                  onFocus={true}
                                  value={this.state.employmentHistory[index].employeerName}
                                  returnKeyType={'done'}
                                  onChangeText={(text) => {
                                    this._oNChangeDetails(text, index, 'employeer')
                                  }}
                                />
                              </View>
                              {
                                this.state.setErrors.field == "employeerName" && (
                                  <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                )
                              }

                              <View style={{ marginTop: 5 }}>
                                <CustomTextInput
                                  placeholder='ADDRESS'
                                  keyboardType='default'
                                  floatingText="ADDRESS"
                                  onFocus={true}
                                  value={this.state.employmentHistory[index].address}
                                  returnKeyType={'done'}
                                  onChangeText={(text) => {
                                    this._oNChangeDetails(text, index, 'address')
                                  }}
                                />
                              </View>
                              {
                                this.state.setErrors.field == "address" && (
                                  <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                )
                              }

                              <View style={{ marginTop: 5 }}>
                                <CustomTextInput
                                  placeholder='POSITION HELD'
                                  keyboardType='default'
                                  floatingText="POSITION HELD"
                                  onFocus={true}
                                  value={this.state.employmentHistory[index].position}
                                  returnKeyType={'done'}
                                  onChangeText={(text) => {
                                    this._oNChangeDetails(text, index, 'position')
                                  }}
                                />
                              </View>
                              {
                                this.state.setErrors.field == "position" && (
                                  <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                )
                              }

                              <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginTop: 5, flex: 1, marginRight: 5 }}>
                                  <TouchableOpacity
                                  // onPress={() => this._setDate("start")}
                                  >
                                    <CustomTextInput
                                      refer={"START_DATE"}
                                      placeholder='START DATE'
                                      keyboardType='default'
                                      floatingText="START DATE"
                                      editable={true}
                                      onFocus={true}
                                      showSoftInputOnFocus={false}
                                      caretHidden={true}
                                      style={{ paddingLeft: 10 }}
                                      isVisibleCalendar={true}
                                      value={this.state.employmentHistory[index].startDate}
                                      returnKeyType={'done'}
                                      onTouchEnd={() => this.setState({ isStartDatePickerVisible: true, listPos: index })}
                                    />

                                    <DateTimePickerModal
                                      isVisible={this.state.isStartDatePickerVisible}
                                      mode="date"
                                      locale="en_GB"
                                      date={new Date()}
                                      onConfirm={(date) => this.handleStartDateConfirm(date)}
                                      onCancel={() => this.setState({ isStartDatePickerVisible: false })}
                                    />

                                  </TouchableOpacity>

                                </View>
                                <View style={{ marginTop: 5, flex: 1, marginLeft: 5 }}>
                                  <TouchableOpacity
                                  // onPress={() => this._setDate("end")}
                                  >
                                    <CustomTextInput
                                      placeholder='END DATE'
                                      keyboardType='default'
                                      floatingText="END DATE"
                                      editable={true}
                                      onFocus={true}
                                      showSoftInputOnFocus={false}
                                      caretHidden={true}
                                      style={{ paddingLeft: 10 }}
                                      isVisibleCalendar={true}
                                      value={this.state.employmentHistory[index].endDate}
                                      returnKeyType={'done'}
                                      onTouchEnd={() => this.setState({ isEndDatePickerVisible: true, listPos: index })}
                                    />

                                    <DateTimePickerModal
                                      isVisible={this.state.isEndDatePickerVisible}
                                      mode="date"
                                      locale="en_GB"
                                      date={new Date()}
                                      onConfirm={(date) => this.handleEndDateConfirm(date)}
                                      onCancel={() => this.setState({ isEndDatePickerVisible: false })}
                                    />

                                  </TouchableOpacity>

                                </View>
                              </View>

                              <View style={{ marginTop: 5 }}>
                                <CustomTextInput
                                  placeholder='REASON FOR LEAVING'
                                  keyboardType='default'
                                  style={{ textAlignVertical: "top", height: 150, }}
                                  floatingText="REASON FOR LEAVING"
                                  onFocus={true}
                                  value={this.state.employmentHistory[index].reason}
                                  returnKeyType={'done'}
                                  onChangeText={(text) => {
                                    this._oNChangeDetails(text, index, 'reason')
                                  }}
                                />
                              </View>
                              {
                                this.state.setErrors.field == "reason" && (
                                  <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                                )
                              }

                            </View>

                            {
                              this.state.isDeleteOn ?
                                <TouchableOpacity style={{ width: '5%', marginTop: 30, end: -6 }}
                                  onPress={() => this.deleteEmploymentHistory(item)}>
                                  <Image style={{ height: 20, width: 20, padding: 12 }} source={require('./../../../assets/trash_icon.png')} />
                                </TouchableOpacity>
                                :
                                <View></View>
                            }


                          </View>
                          <Text style={{ height: 1, backgroundColor: '#585858', marginTop: 10 }}> </Text>
                        </View>

                      }
                    />
                    <TouchableOpacity style={{ marginTop: -45, alignSelf: 'center', padding: 5, }}
                      onPress={() => this._addEmployeementHistory()}
                    >
                      <Text
                        style={{
                          color: "#000000",
                          fontSize: 12,
                          fontFamily: "montserrat_regular"
                        }}
                      >+ ADD ANOTHER</Text>
                    </TouchableOpacity>


                  </View>

                  <Text style={{ height: 1, backgroundColor: '#585858', marginTop: 10 }}> </Text>

                  <Text style={{
                    fontSize: 12,
                    fontFamily: 'montserrat_light',
                    color: '#000000',
                    marginLeft: 10,
                    marginRight: 10,
                    marginTop: 20,
                    borderRadius: 10,
                    padding: 20,
                    backgroundColor: '#CAE8A2'
                  }}>Note:Please note a full 10 years employment history must be provided,
                    or your employment history since leaving full time education if less than 10 years.
                    All gaps over 2 months in employment history must be detailed with a note of explanation</Text>

                  <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                    onPress={() => this.onSaveBtn()}
                  >
                    <Text style={styles.loginText}>SAVE</Text>
                  </TouchableOpacity>

                </View>




              </ScrollView>
            </View>



          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _addEmployeementHistory = () => {

    var employmentHistory = {}
    let tempList = [...this.state.employmentHistory]
    employmentHistory.employeerName = ''
    employmentHistory.address = ''
    employmentHistory.position = ''
    employmentHistory.startDate = ''
    employmentHistory.endDate = ''
    employmentHistory.reason = ''
    tempList.push(employmentHistory)
    this.setState({ employmentHistory: tempList, isDeleteOn: true });

  }

  _oNChangeDetails = (text, pos, type) => {

    if (type == 'employeer') {
      let tempList = [...this.state.employmentHistory]
      tempList[pos].employeerName = text
      this.setState({ employmentHistory: tempList });
    } else if (type == 'address') {
      let tempList = [...this.state.employmentHistory]
      tempList[pos].address = text
      this.setState({ employmentHistory: tempList });
    } else if (type == 'position') {
      let tempList = [...this.state.employmentHistory]
      tempList[pos].position = text
      this.setState({ employmentHistory: tempList });
    } else if (type == 'reason') {
      let tempList = [...this.state.employmentHistory]
      tempList[pos].reason = text
      this.setState({ employmentHistory: tempList });
    }

  }

  handleStartDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    let tempList = [...this.state.employmentHistory]
    tempList[this.state.listPos].startDate = dateTimeString
    tempList[this.state.listPos].endDate = ''
    this.setState({
      employmentHistory: tempList,
      isStartDatePickerVisible: false,
    });

  };

  handleEndDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    let tempList = [...this.state.employmentHistory]
    var ifStart = tempList[this.state.listPos].startDate;
    var startDateSplit = tempList[this.state.listPos].startDate.split("/");

    var start = new Date(startDateSplit[2] + "-" + startDateSplit[1] + "-" + startDateSplit[0]);
    var end = new Date(date);

    if (ifStart !== '') {
      if (start.getTime() > end.getTime()) {
        tempList[this.state.listPos].endDate = ''
        this.setState({
          employmentHistory: tempList,
          isEndDatePickerVisible: false,
        });
        setToastMsg("Date must be greater than start date.");
        console.log("end date is smaller");
      } else {
        tempList[this.state.listPos].endDate = dateTimeString
        this.setState({
          employmentHistory: tempList,
          isEndDatePickerVisible: false,
        })
        console.log("end date is greater");
      }
    } else {
      this.setState({ isEndDatePickerVisible: false });
      setToastMsg("Please select start date first.");
      console.log("start date not selected yet.");
    }

  };

  deleteEmploymentHistory = (e) => {

    Alert.alert(
      "Confirm to Delete?",
      'Are you sure you want to delete this history.',
      [
        { text: 'NO', onPress: () => { console.log('No pressed') } },
        {
          text: 'YES', onPress: () => {
            var array = [...this.state.employmentHistory];
            var index = array.indexOf(e)
            if (index !== -1) {
              array.splice(index, 1);
              this.setState({ employmentHistory: array });
            }
            console.log(this.state.employmentHistory.indexOf(e));

            if (this.state.employmentHistory.length == 1) {
              this.setState({ isDeleteOn: false });
            }
          }
        },

      ],
      { cancelable: false },
    )

  }


}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#585858',
    textAlign: 'center',
    marginTop: 10
  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }

});

export default connect(mapStateToPrpos)(WorkerEmploymentHistory);