/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView,
  Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import { StackActions, NavigationActions } from 'react-navigation';
import CustomTextInput from '../../../components/CustomTextInput';
import Api from '../../../apis/Api';
import AppConstants from '../../../apis/AppConstants';
import { androidCameraPermission } from '../../../../permissions';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import ImagePicker from 'react-native-image-crop-picker';
import Loader from '../../../utils/Loader';
import { setToastMsg } from '../../../utils/ToastMessage';
import moment from 'moment';
import { connect } from 'react-redux';


const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
  }
}

const { width } = Dimensions.get('window');


class WorkerEmploymentAct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      is24Hour: false,
      how_heard: '',
      signature: '',
      additional_info: '',
      date: '',
      isSignUpdate: '0',
      isLoading: false,
      isDatePickerVisible: false,

      previousData: this.props.navigation.getParam('previousData', 'NO-item'),
      isUpdate: this.props.navigation.getParam('isUpdate', 'NO-item'),

      setErrors: {
        field: '',
        message: ''
      },

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    if (this.state.isUpdate == "1") {
      this.setState({
        how_heard: this.state.previousData.how_heard,
        additional_info: this.state.previousData.additional_info,
        date: this.state.previousData.date,
        signature: this.state.previousData.sign,
      })
      if (this.state.previousData.is_48hours.toString() == "WISH") {
        this.setState({ is24Hour: true })
      }
    }
  }

  onSelectImage = async () => {
    const permissionStatus = await androidCameraPermission()

    if (permissionStatus || Platform.OS == 'ios') {
      Alert.alert(
        'Upload Signature',
        'Choose an option',
        [
          { text: 'Close', onPress: () => console.log('Close pressed') },
          { text: 'Camera', onPress: () => this.onCamera() },
          { text: 'Gallery', onPress: () => this.onGallery() },
        ]
      )
    }
  }

  onCamera = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
      freeStyleCropEnabled: true,
      includeBase64: false,
    }).then(image => {
      console.log(image);
      this.setState({
        signature: image.path,
        isSignUpdate: "1",
      })
    }).catch((err) => {
      console.log("openCamera erroe" + err.toString())
      setToastMsg(err.toString());
    });

  }

  onGallery = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      cropping: true,
      freeStyleCropEnabled: true,
      includeBase64: false,
    }).then(image => {
      console.log(image);
      this.setState({
        signature: image.path,
        isSignUpdate: "1",
      })
    }).catch((err) => {
      console.log("openGallery erroe" + err.toString())
      setToastMsg(err.toString());
    });

  }

  checkValidation = () => {
    let howHeardError = { field: '', message: '' }
    let signatureError = { field: '', message: '' }
    let additionalError = { field: '', message: '' }
    let dateError = { field: '', message: '' }

    if (this.state.how_heard == '') {
      howHeardError.field = "how_heard";
      howHeardError.message = "How did you here field is required!";
      this.setState({ setErrors: howHeardError })
    } else if (this.state.signature == '') {
      signatureError.field = "signature";
      signatureError.message = "Please add signature!";
      this.setState({ setErrors: signatureError })
    } else if (this.state.additional_info == '') {
      additionalError.field = "additional_info";
      additionalError.message = "Please provide some additional informations!";
      this.setState({ setErrors: additionalError })
    } else if (this.state.date == '') {
      dateError.field = "date";
      dateError.message = "Date is required!";
      this.setState({ setErrors: dateError })
    } else {
      this.setState({ setErrors: { field: '', message: '' } })
      this.onSaveBtn();
    }
  }

  onSaveBtn = () => {
    this.setState({ isLoading: true })

    let is24HourRec = "NOT WISH";
    if (this.state.is24Hour) {
      is24HourRec = "WISH"
    }

    console.log("--------------------")
    console.log(is24HourRec.toString())
    console.log(this.state.how_heard.toString())
    console.log(this.state.additional_info.toString())
    console.log(this.state.date.toString())


    let formData = new FormData();
    formData.append('worker_id', this.props.userid);
    formData.append('user_id', this.props.userid);
    formData.append('is_48hours', is24HourRec.toString());
    formData.append('how_heard', this.state.how_heard.toString());
    if (this.state.isSignUpdate == "1") {
      if (this.state.signature != "") {
        formData.append('sign', {
          name: 'photo.jpg',
          type: 'image/jpg',
          uri: this.state.signature,
        });
      }
    }
    formData.append('additional_info', this.state.additional_info.toString());
    formData.append('date', this.state.date.toString());

    Api._workerEmploymentActUpdate(formData)
      .then((response) => {
        console.log(response)

        this.setState({ isLoading: false })
        setToastMsg(response.message.toString());
        this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });

  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15, textTransform: 'uppercase' }}>Employment Act</Text>

              </View>
              <Loader isLoading={this.state.isLoading} />
              <ScrollView>
                <View style={{}}>
                  <Text style={styles.heading_mail}>Employment Act and Sign</Text>

                  <View style={{ marginLeft: 50, marginRight: 50 }}>
                    <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 20, marginTop: 20 }}>

                      <View>
                        <Text
                          style={{
                            color: "#393FA0",
                            marginLeft: 15,
                            paddingRight: 15,
                            paddingLeft: 5,
                            padding: 5,
                            fontSize: 13,
                            fontFamily: "montserrat_regular"
                          }}
                        >Do you wish to work more than 48 hrs per week?</Text>


                        <View style={{ flexDirection: 'row' }}>
                          <TouchableOpacity style={{ alignItems: 'center', marginLeft: 15, marginBottom: 10 }}
                            onPress={() => this.setState({ is24Hour: !this.state.is24Hour })}
                          >
                            <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                              {
                                (this.state.is24Hour) ?
                                  (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_on_icn.png')} />)
                                  :
                                  (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_off_icn.png')} />)
                              }
                              <Text
                                style={{
                                  color: "#000000",
                                  fontSize: 15,
                                  fontFamily: "montserrat_regular",
                                  padding: 10
                                }}
                              >I WISH</Text>
                            </View>

                          </TouchableOpacity>
                          <TouchableOpacity style={{ alignItems: 'center', marginLeft: 15, marginBottom: 10 }}
                            onPress={() => this.setState({ is24Hour: !this.state.is24Hour })}
                          >
                            <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                              {
                                (this.state.is24Hour) ?
                                  (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_off_icn.png')} />)
                                  :
                                  (<Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../../assets/selectshift_on_icn.png')} />)
                              }
                              <Text
                                style={{
                                  color: "#000000",
                                  fontSize: 15,
                                  fontFamily: "montserrat_regular",
                                  padding: 10
                                }}
                              >NOT WISH</Text>
                            </View>

                          </TouchableOpacity>
                        </View>

                      </View>

                    </View>

                    <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 20, marginTop: 20 }}>

                      <View>
                        <Text
                          style={{
                            color: "#393FA0",
                            marginLeft: 15,
                            paddingRight: 15,
                            paddingLeft: 5,
                            padding: 5,
                            fontSize: 13,
                            fontFamily: "montserrat_regular"
                          }}
                        >How did you hear about ABCD Corp?{"\n"} Please provide detail:</Text>
                        <TextInput
                          style={{ height: 140, textAlignVertical: "top", color: '#000000', fontSize: 15, marginLeft: 15, padding: 5 }}
                          multiline={true}
                          placeholder="Input Detail"
                          onChangeText={(text) => this.setState({ how_heard: text })}
                          value={this.state.how_heard}
                          placeholderTextColor='#585858'
                        />


                      </View>

                    </View>
                    {
                      this.state.setErrors.field == "how_heard" && (
                        <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                      )
                    }

                    <View style={{ borderColor: '#00D5E1', borderWidth: 1, borderRadius: 20, marginTop: 20 }}>

                      <View>
                        <Text
                          style={{
                            color: "#393FA0",
                            marginLeft: 15,
                            paddingRight: 15,
                            paddingLeft: 5,
                            padding: 5,
                            fontSize: 13,
                            fontFamily: "montserrat_regular"
                          }}
                        >Signed</Text>

                        <TouchableOpacity style={{ alignSelf: 'center', alignItems: 'center' }}
                          onPress={() => this.onSelectImage()}
                        >
                          <View>
                            {
                              this.state.signature != "" ?
                                (this.state.isSignUpdate == "0" ?
                                  <Image style={{ height: 50, width: 80, padding: 10, alignSelf: 'center' }}
                                    source={{ uri: AppConstants.IMAGE_URL + this.state.signature }} resizeMode='stretch' /> :
                                  <Image style={{ height: 50, width: 80, padding: 10, alignSelf: 'center' }}
                                    source={{ uri: this.state.signature }} resizeMode='stretch' />)
                                :
                                <Image style={{ height: 50, width: 80, padding: 10, alignSelf: 'center' }}
                                  source={require('./../../../assets/upload_img.png')} resizeMode='stretch' />
                            }

                            <Text
                              style={{ color: "#000000", fontSize: 15, fontFamily: "montserrat_regular", padding: 10, marginTop: 10 }}
                            >Upload Signature</Text>
                          </View>

                        </TouchableOpacity>
                      </View>

                    </View>
                    {
                      this.state.setErrors.field == "signature" && (
                        <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                      )
                    }

                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='ANY ADDITIONAL INFORMATION'
                        keyboardType='default'
                        floatingText="ANY ADDITIONAL INFORMATION"
                        onFocus={true}
                        style={{ textAlignVertical: "top", height: 150, }}
                        value={this.state.additional_info}
                        returnKeyType={'done'}
                        onChangeText={(text) => {
                          this.setState({
                            additional_info: text,
                          });
                        }}
                      />
                    </View>
                    {
                      this.state.setErrors.field == "additional_info" && (
                        <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                      )
                    }

                    <View style={{ marginTop: 5, }}>
                      <CustomTextInput
                        placeholder='DATE'
                        keyboardType='default'
                        onFocus={true}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        isVisibleCalendar={true}
                        floatingText="DATE"
                        value={this.state.date}
                        returnKeyType={'done'}
                        onTouchEnd={() => this.setState({ isDatePickerVisible: true })}
                      />
                      <DateTimePickerModal
                        isVisible={this.state.isDatePickerVisible}
                        mode="date"
                        date={new Date()}
                        onConfirm={(date) => this.handleConfirm(date)}
                        onCancel={() => this.setState({ isDatePickerVisible: false })}
                      />
                    </View>
                    {
                      this.state.setErrors.field == "date" && (
                        <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                      )
                    }

                  </View>

                  <Text style={{
                    fontSize: 12,
                    fontFamily: 'montserrat_light',
                    color: '#000000',
                    marginLeft: 20,
                    marginRight: 20,
                    marginTop: 20,
                    borderRadius: 10,
                    padding: 20,
                    backgroundColor: '#CAE8A2'
                  }}>Note:This employment is not exempt from the provisioms of the rehabilition of young
                    offenders Act 1947 you are not therefore entailed to with hold information requested by the
                    company about any previous convictions in this country or abroad you may have, even if in
                    other circumstances these would appear spent.I confirm that the information I have given
                    is true. I understand that if information given on the application from is found to be false..
                  </Text>


                  <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                    onPress={() => this.checkValidation()}
                  >
                    <Text style={styles.loginText}>SAVE</Text>
                  </TouchableOpacity>
                </View>

              </ScrollView>
            </View>



          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  handleConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    this.setState({
      date: dateTimeString,
      isDatePickerVisible: false,
    })

    console.log(dateTimeString)
  };

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center'
  },
  logoImage: {
    width: 230,
    height: 80,
    alignSelf: 'center',
    position: 'absolute',
    top: 75


  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  heading_mail: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#585858',
    textAlign: 'center',
    marginTop: 10
  },
  heading_type: {
    alignSelf: 'center',
    fontSize: 15,
    fontFamily: 'montserrat_regular',
    color: '#000000',
    marginTop: 7

  },

  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  forgotPassHold: {
    paddingTop: 10,
    paddingBottom: 30,
    alignSelf: 'flex-end',
    marginRight: 20

  },
  forgotPassDescription: {
    color: '#585858',
    fontSize: 13,
    fontFamily: "montserrat_regular",
    textAlign: 'center'

  },
  loginHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginLeft: 3,
    flex: 1
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  registerHold: {
    padding: 10,
    marginTop: 20,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginRight: 3,
    flex: 1
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_regular",
    alignSelf: 'center'

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }

});

export default connect(mapStateToPrpos)(WorkerEmploymentAct);