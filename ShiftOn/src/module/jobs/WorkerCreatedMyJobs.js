/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity,
    ScrollView,
    Alert,
    SafeAreaView,
    Dimensions,
    TextInput, ToastAndroid,
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import Api from '../../apis/Api';
import { setToastMsg } from '../../utils/ToastMessage';
import Loader from '../../utils/Loader';
import { connect } from 'react-redux';


const mapStateToPrpos = (props) => {
    return {
        userName: props.userLoginDetails.name,
        userid: props.userLoginDetails.id,
    }
}

class WorkerCreatedMyJobs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            apiData: [],
            isLoading: true,

        };
        console.log(this.props.navigation);
    }


    componentDidMount() {
        this.navFocusListener = this.props.navigation.addListener('didFocus', () => {
            this._workerMyJobListing();
        });
    }

    componentWillUnmount() {
        this.navFocusListener.remove();
    }

    _workerMyJobListing = () => {
        this.setState({ isLoading: true })

        let rawData = {
            "user_id": this.props.userid,
        }
        Api._userCalenderListApi(rawData)
            .then((response) => {
                console.log(response.data.WorkerRefrences)

                this.setState({ apiData: response.data.WorkerRefrences, isLoading: false, })
                //setToastMsg(response.message.toString());

            })
            .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false, })
            });
    }


    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
                <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

                    <View style={styles.mainContainer}>
                        <View style={{ backgroundColor: '#00D5E1' }}>
                            <TouchableOpacity
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                                onPress={() => this.props.navigation.goBack()}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                            </TouchableOpacity>
                            <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>MY JOBS</Text>
                            <TouchableOpacity
                                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20, right: 0 }}
                                onPress={() => this.props.navigation.navigate('WorkerAddNewCalender')}
                            >
                                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/more_icn.png')} />
                            </TouchableOpacity>
                        </View>

                        <Loader isLoading={this.state.isLoading} />

                        <View style={{ margin: 20, }}>

                            {
                                this.state.apiData.length > 0 ?

                                    (<FlatList
                                        style={{ width: '100%', marginBottom: 40 }}
                                        data={this.state.apiData}
                                        keyExtractor={(item, index) => index.toString()}
                                        //ItemSeparatorComponent={this.ItemSeparator}
                                        renderItem={({ item, index }) =>

                                            <TouchableOpacity activeOpacity={1} >
                                                <View style={styles.rowContainer}>

                                                    <TouchableOpacity
                                                        activeOpacity={1}
                                                        style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                                                        <Image style={{ height: 40, width: 42, tintColor: '#000000' }} source={require('./../../assets/jobs_icn.png')} />
                                                    </TouchableOpacity>
                                                    <TouchableOpacity activeOpacity={1}
                                                        style={{ width: "80%" }}>
                                                        <Text
                                                            style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                                                        >{item.comment}</Text>

                                                        <View style={{ marginTop: 5, flexDirection: "row" }}>
                                                            <Image source={require('./../../assets/pin_icn.png')} style={{ width: 10, height: 15, alignSelf: 'center' }} resizeMode='stretch' />
                                                            <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>
                                                                {item.location} </Text>
                                                        </View>

                                                        <View style={{ marginTop: 5, flexDirection: "row", flex: 1.5, marginRight: 2 }}>
                                                            <Image source={require('./../../assets/calendar_icn_b.png')} style={{ width: 12, height: 12, alignSelf: 'center' }} resizeMode='stretch' />
                                                            <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>{item.start_date} to {item.finish_date}</Text>
                                                        </View>
                                                        <View style={{ marginTop: 5, flexDirection: "row", flex: 1, marginLeft: 2 }}>
                                                            <Image source={require('./../../assets/time_icn_a.png')} style={{ width: 12, height: 12, alignSelf: 'center' }} resizeMode='stretch' />
                                                            <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>{item.start_time} to {item.end_time}</Text>
                                                        </View>

                                                    </TouchableOpacity>

                                                </View>
                                            </TouchableOpacity>

                                        }
                                    />)
                                    :
                                    <View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                                        <Text
                                            numberOfLines={1}
                                            style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                                        >No Data Available!</Text>
                                    </View>
                            }

                        </View>

                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FFFFFF',
        flex: 1,

    },

    tabHolder: {
        padding: 5,
        backgroundColor: "#00D5E1",
        borderRadius: 30,
        flex: 1
    },
    rowContainer: {
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingVertical: 5,
        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        borderColor: '#EAEAEA',
        borderWidth: 2,
        marginBottom: 5


    },


});

export default connect(mapStateToPrpos)(WorkerCreatedMyJobs);