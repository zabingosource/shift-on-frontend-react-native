/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView,
  Dimensions, TextInput, ToastAndroid,
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import ReactNativeModal from 'react-native-modal';
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';
import { connect } from 'react-redux';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
  }
}


class UnitAvailabilityList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      topDetailData: this.props.navigation.getParam('backPageDetails', 'NO-item'),
      backAgencyId: this.props.navigation.getParam('backAgencyId', 'NO-item'),
      search_keyword: '',
      selectedTab: '1',
      apiData: [],
      apiPendingData: [],
      apiAcceptedData: [],
      apiDeclinedData: [],
      isLoading: false,

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    this._unitRepostedJobListing();
  }

  _unitRepostedJobListing = () => {
    this.setState({ isLoading: true })
    let rawData = {
      "job_id": this.state.topDetailData.job_id,
      "unit_id": this.state.topDetailData.unit_id,
    }

    Api._unitRepostedJobListing(rawData)
      .then((response) => {

        console.log(JSON.stringify(response.data));

        this.apiApplicantData(response.data);
        this.apiAcceptedData(response.data);
        this.apiDeclinedData(response.data);

        this.setState({ isLoading: false });
        //setToastMsg(response.message.toString());

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  apiApplicantData = (responseData) => {
    var tempData = [];
    for (var i = 0; i < responseData.length; i++) {
      if (responseData[i].status.toString() == "") {
        if (responseData[i].worker != null) {
          tempData.push(responseData[i])
        }
      }
    }
    this.setState({ apiPendingData: tempData, apiData: tempData })
  }

  apiAcceptedData = (responseData) => {
    var tempData = [];
    for (var i = 0; i < responseData.length; i++) {
      if (responseData[i].status.toString() == "1") {
        if (responseData[i].worker != null) {
          tempData.push(responseData[i])
        }
      }
    }
    this.setState({ apiAcceptedData: tempData })
  }

  apiDeclinedData = (responseData) => {
    var tempData = [];
    for (var i = 0; i < responseData.length; i++) {
      if (responseData[i].status.toString() == "0") {
        if (responseData[i].worker != null) {
          tempData.push(responseData[i])
        }
      }
    }
    this.setState({ apiDeclinedData: tempData })
  }

  //// on button accepted/rejected click...
  _unitRepostedAcceptedRejected = (worker_id, agency_id, status) => {
    this.setState({ isLoading: true });
    let rawData = {
      "job_id": this.state.topDetailData.job_id,
      "worker_id": worker_id,
      "unit_id": this.props.userid,
      "agency_id": agency_id,
      "status": status,
    }

    console.log(rawData);

    Api._unitRepostedAcceptedRejected(rawData)
      .then((response) => {

        console.log(response.data)

        setToastMsg(response.message.toString());
        if (response.status.toString() == "1") {
          this._unitRepostedJobListing();
        }

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

          <View style={styles.mainContainer}>
            <View style={{ backgroundColor: '#00D5E1' }}>
              <TouchableOpacity
                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                //onPress={() => this._showModal(true)}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
              </TouchableOpacity>
              <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>AVAILABILITY LIST</Text>

            </View>
            <Loader isLoading={this.state.isLoading} />

            <TouchableOpacity activeOpacity={0.9} >
              <View style={styles.headerContainer}>
                {
                  (this.state.topDetailData.job_details.image) ?
                    <Image style={{ height: 90, width: 90, borderRadius: 50, alignSelf: 'center', padding: 5 }} source={{ uri: AppConstants.IMAGE_URL + this.state.topDetailData.job_details.image }} />
                    :
                    <Image style={{ height: 90, width: 90, borderRadius: 50, alignSelf: 'center', padding: 5 }} source={require('./../../assets/hospital_green.jpg')} />
                }

                <View style={{ width: "65%", padding: 10, }}>
                  <View style={{ flexDirection: 'row' }}>
                    {/* <Text
                      numberOfLines={1}
                      style={{ color: '#D90000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                    >001</Text> */}
                    <Text
                      numberOfLines={1}
                      style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                    >{this.state.topDetailData.job_details.job_title}</Text>
                  </View>

                  <Text
                    numberOfLines={1}
                    style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                  >{this.state.topDetailData.job_details.job_description} </Text>
                  <View style={{ marginTop: 5, flexDirection: "row" }}>
                    <Image source={require('./../../assets/pin_icn.png')} style={{ width: 10, height: 15 }} resizeMode='stretch' />
                    <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>
                      {this.state.topDetailData.job_details.location}{this.state.topDetailData.job_details.landmark ? ", " + this.state.topDetailData.job_details.landmark : ''}{this.state.topDetailData.job_details.zip ? ", " + this.state.topDetailData.job_details.zip : ''}
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'column' }}>
                    <View style={{ marginTop: 5, flexDirection: "row", marginRight: 2 }}>
                      <Image source={require('./../../assets/calendar_icn_b.png')} style={{ width: 10, height: 10 }} resizeMode='stretch' />
                      <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>{this.state.topDetailData.job_details.shift.start_date} to {this.state.topDetailData.job_details.shift.end_date}</Text>
                    </View>
                    <View style={{ marginTop: 5, flexDirection: "row" }}>
                      <Image source={require('./../../assets/time_icn_a.png')} style={{ width: 10, height: 10 }} resizeMode='stretch' />
                      <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12 }}>{this.state.topDetailData.job_details.shift.start_time} to {this.state.topDetailData.job_details.shift.end_time}</Text>
                    </View>

                  </View>

                </View>

                <View style={{ marginTop: 5 }}>
                  <Text
                    style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 7, alignSelf: 'center' }}
                  >REQUIRED</Text>
                  <Text
                    style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 15, alignSelf: 'center' }}
                  >{this.state.topDetailData.job_details.required_people}</Text>

                </View>

              </View>
            </TouchableOpacity>


            <View style={{ marginLeft: 10, marginRight: 10 }}>

              {
                this._returnTab()
              }

              {/* <View style={{ flexDirection: 'row', marginTop: 10, backgroundColor: '#EAEAEA', borderRadius: 40 }}>

                <Image style={{ height: 30, width: 30, padding: 7, position: 'absolute', end: 5, top: 3.5 }} source={require('./../../assets/search_icn.png')} />
                <TextInput
                  style={{ width: '85%', padding: 5, marginLeft: 10, marginRight: 40, color: '#969696', fontFamily: 'montserrat_regular', fontSize: 15 }}
                  placeholder='Search by agency name'
                  keyboardType='default'
                  value={this.state.search_keyword}
                  returnKeyType={'done'}
                  onChangeText={(text) => {
                    this.setState({
                      search_keyword: text,
                    });
                  }}
                />

              </View> */}

              {
                this.apiDataListing()
              }

            </View>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }


  _onChangeTab(tab) {
    this.setState({ selectedTab: tab });
  }

  apiDataListing = () => {

    if (this.state.selectedTab == "1") {
      return (

        this.state.apiPendingData.length > 0 ?

          <View style={{ marginTop: 10, marginBottom: 20 }}>
            <FlatList
              style={{ width: '100%', marginBottom: 370 }}
              data={this.state.apiPendingData}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item, index }) =>

              (<TouchableOpacity activeOpacity={0.9}
                onPress={() => this.props.navigation.navigate('WorkerWebView', { workerId: item.worker.id })}
              >

                <View style={styles.rowContainer}>

                  {
                    (item.worker.worker_details.image) ?
                      <Image style={{ height: 120, width: 100, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={{ uri: AppConstants.IMAGE_URL + item.worker.worker_details.image }} resizeMode='cover' />
                      :
                      <Image style={{ height: 120, width: 100, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={require('./../../assets/woman_face.jpeg')} resizeMode='cover' />
                  }
                  <View style={{ width: "55%", paddingHorizontal: 10 }}>
                    <Text
                      numberOfLines={1}
                      style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                    >{item.worker.worker_details.fore_name} {item.worker.worker_details.sur_name}</Text>

                    <Text
                      numberOfLines={1}
                      style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 18, }}
                    >{item.worker.worker_details.applied_for}</Text>

                    <View style={{ width: '100%', flexDirection: 'row', marginTop: 10 }}>
                      <TouchableOpacity style={styles.loginHold}
                        onPress={() => this._unitRepostedAcceptedRejected(item.worker_id, item.agency_id, "1")}
                      >
                        <Text style={styles.loginText}>ACCEPT</Text>
                      </TouchableOpacity>

                      <TouchableOpacity style={styles.registerHold}
                        onPress={() => this._unitRepostedAcceptedRejected(item.worker_id, item.agency_id, "0")}
                      >
                        <Text style={styles.registerText}>REJECT</Text>
                      </TouchableOpacity>

                    </View>

                  </View>

                  {/* <View style={{ marginTop: 5 }}>
                    <Image style={{ height: 40, width: 70, padding: 5, alignSelf: 'center' }} source={require('./../../assets/whb_icn.png')} resizeMode='stretch' />

                  </View> */}

                </View>
              </TouchableOpacity>)
              }
            />
          </View>
          :
          <View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
            <Text
              numberOfLines={1}
              style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
            >No Data Available!</Text>
          </View>
      )
    } else if (this.state.selectedTab == "2") {
      return (

        this.state.apiAcceptedData.length > 0 ?

          <View style={{ marginTop: 10, marginBottom: 20 }}>
            <FlatList
              style={{ width: '100%', marginBottom: 370 }}
              data={this.state.apiAcceptedData}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item, index }) =>

              (<TouchableOpacity activeOpacity={0.9} >

                <View style={styles.rowContainer}>
                  {
                    (item.worker.worker_details.image) ?
                      <Image style={{ height: 120, width: 100, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={{ uri: AppConstants.IMAGE_URL + item.worker.worker_details.image }} resizeMode='cover' />
                      :
                      <Image style={{ height: 120, width: 100, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={require('./../../assets/woman_face.jpeg')} resizeMode='cover' />
                  }
                  <View style={{ width: "55%", padding: 10, }}>
                    <Text
                      numberOfLines={1}
                      style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                    >{item.worker.worker_details.fore_name} {item.worker.worker_details.sur_name}</Text>

                    <Text
                      numberOfLines={1}
                      style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 18, }}
                    >{item.worker.worker_details.applied_for}</Text>

                  </View>

                  {/* <View style={{ marginTop: 5 }}>
                    <Image style={{ height: 40, width: 70, padding: 5, alignSelf: 'center' }} source={require('./../../assets/whb_icn.png')} resizeMode='stretch' />

                  </View> */}

                </View>
              </TouchableOpacity>)
              }
            />
          </View>
          :
          <View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
            <Text
              numberOfLines={1}
              style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
            >No Data Available!</Text>
          </View>
      )
    } else {
      return (

        this.state.apiDeclinedData.length > 0 ?

          <View style={{ marginTop: 10, marginBottom: 20 }}>
            <FlatList
              style={{ width: '100%', marginBottom: 370 }}
              data={this.state.apiDeclinedData}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item, index }) =>

              (<TouchableOpacity activeOpacity={0.9} >

                <View style={styles.rowContainer}>
                  {
                    (item.worker.worker_details.image) ?
                      <Image style={{ height: 120, width: 100, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={{ uri: AppConstants.IMAGE_URL + item.worker.worker_details.image }} resizeMode='cover' />
                      :
                      <Image style={{ height: 120, width: 100, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={require('./../../assets/woman_face.jpeg')} resizeMode='cover' />
                  }
                  <View style={{ width: "55%", padding: 10, }}>
                    <Text
                      numberOfLines={1}
                      style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                    >{item.worker.worker_details.fore_name} {item.worker.worker_details.sur_name}</Text>

                    <Text
                      numberOfLines={1}
                      style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 18, }}
                    >{item.worker.worker_details.applied_for}</Text>

                  </View>

                  {/* <View style={{ marginTop: 5 }}>
                    <Image style={{ height: 40, width: 70, padding: 5, alignSelf: 'center' }} source={require('./../../assets/whb_icn.png')} resizeMode='stretch' />

                  </View> */}

                </View>
              </TouchableOpacity>)
              }
            />
          </View>
          :
          <View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
            <Text
              numberOfLines={1}
              style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
            >No Data Available!</Text>
          </View>
      )
    }

  }

  _returnTab() {

    return (
      <View style={{ flexDirection: 'row', marginTop: 10, backgroundColor: '#000000', borderRadius: 40 }}>
        <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "1" ? "#00D5E1" : "#000000" }]}
          activeOpacity={1}
          onPress={() => this._onChangeTab("1")}
        >
          <Text style={{ alignSelf: 'center', fontSize: 14, padding: 5, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "1" ? "#000000" : "#FFFFFF" }}>APPLICANTS</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "2" ? "#00D5E1" : "#000000" }]}
          activeOpacity={1}
          onPress={() => this._onChangeTab("2")}
        >
          <Text style={{ alignSelf: 'center', fontSize: 14, padding: 5, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "2" ? "#000000" : "#FFFFFF" }}>ACCEPTED</Text>
        </TouchableOpacity>

        <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "3" ? "#00D5E1" : "#000000" }]}
          activeOpacity={1}
          onPress={() => this._onChangeTab("3")}
        >
          <Text style={{ alignSelf: 'center', fontSize: 14, padding: 5, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "3" ? "#000000" : "#FFFFFF" }}>REJECTED</Text>
        </TouchableOpacity>

      </View>
    )

  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },

  tabHolder: {
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 30,
    flex: 1
  },
  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5
  },

  headerContainer: {
    height: 130,
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderColor: '#EAEAEA',
    borderBottomWidth: 2,
    marginBottom: 5,
    padding: 10


  },
  loginHold: {
    width: '60%',
    padding: 5,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginRight: 2,
    alignItems: 'center'
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 14,
    fontFamily: "montserrat_semibold",
    padding: 3

  },
  registerHold: {
    width: '60%',
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginLeft: 5,
    alignItems: 'center'
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 14,
    fontFamily: "montserrat_semibold",
    padding: 3,

  },
  radioIcon: {
    width: 20,
    height: 20,
    marginRight: 5,
    alignSelf: 'center'

  },
  filterListItem: {
    padding: 5,
    flexDirection: 'row',
    marginLeft: 8,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  starStyle: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    margin: 2

  },
});

export default connect(mapStateToPrpos)(UnitAvailabilityList);