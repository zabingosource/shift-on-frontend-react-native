/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity, ScrollView, Alert, SafeAreaView,
  Dimensions, TextInput, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import ReactNativeModal from 'react-native-modal';
import ControlerCalender from '../calendar/ControlerCalender';
import Api from '../../apis/Api';
import AppConstants from '../../apis/AppConstants';
import Loader from '../../utils/Loader';
import { setToastMsg } from '../../utils/ToastMessage';
import { connect } from 'react-redux';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
  return {
    userid: props.userLoginDetails.id,
    imgUrl: props.defaultImageUrl.url,
  }
}


class UnitAgencyAvailabilityList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      topDetailData: this.props.navigation.getParam('backPageDetails', 'NO-item'),
      search_keyword: '',
      selectedFilterTab: '1',
      modal_vissible: false,
      shortOptionDate: [],
      customStartDate: '',
      customEndDate: '',
      shortItem: '',
      shortItemValues: 'all_time',
      selectedStar: [false, false, false, false, false],
      agencyDataListing: [],
      searchQueryData: [],
      isLoading: false,
      isStartDatePickerVisible: false,
      isEndDatePickerVisible: false,
      compareDateValues: '',
    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    var shortOptionDate = [
      {
        option: 'All Time',
        optionValue: 'all_time',
        isSelected: false
      }, {
        option: 'Last Month',
        optionValue: 'last_month',
        isSelected: false
      }, {
        option: 'Last Week',
        optionValue: 'last_week',
        isSelected: false
      }, {
        option: 'Last 24 Hrs',
        optionValue: 'last_24_hrs',
        isSelected: false
      }, {
        option: 'Custom',
        optionValue: 'custom',
        isSelected: false
      },
    ];
    var temp = []
    this.setState({ shortOptionDate: [] });
    if (!this.state.shortOptionDate.length > 0) {
      for (var i = 0; i < shortOptionDate.length; i++) {
        temp.push(shortOptionDate[i])
      }
      this.setState({ shortOptionDate: temp });
    }

    this._unitJobAgencyAvailabilityListing();

  }

  _unitJobAgencyAvailabilityListing = () => {
    this.setState({ isLoading: true });

    let rawData = {
      "unit_id": this.props.userid,
      "job_id": this.state.topDetailData.id,
      "filter": this.state.shortItemValues,
      "from": this.state.customStartDate,
      "to": this.state.customEndDate,
    }
    Api._unitJobAgencyAvailabilityListing(rawData)
      .then((response) => {

        console.log(response)
        var tempData = [];
        for (var i = 0; i < response.data.length; i++) {
          if (response.data[i].agency_status.toString() == "1" && response.data[i].agency != null) {
            tempData.push(response.data[i])
          }
        }

        this.setState({
          isLoading: false,
          agencyDataListing: tempData,
          searchQueryData: tempData,
        })

        //setToastMsg(response.message.toString());

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });
  }

  _unitJobPostToAgency = (id) => {
    this.setState({ isLoading: true })

    let rawData = {
      "job_id": this.state.topDetailData.id,
      "unit_id": this.props.userid,
      "agency_id": id,
    }
    Api._unitJobPostToAgency(rawData)
      .then((response) => {

        console.log(response)
        setToastMsg(response.message.toString());
        this._unitJobAgencyAvailabilityListing();
        //this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });
  }

  _unitJobOpenClose = (status) => {

    Alert.alert(
      '',
      'Are you sure, you want to close this job?',
      [
        { text: 'NO', onPress: () => console.log('Close pressed') },
        {
          text: 'YES', onPress: () => {

            this.setState({ isLoading: true })

            let rawData = {
              "id": this.state.topDetailData.id,
              "status": status
            }
            Api._unitJobOpenClose(rawData)
              .then((response) => {

                console.log(response)
                setToastMsg(response.message.toString());
                this.props.navigation.goBack();

              })
              .catch((err) => {
                console.log(err);
                setToastMsg("Somthing went wrong.");
                this.setState({ isLoading: false })
              });

          }
        },
      ]
    )
  }

  searchFilter = (text) => {
    if (text) {
      const newData = this.state.searchQueryData.filter((item) => {
        const itemData = item.agency.name ? item.agency.name.toUpperCase() : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      this.setState({ agencyDataListing: newData, search_keyword: text })
    } else {
      this.setState({ agencyDataListing: this.state.searchQueryData, search_keyword: text })
    }
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

          <View style={styles.mainContainer}>
            <View style={{ backgroundColor: '#00D5E1' }}>
              <TouchableOpacity
                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                //onPress={() => this._showModal(true)}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
              </TouchableOpacity>
              <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>AVAILABILITY LIST</Text>

            </View>
            <Loader isLoading={this.state.isLoading} />

            <TouchableOpacity
              activeOpacity={0.9}
            //onPress={() => this.props.navigation.navigate('Login')}
            >
              <View style={styles.headerContainer}>
                <TouchableOpacity activeOpacity={0.7}
                  style={{ width: "18%", justifyContent: 'center', alignItems: 'center' }}
                  onPress={() => this.props.navigation.navigate('ViewJobDetail', { jobData: this.state.topDetailData.id })}
                >
                  <Image style={{ height: 42, width: 40, tintColor: '#000000' }} source={require('./../../assets/shift2_icn.png')} />
                </TouchableOpacity>

                <View style={{ width: "67%" }}>
                  <View style={{ flexDirection: 'row' }}>

                    <Text
                      numberOfLines={1}
                      style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                    >{this.state.topDetailData.job_title}</Text>
                  </View>

                  <Text
                    numberOfLines={1}
                    style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                  >{this.state.topDetailData.job_description} </Text>
                  <View style={{ marginTop: 5, flexDirection: "row" }}>
                    <Image source={require('./../../assets/pin_icn.png')} style={{ width: 10, height: 15 }} resizeMode='stretch' />
                    <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>
                      {this.state.topDetailData.location}{this.state.topDetailData.landmark ? ", " + this.state.topDetailData.landmark : ''}{this.state.topDetailData.zip ? ", " + this.state.topDetailData.zip : ''}
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'column' }}>
                    <View style={{ marginTop: 5, flexDirection: "row", marginRight: 2 }}>
                      <Image source={require('./../../assets/calendar_icn_b.png')} style={{ width: 10, height: 10 }} resizeMode='stretch' />
                      <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>{this.state.topDetailData.shift.start_date} to {this.state.topDetailData.shift.end_date}</Text>
                    </View>
                    <View style={{ marginTop: 5, flexDirection: "row" }}>
                      <Image source={require('./../../assets/time_icn_a.png')} style={{ width: 10, height: 10 }} resizeMode='stretch' />
                      <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12 }}>{this.state.topDetailData.shift.start_time} to {this.state.topDetailData.shift.end_time}</Text>
                    </View>

                  </View>

                </View>

                <View style={{ width: '15%', marginTop: 5 }}>
                  <Text
                    style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 7, alignSelf: 'center' }}
                  >REQUIRED</Text>
                  <Text
                    style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 15, alignSelf: 'center' }}
                  >{this.state.topDetailData.required_people}</Text>

                  {
                    this.state.topDetailData.status.toString() == "1" ?
                      <TouchableOpacity
                        style={{ backgroundColor: '#00D5E1', padding: 5, alignSelf: 'center', borderRadius: 5, marginTop: 15, }}
                        onPress={() => {
                          this._unitJobOpenClose('0')
                        }}
                      >
                        <Text style={{ alignSelf: 'center', textAlign: 'center', fontSize: 10, fontFamily: 'montserrat_regular', color: "#ffffff", }}>Close Job</Text>
                      </TouchableOpacity>
                      :
                      <View style={{ marginTop: 15, }}>
                        <Text style={{ fontSize: 11, textAlign: 'center', color: "red", fontFamily: 'montserrat_regular', marginBottom: 10, }}>Already Closed</Text>
                      </View>
                  }

                </View>

              </View>
            </TouchableOpacity>


            <ReactNativeModal
              style={{ justifyContent: 'center' }}
              isVisible={this.state.modal_vissible}
              onBackdropPress={() => this._showModal(!this.state.modal_vissible)}
              onBackButtonPress={() => this._showModal(!this.state.modal_vissible)}
            >
              <View style={{ backgroundColor: '#EBEBEB', marginLeft: 40, marginRight: 40, borderRadius: 10 }}>
                <Text style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 16, padding: 10, alignSelf: 'center', marginTop: 10 }}
                >Filter By</Text>

                <View style={{ flexDirection: 'row', margin: 10, backgroundColor: '#626262', borderRadius: 40 }}>
                  <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedFilterTab == "1" ? "#00D5E1" : "#626262" }]}
                    activeOpacity={1}
                    onPress={() => this._resetDateRatting("1")}
                  >
                    <Text style={{ alignSelf: 'center', fontSize: 16, padding: 10, fontFamily: 'montserrat_regular', color: this.state.selectedFilterTab == "1" ? "#000000" : "#FFFFFF" }}>DURATION</Text>
                  </TouchableOpacity>

                  {/* <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedFilterTab == "2" ? "#00D5E1" : "#626262" }]}
                    activeOpacity={1}
                    onPress={() => this._resetDateRatting("2")}
                  >
                    <Text style={{ alignSelf: 'center', fontSize: 16, padding: 10, fontFamily: 'montserrat_regular', color: this.state.selectedFilterTab == "2" ? "#000000" : "#FFFFFF" }}>RATING</Text>
                  </TouchableOpacity> */}

                </View>
                {
                  (this.state.selectedFilterTab == 1) ? (<View>
                    <FlatList
                      style={{ width: '100%', marginTop: 5 }}
                      data={this.state.shortOptionDate}
                      keyExtractor={(item, index) => index.toString()}
                      ItemSeparatorComponent={this.ItemSeparator}
                      renderItem={({ item, index }) =>

                        <TouchableOpacity
                          activeOpacity={0.7}
                          onPress={() => this._optionDate(item)}
                        >
                          <View style={styles.filterListItem}>
                            {
                              (item.isSelected) ?
                                (<Image style={styles.radioIcon} source={require('../../assets/selecttime_on_icn.png')} />)
                                :
                                // (<Image style={styles.radioIcon} source={require('../../assets/selecttime_off_icn.png')} />)
                                null
                            }
                            <View style={{ justifyContent: 'center' }}>
                              <Text style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 15, padding: 10, }}>{item.option}</Text>

                            </View>

                          </View>

                        </TouchableOpacity>
                      }
                    />
                    {
                      (this.state.shortItem == 'Custom') ? (<View>
                        <TextInput
                          editable={true}
                          style={{
                            fontSize: 18,
                            padding: 2,
                            backgroundColor: '#FFFFFF',
                            borderColor: '#00D5E1',
                            borderRadius: 20,
                            borderWidth: 2,
                            paddingLeft: 30,
                            paddingRight: 30,
                            marginLeft: 20,
                            marginRight: 20,
                            color: '#464646',
                            fontFamily: "montserrat_regular"
                          }}
                          placeholder={'DD/MM/YYYY'}
                          value={this.state.customStartDate}
                          showSoftInputOnFocus={false}
                          underlineColorAndroid="transparent"
                          autoCorrect={false}
                          accessible={false}
                          onTouchEnd={() => { this.setState({ isStartDatePickerVisible: true }) }}
                          caretHidden={true}
                        />
                        <DateTimePickerModal
                          isVisible={this.state.isStartDatePickerVisible}
                          mode="date"
                          date={new Date()}
                          onConfirm={(date) => this.handleStartDateConfirm(date)}
                          onCancel={() => this.setState({ isStartDatePickerVisible: false })}
                        />
                        <Text style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 16, padding: 10, alignSelf: 'center' }}
                        >TO</Text>
                        <TextInput
                          editable={true}
                          style={{
                            fontSize: 18,
                            padding: 2,
                            backgroundColor: '#FFFFFF',
                            borderColor: '#00D5E1',
                            borderRadius: 20,
                            borderWidth: 2,
                            paddingLeft: 30,
                            paddingRight: 30,
                            color: '#464646',
                            marginLeft: 20,
                            marginRight: 20,
                            fontFamily: "montserrat_regular"
                          }}
                          placeholder={'DD/MM/YYYY'}
                          value={this.state.customEndDate}
                          showSoftInputOnFocus={false}
                          underlineColorAndroid="transparent"
                          autoCorrect={false}
                          accessible={false}
                          onTouchEnd={() => { this.setState({ isEndDatePickerVisible: true }) }}
                          caretHidden={true}
                        />
                        <DateTimePickerModal
                          isVisible={this.state.isEndDatePickerVisible}
                          mode="date"
                          date={new Date()}
                          onConfirm={(date) => this.handleEndDateConfirm(date)}
                          onCancel={() => this.setState({ isEndDatePickerVisible: false, })}
                        />
                      </View>
                      ) : (<View></View>)
                    }


                  </View>
                  ) : (<View style={{ marginTop: 10, marginBottom: 10 }}>
                    <TouchableOpacity
                      style={{ alignSelf: "center", padding: 5 }}
                      onPress={() => this._selectStar(0)}
                    >
                      <View style={{ flexDirection: 'row' }}>
                        {
                          (this.state.selectedStar[0]) ? (<Image style={styles.radioIcon} source={require('../../assets/selecttime_on_icn.png')} />) : (<Image style={styles.radioIcon} source={require('../../assets/selecttime_off_icn.png')} />)
                        }
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{ alignSelf: "center", padding: 5 }}
                      onPress={() => this._selectStar(1)}
                    >
                      <View style={{ flexDirection: 'row' }}>
                        {
                          (this.state.selectedStar[1]) ? (<Image style={styles.radioIcon} source={require('../../assets/selecttime_on_icn.png')} />) : (<Image style={styles.radioIcon} source={require('../../assets/selecttime_off_icn.png')} />)
                        }
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{ alignSelf: "center", padding: 5 }}
                      onPress={() => this._selectStar(2)}
                    >
                      <View style={{ flexDirection: 'row' }}>
                        {
                          (this.state.selectedStar[2]) ? (<Image style={styles.radioIcon} source={require('../../assets/selecttime_on_icn.png')} />) : (<Image style={styles.radioIcon} source={require('../../assets/selecttime_off_icn.png')} />)
                        }
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{ alignSelf: "center", padding: 5 }}
                      onPress={() => this._selectStar(3)}
                    >
                      <View style={{ flexDirection: 'row' }}>
                        {
                          (this.state.selectedStar[3]) ? (<Image style={styles.radioIcon} source={require('../../assets/selecttime_on_icn.png')} />) : (<Image style={styles.radioIcon} source={require('../../assets/selecttime_off_icn.png')} />)
                        }
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_b.png')} style={styles.starStyle} resizeMode='stretch' />
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{ alignSelf: "center", padding: 5 }}
                      onPress={() => this._selectStar(4)}
                    >
                      <View style={{ flexDirection: 'row' }}>
                        {
                          (this.state.selectedStar[4]) ? (<Image style={styles.radioIcon} source={require('../../assets/selecttime_on_icn.png')} />) : (<Image style={styles.radioIcon} source={require('../../assets/selecttime_off_icn.png')} />)
                        }
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                        <Image source={require('./../../assets/rate_a.png')} style={styles.starStyle} resizeMode='stretch' />
                      </View>
                    </TouchableOpacity>

                  </View>

                  )
                }

                <View style={{ flexDirection: 'row', marginTop: 20, marginBottom: 30, marginRight: 20, marginLeft: 20 }}>
                  <TouchableOpacity style={styles.loginHold}
                    onPress={() => this._showModal(false, "cancel")}
                  >
                    <Text style={styles.loginText}>CANCEL</Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={styles.registerHold}
                    onPress={() => this._showModal(false, "apply")}
                  >
                    <Text style={styles.registerText}>APPLY</Text>
                  </TouchableOpacity>

                </View>
              </View>
            </ReactNativeModal>


            <View style={{ marginLeft: 10, marginRight: 10 }}>

              {
                this._returnTab()
              }


              <View style={{ flexDirection: 'row', marginTop: 10, backgroundColor: '#EAEAEA', borderRadius: 40 }}>

                <Image style={{ height: 30, width: 30, padding: 7, position: 'absolute', end: 5, top: 3.5 }} source={require('./../../assets/search_icn.png')} />

                <TextInput
                  style={{ width: '85%', paddingHorizontal: 8, paddingTop: 8, paddingBottom: 10, marginLeft: 10, marginRight: 40, color: '#969696', fontFamily: 'montserrat_regular', fontSize: 15 }}
                  placeholder='Search by agency name'
                  keyboardType='default'
                  value={this.state.search_keyword}
                  returnKeyType={'done'}
                  onChangeText={(text) => { this.searchFilter(text) }}
                />

              </View>

              <View style={{ marginTop: 10, marginBottom: 20 }}>

                {
                  (this.state.agencyDataListing.length > 0) ?

                    (<FlatList
                      style={{ width: '100%', marginBottom: 370 }}
                      data={this.state.agencyDataListing}
                      keyExtractor={(item, index) => index.toString()}
                      //ItemSeparatorComponent={this.ItemSeparator}
                      renderItem={({ item, index }) =>

                        <TouchableOpacity activeOpacity={0.9}
                          onPress={() => this.props.navigation.navigate('ViewAgencyDetail', { agencyId: item.agency.id })}
                        >
                          <View style={styles.rowContainer}>
                            <View style={{ width: '24%' }}>
                              {
                                (item.agency.image) ?
                                  (<Image style={{ height: 120, width: '100%', borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={{ uri: AppConstants.IMAGE_URL + item.agency.image }} resizeMode='cover' />)
                                  :
                                  (<Image style={{ height: 120, width: '100%', borderTopLeftRadius: 20, borderBottomLeftRadius: 20, padding: 10 }} source={{ uri: this.props.imgUrl }} resizeMode='cover' />)
                              }
                            </View>

                            <View style={{ width: "58%", padding: 7, }}>
                              <Text numberOfLines={1}
                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                              >{item.agency.name}</Text>
                              <Text
                                numberOfLines={1}
                                style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12, }}
                              >{item.agency.trading_name} </Text>

                              <View style={{ marginTop: 6, flexDirection: "row" }}>
                                <Image source={require('./../../assets/email_icon.png')} style={{ width: 12, height: 12, alignSelf: 'center', }} resizeMode='stretch' />
                                <Text numberOfLines={1}
                                  style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10, }}
                                >{item.agency.email} </Text>
                              </View>

                              <View style={{ marginTop: 3, flexDirection: "row" }}>
                                <Image source={require('./../../assets/pin_icn.png')} style={{ width: 10, height: 15, alignSelf: 'center' }} resizeMode='stretch' />
                                <Text numberOfLines={1}
                                  style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 11, }}
                                >{item.agency.address} </Text>
                              </View>

                              <View style={{ marginTop: 3, flexDirection: "row" }}>
                                <Image source={require('./../../assets/pin_icn.png')} style={{ width: 10, height: 15, alignSelf: 'center' }} resizeMode='stretch' />
                                <Text numberOfLines={1}
                                  style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 11, }}
                                >{item.agency.landmark ? item.agency.landmark : ''}{item.agency.post_code ? ", " + item.agency.post_code : ''}</Text>
                              </View>

                            </View>
                            <View style={{ width: '18%', marginTop: 5, alignItems: 'center' }}>

                              <Text style={{ color: '#FF7B68', fontFamily: 'montserrat_semibold', fontSize: 20, alignSelf: 'center' }}>
                                {item.workers_count}
                              </Text>

                              <Text style={{ marginTop: -2, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10, alignSelf: 'center' }}>
                                WORKERS
                              </Text>
                              {
                                item.posted == "0" ?
                                  (<TouchableOpacity style={{ padding: 2, backgroundColor: "#00D5E1", borderRadius: 10, alignSelf: 'center', marginTop: 35 }}
                                    onPress={() => {
                                      this.state.topDetailData.status.toString() == "0" ?
                                        setToastMsg("This job is already closed.")
                                        :
                                        this._unitJobPostToAgency(item.agency.id)
                                    }}
                                  >
                                    <Text style={styles.loginText}>POST</Text>
                                  </TouchableOpacity>)
                                  :
                                  (<TouchableOpacity activeOpacity={1} style={{ marginTop: 28, }}>
                                    <Text style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10, }}>Posted on:-</Text>
                                    <Text style={{ color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10, }}>
                                      {moment(item.posted_date).format('yyyy-MM-DD')}
                                    </Text>
                                  </TouchableOpacity>)
                              }
                            </View>

                          </View>
                        </TouchableOpacity>

                      }
                    />)
                    :
                    (<View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                      <Text
                        numberOfLines={1}
                        style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                      >No Data Available!</Text>
                    </View>)
                }


              </View>

              <View>



              </View>

            </View>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _resetDateRatting = (tab) => {
    this.setState({ selectedFilterTab: tab });
    if (tab == 1) {
      var starArray = [false, false, false, false, false]
      this.setState({ selectedStar: starArray });
    } else {
      var temp = []
      for (var i = 0; i < this.state.shortOptionDate.length; i++) {
        this.state.shortOptionDate[i].isSelected = false;
        temp.push(this.state.shortOptionDate[i])
      }
      this.setState({ shortOptionDate: temp, shortItem: '' });
    }


  }

  _selectStar = (statrno) => {

    if (statrno == 0) {
      var starArray = [true, false, false, false, false]
      this.setState({ selectedStar: starArray });
    } else if (statrno == 1) {
      var starArray = [false, true, false, false, false]
      this.setState({ selectedStar: starArray });
    } else if (statrno == 2) {
      var starArray = [false, false, true, false, false]
      this.setState({ selectedStar: starArray });
    } else if (statrno == 3) {
      var starArray = [false, false, false, true, false]
      this.setState({ selectedStar: starArray });
    } else if (statrno == 4) {
      var starArray = [false, false, false, false, true]
      this.setState({ selectedStar: starArray });
    }
  }

  _optionDate = (sortItem) => {
    var tempList = []
    for (var i = 0; i < this.state.shortOptionDate.length; i++) {
      if (sortItem.option == this.state.shortOptionDate[i].option) {
        this.state.shortOptionDate[i].isSelected = true;
      } else {
        this.state.shortOptionDate[i].isSelected = false;
      }
      tempList.push(this.state.shortOptionDate[i])
    }
    this.setState({
      shortOptionDate: tempList,
      shortItem: sortItem.option.trim(),
      shortItemValues: sortItem.optionValue.trim(),
    });

  }


  _showModal = (visible, button) => {
    if (button == "apply") {
      if (this.state.shortItem !== '') {
        if (this.state.shortItem == "Custom") {
          if (this.state.customEndDate !== '') {
            this.setState({ modal_vissible: visible });
            this._unitJobAgencyAvailabilityListing();
          } else {
            setToastMsg("Date fields can't be empty!");
          }
        } else {
          this.setState({ modal_vissible: visible });
          this._unitJobAgencyAvailabilityListing();
        }
      } else {
        setToastMsg("Please select any options!");
      }
    } else {
      this.setState({ modal_vissible: visible });
    }


  }

  _returnTab() {
    return (
      <View>
        <View style={{ flexDirection: 'row', marginTop: 10, backgroundColor: '#FFFFFF', alignSelf: 'center', }}>
          <Text style={{ fontSize: 14, padding: 7, fontFamily: 'montserrat_semibold', color: "#000000" }}>AGENCIES</Text>

        </View>
        <TouchableOpacity
          style={{ alignSelf: 'flex-end', position: 'absolute', marginTop: 10 }}
          onPress={() => this._showModal(true)}
        //onPress={() => alert("pressed")}
        >
          <Image style={{ height: 30, width: 70, padding: 5, alignSelf: 'center' }} source={require('./../../assets/filter_btn_a.png')} resizeMode='stretch' />

        </TouchableOpacity>
      </View>

    )

  }

  // this is for start date picker...
  handleStartDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    this.setState({
      customStartDate: dateTimeString,
      customEndDate: "",
      compareDateValues: date,
      isStartDatePickerVisible: false,
    })

    console.log(dateTimeString)
  };

  // this is for end date picker...
  handleEndDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    var start = new Date(this.state.compareDateValues);
    var end = new Date(date);

    if (this.state.compareDateValues != '') {
      if (start.getTime() > end.getTime()) {
        this.setState({
          isEndDatePickerVisible: false,
          customEndDate: "",
        });
        setToastMsg("Date must be greater than start date.");
        console.log("end date is smaller");
      } else {
        this.setState({
          customEndDate: dateTimeString,
          isEndDatePickerVisible: false,
        })
        console.log("end date is greater");
      }
    } else {
      this.setState({ isEndDatePickerVisible: false });
      setToastMsg("Please select start date first.");
      console.log("start date not selected yet.");
    }
    console.log(dateTimeString);
  };



}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },

  tabHolder: {
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 30,
    flex: 1
  },
  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5,
    width: '100%',
  },

  headerContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderColor: '#EAEAEA',
    borderBottomWidth: 2,
    marginBottom: 5,
    padding: 10

  },
  loginHold: {
    flex: 1,
    padding: 5,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginRight: 5
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 14,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center',
    padding: 5

  },
  registerHold: {
    flex: 1,
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginLeft: 5
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 15,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center',
    padding: 5,

  },
  radioIcon: {
    width: 20,
    height: 20,
    marginRight: 5,
    alignSelf: 'center'

  },
  filterListItem: {
    paddingVertical: 5,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  starStyle: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    margin: 2

  },
});

export default connect(mapStateToPrpos)(UnitAgencyAvailabilityList);