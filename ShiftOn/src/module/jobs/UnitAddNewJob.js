/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  Modal,
  SafeAreaView,
  Dimensions,
  TextInput,
  CheckBox, ToastAndroid
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import RadioGroup from 'react-native-radio-buttons-group';
import ModalPicker from '../../components/ModalPicker';
import { setToastMsg } from '../../utils/ToastMessage';
import Api from '../../apis/Api';
import Loader from '../../utils/Loader';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
//import ModalSelector from 'react-native-modal-selector-searchable';
import ModalSelector from '../../components/CustomModelSelector';
import { connect } from 'react-redux';


const { width } = Dimensions.get('window');

const mapStateToPrpos = (props) => {
  return {
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
  }
}


class UnitAddNewJob extends Component {
  constructor(props) {
    super(props);
    this.state = {
      job_title: '',
      job_description: '',
      start_date: '',
      finish_date: '',
      startTime: '',
      endTime: '',
      weekdays: [],
      locationId: '',
      full_location: '',
      unit_location: '',
      landmark: '',
      zip: '',
      currencyValue: '',
      isLoading: false,
      selectedRole: "",
      selectedRoleId: "",
      selectedShift: '',
      selectedShiftid: '',
      selectedWard: '',
      selectedWardId: '',
      no_of_employee: '',
      isRoleModalVisible: false,
      roleData: [],
      shiftData: [],
      locationData: [],
      wardData: [],
      isStartDatePickerVisible: false,
      isEndDatePickerVisible: false,
      isStartTimePickerVisible: false,
      isEndTimePickerVisible: false,

      // for compare start and end date
      compareDateValues: '',
      compareTimeValues: '',

      isButtonEnable: false,
      setErrors: {
        field: '',
        message: ''
      }

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    this._unitRoleListing();
  }

  // This is for store role listing data in dropdown from unitRoleListing api...
  _unitRoleListing = () => {
    this.setState({ isLoading: true });

    var formData = new FormData();
    formData.append("unit_id", this.props.userid);

    Api._unitRoleListing(formData)
      .then((response) => {
        console.log(response.data)

        var tempData = [];
        for (var i = 0; i < response.data.length; i++) {
          if (response.data[i].is_active.toString() == "1") {
            tempData.push(response.data[i])
          }
        }

        let newArray = tempData.map((item) => {
          return (
            {
              "key": item.id,
              "label": item.name
            }
          )
        })
        this.setState({ roleData: newArray })
        // setToastMsg(response.message.toString());
        this._unitShiftListing();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }

  _unitShiftListing = () => {
    let rawData = {
      "shift_user_type": "unit",
      "user_id": this.props.userid,
    }
    Api._unitShiftListing(rawData)
      .then((response) => {
        console.log(response.data)

        var tempData = [];
        for (var i = 0; i < response.data.length; i++) {
          if (response.data[i].status.toString() == "1") {
            tempData.push(response.data[i])
          }
        }

        let newArray = tempData.map((item) => {
          return (
            {
              "key": item.id,
              "label": item.shift_title,
              "start_time": item.start_time,
              "end_time": item.end_time,
              "start_date": item.start_date,
              "end_date": item.end_date,
            }
          )
        })
        this.setState({ shiftData: newArray })
        //setToastMsg(response.message.toString());
        this._unitWardListing();
      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });
  }

  _unitWardListing = () => {
    var rawData = { "unit_id": this.props.userid }

    Api._unitWardListing(rawData)
      .then((response) => {
        console.log(response.data)

        var tempData = [];
        for (var i = 0; i < response.data.length; i++) {
          if (response.data[i].is_active.toString() == "1") {
            tempData.push(response.data[i])
          }
        }

        let newArray = tempData.map((item) => {
          return (
            {
              "key": item.id,
              "label": item.name,
            }
          )
        })
        this.setState({ wardData: newArray })
        //setToastMsg(response.message.toString());
        this.unitLocationListing();
      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });
  }

  // This is for store unit locations data in dropdown from unitLocationListing api...
  unitLocationListing = () => {

    var rawData = { "unit_id": this.props.userid }

    Api._getUnitAllLocations(rawData)
      .then((response) => {
        console.log(response.data)

        var tempData = [];
        for (var i = 0; i < response.data.length; i++) {
          if (response.data[i].status.toString() == "1") {
            tempData.push(response.data[i])
          }
        }

        let newArray = tempData.map((item) => {
          return (
            {
              "key": item.id,
              "label": item.address + ", " + item.landmark + ", " + item.post_code,
              "address": item.address,
              "landmark": item.landmark,
              "zip": item.post_code,
              "currencyId": item.currency,
            }
          )
        })
        this.setState({ locationData: newArray, isLoading: false })

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false });
      });
  }


  checkValidation = () => {
    let titleError = { field: '', message: '' }
    let roleError = { field: '', message: '' }
    let shiftError = { field: '', message: '' }
    let descriptionError = { field: '', message: '' }
    let noOfEmpError = { field: '', message: '' }
    let startDateError = { field: '', message: '' }
    let finishDateError = { field: '', message: '' }
    let startTimeError = { field: '', message: '' }
    let endTimeError = { field: '', message: '' }
    let locationError = { field: '', message: '' }
    let wardError = { field: '', message: '' }

    if (this.state.job_title == '') {
      titleError.field = "title";
      titleError.message = "Job title is required!";
      this.setState({ setErrors: titleError })
    } else if (this.state.job_description == '') {
      descriptionError.field = "description";
      descriptionError.message = "Description is required!";
      this.setState({ setErrors: descriptionError })
    } else if (this.state.selectedRole == '') {
      roleError.field = "role";
      roleError.message = "Role is required!";
      this.setState({ setErrors: roleError })
    } else if (this.state.selectedShift == '') {
      shiftError.field = "shift";
      shiftError.message = "Shift is required!";
      this.setState({ setErrors: shiftError })
    } else if (this.state.full_location == '') {
      locationError.field = "unit_location";
      locationError.message = "Location is required!";
      this.setState({ setErrors: locationError })
    } else if (this.state.selectedWard == '') {
      wardError.field = "ward";
      wardError.message = "Ward is required!";
      this.setState({ setErrors: wardError })
    } else if (this.state.no_of_employee == '') {
      noOfEmpError.field = "employee";
      noOfEmpError.message = "Please add required number of employees!";
      this.setState({ setErrors: noOfEmpError })
    } else {
      this.setState({ setErrors: { field: '', message: '' } })
      this.checkPayrateLinkOrNot();
    }

  }

  checkPayrateLinkOrNot = () => {
    this.setState({ isLoading: true })

    var rawData = {
      "ward_id": this.state.selectedWardId,
      "shift_id": this.state.selectedShiftid,
      "unit_id": this.props.userid,
      "role": this.state.selectedRole
    }

    Api._unitCheckPayrateLinkStatus(rawData)
      .then((response) => {

        console.log(response.status)
        if (response.status == 1) {
          this._unitAddJob()
        } else {
          this.setState({ isLoading: false })
          alert('No one Payrate is link with this Role, Shift and Ward.')
        }

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });

  }

  // Call AddJob Api after click submit button...
  _unitAddJob = () => {

    let formData = new FormData();
    formData.append("user_id", this.props.userid);
    formData.append("job_type", "unit");
    formData.append("job_title", this.state.job_title);
    formData.append("job_description", this.state.job_description);
    formData.append("job_role", this.state.selectedRoleId);
    formData.append("shift_id", this.state.selectedShiftid);
    formData.append("ward", this.state.selectedWardId);
    formData.append("unit_id", this.props.userid);
    formData.append("location", this.state.locationId);
    formData.append("landmark", this.state.landmark);
    formData.append("zip", this.state.zip);
    formData.append("required_people", this.state.no_of_employee);

    Api._unitAddJob(formData)
      .then((response) => {

        console.log(response)
        this.setState({ isLoading: false })
        setToastMsg(response.message.toString());
        this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });
  }


  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>

              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>ADD NEW JOB</Text>

              </View>
              <Loader isLoading={this.state.isLoading} />
              <ScrollView>
                <View style={{ marginTop: 10, marginLeft: 30, marginRight: 30 }}>

                  <TouchableOpacity>
                    <CustomTextInput
                      placeholder='JOB TITLE'
                      floatingText="JOB TITLE"
                      keyboardType='default'
                      value={this.state.job_title}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({ job_title: text });
                      }}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "title" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <TouchableOpacity>
                    <CustomTextInput
                      placeholder='JOB DESCRIPTION'
                      floatingText="JOB DESCRIPTION"
                      keyboardType='default'
                      style={{ textAlignVertical: "top", height: 120, }}
                      multiline={true}
                      value={this.state.job_description}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({ job_description: text });
                      }}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "description" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <ModalSelector
                    data={this.state.roleData}
                    initValue="SELECT ROLE"
                    accessible={true}
                    animationType="fade"
                    search={false}
                    scrollViewAccessibilityLabel={'Scrollable options'}
                    cancelButtonAccessibilityLabel={'Cancel Button'}
                    onChange={(option) => { this.setState({ selectedRole: option.label, selectedRoleId: option.key }) }}>
                    <CustomTextInput
                      placeholder='SELECT ROLE'
                      floatingText="SELECT ROLE"
                      editable={false}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleDropDown={true}
                      returnKeyType={'done'}
                      value={this.state.selectedRole}
                    />
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "role" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <ModalSelector
                    data={this.state.shiftData}
                    initValue="SELECT SHIFT"
                    accessible={true}
                    animationType="fade"
                    search={false}
                    scrollViewAccessibilityLabel={'Scrollable options'}
                    cancelButtonAccessibilityLabel={'Cancel Button'}
                    onChange={(option) => {
                      this.setState({
                        selectedShiftid: option.key,
                        selectedShift: option.label,
                        startTime: option.start_time,
                        endTime: option.end_time,
                        start_date: option.start_date,
                        finish_date: option.end_date,
                      })
                    }}>
                    <CustomTextInput
                      placeholder='SELECT SHIFT'
                      floatingText="SELECT SHIFT"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleDropDown={true}
                      value={this.state.selectedShift}
                      returnKeyType={'done'}
                    />
                  </ModalSelector>
                  {
                    this.state.setErrors.field == "shift" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  {/* <TouchableOpacity style={{ marginTop: 5 }} >
                    <CustomTextInput
                      placeholder='START DATE'
                      floatingText="START DATE"
                      editable={false}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      // style={{ fontSize: 18, paddingLeft: 10 }}
                      isVisibleCalendar={true}
                      value={this.state.start_date}
                      returnKeyType={'done'}
                      onTouchEnd={() => { this.setState({ isStartDatePickerVisible: false }) }}
                    />
                    <DateTimePickerModal
                      isVisible={this.state.isStartDatePickerVisible}
                      mode="date"
                      date={new Date()}
                      onConfirm={(date) => this.handleStartDateConfirm(date)}
                      onCancel={() => this.setState({ isStartDatePickerVisible: false })}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "start_date" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <TouchableOpacity style={{ marginTop: 5 }} >
                    <CustomTextInput
                      placeholder='FINISH DATE'
                      floatingText="FINISH DATE"
                      editable={false}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleCalendar={true}
                      value={this.state.finish_date}
                      returnKeyType={'done'}
                      onTouchEnd={() => { this.setState({ isEndDatePickerVisible: false }) }}
                    />
                    <DateTimePickerModal
                      isVisible={this.state.isEndDatePickerVisible}
                      mode="date"
                      date={new Date()}
                      onConfirm={(date) => this.handleEndDateConfirm(date)}
                      onCancel={() => this.setState({ isEndDatePickerVisible: false, })}
                    />
                  </TouchableOpacity>
                  {
                    this.state.setErrors.field == "finish_date" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }} >
                    <CustomTextInput
                      placeholder='START TIME'
                      floatingText="START TIME"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleCalendar={true}
                      value={this.state.startTime}
                      returnKeyType={'done'}
                      onTouchEnd={() => this.setState({ isStartTimePickerVisible: false })}
                    />
                    <DateTimePickerModal
                      isVisible={this.state.isStartTimePickerVisible}
                      mode="time"
                      locale="en_GB"
                      date={new Date()}
                      onConfirm={(date) => this.handleStartTimeConfirm(date)}
                      onCancel={() => this.setState({ isStartTimePickerVisible: false, })}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "start_time" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }} >
                    <CustomTextInput
                      placeholder='END TIME'
                      floatingText="END TIME"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      isVisibleCalendar={true}
                      value={this.state.endTime}
                      returnKeyType={'done'}
                      onTouchEnd={() => this.setState({ isEndTimePickerVisible: false })}
                    />
                    <DateTimePickerModal
                      isVisible={this.state.isEndTimePickerVisible}
                      mode="time"
                      locale="en_GB"
                      date={new Date()}
                      onConfirm={(date) => this.handleEndTimeConfirm(date)}
                      onCancel={() => this.setState({ isEndTimePickerVisible: false, })}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "end_time" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  } */}

                  <View style={{ marginTop: 5 }}>
                    <ModalSelector
                      data={this.state.locationData}
                      keyExtractor={(item, index) => item.key.toString()}
                      initValue="UNIT/LOCATION"
                      accessible={true}
                      animationType="fade"
                      search={false}
                      scrollViewAccessibilityLabel={'Scrollable options'}
                      cancelButtonAccessibilityLabel={'Cancel Button'}
                      onChange={(option) => {
                        this.setState({
                          locationId: option.key,
                          full_location: option.label,
                          unit_location: option.address,
                          landmark: option.landmark,
                          zip: option.zip,
                          currencyValue: option.currencyId,
                        })
                        {
                          option.currencyId ?
                            (
                              console.log("currency added"),
                              this.setState({ isButtonEnable: true })
                            )
                            :
                            (
                              console.log("currency not added"),
                              alert('Currency has not set yet in this location'),
                              this.setState({ isButtonEnable: false })
                            )
                        }

                      }}>
                      <CustomTextInput
                        placeholder='UNIT/LOCATION'
                        keyboardType='default'
                        floatingText="UNIT/LOCATION"
                        editable={true}
                        onFocus={false}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        isVisibleDropDown={true}
                        value={this.state.full_location}
                        returnKeyType={'done'}
                      />
                    </ModalSelector>
                  </View>
                  {
                    this.state.setErrors.field == "unit_location" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <ModalSelector
                    data={this.state.wardData}
                    initValue="WARD"
                    accessible={true}
                    animationType="fade"
                    search={false}
                    scrollViewAccessibilityLabel={'Scrollable options'}
                    cancelButtonAccessibilityLabel={'Cancel Button'}
                    onChange={(option) => {
                      this.setState({ selectedWard: option.label, selectedWardId: option.key })
                    }}>

                    <View style={{ marginTop: 5 }}>
                      <CustomTextInput
                        placeholder='WARD'
                        keyboardType='default'
                        floatingText="WARD"
                        value={this.state.selectedWard}
                        editable={true}
                        onFocus={true}
                        showSoftInputOnFocus={false}
                        caretHidden={true}
                        isVisibleDropDown={true}
                        returnKeyType={'done'}

                      />
                    </View>
                  </ModalSelector>

                  {
                    this.state.setErrors.field == "landmark" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='REQUIRED NO OF EMPLOYEE'
                      keyboardType='numeric'
                      floatingText="REQUIRED NO OF EMPLOYEE"
                      value={this.state.no_of_employee}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({ no_of_employee: text });
                      }}
                    />
                  </View>
                  {
                    this.state.setErrors.field == "employee" && (
                      <Text style={styles.errors}>{this.state.setErrors.message}</Text>
                    )
                  }


                </View>
              </ScrollView>
            </View>
            <View >

              <View style={styles.shadow}></View>

              {
                this.state.isButtonEnable ?
                  <TouchableOpacity activeOpacity={0.7}
                    style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                    onPress={() => this.checkValidation()}
                  >
                    <Text style={styles.loginText}>SUBMIT</Text>
                  </TouchableOpacity>
                  :
                  <TouchableOpacity activeOpacity={1}
                    style={{ padding: 10, marginTop: 20, backgroundColor: "#c2c2c2", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                    onPress={() => { }}
                  >
                    <Text style={styles.loginText}>SUBMIT</Text>
                  </TouchableOpacity>
              }

            </View>


          </View>
        </SafeAreaView>
      </Fragment>
    );
  }


  // this is for start date picker...
  handleStartDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    this.setState({
      start_date: dateTimeString,
      finish_date: "",
      compareDateValues: date,
      isStartDatePickerVisible: false,
    })

    console.log(dateTimeString)
  };

  // this is for end date picker...
  handleEndDateConfirm = (date) => {
    let dateTimeString = moment(date).local().format('DD/MM/YYYY');

    var start = new Date(this.state.compareDateValues);
    var end = new Date(date);

    if (this.state.compareDateValues != '') {
      if (start.getTime() > end.getTime()) {
        this.setState({
          isEndDatePickerVisible: false,
          finish_date: "",
        });
        setToastMsg("Date must be greater than start date.");
        console.log("end date is smaller");
      } else {
        this.setState({
          finish_date: dateTimeString,
          isEndDatePickerVisible: false,
        })
        console.log("end date is greater");
      }
    } else {
      this.setState({ isEndDatePickerVisible: false });
      setToastMsg("Please select start date first.");
      console.log("start date not selected yet.");
    }
    console.log(dateTimeString);
  };


  // this is for start time picker...
  handleStartTimeConfirm = (time) => {

    let dateTimeString = moment(time).local().format('HH:mm');

    this.setState({
      startTime: dateTimeString,
      compareTimeValues: time,
      isStartTimePickerVisible: false,
    })

    console.log(dateTimeString)
  };

  // this is for end time picker...
  handleEndTimeConfirm = (time) => {
    let dateTimeString = moment(time).local().format('HH:mm');

    var start = new Date(this.state.compareTimeValues);
    var end = new Date(time);

    if (this.state.compareTimeValues != '') {
      if (start.getTime() > end.getTime()) {
        this.setState({
          isEndTimePickerVisible: false,
        })
        setToastMsg("Selected Time must be greater than Start time.");
        console.log("end time is smaller")
      } else {
        this.setState({
          endTime: dateTimeString,
          isEndTimePickerVisible: false,
        })
        console.log("end time is greater")
      }
    } else {
      this.setState({ isEndTimePickerVisible: false })
      setToastMsg("Please select Start time first.");
      console.log("start time not selected yet.")
    }
    console.log(dateTimeString)
  };


}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center',

  },

  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5


  },
  floatingLabelText: {
    color: "#393FA0",
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    paddingRight: 5,

    fontSize: 14,
    fontFamily: "montserrat_regular"
  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  profileImage: {
    height: 150,
    width: 150,
    position: 'absolute',
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: 60

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  },
  errors: {
    color: 'red',
    fontSize: 14,
    marginLeft: 10,
  }

});

export default connect(mapStateToPrpos)(UnitAddNewJob);