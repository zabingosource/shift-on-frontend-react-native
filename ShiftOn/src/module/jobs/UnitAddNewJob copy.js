/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput,
  CheckBox
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { StackActions, NavigationActions } from 'react-navigation';
import { FlatList } from 'react-native-gesture-handler';
import RadioGroup from 'react-native-radio-buttons-group';




const { width } = Dimensions.get('window');

const data = [
  {
    label: 'MORNING',
  },
  {
    label: 'DAY',
  },
  {
    label: 'NIGHT',
  },

]


const Jobdata = [
  {
    storename: 'Store Item 1',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  }, {
    storename: 'Store Item 1',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 2',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 3',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 4',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 5',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 6',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
  {
    storename: 'Store Item 7',
    storeaddress: '141 w 20h st New York 10011',
    storephone: '9658425154',
    opentime: '10:00 AM',
    closetime: '10:00 PM',
  },
];

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});
const resetToHome = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'WorkerHome' })],
});

export default class UnitAddNewJob extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      noofEmployee: '',
      start_date: '',
      finish_date: '',
      startTime: '',
      endTime: '',
      comment: '',
      acvity: '',
      zip: '',
      unit: '',
      ward: '',
      weekdays: []

    };
    console.log(this.props.navigation);
  }



  componentDidMount() {
    const daysdata = [
      {
        label: 'SUN',
        isSelected: false,
      },
      {
        label: 'MON',
        isSelected: false,
      },
      {
        label: 'TUE',
        isSelected: false,
      },
      {
        label: 'WED',
        isSelected: false,
      },
      {
        label: 'THU',
        isSelected: false,
      },
      {
        label: 'FRI',
        isSelected: false,
      },
      {
        label: 'SAT',
        isSelected: false,
      },
    ]
    var days = [];
    for (var i = 0; i < daysdata.length; i++) {
      days.push(daysdata[i])
    }
    this.setState({
      weekdays: days,
    });
    console.log("weekdays===========", this.state.weekdays);
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >
          <View style={styles.mainContainer}>


            <View style={styles.mainContainer}>



              <View style={{ backgroundColor: '#00D5E1' }}>
                <TouchableOpacity
                  style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>ADD NEW JOB</Text>

              </View>
              <ScrollView>
                <View style={{ marginTop: 10, marginLeft: 30, marginRight: 30 }}>

                  <TouchableOpacity
                  // onPress={() => this._setDate("start")}
                  >
                    <CustomTextInput
                      placeholder='JOB TITLE'
                      floatingText="JOB TITLE"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      //style={{ fontSize: 18, paddingLeft: 10 }}
                      isVisibleDropDown={true}
                      value={this.state.acvity}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          acvity: text,
                        });
                      }}
                      onTouchEnd={() => this._setDate("acvity")}
                    />

                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{ marginTop: 5 }}
                  // onPress={() => this._setDate("start")}
                  >
                    <CustomTextInput
                      placeholder='GENDER'
                      floatingText="GENDER"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      //style={{ fontSize: 18, paddingLeft: 10 }}
                      isVisibleDropDown={true}
                      value={this.state.acvity}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          acvity: text,
                        });
                      }}
                      onTouchEnd={() => this._setDate("acvity")}
                    />

                  </TouchableOpacity>

                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='REQUIRED NO OF EMPLOYEE'
                      keyboardType='numeric'
                      floatingText="REQUIRED NO OF EMPLOYEE"
                      value={this.state.noofEmployee}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          noofEmployee: text,
                        });
                      }}
                    />
                  </View>
                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='SHIFT DESCRIPTION'
                      keyboardType='default'
                      floatingText="SHIFT DESCRIPTION"
                      value={this.state.unit}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          unit: text,
                        });
                      }}
                    />
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginTop: 5, flex: 1, marginRight: 3 }}>
                      <TouchableOpacity
                      // onPress={() => this._setDate("start")}
                      >
                        <CustomTextInput
                          placeholder='START TIME'
                          floatingText="START TIME"
                          editable={true}
                          onFocus={true}
                          showSoftInputOnFocus={false}
                          caretHidden={true}
                          //style={{ fontSize: 18, paddingLeft: 10 }}
                          value={this.state.startTime}
                          returnKeyType={'done'}
                          onChangeText={(text) => {
                            this.setState({
                              startTime: text,
                            });
                          }}
                          onTouchEnd={() => this._setDate("start")}
                        />

                      </TouchableOpacity>

                    </View>
                    <View style={{ marginTop: 5, flex: 1, marginLeft: 3 }}>
                      <TouchableOpacity
                      // onPress={() => this._setDate("end")}
                      >
                        <CustomTextInput
                          placeholder='END TIME'
                          floatingText="END TIME"
                          editable={true}
                          onFocus={true}
                          showSoftInputOnFocus={false}
                          caretHidden={true}
                          //style={{ fontSize: 18, paddingLeft: 10 }}
                          value={this.state.endTime}
                          returnKeyType={'done'}
                          onTouchEnd={() => this._setDate("end")}
                          onChangeText={(text) => {
                            this.setState({
                              endTime: text,
                            });
                          }}
                        />

                      </TouchableOpacity>

                    </View>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginTop: 5, flex: 1, marginRight: 3 }}>
                      <TouchableOpacity
                      // onPress={() => this._setDate("start")}
                      >
                        <CustomTextInput
                          placeholder='DURATION'
                          floatingText="DURATION"
                          editable={true}
                          onFocus={true}
                          showSoftInputOnFocus={false}
                          caretHidden={true}
                          //style={{ fontSize: 18, paddingLeft: 10 }}
                          value={this.state.startTime}
                          returnKeyType={'done'}
                          onChangeText={(text) => {
                            this.setState({
                              startTime: text,
                            });
                          }}
                          onTouchEnd={() => this._setDate("start")}
                        />

                      </TouchableOpacity>

                    </View>
                    <View style={{ marginTop: 5, flex: 1, marginLeft: 3 }}>
                      <TouchableOpacity
                      // onPress={() => this._setDate("end")}
                      >
                        <CustomTextInput
                          placeholder='BREAK'
                          floatingText="BREAK"
                          editable={true}
                          onFocus={true}
                          showSoftInputOnFocus={false}
                          caretHidden={true}
                          // style={{ fontSize: 18, paddingLeft: 10 }}
                          value={this.state.endTime}
                          returnKeyType={'done'}
                          onTouchEnd={() => this._setDate("end")}
                          onChangeText={(text) => {
                            this.setState({
                              endTime: text,
                            });
                          }}
                        />

                      </TouchableOpacity>

                    </View>
                  </View>

                  <TouchableOpacity
                    style={{ marginTop: 5 }}
                  // onPress={() => this._setDate("start")}
                  >
                    <CustomTextInput
                      placeholder='START DATE'
                      floatingText="START DATE"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      // style={{ fontSize: 18, paddingLeft: 10 }}
                      isVisibleCalendar={true}
                      value={this.state.start_date}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          start_date: text,
                        });
                      }}
                      onTouchEnd={() => this._setDate("start_date")}
                    />

                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{ marginTop: 5 }}
                  // onPress={() => this._setDate("start")}
                  >
                    <CustomTextInput
                      placeholder='END DATE'
                      floatingText="END DATE"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      //style={{ fontSize: 18, paddingLeft: 10 }}
                      isVisibleCalendar={true}
                      value={this.state.finish_date}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          date: text,
                        });
                      }}
                      onTouchEnd={() => this._setDate("finish_date")}
                    />

                  </TouchableOpacity>

                  <View style={{ marginTop: 10 }}>

                    <FlatList
                      style={{ width: '100%', borderRadius: 15, borderColor: '#00D5E1', borderWidth: 2, marginTop: 12, padding: 5 }}
                      data={this.state.weekdays}
                      numColumns={4}
                      keyExtractor={(item, index) => index.toString()}
                      //ItemSeparatorComponent={this.ItemSeparator}
                      renderItem={({ item, index }) =>

                        <TouchableOpacity
                          activeOpacity={0.9}
                          style={{ margin: 5 }}
                        //onPress={() => this.props.navigation.navigate('WorkerJobDetails', { position: index })}
                        >
                          <View style={{ flexDirection: 'row', backgroundColor: '#00D5E1', borderRadius: 5 }}>
                            <CheckBox
                              value={item.isSelected}
                               onValueChange={() => this._setDay(item)}
                              style={{ alignSelf: "center", }}
                            />
                            <Text style={{ alignSelf: 'center', fontSize: 14, paddingTop: 10, paddingBottom: 10, paddingRight: 5, fontFamily: 'montserrat_regular', color: "#000000" }}>{item.label}</Text>

                          </View>
                        </TouchableOpacity>
                      }
                    />
                    <Text style={{ position: 'absolute',padding:5, marginLeft: 20, color: '#393FA0', backgroundColor: '#FFFFFF', fontFamily: 'montserrat_regular', fontSize: 10 }}>SELECT DAYS</Text>
                  </View>

                  <TouchableOpacity
                    style={{ marginTop: 5 }}
                  // onPress={() => this._setDate("start")}
                  >
                    <CustomTextInput
                      placeholder='ACTIVITY DESCRIPTION'
                      floatingText="ACTIVITY DESCRIPTION"
                      editable={true}
                      onFocus={true}
                      multiline={true}
                      style={{ textAlignVertical: "top", height: 150 }}
                      value={this.state.comment}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          comment: text,
                        });
                      }}
                    />

                  </TouchableOpacity>



                  <TouchableOpacity
                    style={{ marginTop: 5 }}
                  // onPress={() => this._setDate("start")}
                  >
                    <CustomTextInput
                      placeholder='PROFILE'
                      floatingText="PROFILE"
                      editable={true}
                      onFocus={true}
                      showSoftInputOnFocus={false}
                      caretHidden={true}
                      // style={{ fontSize: 18, paddingLeft: 10 }}
                      isVisibleDropDown={true}
                      value={this.state.acvity}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          acvity: text,
                        });
                      }}
                      onTouchEnd={() => this._setDate("acvity")}
                    />

                  </TouchableOpacity>
                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='ZIP CODE'
                      keyboardType='default'
                      floatingText="ZIP CODE"
                      value={this.state.zip}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          zip: text,
                        });
                      }}
                    />
                  </View>
                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='UNIT/LOCATION'
                      keyboardType='default'
                      floatingText="UNIT/LOCATION"
                      value={this.state.unit}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          unit: text,
                        });
                      }}
                    />
                  </View>
                  <View style={{ marginTop: 5 }}>
                    <CustomTextInput
                      placeholder='LANDMARK'
                      keyboardType='default'
                      floatingText="LANDMARK"
                      value={this.state.ward}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          ward: text,
                        });
                      }}
                    />
                  </View>

                  <View style={{ marginTop: 5, marginBottom: 10 }}>
                    <CustomTextInput
                      placeholder='WARD'
                      keyboardType='default'
                      floatingText="WARD"
                      value={this.state.ward}
                      returnKeyType={'done'}
                      onChangeText={(text) => {
                        this.setState({
                          ward: text,
                        });
                      }}
                    />
                  </View>

                  <View style={{ marginTop: 10, marginBottom: 20 }}>
                    <Text style={styles.floatingLabelText}>{"SELECT SHIFT"}</Text>
                    <RadioGroup
                      radioButtons={data}
                      flexDirection='row'
                      color={'#657CF4'}
                      onPress={this._onPressRadio}

                    />

                  </View>
                </View>
              </ScrollView>
            </View>
            <View >

              <View style={styles.shadow}></View>
              {/*  <Image style={{ width:'100%',height:40,}} source={require('./../../assets/border.png')} /> */}
              <TouchableOpacity style={{ padding: 10, marginTop: 20, backgroundColor: "#657CF4", borderRadius: 20, marginLeft: 40, marginRight: 40, marginBottom: 10 }}
                onPress={() => this.props.navigation.goBack()}
              >
                <Text style={styles.loginText}>SUBMIT</Text>
              </TouchableOpacity>

            </View>


          </View>
        </SafeAreaView>
      </Fragment>
    );
  }
  _setDay = (day)=> {
console.log("day =========",day);
    var days = [];
    for (var i = 0; i < this.state.weekdays.length; i++) {
      if(day.label== this.state.weekdays[i].label){
        this.state.weekdays[i].isSelected = ! this.state.weekdays[i].isSelected
      }
      days.push(this.state.weekdays[i])
    }
    this.setState({
      weekdays: days,
    });
  }
  _setDate = (type) => {
    if (type == 'start_date') {
      this.setState({ start_date: "15/09/2020" });
    } else if (type == 'finish_date') {
      this.setState({ finish_date: "20/09/2020" });
    } else if (type == 'start') {
      this.setState({ startTime: "10:00 AM" });
    } else if (type == 'end') {
      this.setState({ endTime: "04:00 PM" });
    } else {
      this.setState({ acvity: "Lorem ipsum" });
    }


  }
  _onPressRadio = () => {


  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },
  headerBg: {
    width: width,
    height: 150,
    alignSelf: 'center',

  },

  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5


  },
  floatingLabelText: {
    color: "#393FA0",
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    paddingRight: 5,

    fontSize: 14,
    fontFamily: "montserrat_regular"
  },
  heading: {
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'montserrat_semibold',
    color: '#353C9E',
    marginTop: 10

  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center'

  },
  textInput_style: {
    flex: 1,
    fontSize: 20,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderColor: '#00D5E1',
    borderRadius: 20,
    borderWidth: 2,
    color: 'rgb(63, 82, 110)',
  },
  profileImage: {
    height: 150,
    width: 150,
    position: 'absolute',
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: 60

  },
  shadow: {
    flexDirection: 'column-reverse',
    backgroundColor: "#F3F3F3",
    height: 0.2,
    elevation: 3,
    shadowColor: "#F3F3F3",
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: {
      height: 10,
      width: 10
    }

  },

});

