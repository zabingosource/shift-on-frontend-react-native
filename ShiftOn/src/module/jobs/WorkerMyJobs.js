/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput, ToastAndroid,
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import Api from '../../apis/Api';
import { setToastMsg } from '../../utils/ToastMessage';
import Loader from '../../utils/Loader';
import { connect } from 'react-redux';


const mapStateToPrpos = (props) => {
  return {
    isRefresh: props.refreshFunction.isRefresh,
    userName: props.userLoginDetails.name,
    userid: props.userLoginDetails.id,
    userImage: props.userLoginDetails.image,
  }
}

class WorkerMyJobs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchQuery: '',
      selectedTab: '1',
      defaultTab: '1',
      apiData: [],
      filterData: [],
      apiPendingData: [],
      apiAcceptedData: [],
      apiDeclinedData: [],
      isLoading: true,

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {
    this.navFocusListener = this.props.navigation.addListener('didFocus', () => {
      this._workerMyJobListing();
    });
  }

  componentWillUnmount() {
    this.navFocusListener.remove();
  }

  _workerMyJobListing = () => {
    this.setState({ isLoading: true, selectedTab: this.state.defaultTab });

    var formData = new FormData();
    formData.append("worker_id", this.props.userid);

    Api._workerMyJobListing(formData)
      .then((response) => {

        console.log(response.data);

        this.apiAcceptedData(response.data);
        this.apiPendingData(response.data);
        this.apiDeclinedData(response.data);

        this.setState({ isLoading: false });

        // for set default tab
        this._onChangeTab(this.state.defaultTab)
        //setToastMsg(response.message.toString());

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false, })
      });
  }

  apiAcceptedData = (responseData) => {
    var tempData = [];
    for (var i = 0; i < responseData.length; i++) {
      if (responseData[i].status.toString() == "1" || responseData[i].status.toString() == "11") {
        if (responseData[i].job_details != null) {
          tempData.push(responseData[i])
        }
      }
    }
    this.setState({
      apiAcceptedData: tempData,
      apiData: tempData,
      filterData: tempData,
    })
  }

  apiPendingData = (responseData) => {
    var tempData = [];
    for (var i = 0; i < responseData.length; i++) {
      if (responseData[i].status.toString() == "") {
        if (responseData[i].job_details != null) {
          tempData.push(responseData[i])
        }
      }
    }
    this.setState({
      apiPendingData: tempData,
    })
  }

  apiDeclinedData = (responseData) => {
    var tempData = [];
    for (var i = 0; i < responseData.length; i++) {
      if (responseData[i].status.toString() == "0") {
        if (responseData[i].job_details != null) {
          tempData.push(responseData[i])
        }
      }
    }
    this.setState({ apiDeclinedData: tempData })
  }

  _workerJobConfirmation = (item) => {
    this.setState({ isLoading: true })

    var rawData = {
      "job_id": item.job_id,
      "workers_id": this.props.userid,
      "agency_id": item.agency_id,
    }

    Api._workerJobConfirmation(rawData)
      .then((response) => {

        console.log(response.data)
        if (response.status.toString() == "1") {
          this._workerMyJobListing();
        }
        this.setState({ selectedTab: '1' })
        setToastMsg(response.message.toString());

      })
      .catch((err) => {
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false })
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

          <View style={styles.mainContainer}>
            <View style={{ backgroundColor: '#00D5E1' }}>
              <TouchableOpacity
                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
              </TouchableOpacity>
              <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>REQUESTED JOBS</Text>

            </View>

            <Loader isLoading={this.state.isLoading} />

            <View style={{ marginLeft: 10, marginRight: 10 }}>
              <View style={{ flexDirection: 'row', marginTop: 20, backgroundColor: '#626262', borderRadius: 40 }}>
                <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "1" ? "#00D5E1" : "#626262" }]}
                  activeOpacity={1}
                  onPress={() => this._onChangeTab("1")}
                >
                  <Text style={{ alignSelf: 'center', fontSize: 14, padding: 7, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "1" ? "#000000" : "#FFFFFF" }}>LIST</Text>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "2" ? "#00D5E1" : "#626262" }]}
                  activeOpacity={1}
                  onPress={() => this._onChangeTab("2")}
                >
                  <Text style={{ alignSelf: 'center', fontSize: 14, padding: 7, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "2" ? "#000000" : "#FFFFFF" }}>PENDING</Text>
                </TouchableOpacity>


                <TouchableOpacity style={[styles.tabHolder, { backgroundColor: this.state.selectedTab == "3" ? "#00D5E1" : "#626262" }]}
                  activeOpacity={1}
                  onPress={() => this._onChangeTab("3")}
                >
                  <Text style={{ alignSelf: 'center', fontSize: 14, padding: 7, fontFamily: 'montserrat_regular', color: this.state.selectedTab == "3" ? "#000000" : "#FFFFFF" }}>DECLINED</Text>
                </TouchableOpacity>


              </View>
              <View style={{ flexDirection: 'row', marginTop: 20, backgroundColor: '#EAEAEA', borderRadius: 40 }}>

                <Image style={{ height: 30, width: 30, padding: 5, position: 'absolute', end: 5, top: 3.5 }} source={require('./../../assets/search_icn.png')} />

                <TextInput
                  style={{ width: '85%', paddingHorizontal: 8, paddingTop: 8, paddingBottom: 10, marginLeft: 10, marginRight: 40, color: '#969696', fontFamily: 'montserrat_regular', fontSize: 12 }}
                  placeholder='Search by agency name'
                  keyboardType='default'
                  value={this.state.searchQuery}
                  returnKeyType={'done'}
                  onChangeText={(text) => { this.searchFilter(text) }}
                />

              </View>
              <View style={{ marginTop: 20, marginBottom: 20 }}>

                {
                  this.state.apiData.length > 0 ?

                    (<FlatList
                      style={{ width: '100%', marginBottom: 290 }}
                      data={this.state.apiData}
                      keyExtractor={(item, index) => index.toString()}
                      //ItemSeparatorComponent={this.ItemSeparator}
                      renderItem={({ item, index }) =>

                        <TouchableOpacity activeOpacity={1} >
                          <View style={styles.rowContainer}>

                            <TouchableOpacity style={{ width: '16%', justifyContent: 'center', alignItems: 'center' }}
                              onPress={() => {
                                this.setState({ defaultTab: this.state.selectedTab }),
                                  this.props.navigation.navigate('ViewJobDetail', { jobData: item.job_details.id })
                              }}
                            >
                              <Image style={{ height: 40, width: 42, tintColor: '#000000' }} source={require('./../../assets/jobs_icn.png')} />
                            </TouchableOpacity>

                            <TouchableOpacity style={{ width: "64%" }}
                              onPress={() => { this.onNavigationChange(item, item.id) }}
                            >
                              <Text
                                style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                              >{item.job_details.job_title}</Text>
                              <Text
                                numberOfLines={1}
                                style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                              >{item.job_details.job_description} </Text>
                              <View style={{ marginTop: 5, flexDirection: "row" }}>
                                <Image source={require('./../../assets/pin_icn.png')} style={{ width: 10, height: 15, alignSelf: 'center' }} resizeMode='stretch' />
                                <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>
                                  {item.job_details.location}{item.job_details.landmark ? ", " + item.job_details.landmark : ''}{item.job_details.zip ? ", " + item.job_details.zip : ''}
                                </Text>
                              </View>
                              <View style={{ marginTop: 5, flexDirection: "row", flex: 1.5, marginRight: 2 }}>
                                <Image source={require('./../../assets/calendar_icn_b.png')} style={{ width: 12, height: 12, alignSelf: 'center' }} resizeMode='stretch' />
                                <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>{item.job_details.shift.start_date} to {item.job_details.shift.end_date}</Text>
                              </View>
                              <View style={{ marginTop: 5, flexDirection: "row", flex: 1, marginLeft: 2 }}>
                                <Image source={require('./../../assets/time_icn_a.png')} style={{ width: 12, height: 12, alignSelf: 'center' }} resizeMode='stretch' />
                                <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>{item.job_details.shift.start_time} to {item.job_details.shift.end_time}</Text>
                              </View>
                            </TouchableOpacity>

                            <View style={{ width: '20%', justifyContent: 'flex-end', alignItems: 'center', marginBottom: 5 }}>
                              {
                                item.status.toString() == "11" ?
                                  <TouchableOpacity
                                    style={{ padding: 6, backgroundColor: '#657CF4', borderRadius: 10, }}
                                    onPress={() => this._workerJobConfirmation(item)}
                                  >
                                    <Text style={{ color: '#FFFFFF', fontFamily: 'montserrat_semibold', fontSize: 10, }}>
                                      CONFIRM
                                    </Text>
                                  </TouchableOpacity>
                                  :
                                  <View style={{ width: '16%' }}></View>
                              }
                            </View>
                          </View>
                        </TouchableOpacity>

                      }
                    />)
                    :
                    <View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center' }}>
                      <Text
                        numberOfLines={1}
                        style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 20, }}
                      >No Data Available!</Text>
                    </View>
                }
              </View>

            </View>

          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _onChangeTab(tab) {

    if (tab == "1") {
      this.setState({
        selectedTab: "1",
        apiData: this.state.apiAcceptedData,
        filterData: this.state.apiAcceptedData,
        searchQuery: '',
      });
    } else if (tab == "2") {
      this.setState({
        selectedTab: "2",
        apiData: this.state.apiPendingData,
        filterData: this.state.apiPendingData,
        searchQuery: '',
      });
    } else {
      this.setState({
        selectedTab: "3",
        apiData: this.state.apiDeclinedData,
        filterData: this.state.apiDeclinedData,
        searchQuery: '',
      });
    }
  }

  onNavigationChange = (data, id) => {
    if (this.state.selectedTab == "2") {
      this.setState({ defaultTab: this.state.selectedTab })
      this.props.navigation.navigate('WorkerJobDetails', {
        jobDetails: data, job_id: id, worker_id: this.props.userid
      })
    }
  }

  searchFilter = (text) => {
    if (text) {
      const newData = this.state.filterData.filter((item) => {
        const itemData = item.agency.name ? item.agency.name.toUpperCase() : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      this.setState({ apiData: newData, searchQuery: text });
    } else {
      this.setState({ apiData: this.state.filterData, searchQuery: text });
    }
  }

}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },

  tabHolder: {
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 30,
    flex: 1
  },
  rowContainer: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5


  },


});

export default connect(mapStateToPrpos)(WorkerMyJobs);