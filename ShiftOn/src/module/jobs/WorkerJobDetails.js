/**
 * 27.12.2019 started working on zcart k
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  SafeAreaView,
  Dimensions,
  TextInput, ToastAndroid,
} from 'react-native';
import { OutlinedTextField } from 'react-native-material-textfield';
import CustomTextInput from '../../components/CustomTextInput';
import { FlatList } from 'react-native-gesture-handler';
import ReactNativeModal from 'react-native-modal';
import Api from '../../apis/Api';
import { setToastMsg } from '../../utils/ToastMessage';
import Loader from '../../utils/Loader';
import { connect } from 'react-redux';
import moment, { now } from 'moment';

export default class WorkerJobDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      isLoading: false,
      backData: this.props.navigation.getParam('jobDetails', 'NO-item'),

    };
    console.log(this.props.navigation);
  }


  componentDidMount() {

  }

  _workerMyJobUpdateToWorker = (job_id, status) => {
    this.setState({ isLoading: true });

    var formData = new FormData();
    formData.append("id", job_id);
    formData.append("status", status);

    Api._workerMyJobUpdateToWorker(formData)
      .then((response) => {

        console.log(response.data)
        this.setState({ isLoading: false, })

        setToastMsg(response.message.toString());
        this.props.navigation.goBack();

      })
      .catch((err) => {
        console.log(err);
        console.log(err);
        setToastMsg("Somthing went wrong.");
        this.setState({ isLoading: false, })
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: '#FFFFFF' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }} >

          <View style={styles.mainContainer}>
            <View style={{ backgroundColor: '#00D5E1' }}>
              <TouchableOpacity
                style={{ position: 'absolute', paddingTop: 20, paddingBottom: 20, paddingLeft: 10, paddingRight: 20 }}
                //onPress={() => this._showModal(true)}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image style={{ height: 15, width: 15, padding: 10 }} source={require('./../../assets/left_icn.png')} />
              </TouchableOpacity>
              <Text style={{ alignSelf: 'center', fontFamily: 'montserrat_regular', color: '#FFFFFF', fontSize: 18, padding: 15 }}>JOB DETAILS</Text>

            </View>

            <Loader isLoading={this.state.isLoading} />

            <TouchableOpacity activeOpacity={0.9} >
              <View style={styles.headerContainer}>

                <View style={{ width: '16%', justifyContent: 'center', alignItems: 'center' }}>
                  <Image style={{ height: 40, width: 42, tintColor: '#000000' }} source={require('./../../assets/jobs_icn.png')} />
                </View>
                <View style={{ width: "68%", paddingHorizontal: 10, paddingVertical: 2 }}>
                  <View style={{ flexDirection: 'row' }}>

                    <Text
                      numberOfLines={1}
                      style={{ color: '#000000', fontFamily: 'montserrat_semibold', fontSize: 14, }}
                    >{this.state.backData.job_details.job_title}</Text>
                  </View>

                  <Text
                    numberOfLines={1}
                    style={{ color: '#000000', fontFamily: 'montserrat_light', fontSize: 14, }}
                  >{this.state.backData.job_details.job_description} </Text>
                  <View style={{ marginTop: 5, flexDirection: "row", alignItems: 'center' }}>
                    <Image source={require('./../../assets/pin_icn.png')} style={{ width: 10, height: 15 }} resizeMode='stretch' />
                    <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>
                      {this.state.backData.job_details.location}{this.state.backData.job_details.landmark ? ", " + this.state.backData.job_details.landmark : ''}{this.state.backData.job_details.zip ? ", " + this.state.backData.job_details.zip : ''}
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'column' }}>
                    <View style={{ marginTop: 5, flexDirection: "row", marginRight: 2, alignItems: 'center' }}>
                      <Image source={require('./../../assets/calendar_icn_b.png')} style={{ width: 10, height: 10 }} resizeMode='stretch' />
                      <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 10 }}>
                        {this.state.backData.job_details.shift.start_date} to {this.state.backData.job_details.shift.end_date}
                      </Text>
                    </View>
                    <View style={{ marginTop: 5, flexDirection: "row", alignItems: 'center' }}>
                      <Image source={require('./../../assets/time_icn_a.png')} style={{ width: 10, height: 10 }} resizeMode='stretch' />
                      <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12 }}>
                        {this.state.backData.job_details.shift.start_time} to {this.state.backData.job_details.shift.end_time}
                      </Text>
                    </View>

                    <View style={{ marginTop: 5, flexDirection: "row", alignItems: 'center' }}>
                      <Image source={require('./../../assets/time_icn_a.png')} style={{ width: 10, height: 10 }} resizeMode='stretch' />
                      <Text numberOfLines={1} style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12 }}>
                        Posted date- {moment(this.state.backData.job_details.created_at).format('yyyy-MM-DD hh:mm A')}
                      </Text>
                    </View>

                  </View>

                  {/* <Text style={{ marginLeft: 3, color: '#000000', fontFamily: 'montserrat_regular', fontSize: 12 }}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</Text> */}

                  {
                    this._returnButton()
                  }

                </View>
              </View>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </Fragment>
    );
  }


  _returnButton = () => {
    const { navigation } = this.props;

    const worker_id = navigation.getParam('worker_id', 'NO-item');
    const job_id = navigation.getParam('job_id', 'NO-item');

    return (
      <View style={{ flexDirection: 'row', marginTop: 10 }}>
        <TouchableOpacity style={styles.loginHold}
          onPress={() => this._workerMyJobUpdateToWorker(job_id, "1")}
        >
          <Text style={styles.loginText}>ACCEPT</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.registerHold}
          onPress={() => this._workerMyJobUpdateToWorker(job_id, "0")}
        >
          <Text style={styles.registerText}>REJECT</Text>
        </TouchableOpacity>

      </View>

    )

    // if (position == 0) {


    // } else {
    //   return (
    //     <View style={{ flexDirection: 'row', marginTop: 10 }}>
    //       <TouchableOpacity style={styles.loginHold}
    //         onPress={() => this.props.navigation.goBack()}
    //       >
    //         <Text style={styles.loginText}>CONFIRM</Text>
    //       </TouchableOpacity>

    //       <TouchableOpacity style={styles.registerHold}
    //         onPress={() => this.props.navigation.goBack()}
    //       >
    //         <Text style={styles.registerText}>DENY</Text>
    //       </TouchableOpacity>

    //     </View>
    //   )
    // }

  }


}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFFFFF',
    flex: 1,

  },

  tabHolder: {
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 30,
    flex: 1
  },
  rowContainer: {
    flexDirection: 'row',
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    borderColor: '#EAEAEA',
    borderWidth: 2,
    marginBottom: 5
  },

  headerContainer: {

    flexDirection: 'row',
    backgroundColor: "#FFFFFF",

    marginBottom: 5,
    padding: 10


  },
  loginHold: {
    flex: 1,
    padding: 5,
    backgroundColor: "#657CF4",
    borderRadius: 20,
    marginRight: 5
  },
  loginText: {
    color: '#FFFFFF',
    fontSize: 15,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center',
    padding: 5

  },
  registerHold: {
    flex: 1,
    padding: 5,
    backgroundColor: "#00D5E1",
    borderRadius: 20,
    marginLeft: 5
  },
  registerText: {
    color: '#FFFFFF',
    fontSize: 15,
    fontFamily: "montserrat_semibold",
    alignSelf: 'center',
    padding: 5,

  },
  radioIcon: {
    width: 20,
    height: 20,
    marginRight: 5,
    alignSelf: 'center'

  },
  filterListItem: {
    padding: 5,
    flexDirection: 'row',
    marginLeft: 8,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  starStyle: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    margin: 2

  },
});

