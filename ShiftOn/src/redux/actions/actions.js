
export const refreshFunction = (text) => {
    return {
        type: "IS_REFRESH",
        isRefresh: text,
    }
}

export const loginUserDetails = (id, name, email, image, roleType) => {
    return {
        type: "USER_DETAILS",
        userId: id,
        userName: name,
        userEmail: email,
        userImage: image,
        userRoleType: roleType,
    }
}

export const workerSettings = (id, availability, searchable, employed) => {
    return {
        type: "WORKER_SETTING",
        userId: id,
        availability: availability,
        searchable: searchable,
        employed: employed,

    }
}

export const controllerSetting = (id, passiveMood) => {
    return {
        type: "CONTROLLER_SETTING",
        userId: id,
        passiveMood: passiveMood,
    }
}

export const countryStateCityRedux = (countryName, countryId, stateName, stateId, cityName, cityId) => {
    return {
        type: "COUNTRY_STATE_CITY_REDUX",
        countryName: countryName,
        countryId: countryId,
        stateName: stateName,
        stateId: stateId,
        cityName: cityName,
        cityId: cityId,
    }


}

export const defaultImageUrl = (url) => {
    return {
        type: "DEFAULT_IMAGE_URL",
        url: url,
    }


}