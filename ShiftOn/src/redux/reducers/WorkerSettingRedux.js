const initialValue = {
    userId: '',
    availability: '',
    searchable: '',
    employed: '',
}

const workerSettingRedux = (state = initialValue, action) => {
    switch (action.type) {
        case "WORKER_SETTING":
            return {
                ...state, userId: action.userId, availability: action.availability,
                searchable: action.searchable, employed: action.employed,
            }
        default: return state;
    }
}

export default workerSettingRedux;