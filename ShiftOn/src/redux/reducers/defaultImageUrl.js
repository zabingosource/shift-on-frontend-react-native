const initialName = {
    url: '',
}

const defaultImageUrl = (state = initialName, action) => {
    switch (action.type) {
        case "DEFAULT_IMAGE_URL":
            return {
                ...state, url: action.url,
            }
        default: return state;
    }
}

export default defaultImageUrl;