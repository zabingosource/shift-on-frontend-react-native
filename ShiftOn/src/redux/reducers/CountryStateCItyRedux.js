const initialValue = {
    countryName: '',
    stateName: '',
    cityName: '',
    countryId: '',
    stateId: '',
    cityId: '',
}

const countryStateCityReduxValue = (state = initialValue, action) => {
    switch (action.type) {
        case "COUNTRY_STATE_CITY_REDUX":
            return {
                ...state, countryName: action.countryName, stateName: action.stateName, cityName: action.cityName,
                countryId: action.countryId, stateId: action.stateId, cityId: action.cityId,
            }
        default: return state;
    }
}

export default countryStateCityReduxValue;