const initialName = {
    id: '',
    name: 'Hi There!',
    email: 'info@info',
    image: '',
    roleType: '',
}

const userLoginDetails = (state = initialName, action) => {
    switch (action.type) {
        case "USER_DETAILS":
            return {
                ...state, id: action.userId, name: action.userName, email: action.userEmail, image: action.userImage, roleType: action.userRoleType,
            }
        default: return state;
    }
}

export default userLoginDetails;