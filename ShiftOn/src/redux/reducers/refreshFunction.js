const initialName = {
    isRefresh: 'NO',
}

const refreshFunction = (state = initialName, action) => {
    switch (action.type) {
        case "IS_REFRESH":
            return {
                ...state, isRefresh: action.isRefresh,
            }
        default: return state;
    }
}

export default refreshFunction;