import { combineReducers } from "redux";

import refreshFunction from "./refreshFunction";
import userLoginDetails from "./loginUserDetails";
import countryStateCityReduxValue from "./CountryStateCItyRedux";
import workerSettingRedux from "./WorkerSettingRedux";
import controllerSettingRedux from "./ControllerSettingRedux";
import defaultImageUrl from "./defaultImageUrl";

const rootReducer = combineReducers({
    refreshFunction,
    userLoginDetails,
    countryStateCityReduxValue,
    workerSettingRedux,
    controllerSettingRedux,
    defaultImageUrl,
})

export default rootReducer;