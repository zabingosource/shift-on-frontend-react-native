const initialValue = {
    userId: '',
    passiveMood: '',

}

const controllerSettingRedux = (state = initialValue, action) => {
    switch (action.type) {
        case "CONTROLLER_SETTING":
            return {
                ...state, userId: action.userId, passiveMood: action.passiveMood,
            }
        default: return state;
    }
}

export default controllerSettingRedux;