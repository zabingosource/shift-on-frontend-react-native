import React, { Component } from 'react';
import {
    Alert, StatusBar, StyleSheet, Text, TouchableOpacity, View,
} from 'react-native';
import CustomTextInput from '../components/CustomTextInput';
import ModalSelector from 'react-native-modal-selector-searchable';
import Loader from './Loader';
import Api from '../apis/Api';
import { connect } from 'react-redux';
import { countryStateCityRedux } from '../redux/actions/actions';


const mapDispatchToProps = (dispatch) => {
    return {
        countryNameDetails: (txt1, txt2, txt3, txt4, txt5, txt6) => dispatch(countryStateCityRedux(txt1, txt2, txt3, txt4, txt5, txt6)),
    }
}

class CountryStateCity extends Component {

    constructor(props) {
        super(props);
        this.state = {
            countryData: [],
            stateData: [],
            cityData: [],
            countryName: '',
            stateName: '',
            cityName: '',
            countryId: '',
            stateId: '',
            cityId: '',
            isLoading: false,
        }
    }

    componentDidMount() {

        this.setState({ isLoading: true });

        Api._countryListApi()
            .then((response) => {

                console.log("COUNTRY:- ", response.data);
                var count = Object.keys(response.data).length;
                let countryArray = [];
                for (var i = 0; i < count; i++) {
                    countryArray.push({
                        id: response.data[i].id,
                        label: response.data[i].country_name,
                    });
                }
                this.setState({ countryData: countryArray, isLoading: false, });

            })
            .catch((err) => {
                console.log("country error: " + err);
                this.setState({ isLoading: false, });
            });
    };

    handleState = (countryCode, countryName) => {
        let rawData = { "country_id": countryCode, }

        Api._stateListApi(rawData)
            .then((response) => {

                console.log("STATE:- ", response.data);
                var count = Object.keys(response.data).length;
                let stateArray = [];
                for (var i = 0; i < count; i++) {
                    stateArray.push({
                        id: response.data[i].id,
                        label: response.data[i].state_name,
                    });
                }
                this.setState({ stateData: stateArray, isLoading: false, });

            })
            .catch((err) => {
                console.log("state error: " + err);
                this.setState({ isLoading: false, });
            });

        this.props.countryNameDetails(countryName, countryCode, "", "", "", "");

    };

    handleCity = (stateCode, stateName) => {
        let rawData = { "state_id": stateCode, }

        Api._cityListApi(rawData)
            .then((response) => {

                console.log("CITY:- ", response.data);
                var count = Object.keys(response.data).length;
                let cityArray = [];
                for (var i = 0; i < count; i++) {
                    cityArray.push({
                        id: response.data[i].city_id,
                        label: response.data[i].city_name,
                    });
                }
                this.setState({ cityData: cityArray, isLoading: false, });

            })
            .catch((err) => {
                console.log("state error: " + err);
                this.setState({ isLoading: false, });
            });

        this.props.countryNameDetails(this.state.countryName, this.state.countryId, stateName,
            stateCode, "", "")

    };

    nameValues = (cityname, cityid) => {
        this.props.countryNameDetails(this.state.countryName, this.state.countryId, this.state.stateName,
            this.state.stateId, cityname, cityid)
    }

    render() {
        return (
            <View style={styles.container} >

                <View >

                    <Loader isLoading={this.state.isLoading} />

                    <ModalSelector
                        data={this.state.countryData}
                        initValue="COUNTRY"
                        accessible={true}
                        search={true}
                        animationType={"fade"}
                        keyExtractor={item => item.id}
                        labelExtractor={item => item.label}
                        onChange={(option) => {
                            this.setState({
                                countryName: option.label,
                                countryId: option.id,
                                stateName: '',
                                stateId: '',
                                cityName: '',
                                cityId: '',
                                isLoading: true,
                            });
                            this.handleState(option.id, option.label);
                        }}>

                        <View style={{ marginTop: 5 }}>
                            <CustomTextInput
                                placeholder='COUNTRY'
                                keyboardType='default'
                                floatingText="COUNTRY"
                                value={this.state.countryName}
                                editable={false}
                                onFocus={true}
                                showSoftInputOnFocus={true}
                                caretHidden={false}
                                isVisibleDropDown={true}
                                returnKeyType={'done'}
                            />
                        </View>

                    </ModalSelector>

                    <ModalSelector
                        data={this.state.stateData}
                        initValue="STATE"
                        accessible={true}
                        search={true}
                        animationType={"fade"}
                        keyExtractor={item => item.id}
                        labelExtractor={item => item.label}
                        onChange={(option) => {
                            this.setState({
                                stateName: option.label,
                                stateId: option.id,
                                cityName: '',
                                cityId: '',
                                isLoading: true,
                            });
                            this.handleCity(option.id, option.label);
                        }}>

                        <View style={{ marginTop: 5 }}>
                            <CustomTextInput
                                placeholder='STATE'
                                keyboardType='default'
                                floatingText="STATE"
                                value={this.state.stateName}
                                editable={false}
                                onFocus={true}
                                showSoftInputOnFocus={true}
                                caretHidden={false}
                                isVisibleDropDown={true}
                                returnKeyType={'done'}
                            />
                        </View>

                    </ModalSelector>

                    <ModalSelector
                        data={this.state.cityData}
                        initValue="CITY"
                        accessible={true}
                        search={true}
                        animationType={"fade"}
                        keyExtractor={item => item.id}
                        labelExtractor={item => item.label}
                        onChange={(option) => {
                            this.setState({
                                cityName: option.label,
                                cityId: option.id,
                            });
                            this.nameValues(option.label, option.id);
                        }}>

                        <View style={{ marginTop: 5 }}>
                            <CustomTextInput
                                placeholder='CITY'
                                keyboardType='default'
                                floatingText="CITY"
                                value={this.state.cityName}
                                editable={false}
                                onFocus={true}
                                showSoftInputOnFocus={true}
                                caretHidden={false}
                                isVisibleDropDown={true}
                                returnKeyType={'done'}
                            />
                        </View>

                    </ModalSelector>

                </View>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
    },
    dropdown: {
        height: 50,
        borderColor: 'gray',
        borderWidth: 0.5,
        borderRadius: 8,
        paddingHorizontal: 8,
        marginBottom: 10,
    },
    icon: {
        marginRight: 5,
    },
    label: {
        position: 'absolute',
        backgroundColor: 'white',
        left: 22,
        top: 8,
        zIndex: 999,
        paddingHorizontal: 8,
        fontSize: 14,
    },
    placeholderStyle: {
        fontSize: 16,
    },
    selectedTextStyle: {
        fontSize: 16,
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 16,
    },
});

export default connect(null, mapDispatchToProps)(CountryStateCity);