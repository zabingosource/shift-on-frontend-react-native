import { StackActions, NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Alert } from 'react-native';


const resetToLogin = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'Login' })],
});

export const removeLogDetailsToAsyncStorage = async (thisNavigate) => {

    Alert.alert(
        'Confirm to Logout?',
        'Are you sure you want to logout?',
        [
            { text: 'NO', onPress: () => console.log('Close pressed') },
            {
                text: 'YES', onPress: () => { logout(thisNavigate) }
            },
        ]
    )
}

const logout = async (thisNavigate) => {
    try {
        await AsyncStorage.setItem('user_id', '');
        await AsyncStorage.setItem('user_name', '');
        await AsyncStorage.setItem('user_email', '');
        await AsyncStorage.setItem('user_image', '');
        await AsyncStorage.setItem('user_role', '');

    } catch (e) {
        console.log(e);
    }
    thisNavigate.dispatch(resetToLogin);
    console.log("Logout successfully.")
}

export const logoutApi = async (thisNavigate) => {
    try {
        await AsyncStorage.setItem('user_id', '');
        await AsyncStorage.setItem('user_name', '');
        await AsyncStorage.setItem('user_email', '');
        await AsyncStorage.setItem('user_image', '');
        await AsyncStorage.setItem('user_role', '');

    } catch (e) {
        console.log(e);
    }
    thisNavigate.dispatch(resetToLogin);
}