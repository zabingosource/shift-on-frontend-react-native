import React, { useState } from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';
import CustomTextInput from '../components/CustomTextInput';

const DropdownList = ({
    data = [],
    value = {},
    onSelect = () => { },
}) => {

    const [showOption, setShowOption] = useState(false);

    const onSelectedItem = (roleValue) => {
        setShowOption(false)
        onSelect(roleValue)
    }

    return (
        <View>
            {/* <TouchableOpacity style={{ backgroundColor: 'gray', padding: 10, borderRadius: 10 }}
                activeOpacity={0.8}
                onPress={() => setShowOption(!showOption)}
            >
                <Text>{!!value ? value.value : "Choose an option"}</Text>
            </TouchableOpacity> */}

            <View
                style={{ marginTop: 5 }}
            >
                <CustomTextInput
                    placeholder="SELECT ROLE"
                    floatingText="SELECT ROLE"
                    editable={true}
                    onFocus={true}
                    showSoftInputOnFocus={false}
                    caretHidden={true}
                    isVisibleDropDown={true}
                    value={value}
                    returnKeyType={'done'}
                    onTouchEnd={() => setShowOption(!showOption)}
                />

            </View>

            {showOption && (<View style={{
                backgroundColor: '#dedad9', padding: 5, borderRadius: 10,
                marginTop: 3,
            }}>
                {data.map((value, i) => {
                    return (
                        <TouchableOpacity
                            key={String(i)}
                            onPress={() => onSelectedItem(value.name)}
                            style={{ padding: 5, }}
                        >
                            <Text style={{ color: 'white' }}>
                                {value.name}
                            </Text>
                        </TouchableOpacity>
                    )
                })}
            </View>)}
        </View>
    );

}

export default DropdownList;