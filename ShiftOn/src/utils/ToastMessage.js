import Toast from 'react-native-simple-toast';

export const setToastMsg = (msg) => {
    Toast.showWithGravity(msg, Toast.LONG, Toast.BOTTOM);
}