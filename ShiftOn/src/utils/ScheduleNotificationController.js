import PushNotification from "react-native-push-notification";
import moment from "moment";

const date = moment().format('yyyy/MM/DD');


PushNotification.createChannel(
    {
        channelId: "channel-id", // (required)
        channelName: "My channel", // (required)
        channelDescription: "A channel to categorise your notifications", // (optional) default: undefined.
        playSound: false, // (optional) default: true
        soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
        importance: 4, // (optional) default: 4. Int value of the Android notification importance
        vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
    },
    (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
);

export const scheduleNotificationHandler = (id, title, message, apiDate) => {

    PushNotification.localNotificationSchedule({
        channelId: "channel-id", // (required)
        channelName: "My channel", // (required)
        id: id,
        title: title,
        message: message,
        autoCancel: true,
        vibrate: true,
        vibration: 300,
        playSound: true,
        soundName: 'default',
        importance: 'high',
        vibrate: true,
        date: new Date(apiDate),
        allowWhileIdle: true,
        data: { 'screen': 'Calender' },
    });
};

export const getAllScheduledNotifications = async () => {

    var allData = [];
    PushNotification.getScheduledLocalNotifications((data) => {
        //console.log(data);
        allData = data;
    });

    return allData;

}

export const cancelScheduledNotifications = (id) => {
    PushNotification.cancelLocalNotification(id);
}