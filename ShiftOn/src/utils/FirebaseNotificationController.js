import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const notificationListener = async (screen) => {
    messaging().onNotificationOpenedApp(remoteMessage => {
        console.log("background notification: ", remoteMessage.notification)
    });

    messaging().onMessage(async remoteMessage => {
        console.log("foreground notification: ", remoteMessage)
    });

    messaging().getInitialNotification().then(remoteMessage => {
        if (remoteMessage) {
            console.log("Notification caused app to open from quite state: ", remoteMessage.notification)
        }
    })

}