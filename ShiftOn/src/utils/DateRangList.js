import moment from "moment";

// const getDatesInRangeList = (startDate, endDate, color) => {

//     let markedDay = {};
//     getDatesInRange(startDate, endDate).map((item, index) => {
//         if (index == 0) {
//             markedDay[item] = {
//                 startingDay: true,
//                 endingDay: false,
//                 selected: true,
//                 color: color,
//             };
//         } else if (index == getDatesInRange(startDate, endDate).length - 1) {
//             markedDay[item] = {
//                 startingDay: false,
//                 endingDay: true,
//                 selected: true,
//                 color: color,
//             };
//         } else {
//             markedDay[item] = {
//                 selected: true,
//                 color: color,
//             };
//         }

//     });

//     return markedDay;
// }


export function getRandomColor(color) {
    return color[Math.floor((Math.random() * color.length))];
}

export function getDatesInRange(startDate, endDate) {
    const date = new Date(startDate.getTime());

    const dates = [];

    // Exclude end date
    while (date <= endDate) {
        dates.push(
            moment(new Date(date)).local().format("YYYY-MM-DD")
        );
        date.setDate(date.getDate() + 1);
    }

    return dates;
}


