import AppConstants from "./AppConstants";


const formHeader = {
    'Content-Type': 'multipart/form-data',
    // 'Accept': 'application/json',
};

export default class Api {

    // 20/9/2022 @Aman 
    // this login api is not calling for some issues rised...
    static _loginApi(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.LOGIN;
        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log("RESPONSE:- ", responseData)
                return responseData;

            }).catch((error) => {
                console.log("ERROR......");
                console.log("ERROR:- ", error);

            });
    }

    static _workerResitration(formData) {

        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_RESISTRATION;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _getWorkerPersonalInfo(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_GET_DETAIL_INFO;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _workerPersonalInfoUpdate(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_UPDATE_PERSONAL_INFO;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _workerAcademicProfessionalUpdate(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_UPDATE_ACADEMIC_PROFESSIONAL;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _workerCurrentEmployerUpdate(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_UPDATE_CURRENT_EMPLOYER;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _workerEmploymentHistoryUpdate(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_UPDATE_EMPLOYMENT_HISTORY;
        console.log(rowData)

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _workerMncAndSkillUpdate(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_UPDATE_NMC_SKILL;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _workerReferencesUpdate(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_UPDATE_REFERENCES;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _workerKinDetailsUpdate(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_UPDATE_KIN_DETAILS;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _workerCriminalRecordsUpdate(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_UPDATE_CRIMINAL_RECORDS;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _workerEmploymentActUpdate(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_UPDATE_EMPLOYMENT_ACT;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _workerSettingsApi(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_SETTING_API;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _workerAgencyListing(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_GET_AGENCY_LIST;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _workerAgencyListUpdateStatus(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_AGENCY_LIST_UPDATE;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _workerSearchAgency(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_SEARCH_AGENCY;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _workerJoinAgency(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_JOIN_AGENCY;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _unitAgencyListings(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_AGENCY_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitAgencyListingApproval(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_AGENCY_LISTING_APPROVAL;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitRoleListing(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_ROLE_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitAddRole(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_ADD_ROLE;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _unitUpdateRole(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_UPDATE_ROLE;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _unitActiveInactiveRole(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_ACTIVE_INACTIVE_ROLE;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _unitJobListing(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_JOB_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitMainRepostedJobListing(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_MAIN_REPOSTED_JOB_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }


    static _unitJobAgencyAvailabilityListing(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_JOB_AGENCY_AVAILABILITY_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitJobPostToAgency(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_JOB_POST_TO_AGENCY;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitRepostedJobListing(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_REPOSTED_JOB_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitRepostedAcceptedRejected(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_REPOSTED_ACCEPTED_REJECTED;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitAddJob(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_ADD_JOB;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _unitJobOpenClose(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_JOB_OPEN_CLOSE;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _unitCheckPayrateLinkStatus(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_CHECK_PAYRATE_STATUS;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _workerMyJobListing(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_MY_JOB_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _workerMyJobUpdateToWorker(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_MY_JOB_UPDATE_TO_WORKER;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _workerJobConfirmation(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_JOB_CONFIRMATION;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _workerCurrentJobSheet(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_CURRENT_JOB_SHEET_LIST;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _workerAddJobSheet(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_ADD_JOB_SHEET;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _workerTimeSheetList(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_TIME_SHEET_LIST;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _checkWorkerProfileCompletion(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.WORKER_PROFILE_DATA_CHECK_COMPLETION;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _unitAddShift(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_ADD_SHIFT;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _unitUpdateShift(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_UPDATE_SHIFT;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _unitUpdateShiftStatus(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_UPDATE_SHIFT_STATUS;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }


    static _unitShiftListing(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_SHIFT_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitWardListing(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_WARD_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitAddWard(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_ADD_WARD;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitUpdateWard(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_UPDATE_WARD;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitWardStatusUpdate(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_WARD_UPDATE_STATUS;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitPayRatesListings(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_PAY_RATES_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitAddPayRates(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_ADD_PAY_RATES;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitUpdatePayRates(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_UPDATE_PAY_RATES;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitPayRatesStatusUpdate(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_PAY_RATES_UPDATE_STATUS;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitWardShiftListings(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_WARD_SHIFT_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitWardShiftListingsWithLinkedStatus(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_WARD_SHIFT_LISTING_LINKED_STATUS;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitWardShiftLinkingWithStatus(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_WARD_SHIFT_LINKING_WITH_STATUS;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitWardShiftPayrateListings(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_WARD_SHIFT_PAYRATE_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitWardShiftPayrateListsWithLinkedStatus(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_WARD_SHIFT_PAYRATE_LIST_LINKED_STATUS;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitWardShiftPayrateLinkingWithStatus(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_WARD_SHIFT_PAYRATE_LINKING_WITH_STATUS;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _getUnitAllLocations(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_All_LOCATIONS;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitUpdateLocationStatus(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_UPDATE_LOCATION_STATUS;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitTimeSheetList(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_TIME_SHEET_LIST;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitApproveTimeSheet(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_APPROVE_TIME_SHEET;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _unitUpdateProfileImage(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_PROFILE_IMAGE_UPDATE;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _unitRegistration(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_REGISTRATION;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _unitUpdateProfile(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_PROFILE_UPDATE;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _checkUnitExistName(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UNIT_CHECK_NAME_EXISTS;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _unitGetProfile(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.GET_UNIT_PROFILE;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _controllerMyJobListing(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.CONTROLLER_MY_JOB_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _controllerUnitJobListing(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.CONTROLLER_UNIT_JOB_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _controllerAllJobListing(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.CONTROLLER_ALL_JOB_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _controllerAddNewShift(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.ADD_JOB_BY_CONTROLLER;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error);
            });
    }

    static _controllerAgencyUnitList(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.AGENCY_UNIT_LIST;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error);
            });
    }

    static _controllerWorkerAvailablityList1(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.CONTROLLER_WORKER_AVAILITY_LISTING_1;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _controllerWorkerAvailablityListPostToWorker(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.CONTROLLER_WORKER_AVAILITY_LISTING_POST_TO_WORKER;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _controllerWorkerAvailablityList2(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.CONTROLLER_WORKER_AVAILITY_LISTING_2;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _controllerWorkerAvailablityListRepostToUnit(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.CONTROLLER_WORKER_AVAILITY_LISTING_REPOST_TO_UNIT;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _controllerWorkerAvailablityList3(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.CONTROLLER_WORKER_AVAILITY_LISTING_3;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _controllerWorkerAvailablityListConfirmToWorker(rowData) {
        endpoint = AppConstants.BASE_URL + AppConstants.CONTROLLER_WORKER_AVAILITY_LISTING_CONFIRM_TO_WORKER;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rowData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _controllerWorkerAvailablityFinalList(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.CONTROLLER_WORKER_AVAILITY_FINAL_LISTING;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _controllerGetProfileData(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.GET_CONTROLLER_PROFILE;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _controllerUpdateProfile(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.CONTROLLER_PROFILE_UPDATE;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _controllerSettingsApi(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.CONTROLLER_SETTING_API;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }


    static _countryListApi() {
        endpoint = AppConstants.BASE_URL + AppConstants.COUNTRY_LIST_API;

        return fetch(endpoint)
            .then((response) => response.json())
            .then((responseData) => {
                console.log("Country listings")
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _stateListApi(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.STATE_LIST_API;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log("State listings")
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _cityListApi(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.CITY_LIST_API;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log("City listings")
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    /// gender api...
    static _getGenderListApi() {
        endpoint = AppConstants.BASE_URL + AppConstants.GENDER_LIST_API;

        return fetch(endpoint)
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    /// currency api...
    static _getCurrencyListApi() {
        endpoint = AppConstants.BASE_URL + AppConstants.CURRENCY_LIST_API;

        return fetch(endpoint)
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    /// currency api...
    static _addCurrencyToLocationApi(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.CURRENCY_TO_LOCATION_API;

        return fetch(endpoint,
            {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(rawData),

            })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    /// shift type api...
    static _getShiftTypeListApi() {
        endpoint = AppConstants.BASE_URL + AppConstants.SHIFT_TYPE_LIST_API;

        return fetch(endpoint)
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    // Calender sections....
    static _workerAddCalenderApi(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.ADD_WORKEER_CALENDER;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }


    static _userCalenderListApi(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.USER_CALENDER_LIST;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _generateOtpApi(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.GENERATE_OTP;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _updatePasswordApi(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.UPDATE_PASSWORD;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _qualificationListing() {
        endpoint = AppConstants.BASE_URL + AppConstants.QUALIFICATION_LIST_API;

        return fetch(endpoint)
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)

            });
    }

    static _isUserLoggedIn(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.IS_LOGGED_IN_API;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static _defaultImageUrl(rawData) {
        endpoint = AppConstants.BASE_URL + AppConstants.DEFAULT_IMAGE_URL;

        return fetch(endpoint, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(rawData),

        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

    static getCurrentVersionApi(formData) {
        endpoint = AppConstants.BASE_URL + AppConstants.CURRENT_VERSION;

        return fetch(endpoint, {
            method: "POST",
            headers: formHeader,
            body: formData,
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
                return responseData;
            }).catch((error) => {
                console.log("ERROR......");
                console.log(error)
            });
    }

}