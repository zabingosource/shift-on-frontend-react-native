export default class AppConstants {

    static URL = "https://shifton.worksamples.info";
    static BASE_URL = "https://shifton.worksamples.info/api";
    static IMAGE_URL = "https://shifton.worksamples.info/public/uploads/";


    static LOGIN = "/login";


    // Worker section apis list...
    static WORKER_RESISTRATION = "/add_worker";
    static WORKER_GET_DETAIL_INFO = "/get_worker";
    static WORKER_UPDATE_PERSONAL_INFO = "/update_worker";
    static WORKER_UPDATE_ACADEMIC_PROFESSIONAL = "/worker_academic_professional";
    static WORKER_UPDATE_CURRENT_EMPLOYER = "/worker_employment";
    static WORKER_UPDATE_EMPLOYMENT_HISTORY = "/worker_employment_history";
    static WORKER_UPDATE_NMC_SKILL = "/worker_nmc_skills";
    static WORKER_UPDATE_REFERENCES = "/worker_refrences";
    static WORKER_UPDATE_KIN_DETAILS = "/worker_kins";
    static WORKER_UPDATE_CRIMINAL_RECORDS = "/worker_criminal_records";
    static WORKER_UPDATE_EMPLOYMENT_ACT = "/worker_employment_act";
    static WORKER_GET_AGENCY_LIST = "/worker_agency";
    static WORKER_AGENCY_LIST_UPDATE = "/worker_agency_status_update";
    static WORKER_SEARCH_AGENCY = "/agency_search";
    static WORKER_JOIN_AGENCY = "/join_agency";
    static WORKER_MY_JOB_LISTING = "/worker_jobs";
    static WORKER_MY_JOB_ADD_TO_WORKER = "/add_job_to_worker";
    static WORKER_MY_JOB_UPDATE_TO_WORKER = "/update_job_to_worker";
    static WORKER_JOB_CONFIRMATION = "/worker_confirmation";
    static WORKER_CURRENT_JOB_SHEET_LIST = "/worker_current_jobs";
    static WORKER_ADD_JOB_SHEET = "/add_job_sheet";
    static WORKER_TIME_SHEET_LIST = "/worker_timesheet_list";
    static WORKER_PROFILE_DATA_CHECK_COMPLETION = "/worker_profile_data";


    // Unit section apis list...
    static UNIT_AGENCY_LISTING = "/agency_listing";
    static UNIT_AGENCY_LISTING_APPROVAL = "/agency_unit_approve";
    static UNIT_ROLE_LISTING = "/role_listing";
    static UNIT_ADD_ROLE = "/add_role";
    static UNIT_UPDATE_ROLE = "/update_role";
    static UNIT_ACTIVE_INACTIVE_ROLE = "/active_inactive_role";
    static UNIT_JOB_LISTING = "/job_listing";
    static UNIT_MAIN_REPOSTED_JOB_LISTING = "/list_of_reposted_job_for_unit";
    static UNIT_ADD_JOB = "/add_job";
    static UNIT_JOB_OPEN_CLOSE = "/change_job_status";
    static UNIT_CHECK_PAYRATE_STATUS = "/check_payrate_to_shift";
    static UNIT_All_LOCATIONS = "/get_unit_location";
    static UNIT_UPDATE_LOCATION_STATUS = "/unit_location_status_update";
    static UNIT_JOB_AGENCY_AVAILABILITY_LISTING = "/unit_agency_availability";
    static UNIT_JOB_POST_TO_AGENCY = "/post_to_agency";
    static UNIT_REPOSTED_JOB_LISTING = "/list_of_reposted_job";
    static UNIT_REPOSTED_ACCEPTED_REJECTED = "/reposted_accept_reject";
    static UNIT_ADD_SHIFT = "/add_shift";
    static UNIT_UPDATE_SHIFT = "/update_shift";
    static UNIT_UPDATE_SHIFT_STATUS = "/shift_status_update";
    static UNIT_SHIFT_LISTING = "/shift_listing";
    static UNIT_WARD_LISTING = "/ward_listing";
    static UNIT_ADD_WARD = "/ward_add";
    static UNIT_UPDATE_WARD = "/ward_update";
    static UNIT_WARD_UPDATE_STATUS = "/ward_status_update";
    static UNIT_PAY_RATES_LISTING = "/pay_rate_listing";
    static UNIT_ADD_PAY_RATES = "/pay_rate_add";
    static UNIT_UPDATE_PAY_RATES = "/pay_rate_update";
    static UNIT_PAY_RATES_UPDATE_STATUS = "/pay_rate_status_update";
    static UNIT_WARD_SHIFT_LISTING = "/shift_list_by_ward";
    static UNIT_WARD_SHIFT_LISTING_LINKED_STATUS = "/shift_list_with_link_status";
    static UNIT_WARD_SHIFT_LINKING_WITH_STATUS = "/link_shift_to_ward";
    static UNIT_WARD_SHIFT_PAYRATE_LISTING = "/payrate_list_by_shift";
    static UNIT_WARD_SHIFT_PAYRATE_LIST_LINKED_STATUS = "/payrate_list_with_link_status";
    static UNIT_WARD_SHIFT_PAYRATE_LINKING_WITH_STATUS = "/link_payrate_to_shift";

    static UNIT_TIME_SHEET_LIST = "/unit_timesheet_list";
    static UNIT_APPROVE_TIME_SHEET = "/approve_timesheet";
    static GET_UNIT_PROFILE = "/get_unit_data";
    static UNIT_REGISTRATION = "/unit_registration";
    static UNIT_PROFILE_UPDATE = "/update_unit";
    static UNIT_PROFILE_IMAGE_UPDATE = "/update_unit_profile_image";
    static UNIT_CHECK_NAME_EXISTS = "/unit_exists";


    // shift controller section apis list...
    static CONTROLLER_UNIT_JOB_LISTING = "/unit_job_listing_controller";
    static CONTROLLER_MY_JOB_LISTING = "/my_job_listing_controller";
    static CONTROLLER_ALL_JOB_LISTING = "/job_listing_controller";
    //static CREATE_SHIFT_BY_CONTROLLER = "/create_shift_by_controller";
    static ADD_JOB_BY_CONTROLLER = "/add_job";
    static AGENCY_UNIT_LIST = "/agency_units";
    static CONTROLLER_WORKER_AVAILITY_LISTING_1 = "/available_worker1";
    static CONTROLLER_WORKER_AVAILITY_LISTING_POST_TO_WORKER = "/post_to_worker";
    static CONTROLLER_WORKER_AVAILITY_LISTING_2 = "/available_worker2";
    static CONTROLLER_WORKER_AVAILITY_LISTING_REPOST_TO_UNIT = "/repost_job_to_unit";
    static CONTROLLER_WORKER_AVAILITY_LISTING_3 = "/available_worker3";
    static CONTROLLER_WORKER_AVAILITY_LISTING_CONFIRM_TO_WORKER = "/confirm_to_worker";
    static CONTROLLER_WORKER_AVAILITY_FINAL_LISTING = "/job_workers_final";
    static CONTROLLER_PROFILE_UPDATE = "/update_controller_profile";
    static GET_CONTROLLER_PROFILE = "/get_controller_data";


    // Settings apis lists......
    static WORKER_SETTING_API = "/worker_settings";
    static CONTROLLER_SETTING_API = "/controller_settings";

    // Country, State and City listing apis...
    static COUNTRY_LIST_API = "/countries";
    static STATE_LIST_API = "/states_by_country";
    static CITY_LIST_API = "/cities_by_state";

    // Gender data api
    static GENDER_LIST_API = "/get_genders";

    // Currency data api
    static CURRENCY_LIST_API = "/get_currencies";

    // Currency to location data api
    static CURRENCY_TO_LOCATION_API = "/add_curr_to_location";

    // Shift type data api
    static SHIFT_TYPE_LIST_API = "/get_shift_types";

    // Calender sections....
    static ADD_WORKEER_CALENDER = "/add_user_calendar";
    static USER_CALENDER_LIST = "/user_calendar";

    // reset password...
    static GENERATE_OTP = "/generate_otp";
    static UPDATE_PASSWORD = "/update_password";

    // for webview apis...
    static WORKER_DETAILS_WEBVIEW = "/worker_details/";
    static AGENCY_DETAILS_WEBVIEW = "/agency_details/";
    static SHIFT_DETAILS_WEBVIEW = "/shift_detail/";
    static ROLE_DETAILS_WEBVIEW = "/role_details/";
    static WARD_DETAILS_WEBVIEW = "/wards_details/";
    static PAY_RATES_DETAILS_WEBVIEW = "/pay_rates_details/";
    static JOB_DETAILS_WEBVIEW = "/job_details/";

    //for qualification list dropdown...
    static QUALIFICATION_LIST_API = "/qualification_list";

    //for check user is logged in or not...
    static IS_LOGGED_IN_API = "/is_logged_in";

    // Agency or agency user default image url...
    static DEFAULT_IMAGE_URL = "/get_default_image";

    /// version control api
    static CURRENT_VERSION = "/current_version";

}